package com.gatasygatos.controller;

import com.pe.gatasygatos.DAO.ClienteDAO;
import com.pe.gatasygatos.model.entity.Cliente;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author yenny
 */
public class ControllerCliente extends HttpServlet {

    int id;
    ClienteDAO clientdao = new ClienteDAO();
    Cliente clie = new Cliente();
    ArrayList<String> listaErrores = new ArrayList<>();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String accion = request.getParameter("accion");
        if (accion.equals("add")) {
            this.add(request, response);
        }
        if (accion.equals("Actualizar")) {
            this.Edit(request, response);
        }
        if (accion.equals("eliminar")) {
            this.eliminar(request, response);
        }
        if (accion.equals("buscarPorId")) {
            this.BuscarPorId(request, response);
        }
        if (accion.equals("Estado")) {
            this.Estado(request, response);
        }

    }

    private void BuscarPorId(HttpServletRequest request, HttpServletResponse response)
            throws IOException, IOException {

        int codigo = Integer.parseInt(request.getParameter("idCliente"));
        ClienteDAO pdao = new ClienteDAO();
        Cliente obj = pdao.BuscarPorId(codigo);
        request.getSession().setAttribute("cliente", obj);
        response.sendRedirect("VInsertarventa.jsp");

    }

    private void add(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Cliente cli = new Cliente();
        String codigo = request.getParameter("txtCod");
        String cliente = request.getParameter("Txtapellidos");
        String correo = request.getParameter("Txtcorreo");
        String celular = request.getParameter("Txtcelular");
        String direccion = request.getParameter("Txtdireccion");
        String fechaderegistro = request.getParameter("Txtfechaderegistro");
        String idtipodocumento = request.getParameter("Txtidtipodocumento");
        String numerodocumento = request.getParameter("Txtnumerodocumento");
        String sexo = request.getParameter("Txtsexo");
        cli.setCodigo(codigo);
        cli.setCliente(cliente);
        cli.setCorreo(correo);
        cli.setCelular(Integer.parseInt(celular));
        cli.setDireccion(direccion);
        cli.setFechaderegistro(fechaderegistro);
        cli.setIdtipodocumento(Integer.parseInt(idtipodocumento));
        cli.setNumerodocumento(numerodocumento);
        cli.setSexo(sexo);
        cli.setEstado("Activo");
        ClienteDAO clidao = new ClienteDAO();
        boolean validacion = false;
        validacion= clidao.validacion(cli);
        if (validacion ==true){
            response.getWriter().print("no se puede registrar documento duplicado");
        }else{
            if (clidao.add(cli)) {
            response.getWriter().print("ok");
        } else {
            response.getWriter().print("no ha sido registrado correctamente");

        } 
        } 
    }

    private void Edit(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        id = Integer.parseInt(request.getParameter("txtid"));
        String codigo = request.getParameter("Txtcodigo");
        String cliente = request.getParameter("Txtapellidos");
        String correo = request.getParameter("Txtcorreo");
        String celular = request.getParameter("Txtcelular");
        String direccion = request.getParameter("Txtdireccion");
        String fechaderegistro = request.getParameter("Txtfechaderegistro");
        String idtipodocumento = request.getParameter("Txtidtipodocumento");
        String numerodocumento = request.getParameter("Txtnumerodocumento");
        String sexo = request.getParameter("Txtsexo");
        clie.setIdcliente(id);
        clie.setCodigo(codigo);
        clie.setCliente(cliente);
        clie.setCorreo(correo);
        clie.setCelular(Integer.parseInt(celular));
        clie.setDireccion(direccion);
        clie.setFechaderegistro(fechaderegistro);
        clie.setIdtipodocumento(Integer.parseInt(idtipodocumento));
        clie.setNumerodocumento(numerodocumento);
        clie.setSexo(sexo);

        if (clientdao.Edit(clie)) {
            response.getWriter().print("ok");

        } else {
            response.getWriter().print("El cliente no ha sido registrado");

        }
    }

    private void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        id = Integer.parseInt(request.getParameter("idCli"));
        System.out.println("[ID] " + id);
        clientdao.Eliminar(id);
    }
    
    private void Estado(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        id = Integer.parseInt(request.getParameter("id"));
        String estado = ClienteDAO.estado(id);
        if (estado.equalsIgnoreCase("Activo")) {
            clie.setEstado("Desactivado");
        } else {
            clie.setEstado("Activo");
        }
        clientdao.editEstado(clie, id);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
