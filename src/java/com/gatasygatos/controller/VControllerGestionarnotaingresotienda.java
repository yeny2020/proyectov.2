package com.gatasygatos.controller;

import com.pe.gatasygatos.DAO.MovimientoDAO;
import com.pe.gatasygatos.DAO.ProductoDAO;
import com.pe.gatasygatos.model.entity.DetalleMovimiento;
import com.pe.gatasygatos.model.entity.Movimientos;
import com.pe.gatasygatos.model.entity.Producto;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author yenny
 */
public class VControllerGestionarnotaingresotienda extends HttpServlet {

    MovimientoDAO pDAO;
    MovimientoDAO dao = new MovimientoDAO();
    DetalleMovimiento dmov = new DetalleMovimiento();
    int id;
    String NotaSalida = "VInsertarNotaingresotienda.jsp";
    // venta
    String tipocomprobante;
    String numcomprobante;
    // detalle _detalle
    int Nsalida;
    DecimalFormat formateador;

    public VControllerGestionarnotaingresotienda() {
        pDAO = new MovimientoDAO();
        formateador = new DecimalFormat("00000000");
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String accion = request.getParameter("accion");
        if (accion.equals("RegistrarNotaIngreso")) {
            this.RegistrarNotadeIngreso(request, response);
        }
        if (accion.equalsIgnoreCase("procesarCarrito")) {
            this.procesarCarrito(request, response);
        }
        if (accion.equalsIgnoreCase("NotaI")) {
            DetalleSalida(request, response);
        }
        if (accion.equalsIgnoreCase("NotaIActualizar")) {
            NotaIActualizar(request, response);
        }
        if (accion.equals("AnadirCarrito")) {
            this.anadirProducto(request, response);
        }
    }

    private void procesarCarrito(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession sesion = request.getSession();
        ArrayList<DetalleMovimiento> movi;//
        if (sesion.getAttribute("NotaIngreso") == null) {
            movi = new ArrayList<DetalleMovimiento>();

        } else {
            movi = (ArrayList<DetalleMovimiento>) sesion.getAttribute("NotaIngreso");
        }

        if (movi.size() > 0) {
            double subtotal = 0;
            for (int i = 0; i < movi.size(); i++) {

                DetalleMovimiento det = movi.get(i);
                // subtotal+= det.subtotal();
            }
            //Compra
            //insert into compra values(numero_serie , idProveedor , idTRabjador , fecha , impuesto , subtotal , (subtotal + (subtotal * impuesto)));
            //Detalle
            for (int i = 0; i < movi.size(); i++) {

                DetalleMovimiento det = movi.get(i);
                // insert into detallecompra values(null , det.getProducto.getIdproducto() , numero_serie ,det.getCantidad(),det.getPrecioCompra , det.getPrecioVenta );
            }
        } else {
            //Validar si esta vacio
        }

        sesion.setAttribute("NotaIngreso", movi);
        response.sendRedirect("VInsertarNotaingresotienda.jsp");

    }

    private void anadirProducto(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession sesion = request.getSession();
        ArrayList<DetalleMovimiento> carrito;//
        if (sesion.getAttribute("NotaIngreso") == null) {
            carrito = new ArrayList<DetalleMovimiento>();

        } else {
            carrito = (ArrayList<DetalleMovimiento>) sesion.getAttribute("NotaIngreso");
        }
        String[] selecionar = request.getParameterValues("accion");

        Producto p = ProductoDAO.obtenerProducto(Integer.parseInt(request.getParameter("Id")));
        DetalleMovimiento d = new DetalleMovimiento();
        d.setIdproducto(Integer.parseInt(request.getParameter("Id")));
        d.setProducto(p);

        boolean indice = false;
        for (int i = 0; i < carrito.size(); i++) {

            DetalleMovimiento det = carrito.get(i);

            if (det.getIdproducto() == p.getIdproducto()) {
                indice = true;
                break;
            }

        }
        if (!indice) {
            carrito.add(d);
        }
        sesion.setAttribute("NotaIngreso", carrito);
        request.getRequestDispatcher("VInsertarNotaingresotienda.jsp").forward(
                request, response);
    }

    protected void NotaIActualizar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String acceso = "VInsertarNotaingresotienda.jsp";
        MovimientoDAO pdao = new MovimientoDAO();
        List<DetalleMovimiento> listS = ObtenerSesion(request);
        //PROCEDIMIENTO ACTUALIZAR
        int cantidad = Integer.parseInt(request.getParameter("cant"));
        int fila = Integer.parseInt(request.getParameter("fila"));
        DetalleMovimiento det = listS.get(fila);
        det.setCantidad(cantidad);
        listS.set(fila, det);
        //FIN
        GuardarSesion(request, listS);
        RequestDispatcher vista = request.getRequestDispatcher(acceso);
        vista.forward(request, response);
    }

    protected void DetalleSalida(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id;
        if (request.getParameter("id") == null) {
            id = Integer.parseInt(request.getSession().getAttribute("NotaIn").toString());
        } else {
            id = Integer.parseInt(request.getParameter("id"));
        }
        String acceso = "VInsertarNotaingresotienda.jsp";
        MovimientoDAO pdao = new MovimientoDAO();
        List<DetalleMovimiento> listS = pdao.ticketDetalle(id);
        GuardarSesion(request, listS);
        request.getSession().setAttribute("NotaIn", id);
        RequestDispatcher vista = request.getRequestDispatcher(acceso);
        vista.forward(request, response);
    }

    public ArrayList<DetalleMovimiento> ObtenerSesion(HttpServletRequest request) {
        ArrayList<DetalleMovimiento> lista;
        if (request.getSession().getAttribute("NotaIngreso") == null) {
            lista = new ArrayList<>();
        } else {
            lista = (ArrayList<DetalleMovimiento>) request.getSession().getAttribute("NotaIngreso");
        }
        return lista;
    }

    public void GuardarSesion(HttpServletRequest request, List<DetalleMovimiento> lista) {
        request.getSession().setAttribute("NotaIngreso", lista);
    }

   private void RegistrarNotadeIngreso(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession sesion = request.getSession();
        Movimientos v = new Movimientos();
        DetalleMovimiento d = new DetalleMovimiento();
        v.setIdproveedor(Integer.parseInt(request.getParameter("txtIdpro").toUpperCase()));
        v.setIdusuario(1);
        tipocomprobante = request.getParameter("txtTipomovi");
        v.setTipocomprobante(tipocomprobante);
        if (tipocomprobante.equalsIgnoreCase("Nota Ingreso")) {
            v.setSerie("NI01");
        } else {
            v.setSerie("NS01");
        }
        if (tipocomprobante.equalsIgnoreCase("Nota Ingreso")) {
            Nsalida = pDAO.BuscarNnotaingreso();
            Nsalida = Nsalida + 1;
            String formatbol = formateador.format(Nsalida);
            v.setCorrelativo(formatbol);
        } else {

            int Nfactura = pDAO.BuscarNsalida();
            Nfactura = Nfactura + 1;
            String format = formateador.format(Nfactura);
            v.setCorrelativo(format);
        }
        v.setFechayhora(request.getParameter("txtfecha").toUpperCase());
        v.setTienda(request.getParameter("txtTienda").toUpperCase());
        v.setAlmacen(request.getParameter("txtAlmacen").toUpperCase());
        v.setIdmotivo(1);
        v.setEstado("Entregado");
        ArrayList<DetalleMovimiento> detalle = ObtenerSesion(request);
        if (pDAO.insertarMovimiento(v, detalle)) {
            response.getWriter().print("oki");
        } else {
            response.getWriter().print("Nose realizo la venta");

        }
        id = Integer.parseInt(request.getParameter("idmov"));
        v.setEstado("Entregado");
        pDAO.editarEstado(v, id);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
