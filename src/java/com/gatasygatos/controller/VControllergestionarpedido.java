package com.gatasygatos.controller;

import com.pe.gatasygatos.DAO.MovimientoDAO;
import com.pe.gatasygatos.DAO.ProductoDAO;
import com.pe.gatasygatos.DAO.ProvedorDAO;
import com.pe.gatasygatos.model.entity.DetalleMovimiento;
import com.pe.gatasygatos.model.entity.Movimientos;
import com.pe.gatasygatos.model.entity.Producto;
import com.pe.gatasygatos.model.entity.Provedor;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author yenny
 */
public class VControllergestionarpedido extends HttpServlet {
      MovimientoDAO dao = new MovimientoDAO();
    int id;
    MovimientoDAO pDAO;
    // venta
    String tipocomprobante;
    String numcomprobante;
    // detalle _detalle
    int Nboleta;
    DecimalFormat formateador;

    public VControllergestionarpedido() {
        pDAO = new MovimientoDAO();
        formateador = new DecimalFormat("00000000");
    }
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String accion = request.getParameter("accion");

        if (accion.equalsIgnoreCase("procesarCarrito")) {
            this.procesarCarrito(request, response);
        }
        if (accion.equals("AnadirCarrito")) {
            this.anadirCarrito(request, response);
        } else if (accion.equals("RegistrarMovimiento")) {
            this.registrarMovimiento(request, response);
        }

        if (accion.equals("buscarPorId")) {
            this.BuscarPorId(request, response);
        }
        if (accion.equals("Actualizarcantidad")) {
            this.actualizarcantidad(request, response);
        }
    }
    private void BuscarPorId(HttpServletRequest request, HttpServletResponse response)
            throws IOException, IOException {

        int codigo = Integer.parseInt(request.getParameter("idProveedor"));
        ProvedorDAO pdao = new ProvedorDAO();

        Provedor obj = pdao.BuscarPorId(codigo);

        request.getSession().setAttribute("provee", obj);

        response.sendRedirect("VInsertarpedido.jsp");

    }

    private void procesarCarrito(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession sesion = request.getSession();
        ArrayList<DetalleMovimiento> movi;//
        if (sesion.getAttribute("movi") == null) {
            movi = new ArrayList<DetalleMovimiento>();

        } else {
            movi = (ArrayList<DetalleMovimiento>) sesion.getAttribute("movi");
        }

        if (movi.size() > 0) {
            double subtotal = 0;
            for (int i = 0; i < movi.size(); i++) {

                DetalleMovimiento det = movi.get(i);
                // subtotal+= det.subtotal();
            }
            //Compra
            //insert into compra values(numero_serie , idProveedor , idTRabjador , fecha , impuesto , subtotal , (subtotal + (subtotal * impuesto)));
            //Detalle
            for (int i = 0; i < movi.size(); i++) {

                DetalleMovimiento det = movi.get(i);
                // insert into detallecompra values(null , det.getProducto.getIdproducto() , numero_serie ,det.getCantidad(),det.getPrecioCompra , det.getPrecioVenta );
            }
        } else {
            //Validar si esta vacio
        }

        sesion.setAttribute("movi", movi);
        response.sendRedirect("VInsertarpedido.jsp");

    }

    private void anadirCarrito(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession sesion = request.getSession();//
        ArrayList<DetalleMovimiento> movi;//
        if (sesion.getAttribute("movi") == null) {
            movi = new ArrayList<DetalleMovimiento>();
        } else {
            movi = (ArrayList<DetalleMovimiento>) sesion.getAttribute("movi");
        }
        Producto p = ProductoDAO.obtenerProducto(Integer.parseInt(request.getParameter("txtPro_id")));
        DetalleMovimiento d = new DetalleMovimiento();
        d.setIdproducto(Integer.parseInt(request.getParameter("txtPro_id")));
        d.setProducto(p);
        d.setCantidad(Integer.parseInt(request.getParameter("txtPro_cantidad")));
        double subtotal = p.getPrecioVenta() * d.getCantidad();
        //7    if (subtotal > 50) {
        //      d.setDet_com_descuento(subtotal *0.05);
        //    } else {
        //      d.setDet_com_descuento(0);
        //  }
//        boolean flag=false;
//        if(carrito.size() > 0){
//        for (DetalleCompra det : carrito){
//            if(det.getPro_id() == p.getPro_id()){
//               det.setDet_com_cantidad(det.getDet_com_cantidad()+ d.getDet_com_cantidad());
//            flag =true;
//             break;
//            }
//        }
//        }
//              if (flag ) {
//            carrito.add(d);
//        }

        boolean indice = false;
        for (int i = 0; i < movi.size(); i++) {

            DetalleMovimiento det = movi.get(i);
            if (det.getIdproducto() == p.getIdproducto()) {
                // det.setCantidad(det.getCantidad()+ d.getCantidad());//auto incrementar cantidad
                // det.setDet_com_descuento(d.getDet_com_descuento()*det.getDet_com_cantidad());
                indice = true;
                break;
            }
        }
        if (!indice) {
            movi.add(d);
        }

        sesion.setAttribute("movi", movi);
        response.sendRedirect("VInsertarpedido.jsp");

    }

    private void registrarMovimiento(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession sesion = request.getSession();
        Movimientos v = new Movimientos();
        v.setIdproveedor(Integer.parseInt(request.getParameter("txtIdpro").toUpperCase()));
        v.setIdusuario(1);
        tipocomprobante = request.getParameter("txtTipomovi");
        v.setTipocomprobante(tipocomprobante);
        if (tipocomprobante.equalsIgnoreCase("Pedido")) {
            v.setSerie("P001");
        } else {
            v.setSerie("NI01");
        }
        if (tipocomprobante.equalsIgnoreCase("Pedido")) {
            Nboleta = pDAO.BuscarNpedido();
            Nboleta = Nboleta + 1;
            String formatbol = formateador.format(Nboleta);
            v.setCorrelativo(formatbol);
        } else {

            int Nfactura = pDAO.BuscarNnotaingreso();
            Nfactura = Nfactura + 1;
            String format = formateador.format(Nfactura);
            v.setCorrelativo(format);
        }
        v.setFechayhora(request.getParameter("txtfecha").toUpperCase());
        v.setTienda(request.getParameter("txtTienda").toUpperCase());
        v.setAlmacen(request.getParameter("txtAlmacen").toUpperCase());
        v.setIdmotivo(1);
        v.setEstado("Pendiente");
        ArrayList<DetalleMovimiento> detalle = (ArrayList<DetalleMovimiento>) sesion.getAttribute("movi");
        if (pDAO.insertarMovimiento(v, detalle)) {
            request.getSession().removeAttribute("movi");
            request.getSession().removeAttribute("provee");
            response.getWriter().print("oki");
        } else {
            response.getWriter().print("Nose realizo la venta");

        }
    }
    private void actualizarcantidad(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession sesion = request.getSession();
        ArrayList<DetalleMovimiento> car;//
        if (sesion.getAttribute("movi") == null) {
            car = new ArrayList<DetalleMovimiento>();

        } else {
            car = (ArrayList<DetalleMovimiento>) sesion.getAttribute("movi");
        }
        int cantidad = Integer.parseInt(request.getParameter("cant"));
        int fila = Integer.parseInt(request.getParameter("idproducto"));

        DetalleMovimiento det = car.get(fila);
        det.setCantidad(cantidad);
        car.set(fila, det);

        sesion.setAttribute("movi", car);
        response.sendRedirect("VInsertarpedido.jsp");
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
