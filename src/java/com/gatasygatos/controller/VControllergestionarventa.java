
package com.gatasygatos.controller;
import com.pe.gatasygatos.DAO.ProductoDAO;
import com.pe.gatasygatos.DAO.VentaDAO;
import com.pe.gatasygatos.model.entity.DetalleVentas;
import com.pe.gatasygatos.model.entity.Producto;
import com.pe.gatasygatos.model.entity.Venta;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author yenny
 */
public class VControllergestionarventa extends HttpServlet {
    VentaDAO dao = new VentaDAO();
    int id;
    VentaDAO pDAO;
    // venta
    String tipocomprobante;
    String numcomprobante;
    // detalle _detalle
    int Nboleta;
    DecimalFormat formateador;
    Venta v = new Venta();
    DetalleVentas dv = new DetalleVentas();

    public VControllergestionarventa() {
        pDAO = new VentaDAO();
        formateador = new DecimalFormat("00000000");
    }
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
          String accion = request.getParameter("accion");

        if (accion.equalsIgnoreCase("procesarCarrito")) {
            this.procesarCarrito(request, response);
        }
        if (accion.equals("AnadirCarrito")) {
            this.anadirCarrito(request, response);
        }
        if (accion.equals("AnadirCliente")) {
            this.anadirCliente(request, response);
        }
        if (accion.equals("actualizarcantidad")) {
            this.actualizarcantidad(request, response);
        }
        if (accion.equals("RegistrarVenta")) {
            this.registrarVenta(request, response);
        }
        if (accion.equals("estadoanular")) {
            this.Estadoanulado(request, response);
        }
    }
  private void procesarCarrito(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession sesion = request.getSession();
        ArrayList<DetalleVentas> car;//
        if (sesion.getAttribute("car") == null) {
            car = new ArrayList<DetalleVentas>();

        } else {
            car = (ArrayList<DetalleVentas>) sesion.getAttribute("car");
        }
        if (car.size() > 0) {
            for (int i = 0; i < car.size(); i++) {

                DetalleVentas det = car.get(i);
                // subtotal+= det.subtotal();
            }
            //Compra
            //insert into compra values(numero_serie , idProveedor , idTRabjador , fecha , impuesto , subtotal , (subtotal + (subtotal * impuesto)));
            //Detalle
            for (int i = 0; i < car.size(); i++) {

                DetalleVentas det = car.get(i);
                // insert into detallecompra values(null , det.getProducto.getIdproducto() , numero_serie ,det.getCantidad(),det.getPrecioCompra , det.getPrecioVenta );
            }
        } else {
            //Validar si esta vacio
        }
        sesion.setAttribute("car", car);
        response.sendRedirect("VInsertarventa.jsp");
    }

    private void anadirCarrito(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession sesion = request.getSession();//
        ArrayList<DetalleVentas> car;//
        if (sesion.getAttribute("car") == null) {
            car = new ArrayList<DetalleVentas>();
        } else {
            car = (ArrayList<DetalleVentas>) sesion.getAttribute("car");
        }
        Producto p = ProductoDAO.obtenerProducto(Integer.parseInt(request.getParameter("txtPro_id")));
        DetalleVentas d = new DetalleVentas();
        d.setIdproducto(Integer.parseInt(request.getParameter("txtPro_id")));
        d.setProducto(p);
        d.setCantidad(Integer.parseInt(request.getParameter("txtPro_cantidad")));
        //7    if (subtotal > 50) {
        //      d.setDet_com_descuento(subtotal *0.05);
        //    } else {
        //      d.setDet_com_descuento(0);
        //  }
//        boolean flag=false;
//        if(carrito.size() > 0){
//        for (DetalleCompra det : carrito){
//            if(det.getPro_id() == p.getPro_id()){
//               det.setDet_com_cantidad(det.getDet_com_cantidad()+ d.getDet_com_cantidad());
//            flag =true;
//             break;
//            }
//        }
//        }
//              if (flag ) {
//            carrito.add(d);
//        }

        boolean indice = false;
        for (int i = 0; i < car.size(); i++) {

            DetalleVentas det = car.get(i);
            if (det.getIdproducto() == p.getIdproducto()) {
                // det.setCantidad(det.getCantidad()+ d.getCantidad());//auto incrementar cantidad
                // det.setDet_com_descuento(d.getDet_com_descuento()*det.getDet_com_cantidad());
                indice = true;
                break;
            }
        }
        if (!indice) {
            car.add(d);
        }

        sesion.setAttribute("car", car);
        response.sendRedirect("VInsertarventa.jsp");

    }

    private void anadirCliente(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setAttribute("idcli", request.getParameter("id"));
        request.getRequestDispatcher("VInsertarventa.jsp").forward(
                request, response);

    }

    private void actualizarcantidad(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession sesion = request.getSession();
        ArrayList<DetalleVentas> car;//
        if (sesion.getAttribute("car") == null) {
            car = new ArrayList<DetalleVentas>();

        } else {
            car = (ArrayList<DetalleVentas>) sesion.getAttribute("car");
        }
        int cantidad = Integer.parseInt(request.getParameter("cant"));
        int fila = Integer.parseInt(request.getParameter("idproducto"));

        DetalleVentas det = car.get(fila);
        det.setCantidad(cantidad);
        car.set(fila, det);

        sesion.setAttribute("car", car);
        response.sendRedirect("VInsertarventa.jsp");
    }

    private void registrarVenta(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession sesion = request.getSession();
        Venta v = new Venta();
        v.setIdcliente(Integer.parseInt(request.getParameter("txtIdcli").toUpperCase()));
        v.setIdusuario(1);
        tipocomprobante = request.getParameter("txtTipoventa");
        v.setTipocomprobante(tipocomprobante);
        if (tipocomprobante.equalsIgnoreCase("Boleta")) {
            v.setSerie("B001");
        } else {
            v.setSerie("F001");
        }
        if (tipocomprobante.equalsIgnoreCase("Boleta")) {
            Nboleta = pDAO.BuscarNBoleta();
            Nboleta = Nboleta + 1;
            String formatbol = formateador.format(Nboleta);
            v.setCorrelativo(formatbol);
        } else {

            int Nfactura = pDAO.BuscarNfacturas();
            Nfactura = Nfactura + 1;
            String format = formateador.format(Nfactura);
            v.setCorrelativo(format);
        }
        v.setFechayhora(request.getParameter("txtfecha").toUpperCase());
        v.setTienda(request.getParameter("txtTienda").toUpperCase());
        v.setAlmacen(request.getParameter("txtAlmacen").toUpperCase());
        v.setCondicion(request.getParameter("txtCondicion").toUpperCase());
        v.setSubtotal(Double.parseDouble(request.getParameter("txtSubtotal").toUpperCase()));
        v.setIgv(Double.parseDouble(request.getParameter("txtIgv").toUpperCase()));
        v.setTotal(Double.parseDouble(request.getParameter("txtTotal").toUpperCase()));
        v.setEstado("Vendido");
        ArrayList<DetalleVentas> detalle = (ArrayList<DetalleVentas>) sesion.getAttribute("car");
        if (pDAO.insertarVenta(v, detalle)) {
            request.getSession().removeAttribute("cliente");
            request.getSession().removeAttribute("car");
            response.getWriter().print("oki");
        } else {
            request.getSession().removeAttribute("car");
            response.getWriter().print("Nose realizo la venta");

        }
    }

    private void Estadoanulado(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        id = Integer.parseInt(request.getParameter("id"));
        String estado = VentaDAO.estado(id);
        if (estado.equalsIgnoreCase("Vendido")) {

            v.setEstado("Anulado");
            dv.setEstado("Anulado");
        } else {
            v.setEstado("Anulado");
            dv.setEstado("Anulado");
        }

        pDAO.editarEstado(v, id);
        pDAO.editarestadodetalle_venta(dv, id);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
