/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pe.gatasonrisa.model.controller;

import com.pe.gatasygatos.DAO.CompraDAO;
import com.pe.gatasygatos.DAO.ProductoDAO;
import com.pe.gatasygatos.model.entity.Compra;
import com.pe.gatasygatos.model.entity.DetalleCompra;
import com.pe.gatasygatos.model.entity.Producto;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author yenny
 */
public class ControllerCompra extends HttpServlet {

    CompraDAO dao = new CompraDAO();
    int id;
    CompraDAO pDAO;
    // venta
    String tipocomprobante;
    String numcomprobante;
    // detalle _detalle
    int Ncotizacion;
    DecimalFormat formateador;

    public ControllerCompra() {
        pDAO = new CompraDAO();
        formateador = new DecimalFormat("000000");
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String accion = request.getParameter("accion");
        if (accion.equalsIgnoreCase("procesarCarrito")) {
            this.procesarCarrito(request, response);
        }
        if (accion.equals("AnadirProducto")) {
            this.anadirProducto(request, response);
        }
        if (accion.equals("AnadirCarrito")) {
            this.anadirCarrito(request, response);
        }
        if (accion.equals("Actualizarcantidad")) {
            this.Actualizarcantidad(request, response);
        }
        if (accion.equalsIgnoreCase("actualizarpreciocompra")) {
            this.actualizarpreciocompra(request, response);
        } else if (accion.equals("RegistrarCompra")) {
            this.registrarCompra(request, response);
        }
        
        if (accion.equals("actualizarContenido")) {
            this.actualizarContenido(request, response);
         }
    }

    private void procesarCarrito(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession sesion = request.getSession();
        ArrayList<DetalleCompra> car;//
        if (sesion.getAttribute("cotizacion") == null) {
            car = new ArrayList<DetalleCompra>();

        } else {
            car = (ArrayList<DetalleCompra>) sesion.getAttribute("cotizacion");
        }

        if (car.size() > 0) {
            double subtotal = 0;
            for (int i = 0; i < car.size(); i++) {

                DetalleCompra det = car.get(i);
                // subtotal+= det.subtotal();
            }

            //Compra
            //insert into compra values(numero_serie , idProveedor , idTRabjador , fecha , impuesto , subtotal , (subtotal + (subtotal * impuesto)));
            //Detalle
            for (int i = 0; i < car.size(); i++) {

                DetalleCompra det = car.get(i);
                // insert into detallecompra values(null , det.getProducto.getIdproducto() , numero_serie ,det.getCantidad(),det.getPrecioCompra , det.getPrecioVenta );
            }
        } else {
            //Validar si esta vacio
        }

        sesion.setAttribute("cotizacion", car);
        response.sendRedirect("InsertarCompra.jsp");

    }

    private void anadirProducto(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession sesion = request.getSession();
        ArrayList<DetalleCompra> car;//
        if (sesion.getAttribute("cotizacion") == null) {
            car = new ArrayList<DetalleCompra>();

        } else {
            car = (ArrayList<DetalleCompra>) sesion.getAttribute("cotizacion");
        }
        String[] selecionar = request.getParameterValues("accion");

        Producto p = ProductoDAO.obtenerProducto(Integer.parseInt(request.getParameter("Id")));
        DetalleCompra d = new DetalleCompra();
        d.setIdproducto(Integer.parseInt(request.getParameter("Id")));
        d.setProducto(p);

        boolean indice = false;
        for (int i = 0; i < car.size(); i++) {

            DetalleCompra det = car.get(i);

            if (det.getIdproducto() == p.getIdproducto()) {
                indice = true;
                break;
            }

        }
        if (!indice) {
            car.add(d);
        }

        sesion.setAttribute("cotizacion", car);
        request.getRequestDispatcher("InsertarCompra.jsp").forward(
                request, response);

    }

    private void anadirCarrito(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession sesion = request.getSession();//
        ArrayList<DetalleCompra> car;//
        if (sesion.getAttribute("cotizacion") == null) {
            car = new ArrayList<DetalleCompra>();
        } else {
            car = (ArrayList<DetalleCompra>) sesion.getAttribute("cotizacion");
        }
        Producto p = ProductoDAO.obtenerProducto(Integer.parseInt(request.getParameter("txtPro_id")));
        DetalleCompra d = new DetalleCompra();
        d.setIdproducto(Integer.parseInt(request.getParameter("txtPro_id")));
        d.setProducto(p);
        d.setCantidad(Integer.parseInt(request.getParameter("txtPro_cantidad")));
        d.setCosto(Double.parseDouble(request.getParameter("txtCosto")));
        d.setContenido(Integer.parseInt(request.getParameter("txtContenido")));
        double subtotal = p.getPreciocompra() * d.getCantidad();
        //7    if (subtotal > 50) {
        //      d.setDet_com_descuento(subtotal *0.05);
        //    } else {
        //      d.setDet_com_descuento(0);
        //  }
//        boolean flag=false;
//        if(carrito.size() > 0){
//        for (DetalleCompra det : carrito){
//            if(det.getPro_id() == p.getPro_id()){
//               det.setDet_com_cantidad(det.getDet_com_cantidad()+ d.getDet_com_cantidad());
//            flag =true;
//             break;
//            }
//        }
//        }
//              if (flag ) {
//            carrito.add(d);
//        }

        boolean indice = false;
        for (int i = 0; i < car.size(); i++) {

            DetalleCompra det = car.get(i);
            if (det.getIdproducto() == p.getIdproducto()) {
                // det.setCantidad(det.getCantidad()+ d.getCantidad());//auto incrementar cantidad
                // det.setDet_com_descuento(d.getDet_com_descuento()*det.getDet_com_cantidad());
                indice = true;
                break;
            }
        }
        if (!indice) {
            car.add(d);
        }

        sesion.setAttribute("cotizacion", car);
        response.sendRedirect("InsertarCompra.jsp");

    }

    private void Actualizarcantidad(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession sesion = request.getSession();
        ArrayList<DetalleCompra> cotizacion;//
        if (sesion.getAttribute("cotizacion") == null) {
            cotizacion = new ArrayList<DetalleCompra>();

        } else {
            cotizacion = (ArrayList<DetalleCompra>) sesion.getAttribute("cotizacion");
        }
        int cantidad = Integer.parseInt(request.getParameter("cant"));
        int fila = Integer.parseInt(request.getParameter("idproducto"));

        DetalleCompra det = cotizacion.get(fila);
        det.setCantidad(cantidad);
        cotizacion.set(fila, det);

        sesion.setAttribute("cotizacion", cotizacion);
        response.sendRedirect("InsertarCompra.jsp");
    }

    public double subtotal(ArrayList<DetalleCompra> cotizacion) {
        double subtotal = 0;
        for (int i = 0; i < cotizacion.size(); i++) {

            DetalleCompra det = cotizacion.get(i);
            subtotal += det.subtotal();
        }
        return subtotal;

    }

    public double IGV(ArrayList<DetalleCompra> cotizacion) {
        double igv = 0;
        for (int i = 0; i < cotizacion.size(); i++) {

            DetalleCompra det = cotizacion.get(i);
            igv += det.igv();
        }
        return igv;

    }

    private void actualizarpreciocompra(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession sesion = request.getSession();
        ArrayList<DetalleCompra> cotizacion;//
        if (sesion.getAttribute("cotizacion") == null) {
            cotizacion = new ArrayList<DetalleCompra>();

        } else {
            cotizacion = (ArrayList<DetalleCompra>) sesion.getAttribute("cotizacion");
        }

        double pcompra = Double.parseDouble(request.getParameter("cant"));
        int fila = Integer.parseInt(request.getParameter("idproducto"));
        DetalleCompra det = cotizacion.get(fila);
        double subtotal = det.getCantidad() * det.getCosto();
        det.setCosto(pcompra);
        cotizacion.set(fila, det);

        sesion.setAttribute("cotizacion", cotizacion);
        sesion.setAttribute("subtotal", subtotal(cotizacion));
        response.sendRedirect("InsertarCompra.jsp");

    }
        private void actualizarContenido(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        HttpSession sesion = request.getSession();
        ArrayList<DetalleCompra> cotizacion;//
        if (sesion.getAttribute("cotizacion") == null) {
            cotizacion = new ArrayList<DetalleCompra>();

        } else {
            cotizacion = (ArrayList<DetalleCompra>) sesion.getAttribute("cotizacion");
        }
        int contenido = Integer.parseInt(request.getParameter("cant"));
        int fila = Integer.parseInt(request.getParameter("idproducto"));

        DetalleCompra det = cotizacion.get(fila);
        det.setContenido(contenido);
        cotizacion.set(fila, det);

        sesion.setAttribute("cotizacion", cotizacion);
        response.sendRedirect("InsertarCompra.jsp");
    }

    private void registrarCompra(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession sesion = request.getSession();
        Compra c = new Compra();
        c.setIdproveedor(Integer.parseInt(request.getParameter("txtIdproveedor").toUpperCase()));
        c.setIdusuario(1);
        tipocomprobante = request.getParameter("txtTipocompra");
        c.setTipocomprobante(tipocomprobante);
        if (tipocomprobante.equalsIgnoreCase("Orden")) {
            c.setSerie("OC01");
        } else {
            c.setSerie("CT01");
        }
        if (tipocomprobante.equalsIgnoreCase("Cotizacion")) {
            Ncotizacion = pDAO.BuscarNCotizacion();
            Ncotizacion = Ncotizacion + 1;
            String formatbol = formateador.format(Ncotizacion);
            c.setCorrelativo(formatbol);
        } else {

            int Norden = pDAO.BuscarNorden();
            Norden = Norden + 1;
            String format = formateador.format(Norden);
            c.setCorrelativo(format);
        }
        c.setFechaYhora(request.getParameter("txtfecha").toUpperCase());
        c.setTienda(request.getParameter("txtTienda").toUpperCase());
        c.setAlmacen(request.getParameter("txtAlmacen").toUpperCase());
        c.setCondicion(request.getParameter("txtCondicion").toUpperCase());
        c.setIdmotivo(1);
        c.setSubtotal(Double.parseDouble(request.getParameter("txtSubtotal").toUpperCase()));
        c.setIgv(Double.parseDouble(request.getParameter("txtIgv").toUpperCase()));
        c.setTotal(Double.parseDouble(request.getParameter("txtTotal").toUpperCase()));
        if (tipocomprobante.equalsIgnoreCase("Orden")) {
            c.setEstado("Orden");
        } else {
            c.setEstado("Cotizado");
        }
        ArrayList<DetalleCompra> detalle = (ArrayList<DetalleCompra>) sesion.getAttribute("cotizacion");
        if (pDAO.insertarCompra(c, detalle)) {
            request.getSession().removeAttribute("cotizacion");
            request.getSession().removeAttribute("proveedor");
            response.getWriter().print("oki");
        } else {
            request.getSession().removeAttribute("cotizacion");
            response.getWriter().print("Nose realizo la cotizacion");

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>



}
