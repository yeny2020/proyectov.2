/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pe.gatasonrisa.model.controller;

import com.pe.gatasygatos.DAO.CompraDAO;
import com.pe.gatasygatos.model.entity.Compra;
import com.pe.gatasygatos.model.entity.DetalleCompra;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author usuario
 */
public class ControllerDetallleCompra extends HttpServlet {
    String Compra = "Cotizacion.jsp";
    int id;
    CompraDAO pDAO;
    String listar = "DetalleCompra.jsp";
    DetalleCompra dv = new DetalleCompra();
    String ticket = "ticketCompra.jsp";
    String OrdenCompra = "ReferenciarOrden.jsp";
    String RefGR = "ReferenciarGuiaRemision.jsp";
    String RefNI = "ReferenciarNotaIngreso.jsp";
    String RefFC = "ReferenciarFacturaCompra.jsp";
    String ticket4 = "DetalleFactura.jsp";
    String GR = "DetalleGuiaRemision.jsp";
    DecimalFormat formateador;
    Compra v = new Compra();

    public ControllerDetallleCompra() {
        pDAO = new CompraDAO();
        formateador = new DecimalFormat("000000");
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControllerDetallleCompra</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControllerDetallleCompra at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String acceso = "";
        String action = request.getParameter("accion");

        if (action.equalsIgnoreCase("reporte")) {
            request.setAttribute("idco", request.getParameter("id"));
            acceso = ticket;
        }
        if (action.equalsIgnoreCase("RefenciarOrdenCompra")) {
            request.setAttribute("idco", request.getParameter("id"));
            acceso = OrdenCompra;
        }

        if (action.equalsIgnoreCase("ReferenciarGuiaRemision")) {
            request.setAttribute("idco", request.getParameter("id"));
            acceso = RefGR;
        }
        if (action.equalsIgnoreCase("ReferenciarNotaIngreso")) {
            request.setAttribute("idco", request.getParameter("id"));
            acceso = RefNI;
        }
        if (action.equalsIgnoreCase("ReferenciarFacturaCompra")) {
            request.setAttribute("idco", request.getParameter("id"));
            acceso = RefFC;
        }

        if (action.equalsIgnoreCase("GuiaRemision")) {
            request.setAttribute("idco", request.getParameter("id"));
            acceso = GR;
        }

        if (action.equalsIgnoreCase("Factura")) {
            request.setAttribute("idco", request.getParameter("id"));
            acceso = ticket4;
        } else if (action.equalsIgnoreCase("estado")) {
            id = Integer.parseInt(request.getParameter("id"));
            String estado = CompraDAO.estado(id);
            if (estado.equalsIgnoreCase("Generado")) {

                v.setEstado("Generado");
               
            } else {
                v.setEstado("Generado");
               
            }

            pDAO.editarEstado(v, id);
            pDAO.editarestadodetalle_compra(dv, id);
            acceso = Compra;
        }

        RequestDispatcher vista = request.getRequestDispatcher(acceso);
        vista.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
