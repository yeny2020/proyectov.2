/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pe.gatasonrisa.model.controller;

import com.pe.gatasonrisa.commons.Constantes;
import com.pe.gatasygatos.DAO.AlmacenDAO;
import com.pe.gatasygatos.model.entity.Almacen;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author yenny
 */
public class ControllerMaterial extends HttpServlet {
    int id;
    AlmacenDAO  matedao = new AlmacenDAO();
    Almacen mat= new Almacen();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
          String accion = request.getParameter("accion");
        if (accion.equals("add")) {
            this.add(request, response);
       }
                if (accion.equals("Actualizar")) {
            this.Edit(request, response);
       }
         if (accion.equals("eliminar")) {
            this.eliminar(request, response);
       }
           if (accion.equals("Estado")) {
            this.Estado(request, response);
       }
    }
    private void add(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int resultado;
        String cod=request.getParameter("txtCod"); 
        String nom=request.getParameter("txtNom"); 
        Almacen m=new Almacen(cod,nom,"Activo");
        AlmacenDAO mdao=new AlmacenDAO();
        if (mdao.add(m)) {
                response.getWriter().print("ok");

            } else {
                response.getWriter().print("Ya existe el almacen");

            }
   }
   private void Edit(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            id = Integer.parseInt(request.getParameter("txtid"));
            String codigo = request.getParameter("TxtCod");
            String nombre = request.getParameter("TxtNom");
            mat.setIdalmacen(id);
            mat.setCodigo(codigo);
            mat.setNombre(nombre);
             if (matedao.Edit(mat)) {
                response.getWriter().print("ok");

            } else {
                response.getWriter().print("El material no ha sido registrado correctamente");

            }
   }
   private void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       id = Integer.parseInt(request.getParameter("idMat"));
            System.out.println("[ID] "+ id);
            matedao.Eliminar(id);
   }
      private void Estado(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      id=Integer.parseInt(request.getParameter("id"));
       String estado=AlmacenDAO.getmaterialEstado(id);
       if(estado.equalsIgnoreCase("Activo")){
           mat.setEstado("Desactivado");
       }else{
       mat.setEstado("Activo");
        
       }
        matedao.Estado(mat,id);
   }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
