/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pe.gatasonrisa.model.controller;

import com.pe.gatasygatos.DAO.CompraDAO;
import com.pe.gatasygatos.model.entity.Compra;
import com.pe.gatasygatos.model.entity.DetalleCompra;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author usuario
 */
public class ControllerNotaIngreso extends HttpServlet {

    Compra com = new Compra();
    DetalleCompra dcom = new DetalleCompra();
    //---------------------------
    CompraDAO pDAO;
    CompraDAO dao = new CompraDAO();
    int id;
    CompraDAO DAO;
    String RefNI = "ReferenciarNotaIngreso.jsp";
    int Ncotizacion;
    // venta
    String tipocomprobante;
    String numcomprobante;
    // detalle _detalle
    int Nsalida;
    DecimalFormat formateador;

    public ControllerNotaIngreso() {
        DAO = new CompraDAO();
        pDAO = new CompraDAO();
        formateador = new DecimalFormat("000000");
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String accion = request.getParameter("accion");
        if (accion.equals("RegistrarNotaIngreso")) {
            this.RegistrarNotaIngreso(request, response);
        }
        if (accion.equalsIgnoreCase("ReferenciarNotaIngreso")) {
            DetalleNotaIngreso(request, response);
        }
        if (accion.equalsIgnoreCase("ActualizarNotaIngreso")) {
            ActualizarNotaIngreso(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void RegistrarNotaIngreso(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession sesion = request.getSession();
        Compra c = new Compra();
        c.setIdproveedor(Integer.parseInt(request.getParameter("txtIdproveedor").toUpperCase()));
        c.setIdusuario(1);
        tipocomprobante = request.getParameter("txtTipocompra");
        c.setTipocomprobante(tipocomprobante);
        if (tipocomprobante.equalsIgnoreCase("Nota Ingreso")) {
            c.setSerie("NI002");
        } else {
            c.setSerie("CT01");
        }
        if (tipocomprobante.equalsIgnoreCase("Nota Ingreso")) {
            Ncotizacion = pDAO.BuscarNCotizacion();
            Ncotizacion = Ncotizacion + 1;
            String formatbol = formateador.format(Ncotizacion);
            c.setCorrelativo(formatbol);
        } else {

            int Norden = pDAO.BuscarNorden();
            Norden = Norden + 1;
            String format = formateador.format(Norden);
            c.setCorrelativo(format);
        }
        c.setFechaYhora(request.getParameter("txtfecha").toUpperCase());
        c.setTienda(request.getParameter("txtTienda").toUpperCase());
        c.setAlmacen(request.getParameter("txtAlmacen").toUpperCase());
        c.setCondicion(request.getParameter("txtCondicion").toUpperCase());
        c.setIdmotivo(1);
        c.setSubtotal(Double.parseDouble(request.getParameter("txtSubtotal").toUpperCase()));
        c.setIgv(Double.parseDouble(request.getParameter("txtIgv").toUpperCase()));
        c.setTotal(Double.parseDouble(request.getParameter("txtTotal").toUpperCase()));
        if (tipocomprobante.equalsIgnoreCase("Nota Ingreso")) {
            c.setEstado("Pedido");
        } else {
            c.setEstado("Cotizado");
        }
        ArrayList<DetalleCompra> detalle = ObtenerSesion(request);
        if (pDAO.insertarCompra(c, detalle)) {
            request.getSession().removeAttribute("notaIngreso");
            request.getSession().removeAttribute("proveedor");
            response.getWriter().print("oki");
        } else {
            request.getSession().removeAttribute("notaIngreso");
            response.getWriter().print("Nose realizo la cotizacion");

        }
        id = Integer.parseInt(request.getParameter("Idref"));
        String estado = CompraDAO.estado(id);

        c.setEstado("Refenciado");

        pDAO.editarEstado(c, id);
        pDAO.editarestadodetalle_compra(dcom, id);
    }

    private void DetalleNotaIngreso(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id;
        if (request.getParameter("id") == null) {
            id = Integer.parseInt(request.getSession().getAttribute("idco").toString());
        } else {
            id = Integer.parseInt(request.getParameter("id"));
        }
        String acceso = "ReferenciarNotaIngreso.jsp";
        CompraDAO pdao = new CompraDAO();
        List<DetalleCompra> listS = pdao.ticketDetalle(id);
        GuardarSesion(request, listS);
        request.getSession().setAttribute("idco", id);
        RequestDispatcher vista = request.getRequestDispatcher(acceso);
        vista.forward(request, response);
    }

    private void ActualizarNotaIngreso(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String acceso = "ReferenciarNotaIngreso.jsp";
        CompraDAO pdao = new CompraDAO();
        List<DetalleCompra> listS = ObtenerSesion(request);
        //PROCEDIMIENTO ACTUALIZAR
        int cantidad = Integer.parseInt(request.getParameter("cant"));
        int fila = Integer.parseInt(request.getParameter("fila"));
        DetalleCompra det = listS.get(fila);
        det.setCantidad(cantidad);
        listS.set(fila, det);
        //FIN
        GuardarSesion(request, listS);
        RequestDispatcher vista = request.getRequestDispatcher(acceso);
        vista.forward(request, response);
    }

    public ArrayList<DetalleCompra> ObtenerSesion(HttpServletRequest request) {
        ArrayList<DetalleCompra> lista;
        if (request.getSession().getAttribute("notaIngreso") == null) {
            lista = new ArrayList<>();
        } else {
            lista = (ArrayList<DetalleCompra>) request.getSession().getAttribute("notaIngreso");
        }
        return lista;
    }

    public void GuardarSesion(HttpServletRequest request, List<DetalleCompra> lista) {
        request.getSession().setAttribute("notaIngreso", lista);
    }

}
