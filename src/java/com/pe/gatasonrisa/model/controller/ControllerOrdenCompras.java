/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pe.gatasonrisa.model.controller;

import com.pe.gatasygatos.DAO.CompraDAO;
import com.pe.gatasygatos.model.entity.Compra;
import com.pe.gatasygatos.model.entity.DetalleCompra;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author usuario
 */
public class ControllerOrdenCompras extends HttpServlet {
 Compra com = new Compra();
 DetalleCompra dcom=new DetalleCompra();
    //---------------------------
    CompraDAO pDAO;
    CompraDAO dao = new CompraDAO();
    int id;
    CompraDAO DAO;
    String OrdenCompra = "ReferenciarOrden.jsp";
    int Ncotizacion;
    // venta
    String tipocomprobante;
    String numcomprobante;
    // detalle _detalle
    int Nsalida;
    DecimalFormat formateador;

    public ControllerOrdenCompras() {
        DAO = new CompraDAO();
        pDAO = new CompraDAO();
        formateador = new DecimalFormat("000000");
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String accion = request.getParameter("accion");
        if (accion.equals("RegistrarOrdenCompra")) {
            this.RegistrarOrdenCompra(request, response);
        }
        if (accion.equalsIgnoreCase("RefenciarOrdenCompra")) {
            DetalleOrdenCompra(request, response);
        }
        if (accion.equalsIgnoreCase("Actualizarcan")) {
            Actualizarcantidad(request, response);
        }
        if (accion.equals("actualizarContenido")) {
            this.actualizarContenido(request, response);
        }

        if (accion.equalsIgnoreCase("actualizarpreciocompra")) {
            this.actualizarpreciocompra(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void RegistrarOrdenCompra(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession sesion = request.getSession();
        Compra c = new Compra();
        c.setIdproveedor(Integer.parseInt(request.getParameter("txtIdproveedor").toUpperCase()));
        c.setIdusuario(1);
        tipocomprobante = request.getParameter("txtTipocompra");
        c.setTipocomprobante(tipocomprobante);

        c.setSerie("OC01");

        int Norden = pDAO.BuscarNorden();
        Norden = Norden + 1;
        String format = formateador.format(Norden);
        c.setCorrelativo(format);

        c.setFechaYhora(request.getParameter("txtfecha").toUpperCase());
        c.setTienda(request.getParameter("txtTienda").toUpperCase());
        c.setAlmacen(request.getParameter("txtAlmacen").toUpperCase());
        c.setCondicion(request.getParameter("txtCondicion").toUpperCase());
        c.setIdmotivo(1);
        c.setSubtotal(Double.parseDouble(request.getParameter("txtSubtotal").toUpperCase()));
        c.setIgv(Double.parseDouble(request.getParameter("txtIgv").toUpperCase()));
        c.setTotal(Double.parseDouble(request.getParameter("txtTotal").toUpperCase()));
        c.setEstado("Orden");
       
         
          

        ArrayList<DetalleCompra> detalle = ObtenerSesion(request);
        if (pDAO.insertarCompra(c, detalle)) {
            request.getSession().removeAttribute("ordencompra");
            request.getSession().removeAttribute("proveedor");
            response.getWriter().print("oki");
        } else {
            request.getSession().removeAttribute("ordencompra");
            response.getWriter().print("Nose realizo la cotizacion");

        }
        id = Integer.parseInt(request.getParameter("Idref"));
            String estado = CompraDAO.estado(id);
           

                c.setEstado("Refenciado");
              
            

            pDAO.editarEstado(c, id);
            pDAO.editarestadodetalle_compra(dcom, id);

    }

    private void DetalleOrdenCompra(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id;
        if (request.getParameter("id") == null) {
            id = Integer.parseInt(request.getSession().getAttribute("idco").toString());
        } else {
            id = Integer.parseInt(request.getParameter("id"));
        }
        String acceso = "ReferenciarOrden.jsp";
        CompraDAO pdao = new CompraDAO();
        List<DetalleCompra> listS = pdao.ticketDetalle(id);
        GuardarSesion(request, listS);
        request.getSession().setAttribute("idco", id);
        RequestDispatcher vista = request.getRequestDispatcher(acceso);
        vista.forward(request, response);
    }

    public ArrayList<DetalleCompra> ObtenerSesion(HttpServletRequest request) {
        ArrayList<DetalleCompra> lista;
        if (request.getSession().getAttribute("ordencompra") == null) {
            lista = new ArrayList<>();
        } else {
            lista = (ArrayList<DetalleCompra>) request.getSession().getAttribute("ordencompra");
        }
        return lista;
    }

    public void GuardarSesion(HttpServletRequest request, List<DetalleCompra> lista) {
        request.getSession().setAttribute("ordencompra", lista);
    }

    private void actualizarContenido(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession sesion = request.getSession();
        ArrayList<DetalleCompra> cotizacion;//
        if (sesion.getAttribute("ordencompra") == null) {
            cotizacion = new ArrayList<DetalleCompra>();

        } else {
            cotizacion = (ArrayList<DetalleCompra>) sesion.getAttribute("ordencompra");
        }
        int contenido = Integer.parseInt(request.getParameter("cant"));
        int fila = Integer.parseInt(request.getParameter("idproducto"));

        DetalleCompra det = cotizacion.get(fila);
        det.setContenido(contenido);
        cotizacion.set(fila, det);

        sesion.setAttribute("ordencompra", cotizacion);
        response.sendRedirect("ReferenciarOrden.jsp");
    }

     private void Actualizarcantidad(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession sesion = request.getSession();
        ArrayList<DetalleCompra> cotizacion;//
        if (sesion.getAttribute("ordencompra") == null) {
            cotizacion = new ArrayList<DetalleCompra>();

        } else {
            cotizacion = (ArrayList<DetalleCompra>) sesion.getAttribute("ordencompra");
        }
        int cant = Integer.parseInt(request.getParameter("cant"));
        int fila = Integer.parseInt(request.getParameter("idproducto"));

        DetalleCompra det = cotizacion.get(fila);
        det.setCantidad(cant);
        cotizacion.set(fila, det);

        sesion.setAttribute("ordencompra", cotizacion);
        response.sendRedirect("ReferenciarOrden.jsp");
    }
    
    private void actualizarpreciocompra(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession sesion = request.getSession();
        ArrayList<DetalleCompra> ordencompra;//
        if (sesion.getAttribute("ordencompra") == null) {
            ordencompra = new ArrayList<DetalleCompra>();

        } else {
            ordencompra = (ArrayList<DetalleCompra>) sesion.getAttribute("ordencompra");
        }

        double pcompra = Double.parseDouble(request.getParameter("cant"));
        int fila = Integer.parseInt(request.getParameter("idproducto"));
        DetalleCompra det = ordencompra.get(fila);
        double subtotal = det.getCantidad() * det.getCosto();
        det.setCosto(pcompra);
        ordencompra.set(fila, det);

        sesion.setAttribute("ordencompra", ordencompra);
        sesion.setAttribute("subtotal", subtotal(ordencompra));
        response.sendRedirect("ReferenciarOrden.jsp");

    }

    ////////-------------------------------------------------------
    public double subtotal(ArrayList<DetalleCompra> ordencompra) {
        double subtotal = 0;
        for (int i = 0; i < ordencompra.size(); i++) {

            DetalleCompra det = ordencompra.get(i);
            subtotal += det.subtotal();
        }
        return subtotal;

    }

    public double IGV(ArrayList<DetalleCompra> cotizacion) {
        double igv = 0;
        for (int i = 0; i < cotizacion.size(); i++) {

            DetalleCompra det = cotizacion.get(i);
            igv += det.igv();
        }
        return igv;

    }
}
