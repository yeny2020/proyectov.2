/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pe.gatasonrisa.model.controller;
import com.pe.gatasygatos.DAO.ProductoDAO;
import com.pe.gatasygatos.model.entity.Producto;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author yenny
 */
public class ControllerProducto extends HttpServlet {

    int id;
    Producto p = new Producto();
    ProductoDAO pdao = new ProductoDAO();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String accion = request.getParameter("accion");
        if (accion.equals("add")) {
            this.add(request, response);
        }
        if (accion.equals("editar")) {
            this.Editar(request, response);
        }
        if (accion.equals("actualizar")) {
            this.Edit(request, response);
        }

        if (accion.equals("eliminar")) {
            this.eliminar(request, response);
        }
        if (accion.equals("estado")) {
            this.estado(request, response);
        }
    }

    private void add(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Producto p = new Producto();
        String codigo = request.getParameter("Txtcodigo");
        String descripcion = request.getParameter("Txtdescripcion");
        int categoria = Integer.parseInt(request.getParameter("Txtcategoria"));
        int marca = Integer.parseInt(request.getParameter("Txtmarca"));
        int proveedor = Integer.parseInt(request.getParameter("Txtproveedor"));
        int unidadc = Integer.parseInt(request.getParameter("Txtunidadc"));
        int unidadv = Integer.parseInt(request.getParameter("Txtunidadv"));
        int publico = Integer.parseInt(request.getParameter("TxtIdpublico"));
        double pcompra = Double.parseDouble(request.getParameter("Txtpcompra"));
        double pventa = Double.parseDouble(request.getParameter("Txtpventa"));
        String fecha = request.getParameter("Txtfechaderegistro");

        p.setCodigo(codigo);
        p.setDescripcion(descripcion);
        p.setIdcategoria(categoria);
        p.setIdmarca(marca);
        p.setIdProveedor(proveedor);
        p.setIducompra(unidadc);
        p.setIduventa(unidadv);
        p.setIdpublico(publico);
        p.setPreciocompra(pcompra);
        p.setPrecioVenta(pventa);
        p.setFechaRegistro(fecha);
        p.setEstado("Activo");
        ProductoDAO prodao = new ProductoDAO();
        if (prodao.add(p)) {
            response.getWriter().print("ok");

        } else {
            response.getWriter().print("no ha sido registrado correctamente");

        }
    }

    private void Editar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("idpro", request.getParameter("id"));
        request.getRequestDispatcher("EditarProducto.jsp").forward(
                request, response);
    }

    private void Edit(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        id = Integer.parseInt(request.getParameter("txtid"));
        String codigo = request.getParameter("Txtcodigo");
        String descripcion = request.getParameter("Txtdescripcion");
        String categoria = request.getParameter("Txtcategoria");
        String marca = request.getParameter("Txtmarca");
        String proveedor = request.getParameter("Txtproveedor");
        String unidadc = request.getParameter("Txtunidadc");
        String unidadv = request.getParameter("Txtunidadv");
        String pcompra = request.getParameter("Txtpcompra");
        String pventa = request.getParameter("Txtpventa");
        String publico = request.getParameter("TxtIdpublico");
        String fecha = request.getParameter("Txtfechaderegistro");

        p.setIdproducto(id);
        p.setCodigo(codigo);
        p.setDescripcion(descripcion);
        p.setIdcategoria(Integer.parseInt(categoria));
        p.setIdmarca(Integer.parseInt(marca));
        p.setIdProveedor(Integer.parseInt(proveedor));
        p.setIducompra(Integer.parseInt(unidadc));
        p.setIduventa(Integer.parseInt(unidadv));
        p.setIdpublico(Integer.parseInt(publico));
        p.setPreciocompra(Double.parseDouble(pcompra));
        p.setPrecioVenta(Double.parseDouble(pventa));
        p.setFechaRegistro(fecha);
        if (pdao.Edit(p)) {
            response.getWriter().print("ok");

        } else {
            response.getWriter().print("El producto no ha sido registrado correctamente");

        }
    }

    private void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        id = Integer.parseInt(request.getParameter("idPro"));
        pdao.Eliminar(id);
    }

    private void estado(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        id = Integer.parseInt(request.getParameter("id"));
        p.setIdproducto(id);
        String estado = ProductoDAO.estado(id);
        if (estado.equalsIgnoreCase("Activo")) {
            p.setEstado("Desactivado");
        } else {
            p.setEstado("Activo");
        }

        pdao.editEstado(p, id);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
