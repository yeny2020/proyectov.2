/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pe.gatasonrisa.model.controller;

import com.google.gson.Gson;
import com.pe.gatasygatos.DAO.ProvedorDAO;
import com.pe.gatasygatos.model.entity.Provedor;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author usuario
 */
public class ControllerProveedor extends HttpServlet {
     int id;
    Provedor p = new Provedor();
    ProvedorDAO pdao = new ProvedorDAO();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String accion = request.getParameter("accion");
        if (accion.equals("add")) {
            this.add(request, response);
        }
        if (accion.equals("Actualizar")) {
            this.Edit(request, response);
        }
        if (accion.equals("editar")) {
            this.Editar(request, response);
        }
        if (accion.equals("eliminar")) {
            this.eliminar(request, response);
        }
        if (accion.equals("Estado")) {
            this.Estado(request, response);
        }

        if (accion.equals("buscar")) {
            this.Buscar(request, response);
        }

        if (accion.equals("buscarPorId")) {
            this.BuscarPorId(request, response);
        }
        }
     private void BuscarPorId(HttpServletRequest request, HttpServletResponse response)
            throws IOException, IOException {

        int codigo = Integer.parseInt(request.getParameter("idProveedor"));
        ProvedorDAO pdao = new ProvedorDAO();

        Provedor obj = pdao.BuscarPorId(codigo);

        request.getSession().setAttribute("proveedor", obj);

        response.sendRedirect("InsertarCompra.jsp");

    }

    private void Buscar(HttpServletRequest request, HttpServletResponse response)
            throws IOException, IOException {
        PrintWriter out = response.getWriter();

        int codigo = Integer.parseInt(request.getParameter("idProveedor"));
        ProvedorDAO pdao = new ProvedorDAO();
        Gson gson = new Gson();
        Provedor obj = pdao.BuscarPorId(codigo);

        out.print(gson.toJson(obj));

    }

     private void add(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Provedor pro = new Provedor();
        String codigo = request.getParameter("TextCodigo");
        String Razonsocial = request.getParameter("TxtRazonsocial");
        String Direccion = request.getParameter("TxtDireccion");
        String Celular =request.getParameter("TxtCelular");
        String Email = request.getParameter("TxtEmail");
        String Idtipodocumento =request.getParameter("TxtIdtipodocumento");
        String Num =request.getParameter("TxtNumdocumento");
        String FechaRegistro = request.getParameter("TxtFechaRegistro");
        pro.setCodigo(codigo);
        pro.setRazonsocial(Razonsocial);
        pro.setDireccion(Direccion);
        pro.setCelular(Integer.parseInt(Celular));
        pro.setEmail(Email);
        pro.setIdtipodocumento(Integer.parseInt(Idtipodocumento));
        pro.setNumdocumento(Num);
        pro.setFechaderegistro(FechaRegistro);
        pro.setEstado("Activo");
        ProvedorDAO pdo = new ProvedorDAO();

        if (pdo.add(pro)) {
            response.getWriter().print("ok");

        } else {
            response.getWriter().print("no ha sido registrado correctamente");

        }
    }

    private void Edit(HttpServletRequest request, HttpServletResponse response)
            throws IOException, IOException {
        id = Integer.parseInt(request.getParameter("Txtid"));
        String codigo = request.getParameter("TextCodigo");
        String Razonsocial = request.getParameter("TxtRazonsocial");
        String Direccion = request.getParameter("TxtDireccion");
        String Celular = request.getParameter("TxtCelular");
        String Email = request.getParameter("TxtEmail");
        String Idtipodocumento = request.getParameter("TxtIdtipodocumento");
        String Numdocumento = request.getParameter("TxtNumdocumento");
        String FechaRegistro = request.getParameter("TxtFechaRegistro");

        p.setIdproveedor(id);
        p.setCodigo(codigo);
        p.setRazonsocial(Razonsocial);
        p.setDireccion(Direccion);
        p.setCelular(Integer.parseInt(Celular));
        p.setEmail(Email);
        p.setIdtipodocumento(Integer.parseInt(Idtipodocumento));
        p.setNumdocumento(Numdocumento);
        p.setFechaderegistro(FechaRegistro);
        if (pdao.Edit(p)) {
            response.getWriter().print("ok");

        } else {
            response.getWriter().print("El proveedor no ha sido registrado correctamente");

        }
    }

    private void Editar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("idprove", request.getParameter("id"));
        request.getRequestDispatcher("EditarProveedor.jsp").forward(
                request, response);
    }

    private void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        id = Integer.parseInt(request.getParameter("idprove"));
        pdao.Eliminar(id);
    }

    private void Estado(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        id = Integer.parseInt(request.getParameter("id"));
        p.setIdproveedor(id);
        String estado = ProvedorDAO.estado(id);
        if (estado.equalsIgnoreCase("Activo")) {
            p.setEstado("Desactivado");
        } else {
            p.setEstado("Activo");
        }

        pdao.editEstado(p, id);
    }

    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
