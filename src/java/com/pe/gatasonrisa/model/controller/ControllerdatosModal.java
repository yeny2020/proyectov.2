/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pe.gatasonrisa.model.controller;

import com.pe.gatasygatos.DAO.ProductoDAO;
import com.pe.gatasygatos.model.entity.Producto;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author yenny
 */
public class ControllerdatosModal extends HttpServlet {
        int id;
    String agregar = "VCantidadVenta.jsp";
    String pedido = "VCantidadPedido.jsp";
    
    String cantidadcotizacion = "cantidadCotizacion.jsp";
    String cantidadGuiaRemision = "cantidadGuiaRemision.jsp";
    
    String Edit = "EditarMarca.jsp";
    String Editmaterial = "EditarMaterial.jsp";
    String Editcategoria = "EditarCategoria.jsp";
    String EditCliente = "EditarCliente.jsp";
    String EditMotivo= "EditarMotivo.jsp";
    String addCliente = "InsertarCliente.jsp";
    String EditProveedor = "EditarProveedor.jsp";
    String InsertarProveedor = "InsertarProveedor.jsp";
    String EditUcompra = "EditarUnidadCompra.jsp";
    String EditUventa = "EditarUnidadVenta.jsp";
    Producto c = new Producto();
    ProductoDAO cdao = new ProductoDAO();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControllerdatosModal</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControllerdatosModal at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String acceso = "";
        String action = request.getParameter("accion");
         if (action.equalsIgnoreCase("agregar")) {
            request.setAttribute("idproxalm", request.getParameter("id"));
            acceso = agregar;
        }
         if (action.equalsIgnoreCase("pedido")) {
            request.setAttribute("idped", request.getParameter("id"));
            acceso = pedido;
        }
        if (action.equalsIgnoreCase("editar")) {
            request.setAttribute("idmar", request.getParameter("id"));
            acceso = Edit;
        }
         if (action.equalsIgnoreCase("editarmotivo")) {
            request.setAttribute("idmot", request.getParameter("id"));
            acceso = EditMotivo;
        }
        if (action.equalsIgnoreCase("editarcategoria")) {
            request.setAttribute("idcat", request.getParameter("id"));
            acceso = Editcategoria;
        }
        if (action.equalsIgnoreCase("editarmaterial")) {
            request.setAttribute("idmat", request.getParameter("id"));
            acceso = Editmaterial;
        }
        
        
           if (action.equalsIgnoreCase("agregarcotizacion")) {
            request.setAttribute("coti", request.getParameter("id"));
            acceso = cantidadcotizacion;
        }
             if (action.equalsIgnoreCase("cantidadGuiaRemision")) {
            request.setAttribute("coti", request.getParameter("id"));
            acceso = cantidadGuiaRemision;
        }
        if (action.equalsIgnoreCase("editarcliente")) {
            request.setAttribute("idcli", request.getParameter("id"));
            acceso = EditCliente;
        }
        
        
        if (action.equalsIgnoreCase("editarucompra")) {
            request.setAttribute("iduc", request.getParameter("id"));
            acceso = EditUcompra;
        }
         if (action.equalsIgnoreCase("editaruventa")) {
            request.setAttribute("iduv", request.getParameter("id"));
            acceso = EditUventa;
        }
        
        if (action.equalsIgnoreCase("addcliente")) {
            acceso = addCliente;
        }
        if (action.equalsIgnoreCase("editarProveedor")) {
            request.setAttribute("idprove", request.getParameter("id"));
            acceso = EditProveedor;
        }
        
         if (action.equalsIgnoreCase("InsertarProveedor")) {
            acceso = InsertarProveedor;
        }
          
        // fin Estado
        RequestDispatcher vista = request.getRequestDispatcher(acceso);
        vista.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
