package com.pe.gatasygatos.DAO;
import com.pe.gatasygatos.conection.ConexionBD;
import com.pe.gatasygatos.model.entity.Almacen;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
public class AlmacenDAO {
    ConexionBD cn = new ConexionBD();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    Almacen c = new Almacen();
    CallableStatement call=null;
    int men=0;
    String mensaje = "";

    public List ListadoMaterial() {
        ArrayList<Almacen> list = new ArrayList<>();
        String sql = "SELECT * FROM Almacen ORDER BY Idalmacen DESC";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Almacen mat = new Almacen();
                mat.setIdalmacen(rs.getInt("Idalmacen"));
                mat.setCodigo(rs.getString("Codigo"));
                mat.setNombre(rs.getString("Nombre"));
                mat.setEstado(rs.getString("Estado"));
                list.add(mat);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List ListadoEstadoActivos() {
        ArrayList<Almacen> list = new ArrayList<>();
        String sql = "select * from Almacen where Estado='Activo'";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Almacen mat = new Almacen();
                mat.setIdalmacen(rs.getInt("Idalmacen"));
                mat.setNombre(rs.getString("Nombre"));
                list.add(mat);
            }
        } catch (Exception e) {

        }
        return list;
    }

    //Listar Almacen general uso en AlmacenInsertarNotasalida.jsp
    public List Almacengenral() {
        ArrayList<Almacen> list = new ArrayList<>();
        String sql = "select * from Almacen where Idalmacen=2";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Almacen mat = new Almacen();
                mat.setIdalmacen(rs.getInt("Idalmacen"));
                mat.setNombre(rs.getString("Nombre"));
                list.add(mat);
            }
        } catch (Exception e) {
        }
        return list;
    }
    
    public static String estado(int cod) {
        try {
            String sql = "select Estado from Almacen where Idalmacen=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("Estado");
            }
            return "--";
        } catch (Exception e) {
            return "--";
        }
    }
    public static String getmaterial(int cod) {
        try {
            String sql = "select Nombre from Almacen where Idalmacen=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("Nombre");

            }
            return "--";

        } catch (Exception e) {
            return "--";
        }
    }
    public static String getmaterialEstado(int cod) {
        try {
            String sql = "select Estado from Almacen where Idalmacen=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("Estado");
            }
            return "--";

        } catch (Exception e) {
            return "--";
        }
    }
    //llamar nombre  almacen 
    public static String getAlmacenNombre(int cod) {
        try {
            String sql = "Select Nombre from Almacen where Idalmacen=2";
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("Nombre");
            }
            return "--";

        } catch (Exception e) {
            return "--";
        }
    }

    //ESTOC DE PRODUCTOS ALMACEN GENRAL
    public static String estadoAlmacenP(int cod) {
        try {
            String sql = "select Estado from Almacenxproducto where Idalmacen=2 And Idalmacenxproducto=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("estado");
            }
            return "--";
        } catch (Exception e) {
            return "--";
        }
    }
    
    public Almacen list(int id) {
        String sql = "select * from Almacen where Idalmacen=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {

                c.setIdalmacen(rs.getInt("Idalmacen"));
                c.setCodigo(rs.getString("Codigo"));
                c.setNombre(rs.getString("Nombre"));
            }
        } catch (Exception e) {
        }
        return c;
    }
    
    public boolean add(Almacen mat) {
        boolean flag = false;
        String sql = "insert into Almacen(Codigo,Nombre,Estado)values('" + mat.getCodigo() + "','" + mat.getNombre() + "','" + mat.getEstado() + "')";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            if (ps.executeUpdate() == 1) {
                flag = true;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return flag;
    }
    
    public boolean Edit(Almacen mat) {
        boolean flag = false;
        String sql = "update Almacen set Codigo='"+mat.getCodigo()+"',Nombre='" + mat.getNombre() + "'where Idalmacen=" + mat.getIdalmacen();
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            if (ps.executeUpdate() == 1) {
                flag = true;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return flag;
    }
    
    public boolean Estado(Almacen cat, int id) {
        String sql = "update Almacen set Estado='" + cat.getEstado() + "'where Idalmacen=" + id;

        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return false;
    }

    public boolean Eliminar(int id) {
        String sql = "delete from Almacen where Idalmacen=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return false;
    }
    
    public String Numserie() {
        String sql = "{call sp_generar_codigoalmacen()}";
        try {
            con = cn.getConnection();
            call = con.prepareCall(sql);
            rs = call.executeQuery();

            if (rs.next()) {
                mensaje = rs.getString(1);
            }
        } catch (SQLException e) {
        }
        return mensaje;
    }
    public static void main(String[] args) {
        AlmacenDAO td = new AlmacenDAO();
        List<Almacen> lista = td.ListadoMaterial();
        System.out.println("Lista " + lista.size());
        lista.forEach(c -> {
            System.out.println(c.toString());
        });
    }
}
