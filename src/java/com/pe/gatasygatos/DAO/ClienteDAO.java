package com.pe.gatasygatos.DAO;
import com.pe.gatasygatos.conection.ConexionBD;
import com.pe.gatasygatos.model.entity.Cliente;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fanay
 */
public class ClienteDAO {

    ConexionBD cn = new ConexionBD();
    Connection con;
    int mensaje = 0;
    PreparedStatement ps;
    ResultSet rs;
    CallableStatement call = null;
    String mensaj = "";
    Cliente prov = new Cliente();
    Cliente Cliente = new Cliente();

//Listar cliente uso en Cliente.jsp and VInsertarventa.jsp
    public List ListadoCliente() {
        ArrayList<Cliente> listaCliente = new ArrayList<>();
        String sql = "SELECT * FROM Cliente";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Cliente Clientes = new Cliente();
                Clientes.setIdcliente(rs.getInt("Idcliente"));
                Clientes.setCodigo(rs.getString("Codigo"));
                Clientes.setCliente(rs.getString("Cliente"));
                Clientes.setCorreo(rs.getString("Correo"));
                Clientes.setCelular(rs.getInt("Celular"));
                Clientes.setDireccion(rs.getString("Direccion"));
                Clientes.setFechaderegistro(rs.getString("Fechaderegistro"));
                Clientes.setIdtipodocumento(rs.getInt("Idtipodocumento"));
                Clientes.setNumerodocumento(rs.getString("Numerodocumento"));
                Clientes.setSexo(rs.getString("Sexo"));
                Clientes.setEstado(rs.getString("Estado"));
                listaCliente.add(Clientes);
            }
        } catch (Exception e) {
        }
        return listaCliente;
    }

    //Insertar Cliente uso ControllerCliente
    public boolean add(Cliente cli) {
        boolean flag = false;
        String sql = "insert into Cliente(Codigo,Cliente, Correo, Celular, Direccion, Fechaderegistro, Idtipodocumento, Numerodocumento, Sexo,Estado)values('" + cli.getCodigo() + "','" + cli.getCliente() + "','" + cli.getCorreo() + "','" + cli.getCelular() + "','" + cli.getDireccion() + "','" + cli.getFechaderegistro() + "','" + cli.getIdtipodocumento() + "','" + cli.getNumerodocumento() + "','" + cli.getSexo() + "','" + cli.getEstado() + "')";

        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            if (ps.executeUpdate() == 1) {
                flag = true;
            }

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return flag;
    }

//Editar cliente uso en ControllerCliente
    public boolean Edit(Cliente Cliente) {
        boolean flag = false;
        String sql = "UPDATE Cliente set Codigo='" + Cliente.getCodigo() + "',Cliente='" + Cliente.getCliente() + "',Correo='" + Cliente.getCorreo() + "',Celular='" + Cliente.getCelular() + "',Direccion='" + Cliente.getDireccion() + "',Fechaderegistro='" + Cliente.getFechaderegistro() + "',Idtipodocumento='" + Cliente.getIdtipodocumento() + "',Numerodocumento='" + Cliente.getNumerodocumento() + "',Sexo='" + Cliente.getSexo() + "' WHERE Idcliente=" + Cliente.getIdcliente();
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            if (ps.executeUpdate() == 1) {
                flag = true;
            }

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return flag;
    }

    //Eliminar cliente uso en ControllerCliente
    public boolean Eliminar(int id) {
        String sql = "delete from Cliente where Idcliente=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return false;
    }

    public static String getEstado(int cod) {

        try {
            String sql = "select Estado from Cliente where Idcliente=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("Estado");

            }
            return "--";

        } catch (Exception e) {
            return "--";
        }
    }
    
    //Mostrar estado segun el ID del cliente uso en ControllerCliente and Cliente.jsp
    public static String estado(int cod) {
        try {
            String sql = "select Estado from Cliente where Idcliente=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("estado");
            }
            return "--";

        } catch (Exception e) {
            return "--";
        }
    }

    //Editar estado de cliente uso en ControllerCliente
    public boolean editEstado(Cliente cli, int id) {
        String sql = "update Cliente set Estado='" + cli.getEstado() + "' where Idcliente=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return false;
    }

    //Codigo para cliente us en InsertarCliente.jsp
    public String Numserie() {
        String sql = "{call sp_generar_codigocliente()}";
        try {
            con = cn.getConnection();
            call = con.prepareCall(sql);
            rs = call.executeQuery();

            if (rs.next()) {
                mensaj = rs.getString(1);
            }
        } catch (SQLException e) {
        }
        return mensaj;
    }

//Llamar al cliente para seleccionar en VInsertarventa uso en // ControllerCliente
    public Cliente BuscarPorId(int idCliente) {
        Cliente pv = null;
        String sql = "select * from Cliente where Idcliente = ?";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setInt(1, idCliente);
            rs = ps.executeQuery();
            if (rs.next()) {
                pv = new Cliente();
                pv.setIdcliente(rs.getInt("Idcliente"));
                pv.setCodigo(rs.getString("Codigo"));
                pv.setCliente(rs.getString("Cliente"));
                pv.setCorreo(rs.getString("Correo"));
                pv.setCelular(rs.getInt("Celular"));
                pv.setDireccion(rs.getString("Direccion"));
                pv.setFechaderegistro(rs.getString("Fechaderegistro"));
                pv.setIdtipodocumento(rs.getInt("Idtipodocumento"));
                pv.setNumerodocumento(rs.getString("Numerodocumento"));
                pv.setSexo(rs.getString("Sexo"));
                pv.setEstado(rs.getString("Estado"));
            }
        } catch (SQLException e) {
            mensaj = e.getMessage();
        } finally {
            cn.desconectar();
        }
        return pv;
    }

    public static String getcliente(int cod) {
        try {
            String sql = "Select Cliente from Cliente where Idcliente=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("Cliente");
            }
            return "--";
        } catch (Exception e) {
            return "--";
        }
    }

    public static String getnrodocumento(int cod) {
        try {
            String sql = "Select Numerodocumento from Cliente where Idcliente=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("Numerodocumento");
            }
            return "--";

        } catch (Exception e) {
            return "--";
        }
    }

    public static String getDireccion(int cod) {
        try {
            String sql = "Select Direccion from Cliente where Idcliente=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("Direccion");
            }
            return "--";

        } catch (Exception e) {
            return "--";
        }
    }

    public boolean validacion(Cliente cli) {
        boolean flag = false;
        String sql = "select * from Cliente where Numerodocumento=" + cli.getNumerodocumento();
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                flag = true;
            }
        } catch (Exception e) {

        }
        return flag;
    }

//    public static void main(String[] args) {
//
//        ClienteDAO td = new ClienteDAO();
//        List<Cliente> lista = td.ListadoCliente();
//        System.out.println("Lista " + lista.size());
//
//        lista.forEach(c -> {
//            System.out.println(c.toString());
//
//        });
//              //  System.out.println(td.add(new Cliente(0,"0009","loca DDD", "HDG@gmail.com", 123223233, "av. lima 22","2020-11-27", 1,8733463,"M")));
//
//    }
}
