/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pe.gatasygatos.DAO;

import com.pe.gatasygatos.conection.ConexionBD;
import com.pe.gatasygatos.model.entity.Compra;
import com.pe.gatasygatos.model.entity.DetalleCompra;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author usuario
 */
public class CompraDAO {

    ConexionBD cn = new ConexionBD();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    Compra c = new Compra();

    public boolean insertarCompra(Compra varcompra, ArrayList<DetalleCompra> d) {
        boolean rpta = false;
        try {

            con = cn.getConnection();
            CallableStatement cl = con.prepareCall("{call sp_RegistrarCompra(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            cl.registerOutParameter(1, Types.INTEGER);
            cl.setInt(2, varcompra.getIdproveedor());
            cl.setInt(3, varcompra.getIdusuario());
            cl.setString(4, varcompra.getTipocomprobante());
            cl.setString(5, varcompra.getSerie());
            cl.setString(6, varcompra.getCorrelativo());
            cl.setString(7, varcompra.getFechaYhora());
            cl.setString(8, varcompra.getTienda());
            cl.setString(9, varcompra.getAlmacen());
            cl.setString(10, varcompra.getCondicion());
            cl.setInt(11, varcompra.getIdmotivo());
            cl.setDouble(12, varcompra.getSubtotal());
            cl.setDouble(13, varcompra.getIgv());
            cl.setDouble(14, varcompra.getTotal());
            cl.setString(15, varcompra.getEstado());
            if (cl.executeUpdate() == 2) {
                rpta = true;
            }
            varcompra.setIdcompra(cl.getInt(1));
            CallableStatement cl2 = con.prepareCall("{CALL newdetalleCompra(?,?,?,?,?)}");
            for (DetalleCompra aux : d) {
                cl2.setInt(1, varcompra.getIdcompra());
                cl2.setInt(2, aux.getIdproducto());
                cl2.setDouble(3, aux.getCosto());
                cl2.setInt(4, aux.getCantidad());
                cl2.setInt(5, aux.getContenido());
                if (cl2.executeUpdate() == 1) {
                    rpta = true;
                }

            }

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return rpta;
    }

    public static String CodigoCompra() {
        try {
            String sql = "select max(Idcompra)from Compra";
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString(1);

            }
            return "--";

        } catch (Exception e) {
            return "--";
        }
    }

    public boolean editartotal(Compra c) {
        String sql = " update Compra set Totalcompra='" + c.getTotal() + "' where Idcompra=" + c.getIdcompra();
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (SQLException e) {
        }
        return false;

    }

    public boolean editarEstado(Compra c, int id) {
        String sql = " update Compra set Estado='" + c.getEstado() + "' where Idcompra=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (SQLException e) {
        }
        return false;

    }

    public static String estado(int cod) {

        try {
            String sql = "select Estado from Compra where Idcompra=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("Estado");

            }
            return "--";

        } catch (Exception e) {
            return "--";
        }
    }

    public List ListadoDetalle() {
        ArrayList<DetalleCompra> list = new ArrayList<>();
        String sql = "SELECT * FROM Detallecompra WHERE Idcompra=(SELECT MAX(Idcompra) from Compra)";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                DetalleCompra detalle_compra = new DetalleCompra();
                detalle_compra.setIddetallecompra(rs.getInt("Iddetallecompra"));
                detalle_compra.setIdcompra(rs.getInt("Idompra"));
                detalle_compra.setIdproducto(rs.getInt("Idproducto"));
                detalle_compra.setCantidad(rs.getInt("Cantidad"));
                list.add(detalle_compra);
            }

        } catch (Exception e) {

        }
        return list;
    }

    public List ticketDetalle(int id) {
         ArrayList<DetalleCompra> list = new ArrayList<>();
        String sql = "SELECT * FROM Detallecompra WHERE Idcompra=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                DetalleCompra detalle = new DetalleCompra();
                detalle.setIddetallecompra(rs.getInt("Iddetallecompra"));
                detalle.setIdproducto(rs.getInt("Idproducto"));
                detalle.setIdcompra(rs.getInt("Idcompra"));
                 detalle.setCosto(rs.getDouble("Costo"));
                detalle.setContenido(rs.getInt("Contenido"));
                detalle.setCantidad(rs.getInt("Cantidad"));
                
                list.add(detalle);
            }

        } catch (Exception e) {

        }
        return list;
    }

    public boolean editaranulardetalle(DetalleCompra c, int id) {
        String sql = " update Detallecompra set Estado='" + c.getEstado() + "' where Iddetalle=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (SQLException e) {
        }
        return false;

    }

    public boolean editarestadodetalle_compra(DetalleCompra c, int id) {
        String sql = " update Detallecompra set Estado='" + c.getEstado() + "' where Idcompra=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (SQLException e) {
        }
        return false;

    }

    public List ListadoCotizacion() {
        ArrayList<Compra> list = new ArrayList<>();
        String sql = "SELECT * FROM Compra where Tipocomprobante='Cotizacion'";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Compra compra = new Compra();
                compra.setIdcompra(rs.getInt("Idcompra"));
                compra.setIdproveedor(rs.getInt("Idproveedor"));
                compra.setIdusuario(rs.getInt("Idusuario"));
                compra.setTipocomprobante(rs.getString("Tipocomprobante"));
                compra.setSerie(rs.getString("Serie"));
                compra.setCorrelativo(rs.getString("Correlativo"));
                compra.setFechaYhora(rs.getString("Fechayhora"));
                compra.setTienda(rs.getString("Tienda"));
                compra.setAlmacen(rs.getString("Almacen"));
                compra.setCondicion(rs.getString("Condicion"));
                compra.setIdmotivo(rs.getInt("Idmotivo"));
                compra.setSubtotal(rs.getDouble("Subtotal"));
                compra.setIgv(rs.getDouble("Igv"));
                compra.setTotal(rs.getDouble("Total"));
                compra.setEstado(rs.getString("Estado"));
                list.add(compra);
            }
        } catch (Exception e) {

        }
        return list;
    }
    
    //listar orden decompra
    public List ListadoOrdenCompra() {
        ArrayList<Compra> list = new ArrayList<>();
        String sql = "SELECT * FROM Compra where Tipocomprobante='Orden'";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Compra compra = new Compra();
                compra.setIdcompra(rs.getInt("Idcompra"));
                compra.setIdproveedor(rs.getInt("Idproveedor"));
                compra.setIdusuario(rs.getInt("Idusuario"));
                compra.setTipocomprobante(rs.getString("Tipocomprobante"));
                compra.setSerie(rs.getString("Serie"));
                compra.setCorrelativo(rs.getString("Correlativo"));
                compra.setFechaYhora(rs.getString("Fechayhora"));
                compra.setTienda(rs.getString("Tienda"));
                compra.setAlmacen(rs.getString("Almacen"));
                compra.setCondicion(rs.getString("Condicion"));
                compra.setIdmotivo(rs.getInt("Idmotivo"));
                compra.setSubtotal(rs.getDouble("Subtotal"));
                compra.setIgv(rs.getDouble("Igv"));
                compra.setTotal(rs.getDouble("Total"));
                compra.setEstado(rs.getString("Estado"));
                list.add(compra);
            }
        } catch (Exception e) {

        }
        return list;
    }
    
      //listar Guia de remision
    public List ListadoGuiaRemison() {
        ArrayList<Compra> list = new ArrayList<>();
        String sql = "SELECT * FROM Compra where Tipocomprobante='Guia Remision'";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Compra compra = new Compra();
                compra.setIdcompra(rs.getInt("Idcompra"));
                compra.setIdproveedor(rs.getInt("Idproveedor"));
                compra.setIdusuario(rs.getInt("Idusuario"));
                compra.setTipocomprobante(rs.getString("Tipocomprobante"));
                compra.setSerie(rs.getString("Serie"));
                compra.setCorrelativo(rs.getString("Correlativo"));
                compra.setFechaYhora(rs.getString("Fechayhora"));
                compra.setTienda(rs.getString("Tienda"));
                compra.setAlmacen(rs.getString("Almacen"));
                compra.setCondicion(rs.getString("Condicion"));
                compra.setIdmotivo(rs.getInt("Idmotivo"));
                compra.setSubtotal(rs.getDouble("Subtotal"));
                compra.setIgv(rs.getDouble("Igv"));
                compra.setTotal(rs.getDouble("Total"));
                compra.setEstado(rs.getString("Estado"));
                list.add(compra);
            }
        } catch (Exception e) {

        }
        return list;
    }
    
       //listar Factura Compra
    public List ListadoFacturaCompra() {
        ArrayList<Compra> list = new ArrayList<>();
        String sql = "SELECT * FROM Compra where Tipocomprobante='Factura Compra'";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Compra compra = new Compra();
                compra.setIdcompra(rs.getInt("Idcompra"));
                compra.setIdproveedor(rs.getInt("Idproveedor"));
                compra.setIdusuario(rs.getInt("Idusuario"));
                compra.setTipocomprobante(rs.getString("Tipocomprobante"));
                compra.setSerie(rs.getString("Serie"));
                compra.setCorrelativo(rs.getString("Correlativo"));
                compra.setFechaYhora(rs.getString("Fechayhora"));
                compra.setTienda(rs.getString("Tienda"));
                compra.setAlmacen(rs.getString("Almacen"));
                compra.setCondicion(rs.getString("Condicion"));
                compra.setIdmotivo(rs.getInt("Idmotivo"));
                compra.setSubtotal(rs.getDouble("Subtotal"));
                compra.setIgv(rs.getDouble("Igv"));
                compra.setTotal(rs.getDouble("Total"));
                compra.setEstado(rs.getString("Estado"));
                list.add(compra);
            }
        } catch (Exception e) {

        }
        return list;
    }
    
     //listar Nota de ingreso
    public List ListadoNotaIngreso() {
        ArrayList<Compra> list = new ArrayList<>();
        String sql = "SELECT * FROM Compra where Tipocomprobante='Nota Ingreso'";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Compra compra = new Compra();
                compra.setIdcompra(rs.getInt("Idcompra"));
                compra.setIdproveedor(rs.getInt("Idproveedor"));
                compra.setIdusuario(rs.getInt("Idusuario"));
                compra.setTipocomprobante(rs.getString("Tipocomprobante"));
                compra.setSerie(rs.getString("Serie"));
                compra.setCorrelativo(rs.getString("Correlativo"));
                compra.setFechaYhora(rs.getString("Fechayhora"));
                compra.setTienda(rs.getString("Tienda"));
                compra.setAlmacen(rs.getString("Almacen"));
                compra.setCondicion(rs.getString("Condicion"));
                compra.setIdmotivo(rs.getInt("Idmotivo"));
                compra.setSubtotal(rs.getDouble("Subtotal"));
                compra.setIgv(rs.getDouble("Igv"));
                compra.setTotal(rs.getDouble("Total"));
                compra.setEstado(rs.getString("Estado"));
                list.add(compra);
            }
        } catch (Exception e) {

        }
        return list;
    }

    public Compra Reporte(int id) {
        String sql = "select * from Compra where Idcompra=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {

                c.setIdcompra(rs.getInt("Idcompra"));
                c.setIdproveedor(rs.getInt("Idproveedor"));
                c.setIdusuario(rs.getInt("Idusuario"));
                c.setTipocomprobante(rs.getString("Tipocomprobante"));
                c.setSerie(rs.getString("Serie"));
                c.setCorrelativo(rs.getString("Correlativo"));
                c.setFechaYhora(rs.getString("Fechayhora"));
                c.setTienda(rs.getString("Tienda"));
                c.setAlmacen(rs.getString("Almacen"));
                c.setCondicion(rs.getString("Condicion"));
                c.setIdmotivo(rs.getInt("Idmotivo"));
                c.setSubtotal(rs.getDouble("Subtotal"));
                c.setIgv(rs.getDouble("Igv"));
                c.setTotal(rs.getDouble("Total"));
                c.setEstado(rs.getString("Estado"));

            }

        } catch (Exception e) {

        }
        return c;

    }

    public boolean EliminarDetalle(int id) {
        String sql = "delete from Detallecompra where Iddetallecompra=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return false;
    }

    public int BuscarNorden() {
        String sSQL = "SELECT COUNT(*) as Norden FROM Compra WHERE tipocomprobante ='Orden'";

        try {
            int Norden = 0;
            con = cn.getConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            while (rs.next()) {
                Norden = rs.getInt("Norden");
            }

            return Norden;

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return 0;
        }

    }

    public int SerieNfacturas() {
        String sSQL = "SELECT COUNT(*) as Nfactura FROM Compra WHERE tipocomprobante ='Factura'";

        try {
            int Nfactura = 0;
            con = cn.getConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            while (rs.next()) {
                Nfactura = rs.getInt("Nfactura");
            }

            return Nfactura;

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return 0;
        }

    }

    public int BuscarNCotizacion() {
        String sql = "SELECT COUNT(*) as Ncotizacion  FROM Compra WHERE Tipocomprobante ='Cotizacion'";

        try {
            int Nfactura = 0;
            con = cn.getConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Nfactura = rs.getInt("Ncotizacion");
            }

            return Nfactura;

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return 0;
        }

    }

    public int SerieNBoleta() {
        String sql = "SELECT COUNT(*) as Nboleta FROM Compra WHERE Tipocomprobante ='Boleta'";

        try {
            int Nfactura = 0;
            con = cn.getConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Nfactura = rs.getInt("Nboleta");
            }

            return Nfactura;

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return 0;
        }

    }

    public int usu(String usu, String pass) {
        String sql = "SELECT id FROM usuario WHERE usu ='" + usu + "' AND password='" + pass + "'";

        try {
            int Nfactura = 0;
            con = cn.getConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Nfactura = rs.getInt("id");
            }

            return Nfactura;

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return 0;
        }

    }

    public Compra getCompra(int id) {
        Compra com = null;
        try {
            ResultSet res;
            PreparedStatement stmt = this.con.prepareStatement("call getCompras(?)");
            stmt.setInt(1, id);
            res = stmt.executeQuery();
            while (res.next()) {
                com = new Compra();
                com.setIdcompra(rs.getInt("Idcompra"));
                com.setIdproveedor(rs.getInt("Idproveedor"));
                com.setIdusuario(rs.getInt("Idusuario"));
                com.setTipocomprobante(rs.getString("Tipocomprobante"));
                com.setSerie(rs.getString("Serie"));
                com.setCorrelativo(rs.getString("Correlativo"));
                com.setFechaYhora(rs.getString("FechaYhora"));
                com.setTienda(rs.getString("Tienda"));
                com.setAlmacen(rs.getString("Almacen"));
                com.setCondicion(rs.getString("Condicion"));
                com.setIdmotivo(rs.getInt("Idmotivo"));

            }
            res.close();

        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return com;
    }

}
