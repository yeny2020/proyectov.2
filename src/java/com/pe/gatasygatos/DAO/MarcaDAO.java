/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pe.gatasygatos.DAO;

import com.pe.gatasygatos.conection.ConexionBD;
import com.pe.gatasygatos.model.entity.Marca;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MarcaDAO {

    ConexionBD cn = new ConexionBD();
    Connection con;
    int mensaje = 0;
    PreparedStatement ps;
    ResultSet rs;
    Marca c = new Marca();
    CallableStatement call = null;
    int men = 0;
    String mensaj = "";

    //Listado marca uso en Marca.jsp
    public List ListadoMarca() {
        ArrayList<Marca> list = new ArrayList<>();
        String sql = "SELECT * FROM Marca";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Marca cat = new Marca();
                cat.setIdmarca(rs.getInt("Idmarca"));
                cat.setCodigo(rs.getString("Codigo"));
                cat.setNombre(rs.getString("Nombre"));
                cat.setEstado(rs.getString("Estado"));
                list.add(cat);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List ListadoEstadoActivos() {
        ArrayList<Marca> list = new ArrayList<>();
        String sql = "select * from Marca where Estado='Activo'";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Marca cat = new Marca();
                cat.setIdmarca(rs.getInt("Idmarca"));
                cat.setCodigo(rs.getString("Codigo"));
                cat.setNombre(rs.getString("Nombre"));
                list.add(cat);
            }
        } catch (Exception e) {

        }
        return list;
    }

    //Seleccionar Marca dependiendo el ID uso en EditarMarca.jsp
    public Marca BuscarporID(int id) {
        String sql = "select * from Marca where Idmarca=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                c.setIdmarca(rs.getInt("Idmarca"));
                c.setCodigo(rs.getString("Codigo"));
                c.setNombre(rs.getString("Nombre"));
            }
        } catch (Exception e) {
        }
        return c;
    }

    //Insertar marca uso ControllerMarca
    public boolean add(Marca cat) {
        boolean flag = false;
        String sql = "insert into Marca(Codigo,Nombre,Estado)values('" + cat.getCodigo() + "','" + cat.getNombre() + "','" + cat.getEstado() + "')";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            if (ps.executeUpdate() == 1) {
                flag = true;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return flag;
    }

    //Editar marca uso ControllerMarca
    public boolean Edit(Marca mar) {
        boolean flag = false;
        String sql = "update Marca set Codigo='" + mar.getCodigo() + "',Nombre='" + mar.getNombre() + "'where Idmarca=" + mar.getIdmarca();
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            if (ps.executeUpdate() == 1) {
                flag = true;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return flag;
    }

    //Eliminar marca uso ControllerMarca
    public boolean Eliminar(int id) {
        boolean flag = false;
        String sql = "delete from Marca where Idmarca=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            if (ps.executeUpdate() == 1) {
                flag = true;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return flag;
    }

    
    //Mostrar nombre de la marca la lista de producto en Producto.jsp
    public static String getNombreMarca(int cod) {
        try {
            String sql = "select Nombre from Marca where Idmarca=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("Nombre");
            }
            return "--";
        } catch (Exception e) {
            return "--";
        }
    }

    //obtener estado Marca cin el ID uso en ControllerMarca and Marca.jsp
    public static String getmarcaEstado(int cod) {
        try {
            String sql = "select Estado from Marca where Idmarca=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("Estado");
            }
            return "--";
        } catch (Exception e) {
            return "--";
        }
    }
    
    //Editar estado uso ControllerMarca
    public boolean EditarEstado(Marca cat, int id) {
        String sql = "update Marca set Estado='" + cat.getEstado() + "'where Idmarca=" + id;

        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return false;
    }

    //Codigo para Marca uso en Marca.jsp
    public String Numserie() {
        String sql = "{call sp_generar_codigomarca()}";
        try {
            con = cn.getConnection();
            call = con.prepareCall(sql);
            rs = call.executeQuery();
            if (rs.next()) {
                mensaj = rs.getString(1);
            }
        } catch (SQLException e) {
        }
        return mensaj;
    }

}
