/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pe.gatasygatos.DAO;

import com.pe.gatasygatos.conection.ConexionBD;
import com.pe.gatasygatos.model.entity.DetalleMovimiento;
import com.pe.gatasygatos.model.entity.Movimientos;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author yenny
 */
public class MovimientoDAO {

    ConexionBD cn = new ConexionBD();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    Movimientos v = new Movimientos();

    //Insertar cualquier tipo de Movimiento uso en todo los controller pedido y notas
    public boolean insertarMovimiento(Movimientos varmovimiento, ArrayList<DetalleMovimiento> d) {
        boolean rpta = false;
        try {
            con = cn.getConnection();
            CallableStatement cl = con.prepareCall("{call sp_Registrarmovimiento(?,?,?,?,?,?,?,?,?,?,?)}");
            cl.registerOutParameter(1, Types.INTEGER);
            cl.setInt(2, varmovimiento.getIdproveedor());
            cl.setInt(3, varmovimiento.getIdusuario());
            cl.setString(4, varmovimiento.getTipocomprobante());
            cl.setString(5, varmovimiento.getSerie());
            cl.setString(6, varmovimiento.getCorrelativo());
            cl.setString(7, varmovimiento.getFechayhora());
            cl.setString(8, varmovimiento.getTienda());
            cl.setString(9, varmovimiento.getAlmacen());
            cl.setInt(10, varmovimiento.getIdmotivo());
            cl.setString(11, varmovimiento.getEstado());
            if (cl.executeUpdate() == 2) {
                rpta = true;
            }
            varmovimiento.setIdmovimiento(cl.getInt(1));
            CallableStatement cl2 = con.prepareCall("{CALL newdetallemovimiento(?,?,?)}");
            for (DetalleMovimiento aux : d) {
                cl2.setInt(1, varmovimiento.getIdmovimiento());
                cl2.setInt(2, aux.getIdproducto());
                cl2.setDouble(3, aux.getCantidad());
                if (cl2.executeUpdate() == 1) {
                    rpta = true;
                }

            }

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return rpta;
    }

    public static String CodigoMovimiento() {
        try {
            String sql = "select max(Idventa)from Movimiento";
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString(1);

            }
            return "--";

        } catch (Exception e) {
            return "--";
        }
    }

    public boolean editarEstado(Movimientos m, int id) {
        String sql = " update Movimiento set Estado='" + m.getEstado() + "' where Idmovimiento=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (SQLException e) {
        }
        return false;

    }

    public static String estado(int cod) {

        try {
            String sql = "select Estado from Movimiento where Idmovimiento=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("Estado");

            }
            return "--";

        } catch (Exception e) {
            return "--";
        }
    }

    public List ListadoDetalle() {
        ArrayList<DetalleMovimiento> list = new ArrayList<>();
        String sql = "SELECT * FROM Detallemovimiento WHERE Idmovimiento=(SELECT MAX(Idmovimiento) from Movimiento)";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                DetalleMovimiento detalle_movi = new DetalleMovimiento();
                detalle_movi.setIddetallemovimiento(rs.getInt("Iddetallemovimiento"));
                detalle_movi.setIdmovimiento(rs.getInt("Idventa"));
                detalle_movi.setIdproducto(rs.getInt("Idproducto"));
                detalle_movi.setCantidad(rs.getInt("Cantidad"));
                list.add(detalle_movi);
            }

        } catch (Exception e) {

        }
        return list;
    }

    public List ticketDetalle(int id) {
        ArrayList<DetalleMovimiento> list = new ArrayList<>();
        String sql = "SELECT * FROM Detallemovimiento WHERE Idmovimiento=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                DetalleMovimiento detalle = new DetalleMovimiento();
                detalle.setIddetallemovimiento(rs.getInt("Iddetallemovimiento"));
                detalle.setIdmovimiento(rs.getInt("Idmovimiento"));
                detalle.setIdproducto(rs.getInt("Idproducto"));
                detalle.setCantidad(rs.getInt("Cantidad"));
                list.add(detalle);
            }

        } catch (Exception e) {

        }
        return list;
    }

    public boolean editaranulardetalle(DetalleMovimiento dm, int id) {
        String sql = " update Detallemovimiento set Estado='" + dm.getEstado() + "' where Iddetallemovimiento=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (SQLException e) {
        }
        return false;

    }

    public boolean editarestadodetalle_movimiento(DetalleMovimiento mv, int id) {
        String sql = " update Detallemovimiento set Estado='" + mv.getEstado() + "' where Idmovimiento=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (SQLException e) {
        }
        return false;

    }

    //Lista de pedido pendiente uso en VListarpedido.jsp
    public List ListadoMovimiento() {
        ArrayList<Movimientos> list = new ArrayList<>();
        String sql = "SELECT * FROM Movimiento where Tipocomprobante='Pedido'and Estado='Pendiente'";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Movimientos Movi = new Movimientos();
                Movi.setIdmovimiento(rs.getInt("Idmovimiento"));
                Movi.setIdproveedor(rs.getInt("Idproveedor"));
                Movi.setIdusuario(rs.getInt("Idusuario"));
                Movi.setTipocomprobante(rs.getString("Tipocomprobante"));
                Movi.setSerie(rs.getString("Serie"));
                Movi.setCorrelativo(rs.getString("Correlativo"));
                Movi.setFechayhora(rs.getString("Fechayhora"));
                Movi.setTienda(rs.getString("Tienda"));
                Movi.setAlmacen(rs.getString("Almacen"));
                Movi.setIdmotivo(rs.getInt("Idmotivo"));
                Movi.setEstado(rs.getString("Estado"));
                list.add(Movi);
            }
        } catch (Exception e) {

        }
        return list;
    }

    //Lista de Nota salida en  proceso uso en VListarpedido.jsp
    public List ListadoMovimientoprocesandoNS() {
        ArrayList<Movimientos> list = new ArrayList<>();
        String sql = "SELECT * FROM Movimiento where Tipocomprobante='Nota Salida' and Estado='Procesando'";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Movimientos Movi = new Movimientos();
                Movi.setIdmovimiento(rs.getInt("Idmovimiento"));
                Movi.setIdproveedor(rs.getInt("Idproveedor"));
                Movi.setIdusuario(rs.getInt("Idusuario"));
                Movi.setTipocomprobante(rs.getString("Tipocomprobante"));
                Movi.setSerie(rs.getString("Serie"));
                Movi.setCorrelativo(rs.getString("Correlativo"));
                Movi.setFechayhora(rs.getString("Fechayhora"));
                Movi.setTienda(rs.getString("Tienda"));
                Movi.setAlmacen(rs.getString("Almacen"));
                Movi.setIdmotivo(rs.getInt("Idmotivo"));
                Movi.setEstado(rs.getString("Estado"));
                list.add(Movi);
            }
        } catch (Exception e) {

        }
        return list;
    }

    //Lista de Nota ingreso estado  Entregado uso en VListarpedido.jsp
    public List ListadoMovimientoentregado() {
        ArrayList<Movimientos> list = new ArrayList<>();
        String sql = "SELECT * FROM Movimiento where Tipocomprobante='Nota Ingreso' and Estado='Entregado'";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Movimientos Movi = new Movimientos();
                Movi.setIdmovimiento(rs.getInt("Idmovimiento"));
                Movi.setIdproveedor(rs.getInt("Idproveedor"));
                Movi.setIdusuario(rs.getInt("Idusuario"));
                Movi.setTipocomprobante(rs.getString("Tipocomprobante"));
                Movi.setSerie(rs.getString("Serie"));
                Movi.setCorrelativo(rs.getString("Correlativo"));
                Movi.setFechayhora(rs.getString("Fechayhora"));
                Movi.setTienda(rs.getString("Tienda"));
                Movi.setAlmacen(rs.getString("Almacen"));
                Movi.setIdmotivo(rs.getInt("Idmotivo"));
                Movi.setEstado(rs.getString("Estado"));
                list.add(Movi);
            }
        } catch (Exception e) {

        }
        return list;
    }
    public List ListadoNotasalidaprocesando() {
        ArrayList<Movimientos> list = new ArrayList<>();
        String sql = "SELECT * FROM Movimiento where Tipocomprobante='NOTA SALIDA' and Estado='Procesando'";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Movimientos Movi = new Movimientos();
                Movi.setIdmovimiento(rs.getInt("Idmovimiento"));
                Movi.setIdproveedor(rs.getInt("Idproveedor"));
                Movi.setIdusuario(rs.getInt("Idusuario"));
                Movi.setTipocomprobante(rs.getString("Tipocomprobante"));
                Movi.setSerie(rs.getString("Serie"));
                Movi.setCorrelativo(rs.getString("Correlativo"));
                Movi.setFechayhora(rs.getString("Fechayhora"));
                Movi.setTienda(rs.getString("Tienda"));
                Movi.setAlmacen(rs.getString("Almacen"));
                Movi.setIdmotivo(rs.getInt("Idmotivo"));
                Movi.setEstado(rs.getString("Estado"));
                list.add(Movi);
            }
        } catch (Exception e) {

        }
        return list;
    }
    
    //Seleccionar Movimiento dependiendo id uso para reporte y referencia modulo venta
    public Movimientos Reporte(int id) {
        String sql = "select *from Movimiento where Idmovimiento=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {

                v.setIdmovimiento(rs.getInt("Idmovimiento"));
                v.setIdproveedor(rs.getInt("Idproveedor"));
                v.setIdusuario(rs.getInt("Idusuario"));
                v.setTipocomprobante(rs.getString("Tipocomprobante"));
                v.setSerie(rs.getString("Serie"));
                v.setCorrelativo(rs.getString("Correlativo"));
                v.setFechayhora(rs.getString("Fechayhora"));
                v.setTienda(rs.getString("Tienda"));
                v.setAlmacen(rs.getString("Almacen"));
                v.setIdmotivo(rs.getInt("Idmotivo"));
                v.setEstado(rs.getString("Estado"));
            }
        } catch (Exception e) {
        }
        return v;
    }

    public boolean EliminarDetalle(int id) {
        String sql = "delete from Detallemovimiento where Iddetallemovimiento=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return false;
    }

    public int BuscarNpedido() {
        String sSQL = "SELECT COUNT(*) as Npedido FROM Movimiento WHERE tipocomprobante ='Pedido'";

        try {
            int Nfactura = 0;
            con = cn.getConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            while (rs.next()) {
                Nfactura = rs.getInt("Npedido");
            }
            return Nfactura;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return 0;
        }
    }

    public int usu(String usu, String pass) {
        String sql = "SELECT id FROM usuario WHERE usu ='" + usu + "' AND password='" + pass + "'";

        try {
            int Nfactura = 0;
            con = cn.getConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Nfactura = rs.getInt("id");
            }

            return Nfactura;

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return 0;
        }

    }

    public Movimientos getVenta(int id) {
        Movimientos com = null;
        try {
            ResultSet res;
            PreparedStatement stmt = this.con.prepareStatement("call getMovimiento(?)");
            stmt.setInt(1, id);
            res = stmt.executeQuery();
            while (res.next()) {
                com = new Movimientos();
                com.setIdmovimiento(rs.getInt("Idmovimiento"));
                com.setIdproveedor(rs.getInt("Idproveedor"));
                com.setIdusuario(rs.getInt("Idusuario"));
                com.setTipocomprobante(rs.getString("Tipocomprobante"));
                com.setSerie(rs.getString("Serie"));
                com.setCorrelativo(rs.getString("Correlativo"));
                com.setFechayhora(rs.getString("Fechayhora"));
                com.setTienda(rs.getString("Tienda"));
                com.setAlmacen(rs.getString("Almacen"));
                com.setIdmotivo(rs.getInt("Idmotivo"));
            }
            res.close();

        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return com;
    }

    //Contar la cantidad de Nota Salida uso en AControllergestionarnotasalida
    public int BuscarNsalida() {
        String sSQL = "SELECT COUNT(*) as Nsalida FROM Movimiento WHERE tipocomprobante ='NOTA SALIDA'";

        try {
            int Nfactura = 0;
            con = cn.getConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            while (rs.next()) {
                Nfactura = rs.getInt("Nsalida");
            }
            return Nfactura;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return 0;
        }
    }
    
    //Contar la cantidad de Nota Ingreso uso en VControllerGestionarnotaingresotienda
    public int BuscarNnotaingreso() {
        String sql = "SELECT COUNT(*) as Nnotaingreso FROM Movimiento WHERE Tipocomprobante ='Nota Ingreso'";
        try {
            int Nfactura = 0;
            con = cn.getConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Nfactura = rs.getInt("Nnotaingreso");
            }
            return Nfactura;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return 0;
        }
    } 

}
