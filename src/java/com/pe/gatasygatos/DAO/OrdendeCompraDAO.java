/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pe.gatasygatos.DAO;
import com.pe.gatasygatos.conection.ConexionBD;
import com.pe.gatasygatos.model.entity.Producto;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author yenny
 */
public class OrdendeCompraDAO {

    ConexionBD conexion = new ConexionBD();
    String mensaje = "";
    PreparedStatement pst = null;
    CallableStatement call = null;
    ResultSet rs = null;
    Connection conn = null;
    

   

    public String Numserieorden() {

        String sql = "{call sp_generar_serieordencompra()}";

        try {
            conn = conexion.getConnection();
            call = conn.prepareCall(sql);
            rs = call.executeQuery();

            if (rs.next()) {
                mensaje = rs.getString(1);

            }
        } catch (SQLException e) {
            mensaje = e.getMessage();
        } finally {
            conexion.desconectar();
        }
        return mensaje;
    }

    public static String getNombreunidad(int id) {
        try {
            String sql = "SELECT Nombre from UnidadMedidaCompra where Iducompra="+id;

            Connection connection = ConexionBD.ConectarDB();
            PreparedStatement pre = connection.prepareStatement(sql);
            ResultSet rst = pre.executeQuery();
            if (rst.next()) {
                return rst.getString("Nombre");
            }
            return "--";
        } catch (Exception e) {
            return "--";
        }

    }
}
