package com.pe.gatasygatos.DAO;

import com.pe.gatasygatos.Interfaz.CRUDProducto;
import com.pe.gatasygatos.conection.ConexionBD;
import com.pe.gatasygatos.model.entity.Producto;
import com.pe.gatasygatos.model.entity.Productoxalmacen;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProductoDAO implements CRUDProducto {
    ConexionBD cn = new ConexionBD();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    Producto p = new Producto();
    CallableStatement call = null;
    String mensaje = "";

    public Producto BuscarPorId(int idProducto) {
        Producto pro = null;
        String sql = "select * from Producto where Idproducto = ?";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setInt(1, idProducto);
            rs = ps.executeQuery();
            if (rs.next()) {
                pro = new Producto();
                pro.setIdproducto(rs.getInt("Idproducto"));
                pro.setCodigo(rs.getString("Codigo"));
                pro.setDescripcion(rs.getString("Descripcion"));
                pro.setIdcategoria(rs.getInt("Idcategoria"));
                pro.setIdmarca(rs.getInt("Idmarca"));
                pro.setIdProveedor(rs.getInt("IdProveedor"));
                pro.setIducompra(rs.getInt("Iducompra"));
                pro.setIduventa(rs.getInt("Iduventa"));
                pro.setIdpublico(rs.getInt("Idpublico"));
                pro.setPreciocompra(rs.getDouble("Preciocompra"));
                pro.setPrecioVenta(rs.getDouble("Precioventa"));
                pro.setFechaRegistro(rs.getString("Fecharegistro"));
                pro.setEstado(rs.getString("Estado"));
            }

        } catch (SQLException e) {
            mensaje = e.getMessage();
        } finally {
            cn.desconectar();
        }
        return pro;
    }

    public static String getProducto(int cod) {

        try {
            String sql = "select Descripcion from Producto where Idproducto=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("Descripcion");

            }
            return "--";

        } catch (Exception e) {
            return "--";
        }
    }
    public static String getCodProd(int cod) {

        try {
            String sql = "select Codigo from Producto where Idproducto=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("Codigo");

            }
            return "--";

        } catch (Exception e) {
            return "--";
        }
    }

    public static int getProductoprecio(int cod) {
        int mensaje = 0;
        try {
            String sql = "select Precioventa from Producto where Idproducto=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("Precioventa");

            }
            return mensaje;

        } catch (Exception e) {
            return mensaje;
        }
    }
    

    public List ListadoStockMinimo() {
        ArrayList<Producto> list = new ArrayList<>();
        String sql = "select * from Producto where Stocktienda<=Stockminimo AND Estado='Activo'";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Producto prod = new Producto();
                prod.setIdproducto(rs.getInt("Idproducto"));
                prod.setCodigo(rs.getString("Codigo"));
                list.add(prod);
            }
        } catch (Exception e) {

        }
        return list;
    }

    public List ListadoStockmaximoyActivo() {
        ArrayList<Producto> list = new ArrayList<>();
        String sql = "select * from Producto where Stocktienda>10 AND Estado='Activo'";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Producto pro = new Producto();
                pro.setIdproducto(rs.getInt("Idproducto"));
                pro.setCodigo(rs.getString("Codigo"));
                pro.setDescripcion(rs.getString("Descripcion"));
                pro.setIdcategoria(rs.getInt("Idcategoria"));
                pro.setIdProveedor(rs.getInt("IdProveedor"));
                pro.setIducompra(rs.getInt("Iducompra"));
                pro.setIduventa(rs.getInt("Iduventa"));
                pro.setIdpublico(rs.getInt("Publico"));
                pro.setPreciocompra(rs.getDouble("Preciocompra"));
                pro.setPrecioVenta(rs.getDouble("Precioventa"));
                pro.setFechaRegistro(rs.getString("Fecharegistro"));
                pro.setEstado(rs.getString("Estado"));

                list.add(pro);
            }
        } catch (Exception e) {

        }
        return list;
    }
//Listado de producto en JSP Producto
    @Override
    public List ListadoProducto() {
        ArrayList<Producto> list = new ArrayList<>();
        String sql = "select * from Producto ORDER BY Idproducto desc";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Producto pro = new Producto();
                pro.setIdproducto(rs.getInt("Idproducto"));
                pro.setCodigo(rs.getString("Codigo"));
                pro.setDescripcion(rs.getString("Descripcion"));
                pro.setIdcategoria(rs.getInt("Idcategoria"));
                pro.setIdmarca(rs.getInt("Idmarca"));
                pro.setIdProveedor(rs.getInt("IdProveedor"));
                pro.setIducompra(rs.getInt("Iducompra"));
                pro.setIduventa(rs.getInt("Iduventa"));
                pro.setIdpublico(rs.getInt("Idpublico"));
                pro.setPreciocompra(rs.getDouble("Preciocompra"));
                pro.setPrecioVenta(rs.getDouble("Precioventa"));
                pro.setFechaRegistro(rs.getString("Fecharegistro"));
                pro.setEstado(rs.getString("Estado"));

                list.add(pro);
            }

        } catch (Exception e) {

        }
        return list;
    }

    public List ListadoEstadoActivo() {
        ArrayList<Producto> list = new ArrayList<>();
        String sql = "select * from Producto where Estado='Activo'";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Producto prod = new Producto();
                prod.setIdproducto(rs.getInt("Idproducto"));
                prod.setCodigo(rs.getString("Codigo"));
                prod.setDescripcion(rs.getString("Descripcion"));
                prod.setPrecioVenta(rs.getDouble("PrecioVenta"));
                prod.setEstado(rs.getString("Estado"));

                list.add(prod);
            }

        } catch (Exception e) {

        }
        return list;
    }

    @Override
    public Producto list(int id) {

        String sql = "select * from Producto where Idproducto=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {

                p.setIdproducto(rs.getInt("Idproducto"));
                p.setCodigo(rs.getString("Codigo"));
                p.setDescripcion(rs.getString("Descripcion"));
                p.setIdcategoria(rs.getInt("Idcategoria"));
                p.setIdProveedor(rs.getInt("IdProveedor"));
                p.setIducompra(rs.getInt("Iducompra"));
                p.setIduventa(rs.getInt("Iduventa"));
                p.setIdpublico(rs.getInt("Idpublico"));
                p.setPreciocompra(rs.getDouble("Preciocompra"));
                p.setPrecioVenta(rs.getDouble("Precioventa"));
                p.setFechaRegistro(rs.getString("Fecharegistro"));

            }

        } catch (Exception e) {

        }
        return p;
    }

    @Override
    public boolean add(Producto prod) {
        boolean flag = false;
        String sql = "insert into producto(Codigo,Descripcion,Idcategoria,Idmarca,IdProveedor,Iducompra,Iduventa,Idpublico,Preciocompra,Precioventa,Fecharegistro,Estado)"
                + "values('" + prod.getCodigo() + "','" + prod.getDescripcion() + "','" + prod.getIdcategoria() + "','" + prod.getIdmarca() + "','" + prod.getIdProveedor() + "','" + prod.getIducompra() + "','" + prod.getIduventa() +"','" + prod.getIdpublico() + "','" + prod.getPreciocompra() + "','" + prod.getPrecioVenta() + "','" + prod.getFechaRegistro() + "','" + prod.getEstado() + "')";

        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            if (ps.executeUpdate() == 1) {
                flag = true;
            }

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return flag;
    }

    @Override
    public boolean Edit(Producto p) {
        boolean flag = false;
        String sql = "UPDATE Producto set Codigo='" + p.getCodigo() + "',Descripcion='" + p.getDescripcion() + "',Idcategoria='" + p.getIdcategoria() + "',Idmarca='" + p.getIdmarca() + "',IdProveedor='" + p.getIdProveedor() + "',Iducompra='" + p.getIducompra() + "',Iduventa='" + p.getIduventa()+ "',Idpublico='" + p.getIdpublico() + "',Preciocompra='" + p.getPreciocompra() + "',Precioventa='" + p.getPrecioVenta() + "',Fecharegistro='" + p.getFechaRegistro()+"'WHERE Idproducto=" + p.getIdproducto();
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            if (ps.executeUpdate() == 1) {
                flag = true;
            }

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return flag;
    }

    public boolean editEstado(Producto prod, int id) {
        String sql = "update Producto set Estado='" + prod.getEstado() + "' where Idproducto=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return false;
    }

    public static String estado(int cod) {

        try {
            String sql = "select Estado from Producto where Idproducto=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("estado");

            }
            return "--";

        } catch (Exception e) {
            return "--";
        }
    }

    @Override
    public boolean Eliminar(int id) {
        String sql = "delete from Producto where Idproducto=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return false;
    }

    public static Producto obtenerProducto(int Id) {
        Producto pro = null;
        try {
            CallableStatement cl = ConexionBD.Conectar().prepareCall("{CALL sp_listaporid(?)}");
            cl.setInt(1, Id);
            ResultSet rs = cl.executeQuery();
            while (rs.next()) {
                pro = new Producto(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getInt(5), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getInt(9),rs.getDouble(10), rs.getDouble(11),rs.getString(12), rs.getString(13));

            }
        } catch (Exception e) {
        }
        return pro;
    }

    public static ArrayList<Producto> obtenerProductop(int num) {
        ArrayList<Producto> lista = new ArrayList<Producto>();
        try {
            CallableStatement cl = ConexionBD.ConectarDB().prepareCall("{CALL sp_Consultarproductoporproveedor(?)}");
            cl.setInt(1, num);
            ResultSet rs = cl.executeQuery();
            while (rs.next()) {
                Producto p = new Producto(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getInt(5), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getInt(9),rs.getDouble(10), rs.getDouble(11),rs.getString(12), rs.getString(13));
                lista.add(p);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return lista;
    }

    public String Numserie() {
        String sql = "{call sp_generar_codigoproducto()}";

        try {
            con = cn.getConnection();
            call = con.prepareCall(sql);
            rs = call.executeQuery();

            if (rs.next()) {
                mensaje = rs.getString(1);

            }
        } catch (SQLException e) {
        }
        return mensaje;

    }

    public List ListadoStockminimopxa() {
        ArrayList<Productoxalmacen> list = new ArrayList<>();
        String sql = "select * from Almacenxproducto where Stock<=Stockminimo AND Idalmacen=1";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Productoxalmacen pro = new Productoxalmacen();
                pro.setIdalmacenxproducto(rs.getInt("Idalmacenxproducto"));
                pro.setIdalmacen(rs.getInt("Idalmacen"));
                pro.setIdproducto(rs.getInt("Idproducto"));
                pro.setStock(rs.getInt("Stock"));
                pro.setStockminimo(rs.getInt("Stockminimo"));
                pro.setEstado(rs.getString("Estado"));

                list.add(pro);
            }
        } catch (Exception e) {

        }
        return list;
    }
        public List Listadodealmacengeneral() {
        ArrayList<Productoxalmacen> list = new ArrayList<>();
        String sql = "select * from Almacenxproducto where Idalmacen=2";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Productoxalmacen pro = new Productoxalmacen();
                pro.setIdalmacenxproducto(rs.getInt("Idalmacenxproducto"));
                pro.setIdalmacen(rs.getInt("Idalmacen"));
                pro.setIdproducto(rs.getInt("Idproducto"));
                pro.setStock(rs.getInt("Stock"));
                pro.setStockminimo(rs.getInt("Stockminimo"));
                pro.setEstado(rs.getString("Estado"));

                list.add(pro);
            }
        } catch (Exception e) {

        }
        return list;
    }
         //Mostrar nombre de categoria en Producto.jsp
         public static String getNombreprovedor(int cod) {
        try {
            String sql="select Razonsocial from Proveedor where Idproveedor="+cod;
            Connection connection=ConexionBD.Conectar();
            PreparedStatement prepare=connection.prepareStatement(sql);
            ResultSet resultSet=prepare.executeQuery();
          if(resultSet.next()) {
            return resultSet.getString("Razonsocial");
            
            }
          return "--";
            
        } catch (Exception e) {
            return "--";
        }
    }
         //ESTOC DE PRODUCTOS ALMACEN GENRAL
      public List ListadoStockMinimoAlmacenP() {
        ArrayList<Productoxalmacen> list = new ArrayList<>();
        String sql = "select * from Almacenxproducto where Stock<=Stockminimo AND Idalmacen=2";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Productoxalmacen pro = new Productoxalmacen();
                pro.setIdalmacenxproducto(rs.getInt("Idalmacenxproducto"));
                pro.setIdalmacen(rs.getInt("Idalmacen"));
                pro.setIdproducto(rs.getInt("Idproducto"));
                pro.setStock(rs.getInt("Stock"));
                pro.setStockminimo(rs.getInt("Stockminimo"));
                pro.setEstado(rs.getString("Estado"));

                list.add(pro);
            }
        } catch (Exception e) {

        }
        return list;
    }
    
    public static void main(String[] args) {

        ProductoDAO mp = new ProductoDAO();
        List<Producto> lista = mp.ListadoProducto();
        System.out.println("Lista " + lista.size());

        lista.forEach(c -> {
            System.out.println(c.toString());

        });

        System.out.println(mp.add(new Producto(0, "loca", "fffff", 1, 1, 1, 1, 1, 1,50, 10, "2020-11-27", "Activo")));

    }

}
