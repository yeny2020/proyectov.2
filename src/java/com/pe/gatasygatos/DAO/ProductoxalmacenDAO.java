/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pe.gatasygatos.DAO;

import com.pe.gatasygatos.conection.ConexionBD;
import com.pe.gatasygatos.model.entity.Producto;
import com.pe.gatasygatos.model.entity.Productoxalmacen;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author yenny
 */
public class ProductoxalmacenDAO {

    ConexionBD cn = new ConexionBD();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    Productoxalmacen pa = new Productoxalmacen();
    CallableStatement call = null;
    String mensaje = "";

    public List ListadoEstadoActivo() {
        ArrayList<Productoxalmacen> list = new ArrayList<>();
        String sql = "select * from Almacenxproducto where Idalmacen=2";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Productoxalmacen prod = new Productoxalmacen();
                prod.setIdalmacenxproducto(rs.getInt("Idalmacenxproducto"));
                prod.setIdalmacen(rs.getInt("Idalmacen"));
                prod.setIdproducto(rs.getInt("Idproducto"));
                prod.setStock(rs.getInt("Stock"));
                prod.setStockminimo(rs.getInt("Stockminimo"));
                list.add(prod);
            }
        } catch (Exception e) {

        }
        return list;
    }

    public List ListadoporIdtienda() {
        ArrayList<Productoxalmacen> list = new ArrayList<>();
        String sql = "select * from Almacenxproducto p inner join Producto  pp where p.Idproducto=pp.Idproducto And p.Idalmacen=1 And pp.Estado='Activo'";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Productoxalmacen prod = new Productoxalmacen();
                prod.setIdalmacenxproducto(rs.getInt("Idalmacenxproducto"));
                prod.setIdalmacen(rs.getInt("Idalmacen"));
                prod.setIdproducto(rs.getInt("Idproducto"));
                prod.setStock(rs.getInt("Stock"));
                prod.setStockminimo(rs.getInt("Stockminimo"));
                list.add(prod);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List Listadodeproactivosentienda() {
        ArrayList<Productoxalmacen> list = new ArrayList<>();
        String sql = "select * from Almacenxproducto where Idalmacen=1 And Estado='Activo'";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Productoxalmacen prod = new Productoxalmacen();
                prod.setIdalmacenxproducto(rs.getInt("Idalmacenxproducto"));
                prod.setIdalmacen(rs.getInt("Idalmacen"));
                prod.setIdproducto(rs.getInt("Idproducto"));
                prod.setStock(rs.getInt("Stock"));
                prod.setStockminimo(rs.getInt("Stockminimo"));
                prod.setEstado(rs.getString("Estado"));
                list.add(prod);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public static String estado(int cod) {

        try {
            String sql = "select Estado from Almacenxproducto where Idalmacen=1 And Idalmacenxproducto=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("estado");

            }
            return "--";

        } catch (Exception e) {
            return "--";
        }
    }

    public Productoxalmacen listproductoxalmacen(int id) {

        String sql = "select * from Almacenxproducto where Idalmacenxproducto=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {

                pa.setIdalmacenxproducto(rs.getInt("Idalmacenxproducto"));
                pa.setIdalmacen(rs.getInt("Idalmacen"));
                pa.setIdproducto(rs.getInt("Idproducto"));
                pa.setStock(rs.getInt("Stock"));
                pa.setStockminimo(rs.getInt("Stockminimo"));
            }

        } catch (Exception e) {

        }
        return pa;
    }

    public static Productoxalmacen obtenerProductoxalmacen(int Id) {
        Productoxalmacen pro = null;
        try {
            CallableStatement cl = ConexionBD.Conectar().prepareCall("{CALL sp_listaporidproductoxalmacen(?)}");
            cl.setInt(1, Id);
            ResultSet rs = cl.executeQuery();
            while (rs.next()) {
                pro = new Productoxalmacen(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getInt(4), rs.getInt(5));
            }
        } catch (Exception e) {
        }
        return pro;
    }

    public static String getProductodescripcion(int cod) {
        try {
            String sql = "select Descripcion from Producto where Idproducto=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("Descripcion");

            }
            return "--";

        } catch (Exception e) {
            return "--";
        }
    }

    public static String getProductocodigo(int cod) {
        try {
            String sql = "select Codigo from Producto where Idproducto=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("Codigo");

            }
            return "--";

        } catch (Exception e) {
            return "--";
        }
    }
        public static String getUndVenta(int cod) {
        try {
            String sql = "Select Nombre from UnidadMedidaVenta u inner join Producto p where u.Iduventa=p.Iduventa And Idproducto=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("Nombre");

            }
            return "--";

        } catch (Exception e) {
            return "--";
        }
    }

    public static int getStockproducto(int cod) {
        int mensaje = 0;
        try {
            String sql = "select Stock from Almacenxproducto where Idalmacen=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("Precioventa");

            }
            return mensaje;

        } catch (Exception e) {
            return mensaje;
        }
    }

     //STOck DE PRODUCTOS ALMACEN GENRAL
     public static String estadoAlmacenP(int cod) {

        try {
            String sql = "select Estado from Almacenxproducto where Idalmacen=2 And Idalmacenxproducto=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("estado");

            }
            return "--";

        } catch (Exception e) {
            return "--";
        }
    }
     //Validadcion en Vinsertartienda no ingresar un stock mayor a lo que existe
         public static int getValidarstock(int cod) {
        int mensaje = 0;
        try {
            String sql = "Select Stock from almacenxproducto where Idalmacen=1 and Idproducto=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("Stock");

            }
            return mensaje;

        } catch (Exception e) {
            return mensaje;
        }
    }
}
