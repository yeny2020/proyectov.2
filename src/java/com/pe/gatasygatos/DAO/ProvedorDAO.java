/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pe.gatasygatos.DAO;

import com.pe.gatasygatos.conection.ConexionBD;
import com.pe.gatasygatos.model.entity.Provedor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.CallableStatement;

/**
 *
 * @author Angel Albinagorta
 */
public class ProvedorDAO {

    ConexionBD cn = new ConexionBD();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    String mensaje = "";
    Provedor prov = new Provedor();
    private CallableStatement call;

    public Provedor BuscarPorId(int idProveedor) {
        Provedor pv = null;
        String sql = "select * from Proveedor where Idproveedor = ?";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setInt(1, idProveedor);
            rs = ps.executeQuery();
            if (rs.next()) {
                pv = new Provedor();
                pv.setIdproveedor(rs.getInt("Idproveedor"));
                pv.setCodigo(rs.getString("Codigo"));
                pv.setRazonsocial(rs.getString("Razonsocial"));
                pv.setDireccion(rs.getString("Direccion"));
                pv.setCelular(rs.getInt("Celular"));
                pv.setEmail(rs.getString("Email"));
                pv.setIdtipodocumento(rs.getInt("Idtipodocumento"));
                pv.setNumdocumento(rs.getString("Numdocumento"));
                pv.setFechaderegistro(rs.getString("Fechaderegistro"));
            }

        } catch (SQLException e) {
            mensaje = e.getMessage();
        } finally {
            cn.desconectar();
        }
        return pv;
    }

    public List ListadoProveedor() {
        ArrayList<Provedor> list = new ArrayList<>();
        String sql = "SELECT * FROM Proveedor";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Provedor pv = new Provedor();
                pv.setIdproveedor(rs.getInt("Idproveedor"));
                pv.setCodigo(rs.getString("Codigo"));
                pv.setRazonsocial(rs.getString("Razonsocial"));
                pv.setDireccion(rs.getString("Direccion"));
                pv.setCelular(rs.getInt("Celular"));
                pv.setEmail(rs.getString("Email"));
                pv.setIdtipodocumento(rs.getInt("Idtipodocumento"));
                pv.setNumdocumento(rs.getString("Numdocumento"));
                pv.setFechaderegistro(rs.getString("Fechaderegistro"));
                pv.setEstado(rs.getString("Estado"));
                list.add(pv);
           }
        } catch (Exception e) {

        }
        return list;
    }

    public List ListadoEstadoActivos() {

        ArrayList<Provedor> list = new ArrayList<>();
        String sql = "select * from Proveedor where Estado='Activo'";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Provedor pv = new Provedor();
                pv.setIdproveedor(rs.getInt("Idproveedor"));
                pv.setRazonsocial(rs.getString("Codigo"));
                pv.setRazonsocial(rs.getString("Razonsocial"));
                pv.setDireccion(rs.getString("Direccion"));
                pv.setCelular(rs.getInt("Celular"));
                pv.setEmail(rs.getString("Email"));
                pv.setIdtipodocumento(rs.getInt("Idtipodocumento"));
                pv.setNumdocumento(rs.getString("Numdocumento"));
                pv.setFechaderegistro(rs.getString("Fechaderegistro"));
                list.add(pv);
            }

        } catch (SQLException e) {
            mensaje = e.getMessage();
        } finally {
            cn.desconectar();
        }
        return list;
    }

    public static String estado(int cod) {

        try {
            String sql = "select Estado from Proveedor where Idproveedor=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("Estado");

            }
            return "--";

        } catch (Exception e) {
            return "--";
        }
    }

    public static String getproveedor(int cod) {

        try {
            String sql = "select Razonsocial from Proveedor where Idproveedor=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("Nombre");

            }
            return "--";

        } catch (Exception e) {
            return "--";
        }
    }

    public static String getproveedorEstado(int cod) {

        try {
            String sql = "select Estado from Proveedor where Idproveedor=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("Estado");

            }
            return "--";

        } catch (Exception e) {
            return "--";
        }
    }

    public Provedor list(int id) {

        String sql = "select * from Proveedor where Idproveedor=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {

                prov.setIdproveedor(rs.getInt("Idproveedor"));
                prov.setCodigo(rs.getString("Codigo"));
                prov.setRazonsocial(rs.getString("Razonsocial"));
                prov.setDireccion(rs.getString("Direccion"));
                prov.setCelular(rs.getInt("Celular"));
                prov.setEmail(rs.getString("Email"));
                prov.setIdtipodocumento(rs.getInt("Idtipodocumento"));
                prov.setNumdocumento(rs.getString("Numdocumento"));
                prov.setFechaderegistro(rs.getString("Fechaderegistro"));

            }

        } catch (Exception e) {

        }
        return prov;
    }

    public boolean add(Provedor pro) {
        boolean flag = false;
        String sql = "insert into Proveedor(Codigo,Razonsocial,Direccion,Celular, Email, Idtipodocumento,Numdocumento,Fechaderegistro,Estado)values('" + pro.getCodigo() + "','" + pro.getRazonsocial() + "','" + pro.getDireccion() + "','" + pro.getCelular() + "','" + pro.getEmail() + "','" + pro.getIdtipodocumento() + "','" + pro.getNumdocumento() + "','" + pro.getFechaderegistro() + "','" + pro.getEstado() + "')";

        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            if (ps.executeUpdate() == 1) {
                flag = true;
            }

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return flag;
    }

    /*   public boolean Edit(Provedor pro) {
      String sql="update Proveedor set Nombre='"+cat.getNombre()+"'where Idmarca="+cat.getIdmarca();
        
        try {
            con=cn.getConnection();
            ps=con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return false;
    }*/
    public boolean Estado(Provedor pro, int id) {
        String sql = "update Proveedor set Estado='" + pro.getEstado() + "'where Idproveedor=" + id;

        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return false;
    }

    public boolean Eliminar(int id) {
        String sql = "delete from Proveedor where Idproveedor=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return false;
    }
    
   public boolean editEstado(Provedor p, int id) {
        String sql="update Proveedor set Estado='"+p.getEstado()+"'where Idproveedor="+id;
        
        try {
            con=cn.getConnection();
            ps=con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return false;
    }

    public boolean Edit(Provedor pro) {
        boolean flag = false;
        String sql = "UPDATE Proveedor set Codigo='" + pro.getCodigo() + "',Razonsocial='" + pro.getRazonsocial() + "',Direccion='" + pro.getDireccion() + "',Celular='" + pro.getCelular() + "',Email='" + pro.getEmail() + "',Idtipodocumento='" + pro.getIdtipodocumento() + "',Numdocumento='" + pro.getNumdocumento() + "',Fechaderegistro='" + pro.getFechaderegistro() + "'WHERE Idproveedor=" + pro.getIdproveedor();

        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            if (ps.executeUpdate() == 1) {
                flag = true;
            }

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return flag;
    }

    public String Numserie() {
        String sql = "{call sp_generar_codigoproveedor()}";

        try {
            con = cn.getConnection();
            call = con.prepareCall(sql);
            rs = call.executeQuery();

            if (rs.next()) {
                mensaje = rs.getString(1);

            }
        } catch (SQLException e) {
        }
        return mensaje;

    }

    public static String getProveedor(int cod) {

        try {
            String sql = "Select Razonsocial from Proveedor where Idproveedor=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("Razonsocial");

            }
            return "--";

        } catch (Exception e) {
            return "--";
        }
    }

    public static String getEmail(int cod) {

        try {
            String sql = "Select Email from Proveedor where Idproveedor=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("Email");

            }
            return "--";

        } catch (Exception e) {
            return "--";
        }

    }

    public static String getCelular(int cod) {

        try {
            String sql = "Select Celular from Proveedor where Idproveedor=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("Celular");

            }
            return "--";

        } catch (Exception e) {
            return "--";
        }
    }
        public static String getDireccion(int cod) {
        try {
            String sql = "Select Direccion from Proveedor where Idproveedor=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("Direccion");
            }
            return "--";
        } catch (Exception e) {
            return "--";
        }
    }
        
            public static String getCondicion(int cod) {
        try {
            String sql = "Select Condicion from Compra where Idcompra=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("Condicion");
            }
            return "--";
        } catch (Exception e) {
            return "--";
        }
    }
            public static String getRUC(int cod) {
        try {
            String sql = "Select Numdocumento from proveedor where Idproveedor=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("Numdocumento");
            }
            return "--";
        } catch (Exception e) {
            return "--";
        }
    }
            
            public static String getMotivo(int cod) {
        try {
            String sql = "Select Nombre from Motivo where Idmotivo=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("Nombre");
            }
            return "--";
        } catch (Exception e) {
            return "--";
        }
    }
          public static String getNumerodocumento(int cod) {
        try {
            String sql = "Select Numdocumento from Proveedor where Idproveedor=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("Numdocumento");
            }
            return "--";
        } catch (Exception e) {
            return "--";
        }
    }

}
