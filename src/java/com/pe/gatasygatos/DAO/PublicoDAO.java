
package com.pe.gatasygatos.DAO;

import com.pe.gatasygatos.conection.ConexionBD;
import com.pe.gatasygatos.model.entity.Publico;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PublicoDAO {
    ConexionBD cn = new ConexionBD();
    Connection con;
    String mensaje = "";
    PreparedStatement ps;
    ResultSet rs;
    Publico  pblc = new Publico();
    
   public List<Publico> listarPublico() {

        String sql = "SELECT * FROM Publico";

        List<Publico> lista = new ArrayList<>();

        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {
                Publico PL = new Publico();
                PL.setIdpublico(rs.getInt("Idpublico"));
                PL.setCodigo(rs.getInt("Codigo"));
                PL.setNombre(rs.getString("Nombre"));
                lista.add(PL);
             }

        } catch (Exception e) {

        }
        return lista;
    }
        //Mostrar nombre de Publico en Producto.jsp
         public static String getNombrePublico(int cod) {
        try {
            String sql="select Nombre from Publico where Idpublico="+cod;
            Connection connection=ConexionBD.Conectar();
            PreparedStatement prepare=connection.prepareStatement(sql);
            ResultSet resultSet=prepare.executeQuery();
          if(resultSet.next()) {
            return resultSet.getString("Nombre");
            
            }
          return "--";
            
        } catch (Exception e) {
            return "--";
        }
    }

} 
