
package com.pe.gatasygatos.DAO;

import com.pe.gatasygatos.Interfaz.CRUDUsuario;
import com.pe.gatasygatos.conection.ConexionBD;
import com.pe.gatasygatos.model.entity.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class UsuarioDAO implements CRUDUsuario{
    ConexionBD cn = new ConexionBD();
    PreparedStatement ps = null;
    ResultSet rs = null;
    Connection con = null;
    Usuario U=new Usuario();
    
  
    @Override
    public List ListadoUsuario() {
        ArrayList<Usuario> list=new ArrayList<>();
        String sql="select * from usuario";

   try {
            con=cn.getConnection();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()) {     
                
                Usuario usu=new Usuario();
                usu.setId(rs.getInt("id"));
                usu.setIdempleado(rs.getInt("idempleado"));
                usu.setUsu(rs.getString("usu"));
                usu.setPassword(rs.getString("password")); 
                usu.setRol(rs.getString("rol"));
                list.add(usu);
            }
        } catch (Exception e) {
            
        }
        return list;
    
    }

    @Override
    public Usuario list(int id) {
      ArrayList<Usuario> list=new ArrayList<>();
  String sql="select * from usuario where id="+id;
        
        try {
            con=cn.getConnection();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()) { 
                 
                U.setId(rs.getInt("id"));
                U.setIdempleado(rs.getInt("idempleado"));
                U.setUsu(rs.getString("usu"));
                U.setPassword(rs.getString("password")); 
                U.setRol(rs.getString("rol"));
                list.add(U);
            }
            
        } catch (Exception e) {
            
        }
        return U;
    }

    @Override
    public boolean add(Usuario usu) {
      
        String sql="INSERT INTO usuario(idempleado,usu,password,rol)"
               + "VALUES('"+usu.getIdempleado()+"','"+usu.getUsu()+"','"+usu.getPassword()+"','"+usu.getRol()+"')";
       
        try {
            con=cn.getConnection();
            ps=con.prepareStatement(sql);
            ps.executeUpdate();
            
        }catch (Exception e) {
        }
 
       return false;
    }

    @Override
    public boolean Edit(Usuario usu) {
          String sql="update usuario set idempleado='"+usu.getIdempleado()+"',usu='"+usu.getUsu()+"',password='"+usu.getPassword()+"',rol='"+usu.getRol()+"' where id="+usu.getId();
          try {
            con=cn.getConnection();
            ps=con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
       return false;
    }

    @Override
    public boolean Eliminar(int id) {
      String sql="delete from usuario where id="+id; 
        try {
            con=cn.getConnection();
            ps=con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
     return false;
    }
       public  int usu(String usu,String pass){
        String sql ="SELECT id FROM usuario WHERE usu ='"+usu+"' AND password='"+pass+"'";
        
        try {
            int Nfactura =0;
            con=cn.getConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
          while (rs.next()) {
            Nfactura = rs.getInt("id");
            }
         
          return Nfactura;
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return 0;
        }
         
    }
}
