
package com.pe.gatasygatos.DAO;
import com.pe.gatasygatos.conection.ConexionBD;
import com.pe.gatasygatos.model.entity.DetalleVentas;
import com.pe.gatasygatos.model.entity.Venta;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Angel Albinagorta
 */
public class VentaDAO {

    ConexionBD cn = new ConexionBD();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    Venta v = new Venta();

    public boolean insertarVenta(Venta varventa, ArrayList<DetalleVentas> d) {
        boolean rpta = false;
        try {

            con = cn.getConnection();
            CallableStatement cl = con.prepareCall("{call sp_RegistrarVenta(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            cl.registerOutParameter(1, Types.INTEGER);
            cl.setInt(2, varventa.getIdcliente());
            cl.setInt(3, varventa.getIdusuario());
            cl.setString(4, varventa.getTipocomprobante());
            cl.setString(5, varventa.getSerie());
            cl.setString(6, varventa.getCorrelativo());
            cl.setString(7, varventa.getFechayhora());
            cl.setString(8, varventa.getTienda());
            cl.setString(9, varventa.getAlmacen());
            cl.setString(10, varventa.getCondicion());
            cl.setDouble(11, varventa.getSubtotal());
            cl.setDouble(12, varventa.getIgv());
            cl.setDouble(13, varventa.getTotal());
            cl.setString(14, varventa.getEstado());
            if (cl.executeUpdate() == 2) {
                rpta = true;
            }
            varventa.setIdventa(cl.getInt(1));
            CallableStatement cl2 = con.prepareCall("{CALL newdetalleVenta(?,?,?)}");
            for (DetalleVentas aux : d) {
                cl2.setInt(1, varventa.getIdventa());
                cl2.setInt(2, aux.getIdproducto());
                cl2.setDouble(3, aux.getCantidad());
                if (cl2.executeUpdate() == 1) {
                    rpta = true;
                }

            }

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return rpta;
    }

    public static String CodigoVenta() {
        try {
            String sql = "select max(Idventa)from Venta";
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString(1);

            }
            return "--";

        } catch (Exception e) {
            return "--";
        }
    }
    public boolean editarEstado(Venta v, int id) {
        String sql = " update Venta set Estado='" + v.getEstado() + "' where Idventa=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (SQLException e) {
        }
        return false;

    }

    public static String estado(int cod) {

        try {
            String sql = "select Estado from Venta where Idventa=" + cod;
            Connection connection = ConexionBD.Conectar();
            PreparedStatement prepare = connection.prepareStatement(sql);
            ResultSet resultSet = prepare.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("Estado");

            }
            return "--";

        } catch (Exception e) {
            return "--";
        }
    }

    public List ListadoDetalle() {
        ArrayList<DetalleVentas> list = new ArrayList<>();
        String sql = "SELECT * FROM Detalleventa WHERE Idventa=(SELECT MAX(Idventa) from Venta)";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                DetalleVentas detalle_venta = new DetalleVentas();
                detalle_venta.setIddetalleventa(rs.getInt("Iddetalleventa"));
                detalle_venta.setIdventa(rs.getInt("Idventa"));
                detalle_venta.setIdproducto(rs.getInt("Idproducto"));
                detalle_venta.setCantidad(rs.getInt("Cantidad"));
                list.add(detalle_venta);
            }

        } catch (Exception e) {

        }
        return list;
    }

    public List ticketDetalle(int id) {
        ArrayList<DetalleVentas> list = new ArrayList<>();
        String sql = "SELECT * FROM Detalleventa WHERE Idventa=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                DetalleVentas detalle = new DetalleVentas();
                detalle.setIddetalleventa(rs.getInt("Iddetalle"));
                detalle.setIdventa(rs.getInt("Idventa"));
                detalle.setIdproducto(rs.getInt("Idproducto"));
                detalle.setCantidad(rs.getInt("Cantidad"));
                list.add(detalle);
            }

        } catch (Exception e) {

        }
        return list;
    }

    public boolean editaranulardetalle(DetalleVentas v, int id) {
        String sql = " update Detalleventa set Estado='" + v.getEstado() + "' where Iddetalle=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (SQLException e) {
        }
        return false;

    }

    public boolean editarestadodetalle_venta(DetalleVentas v, int id) {
        String sql = " update Detalleventa set Estado='" + v.getEstado() + "' where Idventa=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (SQLException e) {
        }
        return false;

    }

    public List ListadoventaBoleta() {
        ArrayList<Venta> list = new ArrayList<>();
        String sql = "SELECT * FROM Venta where Tipocomprobante='BOLETA'";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Venta venta = new Venta();
                venta.setIdventa(rs.getInt("Idventa"));
                venta.setIdcliente(rs.getInt("Idcliente"));
                venta.setIdusuario(rs.getInt("Idusuario"));
                venta.setTipocomprobante(rs.getString("Tipocomprobante"));
                venta.setSerie(rs.getString("Serie"));
                venta.setCorrelativo(rs.getString("Correlativo"));
                venta.setFechayhora(rs.getString("Fechayhora"));
                venta.setTienda(rs.getString("Tienda"));
                venta.setAlmacen(rs.getString("Almacen"));
                venta.setCondicion(rs.getString("Condicion"));
                venta.setSubtotal(rs.getDouble("Subtotal"));
                venta.setIgv(rs.getDouble("Igv"));
                venta.setTotal(rs.getDouble("Total"));
                venta.setEstado(rs.getString("Estado"));
                list.add(venta);
            }
        } catch (Exception e) {

        }
        return list;
    }
    
    public List ListadoventaFactura() {
        ArrayList<Venta> list = new ArrayList<>();
        String sql = "SELECT * FROM Venta where Tipocomprobante='FACTURA'";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Venta venta = new Venta();
                venta.setIdventa(rs.getInt("Idventa"));
                venta.setIdcliente(rs.getInt("Idcliente"));
                venta.setIdusuario(rs.getInt("Idusuario"));
                venta.setTipocomprobante(rs.getString("Tipocomprobante"));
                venta.setSerie(rs.getString("Serie"));
                venta.setCorrelativo(rs.getString("Correlativo"));
                venta.setFechayhora(rs.getString("Fechayhora"));
                venta.setTienda(rs.getString("Tienda"));
                venta.setAlmacen(rs.getString("Almacen"));
                venta.setCondicion(rs.getString("Condicion"));
                venta.setSubtotal(rs.getDouble("Subtotal"));
                venta.setIgv(rs.getDouble("Igv"));
                venta.setTotal(rs.getDouble("Total"));
                venta.setEstado(rs.getString("Estado"));
                list.add(venta);
            }
        } catch (Exception e) {

        }
        return list;
    }

    public Venta Reporte(int id) {
        String sql = "select *from Venta where Idventa=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {

                v.setIdventa(rs.getInt("Idventa"));
                v.setIdcliente(rs.getInt("Idcliente"));
                v.setIdusuario(rs.getInt("Idusuario"));
                v.setTipocomprobante(rs.getString("Tipocomprobante"));
                v.setSerie(rs.getString("Serie"));
                v.setCorrelativo(rs.getString("Correlativo"));
                v.setFechayhora(rs.getString("Fechayhora"));
                v.setTienda(rs.getString("Tienda"));
                v.setAlmacen(rs.getString("Almacen"));
                v.setCondicion(rs.getString("Condicion"));
                v.setSubtotal(rs.getDouble("Subtotal"));
                v.setIgv(rs.getDouble("Igv"));
                v.setTotal(rs.getDouble("Total"));
                v.setEstado(rs.getString("Estado"));

            }

        } catch (Exception e) {

        }
        return v;

    }

    public boolean EliminarDetalle(int id) {
        String sql = "delete from Detalleventa where Iddetalleventa=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return false;
    }

    public int BuscarNfacturas() {
        String sSQL = "SELECT COUNT(*) as Nfactura FROM venta WHERE tipocomprobante ='Factura'";

        try {
            int Nfactura = 0;
            con = cn.getConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            while (rs.next()) {
                Nfactura = rs.getInt("Nfactura");
            }

            return Nfactura;

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return 0;
        }

    }
        public int SerieNfacturas() {
        String sSQL = "SELECT COUNT(*) as Nfactura FROM venta WHERE tipocomprobante ='Factura'";

        try {
            int Nfactura = 0;
            con = cn.getConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sSQL);
            while (rs.next()) {
                Nfactura = rs.getInt("Nfactura");
            }

            return Nfactura;

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return 0;
        }

    }

    public int BuscarNBoleta() {
        String sql = "SELECT COUNT(*) as Nboleta FROM Venta WHERE Tipocomprobante ='Boleta'";

        try {
            int Nfactura = 0;
            con = cn.getConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Nfactura = rs.getInt("Nboleta");
            }

            return Nfactura;

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return 0;
        }

    }
      public int SerieNBoleta() {
        String sql = "SELECT COUNT(*) as Nboleta FROM Venta WHERE Tipocomprobante ='Boleta'";

        try {
            int Nfactura = 0;
            con = cn.getConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Nfactura = rs.getInt("Nboleta");
            }

            return Nfactura;

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return 0;
        }

    }

    public int usu(String usu, String pass) {
        String sql = "SELECT id FROM usuario WHERE usu ='" + usu + "' AND password='" + pass + "'";

        try {
            int Nfactura = 0;
            con = cn.getConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Nfactura = rs.getInt("id");
            }

            return Nfactura;

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return 0;
        }

    }

    public Venta getVenta(int id) {
        Venta com = null;
        try {
            ResultSet res;
            PreparedStatement stmt = this.con.prepareStatement("call getVentas(?)");
            stmt.setInt(1, id);
            res = stmt.executeQuery();
            while (res.next()) {
                com = new Venta();
                com.setIdventa(rs.getInt("Idventa"));
                com.setIdcliente(rs.getInt("Idcliente"));
                com.setIdusuario(rs.getInt("Idusuario"));
                com.setTipocomprobante(rs.getString("Tipocomprobante"));
                com.setSerie(rs.getString("Serie"));
                com.setCorrelativo(rs.getString("Correlativo"));
                com.setFechayhora(rs.getString("Fechayhora"));
                com.setTienda(rs.getString("Tienda"));
                com.setAlmacen(rs.getString("Almacen"));
                com.setCondicion(rs.getString("Condicion"));
                com.setSubtotal(rs.getDouble("Subtotal"));
                com.setIgv(rs.getDouble("Igv"));
                com.setTotal(rs.getDouble("Total"));
            }
            res.close();

        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return com;
    }
}
