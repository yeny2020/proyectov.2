/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pe.gatasygatos.Interfaz;

import com.pe.gatasygatos.model.entity.Empleado;
import java.util.List;

/**
 *
 * @author Angel Albinagorta
 */
public interface CRUDEmpleado {
     public List ListadoEmpleado();
    public Empleado list(int id);
    public boolean add(Empleado pri);
    public boolean Edit(Empleado pri);
    public boolean Eliminar(int id);
}
