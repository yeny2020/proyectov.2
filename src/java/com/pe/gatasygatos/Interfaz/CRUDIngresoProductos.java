/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pe.gatasygatos.Interfaz;


import com.pe.gatasygatos.model.entity.IngresoProductos;
import java.util.List;

/**
 *
 * @author Angel Albinagorta
 */
public interface CRUDIngresoProductos {
    public List Listado();
    public boolean add(IngresoProductos pro); 
    
    public IngresoProductos list(int id);
    public boolean Edit(IngresoProductos pro);
    public boolean Eliminar(int id);
}
