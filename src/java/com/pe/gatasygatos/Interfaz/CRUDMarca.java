
package com.pe.gatasygatos.Interfaz;



import com.pe.gatasygatos.model.entity.Marca;
import java.util.List;


public interface CRUDMarca {
    public List ListadoMarca();
    public Marca list(int id);
    public boolean add(Marca  mar);
    public boolean Edit(Marca  mar);
    public boolean Eliminar(int id);
}
