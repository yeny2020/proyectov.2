
package com.pe.gatasygatos.Interfaz;



import com.pe.gatasygatos.model.entity.Almacen;
import java.util.List;


public interface CRUDMaterial {
    public List ListadoMaterial();
    public Almacen list(int id);
    public boolean add(Almacen mat);
    public boolean Edit(Almacen mat);
    public boolean Eliminar(int id);
}
