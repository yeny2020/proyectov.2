package com.pe.gatasygatos.model.entity;

/**
 *
 * @author yenny
 */
public class Almacen {

private Integer Idalmacen;
private String Codigo;
private String nombre;
private String Estado;

    public Almacen() {
    }

    public Almacen(Integer Idalmacen, String Codigo, String nombre, String Estado) {
        this.Idalmacen = Idalmacen;
        this.Codigo = Codigo;
        this.nombre = nombre;
        this.Estado = Estado;
    }

    public Almacen(String Codigo, String nombre, String Estado) {
        this.Codigo = Codigo;
        this.nombre = nombre;
        this.Estado = Estado;
    }

    public Integer getIdalmacen() {
        return Idalmacen;
    }

    public void setIdalmacen(Integer Idalmacen) {
        this.Idalmacen = Idalmacen;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

}
