package com.pe.gatasygatos.model.entity;

public class Cliente {
    
private int   Idcliente;
private String  Codigo;
private String Cliente;
private String Correo;
private int Celular;
private String Direccion;
private String Fechaderegistro;
private int Idtipodocumento;
private String Numerodocumento;
private String Sexo;
private String Estado;

public Cliente(){
}

    public Cliente(int Idcliente, String Codigo, String Cliente, String Correo, int Celular, String Direccion, String Fechaderegistro, int Idtipodocumento, String Numerodocumento, String Sexo, String Estado) {
        this.Idcliente = Idcliente;
        this.Codigo = Codigo;
        this.Cliente = Cliente;
        this.Correo = Correo;
        this.Celular = Celular;
        this.Direccion = Direccion;
        this.Fechaderegistro = Fechaderegistro;
        this.Idtipodocumento = Idtipodocumento;
        this.Numerodocumento = Numerodocumento;
        this.Sexo = Sexo;
        this.Estado = Estado;
    }

    public Cliente(String Codigo, String Cliente, String Correo, int Celular, String Direccion, String Fechaderegistro, int Idtipodocumento, String Numerodocumento, String Sexo, String Estado) {
        this.Codigo = Codigo;
        this.Cliente = Cliente;
        this.Correo = Correo;
        this.Celular = Celular;
        this.Direccion = Direccion;
        this.Fechaderegistro = Fechaderegistro;
        this.Idtipodocumento = Idtipodocumento;
        this.Numerodocumento = Numerodocumento;
        this.Sexo = Sexo;
        this.Estado = Estado;
    }



    public int getIdcliente() {
        return Idcliente;
    }

    public void setIdcliente(int Idcliente) {
        this.Idcliente = Idcliente;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }

    public String getCliente() {
        return Cliente;
    }

    public void setCliente(String Cliente) {
        this.Cliente = Cliente;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String Correo) {
        this.Correo = Correo;
    }

    public int getCelular() {
        return Celular;
    }

    public void setCelular(int Celular) {
        this.Celular = Celular;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }

    public String getFechaderegistro() {
        return Fechaderegistro;
    }

    public void setFechaderegistro(String Fechaderegistro) {
        this.Fechaderegistro = Fechaderegistro;
    }

    public int getIdtipodocumento() {
        return Idtipodocumento;
    }

    public void setIdtipodocumento(int Idtipodocumento) {
        this.Idtipodocumento = Idtipodocumento;
    }

    public String getNumerodocumento() {
        return Numerodocumento;
    }

    public void setNumerodocumento(String Numerodocumento) {
        this.Numerodocumento = Numerodocumento;
    }

    public String getSexo() {
        return Sexo;
    }

    public void setSexo(String Sexo) {
        this.Sexo = Sexo;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

  
}