/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pe.gatasygatos.model.entity;

/**
 *
 * @author yenny
 */
public class Compra {

    private int Idcompra;
    private int Idproveedor;
    private int Idusuario;
    private String Tipocomprobante;
    private String Serie;
    private String Correlativo;
    private String FechaYhora;
    private String Tienda;
    private String Condicion;
    private String Almacen;
    private int Idmotivo;
        private double Subtotal;
    private double Igv;
    private double Total;
    private String estado;

    public Compra() {
    }
    

    public Compra(int Idcompra, int Idproveedor, int Idusuario, String Tipocomprobante, String Serie, String Correlativo, String FechaYhora, String Tienda, String Condicion, String Almacen, int Idmotivo, String estado) {
        this.Idcompra = Idcompra;
        this.Idproveedor = Idproveedor;
        this.Idusuario = Idusuario;
        this.Tipocomprobante = Tipocomprobante;
        this.Serie = Serie;
        this.Correlativo = Correlativo;
        this.FechaYhora = FechaYhora;
        this.Tienda = Tienda;
        this.Condicion = Condicion;
        this.Almacen = Almacen;
        this.Idmotivo = Idmotivo;
        this.estado = estado;
    }
    public int getIdcompra() {
        return Idcompra;
    }

    public void setIdcompra(int Idcompra) {
        this.Idcompra = Idcompra;
    }

    public int getIdproveedor() {
        return Idproveedor;
    }

    public void setIdproveedor(int Idproveedor) {
        this.Idproveedor = Idproveedor;
    }

    public int getIdusuario() {
        return Idusuario;
    }

    public void setIdusuario(int Idusuario) {
        this.Idusuario = Idusuario;
    }

    public String getTipocomprobante() {
        return Tipocomprobante;
    }

    public void setTipocomprobante(String Tipocomprobante) {
        this.Tipocomprobante = Tipocomprobante;
    }

    public String getSerie() {
        return Serie;
    }

    public void setSerie(String Serie) {
        this.Serie = Serie;
    }

    public String getCorrelativo() {
        return Correlativo;
    }

    public void setCorrelativo(String Correlativo) {
        this.Correlativo = Correlativo;
    }

    public String getFechaYhora() {
        return FechaYhora;
    }

    public void setFechaYhora(String FechaYhora) {
        this.FechaYhora = FechaYhora;
    }

    public String getTienda() {
        return Tienda;
    }

    public void setTienda(String Tienda) {
        this.Tienda = Tienda;
    }

    public String getCondicion() {
        return Condicion;
    }

    public void setCondicion(String Condicion) {
        this.Condicion = Condicion;
    }

    public String getAlmacen() {
        return Almacen;
    }

    public void setAlmacen(String Almacen) {
        this.Almacen = Almacen;
    }

    public int getIdmotivo() {
        return Idmotivo;
    }

    public void setIdmotivo(int Idmotivo) {
        this.Idmotivo = Idmotivo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public double getSubtotal() {
        return Subtotal;
    }

    public void setSubtotal(double Subtotal) {
        this.Subtotal = Subtotal;
    }

    public double getIgv() {
        return Igv;
    }

    public void setIgv(double Igv) {
        this.Igv = Igv;
    }

    public double getTotal() {
        return Total;
    }

    public void setTotal(double Total) {
        this.Total = Total;
    }


}
