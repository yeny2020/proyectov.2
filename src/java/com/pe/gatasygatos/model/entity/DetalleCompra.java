package com.pe.gatasygatos.model.entity;

public class DetalleCompra {

    private int Iddetallecompra;
    private int Idcompra;
    private int Idproducto;
    private double Costo;
    private int Cantidad;
    private int Contenido;
    private double Estado;
    private Compra compra;
    private Producto producto;
    private UnidadCompra unidadcompra;


 
    public DetalleCompra() {
    }

    public DetalleCompra(int Iddetallecompra, int Idcompra, int Idproducto, double Costo, int Cantidad, int Contenido, double Estado) {
        this.Iddetallecompra = Iddetallecompra;
        this.Idcompra = Idcompra;
        this.Idproducto = Idproducto;
        this.Costo = Costo;
        this.Cantidad = Cantidad;
        this.Contenido = Contenido;
        this.Estado = Estado;
    }

    public DetalleCompra(int Iddetallecompra, int Idcompra, int Idproducto, Double Costo, int Cantidad, int Contenido, double Estado, Compra compra, Producto producto, UnidadCompra unidadcompra) {
        this.Iddetallecompra = Iddetallecompra;
        this.Idcompra = Idcompra;
        this.Idproducto = Idproducto;
        this.Costo = Costo;
        this.Cantidad = Cantidad;
        this.Contenido = Contenido;
        this.Estado = Estado;
        this.compra = compra;
        this.producto = producto;
        this.unidadcompra = unidadcompra;
    }

  
    

    public double subtotal() {
        return (Cantidad * Costo);
    }

    public double igv() {
        return subtotal() * 0.18;
    }

    public int getContenido() {
        return Contenido;
    }

    public void setContenido(int Contenido) {
        this.Contenido = Contenido;
    }

   

    public int getIddetallecompra() {
        return Iddetallecompra;
    }

    public void setIddetallecompra(int Iddetallecompra) {
        this.Iddetallecompra = Iddetallecompra;
    }

    public int getIdcompra() {
        return Idcompra;
    }

    public void setIdcompra(int Idcompra) {
        this.Idcompra = Idcompra;
    }

    public int getIdproducto() {
        return Idproducto;
    }

    public void setIdproducto(int Idproducto) {
        this.Idproducto = Idproducto;
    }

    public double getCosto() {
        return Costo;
    }

    public void setCosto(double Costo) {
        this.Costo = Costo;
    }

  
    public int getCantidad() {
        return Cantidad;
    }

    public void setCantidad(int Cantidad) {
        this.Cantidad = Cantidad;
    }

    public double getEstado() {
        return Estado;
    }

    public void setEstado(double Estado) {
        this.Estado = Estado;
    }
    public Compra getCompra() {
        return compra;
    }

    public void setCompra(Compra compra) {
        this.compra = compra;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public UnidadCompra getUnidadcompra() {
        return unidadcompra;
    }

    public void setUnidadcompra(UnidadCompra unidadcompra) {
        this.unidadcompra = unidadcompra;
    }

    public void setEstado(String cotizacion) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

  
}
