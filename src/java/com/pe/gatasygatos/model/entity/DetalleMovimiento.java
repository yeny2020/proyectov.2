/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pe.gatasygatos.model.entity;

/**
 *
 * @author yenny
 */
public class DetalleMovimiento {

    private int Iddetallemovimiento;
    private int Idmovimiento;
    private int Idproducto;
    private int Cantidad;
    private String Estado;
    private Movimientos Movimiento;
    private Producto producto;

    public DetalleMovimiento() {
    }

    public int getIddetallemovimiento() {
        return Iddetallemovimiento;
    }

    public void setIddetallemovimiento(int Iddetallemovimiento) {
        this.Iddetallemovimiento = Iddetallemovimiento;
    }

    public int getIdmovimiento() {
        return Idmovimiento;
    }

    public void setIdmovimiento(int Idmovimiento) {
        this.Idmovimiento = Idmovimiento;
    }

    public int getIdproducto() {
        return Idproducto;
    }

    public void setIdproducto(int Idproducto) {
        this.Idproducto = Idproducto;
    }

    public int getCantidad() {
        return Cantidad;
    }

    public void setCantidad(int Cantidad) {
        this.Cantidad = Cantidad;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    public Movimientos getMovimiento() {
        return Movimiento;
    }

    public void setMovimiento(Movimientos Movimiento) {
        this.Movimiento = Movimiento;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

}
