/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pe.gatasygatos.model.entity;

public class DetalleVentas {
        private int Iddetalleventa;
        private int Idventa;
	private int Idproducto;
	private int Cantidad;
        private String Estado;
        private Venta venta;
        private Producto producto;
        private Productoxalmacen productoxalmacen;
        
    public DetalleVentas() {
    }
     public double subtotal(){
     return (Cantidad*producto.getPrecioVenta());
    }

    public DetalleVentas(int Iddetalleventa, int Idventa, int Idproducto, int Cantidad) {
        this.Iddetalleventa = Iddetalleventa;
        this.Idventa = Idventa;
        this.Idproducto = Idproducto;
        this.Cantidad = Cantidad;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }
    
    public int getIddetalleventa() {
        return Iddetalleventa;
    }

    public void setIddetalleventa(int Iddetalleventa) {
        this.Iddetalleventa = Iddetalleventa;
    }

    public int getIdventa() {
        return Idventa;
    }

    public void setIdventa(int Idventa) {
        this.Idventa = Idventa;
    }

    public int getIdproducto() {
        return Idproducto;
    }

    public void setIdproducto(int Idproducto) {
        this.Idproducto = Idproducto;
    }

    public int getCantidad() {
        return Cantidad;
    }

    public void setCantidad(int Cantidad) {
        this.Cantidad = Cantidad;
    }
    public Venta getVenta() {
        return venta;
    }

    public void setVenta(Venta venta) {
        this.venta = venta;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Productoxalmacen getProductoxalmacen() {
        return productoxalmacen;
    }

    public void setProductoxalmacen(Productoxalmacen productoxalmacen) {
        this.productoxalmacen = productoxalmacen;
    }

  
    
}
