
package com.pe.gatasygatos.model.entity;

public class  Marca {
    
    int Idmarca;
    String Codigo,Nombre,Estado;

    public Marca(int Idmarca, String Codigo, String Nombre, String Estado) {
        this.Idmarca = Idmarca;
        this.Codigo = Codigo;
        this.Nombre = Nombre;
        this.Estado = Estado;
    }

    public Marca(String Codigo, String Nombre, String Estado) {
        this.Codigo = Codigo;
        this.Nombre = Nombre;
        this.Estado = Estado;
    }

    public Marca() {
    }

    public int getIdmarca() {
        return Idmarca;
    }

    public void setIdmarca(int Idmarca) {
        this.Idmarca = Idmarca;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

   
}
