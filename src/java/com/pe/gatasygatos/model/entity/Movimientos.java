package com.pe.gatasygatos.model.entity;
import java.sql.Timestamp;
/**
 *
 * @author yenny
 */
public class Movimientos {
    private int Idmovimiento;
    private int Idproveedor;
    private int Idusuario;
    private String Tipocomprobante;
    private String Serie;
    private String Correlativo;
    private String Fechayhora;
    private String Tienda;
    private String Almacen;
    private int Idmotivo;
    private String Estado;
    private Cliente cliente;

    public Movimientos() {
    }

    public Movimientos(int Idproveedor, int Idusuario, String Tipocomprobante, String Serie, String Correlativo, String Fechayhora, String Tienda, String Almacen, int Idmotivo, String Estado) {
        this.Idproveedor = Idproveedor;
        this.Idusuario = Idusuario;
        this.Tipocomprobante = Tipocomprobante;
        this.Serie = Serie;
        this.Correlativo = Correlativo;
        this.Fechayhora = Fechayhora;
        this.Tienda = Tienda;
        this.Almacen = Almacen;
        this.Idmotivo = Idmotivo;
        this.Estado = Estado;
    }

    public Movimientos(int Idmovimiento, int Idproveedor, int Idusuario, String Tipocomprobante, String Serie, String Correlativo, String Fechayhora, String Tienda, String Almacen, int Idmotivo, String Estado) {
        this.Idmovimiento = Idmovimiento;
        this.Idproveedor = Idproveedor;
        this.Idusuario = Idusuario;
        this.Tipocomprobante = Tipocomprobante;
        this.Serie = Serie;
        this.Correlativo = Correlativo;
        this.Fechayhora = Fechayhora;
        this.Tienda = Tienda;
        this.Almacen = Almacen;
        this.Idmotivo = Idmotivo;
        this.Estado = Estado;
    }

    public int getIdmovimiento() {
        return Idmovimiento;
    }

    public void setIdmovimiento(int Idmovimiento) {
        this.Idmovimiento = Idmovimiento;
    }

    public int getIdproveedor() {
        return Idproveedor;
    }

    public void setIdproveedor(int Idproveedor) {
        this.Idproveedor = Idproveedor;
    }


    public int getIdusuario() {
        return Idusuario;
    }

    public void setIdusuario(int Idusuario) {
        this.Idusuario = Idusuario;
    }

    public String getTipocomprobante() {
        return Tipocomprobante;
    }

    public void setTipocomprobante(String Tipocomprobante) {
        this.Tipocomprobante = Tipocomprobante;
    }

    public String getSerie() {
        return Serie;
    }

    public void setSerie(String Serie) {
        this.Serie = Serie;
    }

    public String getCorrelativo() {
        return Correlativo;
    }

    public void setCorrelativo(String Correlativo) {
        this.Correlativo = Correlativo;
    }

    public String getFechayhora() {
        return Fechayhora;
    }

    public void setFechayhora(String Fechayhora) {
        this.Fechayhora = Fechayhora;
    }

    public String getTienda() {
        return Tienda;
    }

    public void setTienda(String Tienda) {
        this.Tienda = Tienda;
    }

    public String getAlmacen() {
        return Almacen;
    }

    public void setAlmacen(String Almacen) {
        this.Almacen = Almacen;
    }

  

    public int getIdmotivo() {
        return Idmotivo;
    }

    public void setIdmotivo(int Idmotivo) {
        this.Idmotivo = Idmotivo;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

   
}
