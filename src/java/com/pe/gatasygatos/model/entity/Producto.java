
package com.pe.gatasygatos.model.entity;

public class Producto {
    private int Idproducto;
    private String Codigo;
    private String Descripcion;
    private int Idcategoria;
    private int Idmarca;
    private int IdProveedor; 
    private int Iducompra;
    private int Iduventa;
    private int Idpublico;
    private double Preciocompra;
    private double PrecioVenta;
    private String fechaRegistro;
    private String Estado;
    private Provedor proveedor;
    private Productoxalmacen productoxalmacen;

    
    
    public Producto() {
    }

    public Producto(int Idproducto) {
        this.Idproducto = Idproducto;
    }

    public Producto(int Idproducto, String Codigo, String Descripcion, int Idcategoria, int Idmarca, int IdProveedor, int Iducompra, int Iduventa, int Idpublico, double Preciocompra, double PrecioVenta, String fechaRegistro, String Estado) {
        this.Idproducto = Idproducto;
        this.Codigo = Codigo;
        this.Descripcion = Descripcion;
        this.Idcategoria = Idcategoria;
        this.Idmarca = Idmarca;
        this.IdProveedor = IdProveedor;
        this.Iducompra = Iducompra;
        this.Iduventa = Iduventa;
        this.Idpublico = Idpublico;
        this.Preciocompra = Preciocompra;
        this.PrecioVenta = PrecioVenta;
        this.fechaRegistro = fechaRegistro;
        this.Estado = Estado;
    }


    public int getIdpublico() {
        return Idpublico;
    }

    public void setIdpublico(int Idpublico) {
        this.Idpublico = Idpublico;
    }


    public int getIdproducto() {
        return Idproducto;
    }

    public void setIdproducto(int Idproducto) {
        this.Idproducto = Idproducto;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public int getIdcategoria() {
        return Idcategoria;
    }

    public void setIdcategoria(int Idcategoria) {
        this.Idcategoria = Idcategoria;
    }

    public int getIdmarca() {
        return Idmarca;
    }

    public void setIdmarca(int Idmarca) {
        this.Idmarca = Idmarca;
    }

 
    public int getIdProveedor() {
        return IdProveedor;
    }

    public void setIdProveedor(int IdProveedor) {
        this.IdProveedor = IdProveedor;
    }

    public int getIducompra() {
        return Iducompra;
    }

    public void setIducompra(int Iducompra) {
        this.Iducompra = Iducompra;
    }

    public int getIduventa() {
        return Iduventa;
    }

    public void setIduventa(int Iduventa) {
        this.Iduventa = Iduventa;
    }

    public double getPreciocompra() {
        return Preciocompra;
    }

    public void setPreciocompra(double Preciocompra) {
        this.Preciocompra = Preciocompra;
    }

    public double getPrecioVenta() {
        return PrecioVenta;
    }

    public void setPrecioVenta(double PrecioVenta) {
        this.PrecioVenta = PrecioVenta;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    public Provedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Provedor proveedor) {
        this.proveedor = proveedor;
    }

    public Productoxalmacen getProductoxalmacen() {
        return productoxalmacen;
    }

    public void setProductoxalmacen(Productoxalmacen productoxalmacen) {
        this.productoxalmacen = productoxalmacen;
    }
    

}
