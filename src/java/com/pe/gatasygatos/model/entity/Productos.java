
package com.pe.gatasygatos.model.entity;

import java.sql.Date;

public class Productos {
    
private Integer IdProducto;
private String Nombre;
private String Sexo; 
private String fecha;
private Integer edad;
private String Estado;
private String Ruta;

    public Productos() {
    }

    public Productos(Integer IdProducto, String Nombre, String Sexo, String fecha, Integer edad, String Estado, String Ruta) {
        this.IdProducto = IdProducto;
        this.Nombre = Nombre;
        this.Sexo = Sexo;
        this.fecha = fecha;
        this.edad = edad;
        this.Estado = Estado;
        this.Ruta = Ruta;
    }

    public Integer getIdProducto() {
        return IdProducto;
    }

    public void setIdProducto(Integer IdProducto) {
        this.IdProducto = IdProducto;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getSexo() {
        return Sexo;
    }

    public void setSexo(String Sexo) {
        this.Sexo = Sexo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    public String getRuta() {
        return Ruta;
    }

    public void setRuta(String Ruta) {
        this.Ruta = Ruta;
    }

   
}
