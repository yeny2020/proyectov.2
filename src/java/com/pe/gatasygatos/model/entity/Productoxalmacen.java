
package com.pe.gatasygatos.model.entity;

public class Productoxalmacen {
private int Idalmacenxproducto;
private int  Idalmacen;
private int Idproducto; 
private int  Stock; 
private int  Stockminimo; 
private String Estado;
private Producto producto;
private Almacen almacen;

 public Productoxalmacen(int Idalmacenxproducto) {
        this.Idalmacenxproducto = Idalmacenxproducto;
    }

    public Productoxalmacen() {
    }

    public Productoxalmacen(int Idalmacenxproducto, int Idalmacen, int Idproducto, int Stock, int Stockminimo, Producto producto, Almacen almacen) {
        this.Idalmacenxproducto = Idalmacenxproducto;
        this.Idalmacen = Idalmacen;
        this.Idproducto = Idproducto;
        this.Stock = Stock;
        this.Stockminimo = Stockminimo;
        this.producto = producto;
        this.almacen = almacen;
    }

    public Productoxalmacen(int Idalmacen, int Idproducto, int Stock, int Stockminimo, Producto producto, Almacen almacen) {
        this.Idalmacen = Idalmacen;
        this.Idproducto = Idproducto;
        this.Stock = Stock;
        this.Stockminimo = Stockminimo;
        this.producto = producto;
        this.almacen = almacen;
    }

    public Productoxalmacen(int Idalmacenxproducto, int Idalmacen, int Idproducto, int Stock, int Stockminimo) {
        this.Idalmacenxproducto = Idalmacenxproducto;
        this.Idalmacen = Idalmacen;
        this.Idproducto = Idproducto;
        this.Stock = Stock;
        this.Stockminimo = Stockminimo;
    }

    public Productoxalmacen(int Idalmacenxproducto, Producto producto, Almacen almacen) {
        this.Idalmacenxproducto = Idalmacenxproducto;
        this.producto = producto;
        this.almacen = almacen;
    }

    public int getIdalmacenxproducto() {
        return Idalmacenxproducto;
    }

    public void setIdalmacenxproducto(int Idalmacenxproducto) {
        this.Idalmacenxproducto = Idalmacenxproducto;
    }

    public int getIdalmacen() {
        return Idalmacen;
    }

    public void setIdalmacen(int Idalmacen) {
        this.Idalmacen = Idalmacen;
    }

    public int getIdproducto() {
        return Idproducto;
    }

    public void setIdproducto(int Idproducto) {
        this.Idproducto = Idproducto;
    }

    public int getStock() {
        return Stock;
    }

    public void setStock(int Stock) {
        this.Stock = Stock;
    }

    public int getStockminimo() {
        return Stockminimo;
    }

    public void setStockminimo(int Stockminimo) {
        this.Stockminimo = Stockminimo;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Almacen getAlmacen() {
        return almacen;
    }

    public void setAlmacen(Almacen almacen) {
        this.almacen = almacen;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

  
}
