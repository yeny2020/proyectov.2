/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pe.gatasygatos.model.entity;

/**
 *
 * @author yenny
 */
public class Provedor {

    private int Idproveedor;
    private String Codigo;
    private String Razonsocial;
    private String Direccion;
    private int Celular;
    private String Email;
    private int Idtipodocumento;
    private String Numdocumento;
    private String Fechaderegistro;
    private String Estado;

    public Provedor() {
    }

    public Provedor(int Idproveedor, String Codigo, String Razonsocial, String Direccion, int Celular, String Email, int Idtipodocumento, String Numdocumento, String Fechaderegistro, String Estado) {
        this.Idproveedor = Idproveedor;
        this.Codigo = Codigo;
        this.Razonsocial = Razonsocial;
        this.Direccion = Direccion;
        this.Celular = Celular;
        this.Email = Email;
        this.Idtipodocumento = Idtipodocumento;
        this.Numdocumento = Numdocumento;
        this.Fechaderegistro = Fechaderegistro;
        this.Estado = Estado;
    }

    public int getIdproveedor() {
        return Idproveedor;
    }

    public void setIdproveedor(int Idproveedor) {
        this.Idproveedor = Idproveedor;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }

    public String getRazonsocial() {
        return Razonsocial;
    }

    public void setRazonsocial(String Razonsocial) {
        this.Razonsocial = Razonsocial;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }

    public int getCelular() {
        return Celular;
    }

    public void setCelular(int Celular) {
        this.Celular = Celular;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public int getIdtipodocumento() {
        return Idtipodocumento;
    }

    public void setIdtipodocumento(int Idtipodocumento) {
        this.Idtipodocumento = Idtipodocumento;
    }

    public String getNumdocumento() {
        return Numdocumento;
    }

    public void setNumdocumento(String Numdocumento) {
        this.Numdocumento = Numdocumento;
    }

    public String getFechaderegistro() {
        return Fechaderegistro;
    }

    public void setFechaderegistro(String Fechaderegistro) {
        this.Fechaderegistro = Fechaderegistro;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

}
