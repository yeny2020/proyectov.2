
package com.pe.gatasygatos.model.entity;

public class Publico {
    private Integer Idpublico,Codigo;
    private String Nombre;

    public Publico(Integer Idpublico, Integer Codigo, String Nombre) {
        this.Idpublico = Idpublico;
        this.Codigo = Codigo;
        this.Nombre = Nombre;
    }

    public Publico() {
    }

    public Integer getIdpublico() {
        return Idpublico;
    }

    public void setIdpublico(Integer Idpublico) {
        this.Idpublico = Idpublico;
    }

    public Integer getCodigo() {
        return Codigo;
    }

    public void setCodigo(Integer Codigo) {
        this.Codigo = Codigo;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }
}
