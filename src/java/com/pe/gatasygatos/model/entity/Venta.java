package com.pe.gatasygatos.model.entity;

import java.sql.Timestamp;

/**
 *
 * @author yenny
 */
public class Venta {

    private int Idventa;
    private int Idcliente;
    private int Idusuario;
    private String Tipocomprobante;
    private String Serie;
    private String Correlativo;
    private String Fechayhora;
    private String Tienda;
    private String Almacen;
    private String Condicion;
    private double Subtotal;
    private double Igv;
    private double Total;
    private String Estado;

    private Cliente cliente;

    public Venta() {
    }

    public Venta(int Idventa, int Idcliente, int Idusuario, String Tipocomprobante, String Serie, String Correlativo, String Fechayhora, String Tienda, String Almacen, String Condicion, double Subtotal, double Igv, double Total, String Estado, Cliente cliente) {
        this.Idventa = Idventa;
        this.Idcliente = Idcliente;
        this.Idusuario = Idusuario;
        this.Tipocomprobante = Tipocomprobante;
        this.Serie = Serie;
        this.Correlativo = Correlativo;
        this.Fechayhora = Fechayhora;
        this.Tienda = Tienda;
        this.Almacen = Almacen;
        this.Condicion = Condicion;
        this.Subtotal = Subtotal;
        this.Igv = Igv;
        this.Total = Total;
        this.Estado = Estado;
        this.cliente = cliente;
    }

    public Venta(int Idventa, int Idcliente, int Idusuario, String Tipocomprobante, String Serie, String Correlativo, String Fechayhora, String Tienda, String Almacen, String Condicion, double Subtotal, double Igv, double Total, String Estado) {
        this.Idventa = Idventa;
        this.Idcliente = Idcliente;
        this.Idusuario = Idusuario;
        this.Tipocomprobante = Tipocomprobante;
        this.Serie = Serie;
        this.Correlativo = Correlativo;
        this.Fechayhora = Fechayhora;
        this.Tienda = Tienda;
        this.Almacen = Almacen;
        this.Condicion = Condicion;
        this.Subtotal = Subtotal;
        this.Igv = Igv;
        this.Total = Total;
        this.Estado = Estado;
    }



   


    public int getIdventa() {
        return Idventa;
    }

    public void setIdventa(int Idventa) {
        this.Idventa = Idventa;
    }

    public int getIdcliente() {
        return Idcliente;
    }

    public void setIdcliente(int Idcliente) {
        this.Idcliente = Idcliente;
    }

    public int getIdusuario() {
        return Idusuario;
    }

    public void setIdusuario(int Idusuario) {
        this.Idusuario = Idusuario;
    }

    public String getTipocomprobante() {
        return Tipocomprobante;
    }

    public void setTipocomprobante(String Tipocomprobante) {
        this.Tipocomprobante = Tipocomprobante;
    }

    public String getSerie() {
        return Serie;
    }

    public void setSerie(String Serie) {
        this.Serie = Serie;
    }

    public String getCorrelativo() {
        return Correlativo;
    }

    public void setCorrelativo(String Correlativo) {
        this.Correlativo = Correlativo;
    }

    public String getFechayhora() {
        return Fechayhora;
    }

    public void setFechayhora(String Fechayhora) {
        this.Fechayhora = Fechayhora;
    }

    public String getTienda() {
        return Tienda;
    }

    public void setTienda(String Tienda) {
        this.Tienda = Tienda;
    }

    public String getAlmacen() {
        return Almacen;
    }

    public void setAlmacen(String Almacen) {
        this.Almacen = Almacen;
    }
    public String getCondicion() {
        return Condicion;
    }
    public void setCondicion(String Condicion) {
        this.Condicion = Condicion;
    }
    public double getSubtotal() {
        return Subtotal;
    }

    public void setSubtotal(double Subtotal) {
        this.Subtotal = Subtotal;
    }

    public double getIgv() {
        return Igv;
    }

    public void setIgv(double Igv) {
        this.Igv = Igv;
    }

    public double getTotal() {
        return Total;
    }

    public void setTotal(double Total) {
        this.Total = Total;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }


}
