
<%@page import="com.pe.gatasygatos.model.entity.Empleado"%>
<%@page import="com.pe.gatasygatos.DAO.EmpleadoDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Usuario"%>
<%@page import="com.pe.gatasygatos.DAO.UsuarioDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Categoria</title>

        <%@include file="css-js.jsp" %>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Bootstrap CRUD Data Table for Database with Modal Form</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    </head>
    <body>
        <!--cabecera de Menu -->
        <%@include file="FrmAdmin.jsp" %>
        <!-- pageContent -->
        <section class="full-width pageContent">

            <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
                <!--modal listar-->
                <div class="mdl-tabs__panel is-active" id="tabListCategory">
                    <div class="mdl-grid">
                        <div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--7-col-desktop mdl-cell--2-offset-desktop">
                            <%UsuarioDAO dao = new UsuarioDAO();
                                int id = Integer.parseInt((String) request.getAttribute("idusu"));
                                Usuario p = (Usuario) dao.list(id);
                            %>

                            <div class="full-width panel mdl-shadow--2dp">
                                <form action="Usuariocontroller">
                                    <div class="modal-header">      
                                        <h4 class="modal-title">Editar Marca</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    </div>
                                    <div class="modal-body"> 
                                        <label for="validationDefault01">Seleciona Empleado</label>
                                        <select name="txtidempleado" class="form-control" required="">
                                            <option  value=""disabled="" selected="">Seleciona Empleado</option>
                                            <%   EmpleadoDAO edao = new EmpleadoDAO();
                                                List<Empleado> lista = edao.ListadoEmpleado();
                                                Iterator<Empleado> itera = lista.iterator();
                                                Empleado empl = null;
                                                while (itera.hasNext()) {
                                                    empl = itera.next();%>

                                            <option  value="<%= empl.getId()%>" ><%= empl.getNombre()%></option>
                                            <%}%>

                                        </select>
                                    </div>
                                    <div class="modal-body">
                                        <label>User name</label>
                                        <input name="txtusu" type="text" class="form-control" required>
                                    </div>
                                    <div class="modal-body">
                                        <label >Password</label>
                                        <input name="txtpassword" type="password" id="passwordAdmin" class="form-control" required>
                                    </div>

                                    <div class="modal-body">
                                        <label for="validationDefault01">Seleciona Rol</label>
                                        <select name="txtrol" class="form-control" required="">
                                            <option  value=""disabled="" selected="">Seleciona Rol</option>
                                            <option >Administrador</option>
                                            <option >Ventas</option>
                                            <option >Almacen</option>
                                            <option >Produccion</option>
                                        </select>
                                    </div>
                                    <input type="hidden" name="txtid" value="<%=p.getId()%>">
                                    <div class="modal-footer">
                                        <a href="Admi_Usuario.jsp" class="btn btn-default" >Volver</a>

                                        <input type="submit" name="accion" value="actualizar" class="btn btn-success"/>                      
                                    </div>
                                </form>


                            </div>

                        </div>
                    </div>
                </div>                                                             
            </div>
        </section>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>                                

    </body>



    <script type="text/javascript">
        $(document).ready(function () {
            $('#Datatable').DataTable();
        });



        function sendData(c) {
            var parametros = {"action": "editar", 'id': c};
            $.ajax({
                url: 'editarServlet',
                method: 'POST',
                data: parametros,

                success: function (response) {
                    var resp = $.parseJSON(response);
                    console.log(resp);
                }
            })


        }
    </script>

</html>
