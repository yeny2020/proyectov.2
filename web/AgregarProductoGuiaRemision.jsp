

<%@page import="com.pe.gatasygatos.DAO.UnidadCompraDAO"%>
<%@page import="com.pe.gatasygatos.DAO.MarcaDAO"%>
<%@page import="com.pe.gatasygatos.DAO.CategoriaDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Producto"%>
<%@page import="com.pe.gatasygatos.DAO.ProductoDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Categoria</title>
        <%@include file="css-js.jsp" %> 
        <!--ICONOS-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <!--ICONOS-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
        <!--Estilo tabla-->
        <link rel="stylesheet" href="EstiloAdministrador/css/Stilodetabla.css"> 
        <link href="EstiloAdministrador/css/STYLE_TABLE.css" rel="stylesheet" type="text/css"/>
        <!--Estilo tabla-->
    </head>
    <style>
        /* estado descactivado */
        markdesactivado{

            background-color: #ff1744;
            color: black;
            border-radius: 71px 69px 72px 71px;
            -moz-border-radius: 71px 69px 72px 71px;
            -webkit-border-radius: 71px 69px 72px 71px;
        }
        /* estado activo */
        markactivo{

            background-color: #00FF00;
            color: black;
            border-radius: 71px 69px 72px 71px;
            -moz-border-radius: 71px 69px 72px 71px;
            -webkit-border-radius: 71px 69px 72px 71px;
        }
    </style>

    <body>

        <!--cabecera de Menu -->
        <%@include file="FrmAdmin.jsp" %>

        <!-- pageContent -->
        <section class="full-width pageContent">

            <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
                <!--modal listar-->
                <div class="mdl-tabs__panel is-active" id="tabListCategory">
                    <div class="mdl-grid">
                        <div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--10-col-desktop mdl-cell--1-offset-desktop">
                            <div class="full-width panel mdl-shadow--2dp">
                                <div  style="background-color:#0a8cb3;color: #ffffff;" class="full-width panel-tittle text-center tittles">
                                    Agregar Item
                                </div>
                                <div class="full-width panel-content">                        
                                    <table id="Datatable" class="table table-bordered table-hover projects" >
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Agregar Item</th>
                                                <th>Codigo</th>
                                                <th>Descripcion</th>
                                                <th>Und</th>
                                                <th>Estado</th>
                                                <th>Opciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%ProductoDAO pdao = new ProductoDAO();
                                                List<Producto> listS = pdao.ListadoProducto();
                                                Iterator<Producto> iterr = listS.iterator();
                                                Producto pro = null;
                                                while (iterr.hasNext()) {
                                                    pro = iterr.next();%>

                                            <tr>
                                                <td><%= pro.getIdproducto()%></td>
                                                <td>
                                                    <a  href="ControllerdatosModal?accion=cantidadGuiaRemision&id=<%=pro.getIdproducto()%>"  data-toggle="modal" data-target="#myModalEdit" ><i class="material-icons" style="color: #09bb04" data-toggle="tooltip" title="Ver">&#xe147;</i></a>

                                                </td>
                                                <td ><%= pro.getCodigo()%></td>  
                                                <td ><%= pro.getDescripcion()%></td>
                                                <td><%= UnidadCompraDAO.getNombreUnidadcompra(pro.getIducompra())%></td>                                              
                                                <% String Estado = ProductoDAO.estado(pro.getIdproducto());
                                                    if (Estado.equalsIgnoreCase("Activo")) {%>

                                                <td><markactivo><%= Estado%></markactivo></td>   
                                                <%   } else {%>

                                        <td><markdesactivado><%= Estado%></markdesactivado></td>    
                                            <%     }
                                            %>



                                        <td>
                                            <a  href="Productodetalle.jsp?Id=<%=pro.getIdproducto()%>"><i class="material-icons" style="color: #0000FF" data-toggle="tooltip" title="Ver">&#xe8f4;</i></a>
                                        </td>
                                        </tr>
                                        <%}%>

                                        </tbody>
                                    </table>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>                                                             
            </div>
        </section>
        <!-- Delete Modal HTML -->
        <div id="deleteEmployeeModal" class="modal fade" >
            <div class="modal-dialog" role="document" style="z-index: 9999; width: 450px">
                <div class="modal-content">
                    <form>
                        <div class="modal-header">      
                            <h4 class="modal-title">Eliminar Categoria</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">     
                            <p>Seguro que quiere Eliminar?</p>
                            <!--    <p class="text-warning"><small>This action cannot be undone.</small></p>-->
                        </div>
                        <div class="modal-footer">
                            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                            <input type="submit" class="btn btn-danger" value="Delete">
                            <a href="ControllerCategoria?accion=eliminar&id=  " class="btn btn-danger"><i class="entypo-cancel"></i> Borrar  </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>    

        <!--Formulario de Modificar-->
        <div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document" style="z-index: 9999; width: 450px">
                <div class="modal-content">

                    <div class="modal-body">


                    </div>
                </div>                    
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>                                

    </body>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#Datatable').DataTable();
        });

        function sendData(c) {
            var parametros = {"action": "editar", 'id': c};
            $.ajax({
                url: '../Controller',
                method: 'POST',
                data: parametros,

                success: function (response) {
                    var resp = $.parseJSON(response);


                }
            })


        }
    </script>

</html>
