
<%@page import="com.pe.gatasygatos.model.entity.Productoxalmacen"%>
<%@page import="com.pe.gatasygatos.DAO.ProductoxalmacenDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Movimientos"%>
<%@page import="com.pe.gatasygatos.DAO.MovimientoDAO"%>
<%@page import="com.pe.gatasygatos.DAO.UnidadVentaDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.DetalleMovimiento"%>
<%@page import="com.pe.gatasygatos.DAO.AlmacenDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Almacen"%>
<%@page import="com.pe.gatasygatos.model.entity.Provedor"%>
<%@page import="com.pe.gatasygatos.DAO.ProvedorDAO"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.pe.gatasygatos.model.entity.Producto"%>
<%@page import="com.pe.gatasygatos.DAO.ProductoDAO"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Bootstrap CRUD Data Table for Database with Modal Form</title>
        <%@include file="css-js.jsp" %>
        <!--ICONOS-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <!--ICONOS-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
        <!--Estilo tabla-->
        <!--BOOSTRAP PARA DIV-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <!--BOOSTRAP PARA DIV-->
        <!--Estilo tabla-->
        <link rel="stylesheet" href="EstiloAdministrador/css/Stilodetabla.css"> 
        <!--Estilo tabla-->
        <%--Buscador select.---%>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
        <%-- finBuscador select.---%>

    </head>
    <style> 
        .dtHorizontalVerticalExampleWrapper {
            max-width: 600px;
            margin: 0 auto;
        }
        #dtHorizontalVerticalExample th, td {
            white-space: nowrap;
        }
        table.dataTable thead .sorting:after,
        table.dataTable thead .sorting:before,
        table.dataTable thead .sorting_asc:after,
        table.dataTable thead .sorting_asc:before,
        table.dataTable thead .sorting_asc_disabled:after,
        table.dataTable thead .sorting_asc_disabled:before,
        table.dataTable thead .sorting_desc:after,
        table.dataTable thead .sorting_desc:before,
        table.dataTable thead .sorting_desc_disabled:after,
        table.dataTable thead .sorting_desc_disabled:before {
            bottom: .10em;
        }
    </style>
    <body>
        <!--cabecera de Menu -->
        <%@include file="FrmAlmacen.jsp" %>
        <%    MovimientoDAO dao = new MovimientoDAO();

            int id = Integer.parseInt(request.getSession().getAttribute("NotaSa").toString());
            Movimientos p = (Movimientos) dao.Reporte(id);
        %>
        <!-- pageContent -->
        <section class="full-width pageContent">
            <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
                <!--modal listar-->
                <div class="mdl-tabs__panel is-active" id="tabListCategory">
                    <div class="mdl-grid">
                        <div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop mdl-cell--0-offset-desktop">
                            <div class="full-width panel mdl-shadow--2dp">
                                <div class="full-width panel-tittle  text-center tittles" style="background-color:#F6F200 ;">
                                    GENERAR NOTA DE SALIDA
                                </div>
                                <form id="newnsalida" method="post" name="accion" action="AControllergestionarnotasalida">
                                    <input type="hidden" name="accion" value="RegistrarNotadesalida" />
                                    <div class="d-flex">
                                        <div class="col-md-12 col-md-offset-0" style="color: #0035B0;">
                                            <div class="card">
                                                <div class="card-body  ">
                                                    <div class="d-flex">
                                                        <div class="col-md-12 col-md-offset-0" style="color: #0035B0;">
                                                            <div class="card">
                                                                <div class="card-body  "> 
                                                                    <div class="d-flex"> 
                                                                        <label class="col-sm-2" style="text-align:right">Proveedor:</label>
                                                                        <input type="hidden" name="txtIdpro" value="<%=p.getIdproveedor()%>"style="width:400px;height:20px;">
                                                                        <input type="text" name="txt" value="<%=ProvedorDAO.getProveedor(p.getIdproveedor())%>"  style="width:400px;height:20px;">
                                                                        <label class="col-sm-2" style="text-align:right">Documento:</label>
                                                                        <select name="txtTipomovi" id="tipo_comprobante" required="" style="width:400px;height:20px;">
                                                                            <option value="Nota Salida">NOTA SALIDA</option>
                                                                        </select>
                                                                    </div> 
                                                                    <div class="d-flex">
                                                                        <label class="col-sm-2" style="text-align:right">RUC :</label>
                                                                        <input type="text" name="" value="<%= ProvedorDAO.getNumerodocumento(p.getIdproveedor())%>"  style="width:400px;height:20px;">

                                                                        <label class="col-sm-2" style="text-align:right">Tienda:</label>
                                                                        <select name="txtTienda" id="tipo_comprobante" required="" style="width:400px;height:20px;">
                                                                            <option value="Gatas y Gatos">Gatas y Gatos</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="d-flex">
                                                                        <label class="col-sm-2" style="text-align:right">E-Mail:</label>
                                                                        <input type="text" name="txtTota" value="<%= ProvedorDAO.getEmail(p.getIdproveedor())%>"   style="width:400px;height:20px;">
                                                                        <label class="col-sm-2" style="text-align:right">Almacen:</label>
                                                                        <select name="txtAlmacen" id="tipo_comprobante" required="" style="width:400px;height:20px;">
                                                                            <% AlmacenDAO mat = new AlmacenDAO();
                                                                                List<Almacen> lis = mat.Almacengenral();
                                                                                Iterator<Almacen> it = lis.iterator();
                                                                                Almacen ma = null;
                                                                                while (it.hasNext()) {
                                                                                    ma = it.next();
                                                                            %>
                                                                            <option  value="<%=ma.getNombre()%>" required><%=ma.getNombre()%></option>
                                                                            <%
                                                                                }
                                                                            %>
                                                                        </select>                                                
                                                                    </div>
                                                                    <% Date dNow = new Date();
                                                                        SimpleDateFormat ft
                                                                                = new SimpleDateFormat("dd/MM/yyyy - hh:mm:ss");
                                                                        String currentDate = ft.format(dNow);
                                                                    %>
                                                                    <div class="d-flex">
                                                                        <label class="col-sm-2" style="text-align:right">Emision:</label>
                                                                        <input type="text" name="txtfecha" value="<%=currentDate%>"  style="width:400px;height:20px;">

                                                                        <label class="col-sm-2" style="text-align:right">Motivo:</label>
                                                                        <input type="text" name="txtIdmotivo" value=""  style="width:400px;height:20px;">
                                                                    </div>
                                                                    <div class="d-flex">
                                                                        <label class="col-sm-2" style="text-align:right">Direccion:</label>
                                                                        <input type="text" name="txtDireccion" value="<%= ProvedorDAO.getDireccion(p.getIdproveedor())%>"  style="width:400px;height:20px;">
                                                                        <label class="col-sm-2" style="text-align:right">Responsable:</label>
                                                                        <input type="text" name="txtIdusuario" value="<%=sesion.getAttribute("usuario")%>"  style="width:400px;height:20px;">
                                                                    </div>
                                                                    <div class="d-flex">
                                                                        <label class="col-sm-2" style="text-align:right">Referencia:</label>
                                                                        <input type="text" name="txteferencia" value="<%=p.getSerie()%>-<%=p.getCorrelativo()%>"  style="width:355px;height:20px;">
                                                                    </div>
                                                                    <div class="d-flex">
                                                                        <label class="col-sm-2" style="text-align:right">Referencia:</label>
                                                                        <input type="hidden" name="idmov" value="<%=p.getIdmovimiento()%>"  style="width:355px;height:20px;">
                                                                    </div>
                                                                </div>
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    <div class="d-flex">
                                                        <div class="col-sm-12">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <div class="form-group">                                                   
                                                                        <div class="table-responsive cart_info"  id="cart-container"> 
                                                                            <table id="dt" class="table table-striped table-bordered table-sm " cellspacing="0"
                                                                                   width="100%">
                                                                                <thead>
                                                                                    <tr style="background-color:#F6F200;color:white;">
                                                                                        <th>ID</th>
                                                                                        <th>DESCRIPCION</th>
                                                                                        <th>CANTIDAD</th>
                                                                                        <th>U.MEDIDA</th>
                                                                                        <th>OPCION</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>

                                                                                    <%
                                                                                        ArrayList<DetalleMovimiento> listS = (ArrayList<DetalleMovimiento>) request.getSession().getAttribute("NotaSalida");
                                                                                        Iterator<DetalleMovimiento> iterr = listS.iterator();
                                                                                        DetalleMovimiento pro = null;
                                                                                        int fila = 0;
                                                                                        while (iterr.hasNext()) {
                                                                                            pro = iterr.next();%>
                                                                                    <tr>
                                                                                        <td><%=pro.getIdproducto()%></td>
                                                                                        <td><%= ProductoDAO.getProducto(pro.getIdproducto())%></td>
                                                                                        <td> <input type="number" value="<%= pro.getCantidad()%>" value="1" min="1" max=""   id="txtPro_cantidad<%=fila%>" required="" onchange="actualizarcantidad(<%= fila%>)">
                                                                                        </td>
                                                                                        <td><%=ProductoxalmacenDAO.getUndVenta(pro.getIdproducto())%></td>
                                                                                        <td >
                                                                                            <span id="idarticulo" style="display:none;"><%=pro.getIdproducto()%></span>

                                                                                            <button style="background-color: transparent; color: red; border: none " id="deleteitem" class="delete">
                                                                                                <i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i>
                                                                                            </button>
                                                                                        </td>
                                                                                    </tr>

                                                                                    <%fila++;
                                                                                        }%>

                                                                                </tbody>

                                                                            </table>
                                                                        </div> 

                                                                    </div>
                                                                </div>
                                                            </div>                  
                                                        </div>                                                                                                                                             
                                                    </div>
                                                    <div class="d-flex">
                                                        <div class="col-md-12 col-md-offset-0" style="color: #0035B0;">
                                                            <div class="card">
                                                                <div class="card-body  ">
                                                                    <input class="btn btn-info" type="submit" value="Registrar" name="btnVenta" style="float:right"> 

                                                                    <a class="btn btn-info" href="Venta.jsp" role="button" style="float:right">Salir</a>
                                                                    <a href="#addpedidos" class="btn" style="background-color:#4e96b3;color: #FFFFFF" data-toggle="modal">Agregar Productos</a>
                                                        
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>                                                                                                      
                            </div>
                        </div>                                                             
                    </div>
                </div>
            </div>
        </section>  
        <!-- add Modal HTML -->
        <div id="addpedidos" class="modal fade" >
            <div class="modal-dialog" role="document" style="z-index: 10999; width:1000px">
                <div class="modal-content">

                    <form method="post" action="AControllergestionarnotasalida" accion="AnadirCarrito">
                        <div class="modal-header" >      
                            <h4 class="modal-title">Listado de Productos</h4>
                            <button type="button" class="close " data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>

                        <div class="modal-body">

                            <table id="Datatable" class="table table-bordered table-hover projects" >
                                <thead>
                                    <tr>
                                        <th>Agregar Item</th>
                                        <th>Codigo</th>
                                        <th>Nombre</th>
                                        <th>Stock Minimo</th>
                                        <th>Stock en tienda </th>
                                        <th>Almacen</th>
                                        <th>Estado</th>                                             
                                    </tr>
                                </thead>
                                <tbody>
                                    <%ProductoDAO pdaoStcok = new ProductoDAO();
                                        List<Productoxalmacen> listStock = pdaoStcok.Listadodealmacengeneral();
                                        Iterator<Productoxalmacen> iterstock = listStock.iterator();
                                        Productoxalmacen pstock = null;
                                                while (iterstock.hasNext()) {
                                                    pstock = iterstock.next();%>

                                    <tr>
                                        <td>
                                            <a  href="AControllergestionarnotasalida?accion=AnadirCarrito&Id=<%=pstock.getIdproducto()%>"><i class="material-icons" style="color: #09bb04" data-toggle="tooltip" title="Ver">&#xe147;</i></a>
                                        </td>
                                        <td><%=ProductoxalmacenDAO.getProductocodigo(pstock.getIdproducto())%></td>
                                        <td><%=ProductoxalmacenDAO.getProductodescripcion(pstock.getIdproducto())%></td>
                                        <td style="  background: red;"><%=pstock.getStockminimo()%></td>
                                        <td><%=pstock.getStock()%></td>  

                                        <td ><%= pstock.getIdalmacen()%></td>


                                    </tr>
                                    <%}%>

                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--Data table --->
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>                                
        <!--Data table --->
        <script src="EstiloAdministrador/funcionesyvalidaciones/AlmacenNotadesalida.js" type="text/javascript"></script>
        <script src="EstiloAdministrador/funcionesyvalidaciones/Datatable.js" type="text/javascript"></script>
    </body>

</html>
<script>

</script>
