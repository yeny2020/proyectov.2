<%@page import="com.pe.gatasygatos.DAO.ProvedorDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Movimientos"%>
<%@page import="com.pe.gatasygatos.DAO.MovimientoDAO"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<link href="EstiloAdministrador/css/ESTILO_TABLAS.css" rel="stylesheet" type="text/css"/>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Categoria</title>
        <%@include file="css-js.jsp" %> 
        <!--ICONOS-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <!--ICONOS-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
        <!--Estilo tabla-->
        <link rel="stylesheet" href="EstiloAdministrador/css/Stilodetabla.css"> 
        <link href="EstiloAdministrador/css/STYLE_TABLE.css" rel="stylesheet" type="text/css"/>
        <!--Estilo tabla-->
    </head>
    <style>
        /* estado descactivado */
        markpendiente{
            background-color: #00FF00;
            color: black;
            border-radius: 71px 69px 72px 71px;
            -moz-border-radius: 71px 69px 72px 71px;
            -webkit-border-radius: 71px 69px 72px 71px;
        }
        /* estado activo */
        markprocesando{
            background-color: #F6F200;
            color: black;
            border-radius: 71px 69px 72px 71px;
            -moz-border-radius: 71px 69px 72px 71px;
            -webkit-border-radius: 71px 69px 72px 71px;
        }
        /* estado descactivado */
        markentregado{
            background-color: #0000FF;
            color: black;
            border-radius: 71px 69px 72px 71px;
            -moz-border-radius: 71px 69px 72px 71px;
            -webkit-border-radius: 71px 69px 72px 71px;
        }
    </style>
    <body>
        <!--cabecera de Menu -->
        <%@include file="FrmAlmacen.jsp" %>

        <!-- pageContent -->
        <section class="full-width pageContent">

            <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
                <!--modal listar-->
                <div class="mdl-tabs__panel is-active" id="tabListCategory">
                    <div class="mdl-grid">
                        <div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop mdl-cell--0-offset-desktop">
                            <div class="full-width panel mdl-shadow--2dp">
                                <div  style="background-color: #007bb6;" class="full-width panel-tittle  text-center tittles">
                                    NOTA SALIDA
                                </div>
                                <div class="full-width panel-content">                        
                                    <table id="dtHorizontalVerticalExample" class="table table-striped table-bordered table-sm " cellspacing="0"
                                           width="100%">
                                        <thead >
                                            <tr>
                                                <th>N°</th>
                                                <th>Proveedor</th>
                                                <th>Documento</th>
                                                <th>Serie</th>
                                                <th>Correlativo</th>
                                                <th>Fecha</th>
                                                <th>Estado</th>
                                                <th>opcion</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%  MovimientoDAO pao = new MovimientoDAO();
                                                List<Movimientos> list = pao.ListadoNotasalidaprocesando();
                                                Iterator<Movimientos> iter = list.iterator();
                                                Movimientos mov = null;
                                                while (iter.hasNext()) {
                                                    mov = iter.next();%>
                                            <tr>
                                                <td><%= mov.getIdmovimiento()%></td>
                                                <td ><%= ProvedorDAO.getProveedor(mov.getIdproveedor())%></td>
                                                <td><%= mov.getTipocomprobante()%></td>
                                                <td><%= mov.getSerie()%></td>
                                                <td><%= mov.getCorrelativo()%></td>
                                                <td><%= mov.getFechayhora()%></td>
                                                <% String Estado = mov.getEstado();

                                                    if (Estado.equalsIgnoreCase("Pendiente")) {%>
                                                <td><markpendiente><%= Estado%></markpendiente></td>   
                                                <%   } else if (Estado.equalsIgnoreCase("Procesando")) {
                                                %>
                                        <td><markprocesando><%= Estado%></markprocesando></td>   
                                            <%
                                            } else {%>
                                        <td><markdesactivado><%= Estado%></markdesactivado></td>    
                                            <%     }
                                            %>
                                        <td>
                                            <a href="ControllerNotaingresotienda?accion=NotaI&id=<%= mov.getIdmovimiento()%>" class="btn btn-info" >PDF</a>
                                        </td>
                                        </tr>
                                        <%}%>
                                        </tbody>
                                    </table>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>                                                             
            </div>
        </section>
     <!--Data table --->
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>                                
        <!--Data table --->
        <!--Funciones y validaciones --->
        <script src="EstiloAdministrador/funcionesyvalidaciones/Datatable.js" type="text/javascript"></script>
        <!--Funciones y validaciones --->
    </body>

</html>
