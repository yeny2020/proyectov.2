
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bienvenido</title>
           <%@include file="css-js.jsp" %>
           <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
        <link href="EstiloAdministrador/css/EstilosCEstadisticos.css" rel="stylesheet" type="text/css"/>
        <link href="EstiloAdministrador/css/ICONOS_PRINCIPAL.css" rel="stylesheet" type="text/css"/>
    </head>
     <script>
    /**
     * Array con las imagenes y enlaces que se iran mostrando en la web
     */
    var imagenes=new Array(
       ['assets/img/img1.jpg'],
        ['assets/img/img2.jpg'],
        ['assets/img/img3.jpg'],
        ['assets/img/img4.jpg'],
        ['assets/img/img5.jpg']
    );
 
    /**
     * Funcion para cambiar la imagen y link
     */
    function rotarImagenes()
    {
        // obtenemos un numero aleatorio entre 0 y la cantidad de imagenes que hay
        var index=Math.floor((Math.random()*imagenes.length));
 
        // cambiamos la imagen y la url
        document.getElementById("imagen").src=imagenes[index][0];
    }
 
    /**
     * Función que se ejecuta una vez cargada la página
     */
    onload=function()
    {
        // Cargamos una imagen aleatoria
        rotarImagenes();
 
        // Indicamos que cada 5 segundos cambie la imagen
        setInterval(rotarImagenes,3000);
    }
</script>
    <body>
       <!--cabecera de Menu -->
       <%@include file="FrmAlmacen.jsp" %>
	
	<!-- pageContent -->
	<section class="full-width pageContent">
             <a href="" id="link"><img src="" id="imagen"></a>
		
	</section>
                                                                           
        <!--SEXION DE CANTIDAD DE CONTRNIDO  -->
        <section class="full-width pageContent" >
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <img class="estiloIconos" src="EstiloAdministrador/Imagen/employee__icon-icons.com_76984.png" alt="" />
                                    
                            <p class="pp">Clientes</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>

                        </div>
                        <a href="Cliente.jsp" class="small-box-footer">Más info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <img  class="estiloIconos1" src="EstiloAdministrador/Imagen/ventas.png" alt=""/>

                            <p class="pp">Documentos</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="Venta.jsp" class="small-box-footer">Más info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <img class="estiloIconos" src="EstiloAdministrador/Imagen/Productos.png" alt=""/>

                            <p class="pp">Registros de Productos</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="Producto.jsp" type="button" class="small-box-footer">Más info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <img class="estiloIconos" src="EstiloAdministrador/Imagen/Compras.png" alt=""/>

                            <p class="pp">Reportes</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="C_Compra.jsp" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
            </div>
    </body>
</html>
