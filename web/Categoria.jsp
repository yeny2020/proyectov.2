
<%@page import="com.pe.gatasygatos.DAO.CategoriaDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Categoria"%>
<%@page import="com.pe.gatasygatos.model.entity.Marca"%>
<%@page import="com.pe.gatasygatos.DAO.MarcaDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>CATEGORI</title>
        <%@include file="css-js.jsp" %> 
        <!--ICONOS-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <!--ICONOS-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
        <!--Estilo tabla-->
        <link rel="stylesheet" href="EstiloAdministrador/css/Stilodetabla.css">
        <link href="EstiloAdministrador/css/ColordeEstado.css" rel="stylesheet" type="text/css"/>
        <!--Estilo tabla-->
    </head>
    <body>
        <!--cabecera de Menu -->
        <%@include file="FrmAdmin.jsp" %>
        <!-- pageContent -->
        <section class="full-width pageContent">
            <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
                <!--modal listar-->
                <div class="mdl-tabs__panel is-active" id="tabListCategory">
                    <div class="mdl-grid">
                        <div class="mdl-cell mdl-cell--2-col-phone mdl-cell--8-col-tablet mdl-cell--6-col-desktop mdl-cell--2-offset-desktop">
                            <div class="full-width panel mdl-shadow--2dp">
                                <div class="full-width panel-tittle bg-info text-center tittles">
                                    LISTA CATEGORIA 
                                </div>
                                <div class="full-width panel-content">                        
                                    <div class="col-sm-6 mdl-textfield">   
                                        <a href="#addcategoria" class="btn " style="background-color:#4e96b3;color: #F7F7F5;" data-toggle="modal">Nuevo Categoria</a>
                                    </div>
                                    <div class="table-responsive">
                                        <table id="Datatable" class="table table-bordered table-hover projects" >

                                            <thead style="background-color: skyblue;color: black;font-weight: bold">
                                                <tr>
                                                    <th style="display:none;">codigo</th>
                                                    <th>Codigo</th>
                                                    <th>Nombre</th>
                                                    <th>Estado</th>
                                                    <th>Editar-Estado-Eliminar</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <%   CategoriaDAO dao = new CategoriaDAO();
                                                    List<Categoria> list = dao.ListadoCategoria();
                                                    Iterator<Categoria> iter = list.iterator();
                                                    Categoria per = null;
                                                    while (iter.hasNext()) {
                                                        per = iter.next();
                                                %>
                                                <tr>
                                                    <td  style="display:none;;"id="idcat"><%=per.getIdcategoria()%></td>
                                                    <td><%= per.getCodigo()%></td>
                                                    <td><%= per.getNombre()%></td>
                                                    <% String Estado = CategoriaDAO.getCategoriaEstado(per.getIdcategoria());
                                                    if (Estado.equalsIgnoreCase("Activo")) {%>
                                                    <td><markactivo><%= Estado%></markactivo></td>   
                                                    <%   } else {%>
                                            <td><markdesactivado><%= Estado%></markdesactivado></td>    
                                                <%     }
                                                %> 
                                            <td>
                                                <a href="ControllerdatosModal?accion=editarcategoria&id=<%=per.getIdcategoria()%>"  data-toggle="modal" data-target="#myModalEdit" class="edit"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                                                <a id='btn-eliminar' class="delete" >
                                                    <i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i>
                                                </a>
                                                <a id='btn-estado' class="delete" style="background-color: transparent;">
                                                    <i class="material-icons" data-toggle="tooltip" title="Update" style="color:  #9575cd;">&#xE627;</i>
                                                </a>
                                            </td>
                                            </tr>
                                            <%}%>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>                                                             
            </div>
        </section>

        <!-- add Modal HTML -->
        <div id="addcategoria" class="modal fade" >
            <div class="modal-dialog" role="document" style="z-index: 9999; width: 450px">
                <div class="modal-content">
                    <form id="newcategoria" action="ControllerCategoria" method="Post" name="frm_nuevo">
                        <div class="modal-header">      
                            <h4 class="modal-title">Agregar Categoria</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">  
                            <%
                                CategoriaDAO com = new CategoriaDAO();
                                String numserie = com.Numserie();
                            %>
                            <div class="form-group">
                                <label>Codigo</label>
                                <input type="text" name="txtCod" value="<%=numserie%>"  class="form-control" readonly="" >
                            </div>
                            <div class="form-group">
                                <label>Nombre</label>
                                <input type="text" name="txtNom" class="form-control" placeholder="Nombre">
                            </div>  
                        </div>
                        <div class="modal-footer">
                            <input type="button"  class="btn btn-default" data-dismiss="modal" value="Cancel">
                            <input onclick="return validarcategoria()"  class="btn btn-success" type="submit" name="accion" value="Guardar">
                        </div>
                    </form>
                </div>
            </div>
        </div> 
        <!--Formulario de Modificar-->
        <div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document" style="z-index: 9999; width: 450px">
                <div class="modal-content">

                    <div class="modal-body">
                    </div>
                </div>                    
            </div>
        </div>
        <!--Data table --->
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>                                
        <!--Data table --->
        <!--Funciones y validaciones --->
        <script src="EstiloAdministrador/funcionesyvalidaciones/Categoria.js" type="text/javascript"></script>
        <!--Funciones y validaciones --->
    </body>
</html>