<%@page import="com.pe.gatasygatos.model.entity.TipoDocumento"%>
<%@page import="com.pe.gatasygatos.DAO.TipoDocumentoDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Cliente"%>
<%@page import="com.pe.gatasygatos.DAO.ClienteDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Cliente</title>
        <%@include file="css-js.jsp" %> 
        <!--ICONOS-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <!--ICONOS-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
        <!--Estilo tabla-->
        <link rel="stylesheet" href="EstiloAdministrador/css/Stilodetabla.css"> 
        <link href="EstiloAdministrador/css/ColordeEstado.css" rel="stylesheet" type="text/css"/>
        <!--Estilo tabla-->
    </head>
    <body>
        <!--cabecera de Menu -->
        <%@include file="FrmAdmin.jsp" %>
        <!-- pageContent -->
        <section class="full-width pageContent">
            <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
                <!--modal listar-->
                <div class="mdl-tabs__panel is-active" id="tabListCategory">
                    <div class="mdl-grid">
                        <div  class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop mdl-cell--0-offset-desktop">
                            <div class="full-width panel mdl-shadow--2dp">
                                <div style="background-color:#4e96b3;"  class="full-width panel-tittle  text-center tittles">
                                    Lista Cliente
                                </div>
                                <div class="full-width panel-content">                        
                                    <div class="col-sm-6 mdl-textfield">
                                        <a  class="btn" href="ControllerdatosModal?accion=addCliente" style="background-color:#0A8CB3;color: #ffffff; margin-right:25px;"  data-toggle="modal" data-target="#addcliente" class="edit"><i class="fas fa-folder-plus"></i> &nbsp; Nuevo</a>
                                    </div>                     
                                    <table id="dtHorizontalVerticalExample" class="table table-striped table-bordered table-sm " cellspacing="0"
                                           width="100%">
                                        <thead >
                                            <tr>
                                                <th style="display:none;;">#</th>
                                                <th>Cod</th>
                                                <th>Cliente</th>
                                                <th>Nro. Documento</th>
                                                <th>Correo</th> 
                                                <th>Celular</th> 
                                                <th>Dir</th>
                                                <th>Genero</th>
                                                <th>Fec. Reg</th>
                                                <th>Estado</th>
                                                <th>Opciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <% ClienteDAO cli = new ClienteDAO();
                                                List<Cliente> listar = cli.ListadoCliente();
                                                Iterator<Cliente> iter = listar.iterator();
                                                Cliente cl = null;
                                                while (iter.hasNext()) {
                                                    cl = iter.next();
                                            %>
                                            <tr>
                                                <td  style="display:none;;"id="idcliet"><%=cl.getIdcliente()%></td>
                                                <td><%=cl.getCodigo()%></td>
                                                <td><%=cl.getCliente()%></td>
                                                <td><%=cl.getNumerodocumento()%></td>
                                                <td><%=cl.getCorreo()%></td>
                                                <td><%=cl.getCelular()%></td>
                                                <td><%=cl.getDireccion()%></td>
                                                <td><%=cl.getSexo()%></td>  
                                                <td><%=cl.getFechaderegistro()%></td>
                                                <% String Estado = ClienteDAO.estado(cl.getIdcliente());
                                                    if (Estado.equalsIgnoreCase("Activo")) {%>

                                                <td><markactivo><%= Estado%></markactivo></td>   
                                                <%   } else {%>

                                        <td><markdesactivado><%= Estado%></markdesactivado></td>    
                                            <%     }
                                            %>
                                        <td> 
                                            <a href="ControllerdatosModal?accion=editarcliente&id=<%=cl.getIdcliente()%>"  data-toggle="modal" data-target="#myModalEdit" class="edit"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                                             <a id='btn-estado' class="delete" >
                                                <i class="material-icons" data-toggle="tooltip" title="Delete">&#xE627;</i>
                                            </a>
                                            <a id='btn-eliminar' class="delete" >
                                                <i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i>
                                            </a>
                                           

                                        </td>
                                        </tr>
                                        <%
                                            }
                                        %>
                                        </tbody>
                                    </table>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>                                                             
            </div>
        </section><!-- add Modal HTML por validacion se uso otro modal -->
        <div class="modal fade" id="addcliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document" style="z-index: 9999; width: 450px">
                <div class="modal-content">
                    <%   ClienteDAO com = new ClienteDAO();
                        String numserie = com.Numserie();
                    %>
                    <div class="modal-body"> 
                        <div class="form-group">
                            <label for="txtCod" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Codigo</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"><input type="text" type="text" value="<%=numserie%>"class="form-control" readonly=""><span class="help-block"></span></div>
                        </div>
                    </div> 
                </div>                    
            </div>
        </div>
        <!--Formulario de Modificar-->
        <div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document" style="z-index: 9999; width: 450px">
                <div class="modal-content">

                    <div class="modal-body">
                    </div>
                </div>                    
            </div>
        </div>
        <!--Data table --->
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>                                
        <!--Data table --->
        <!--Funciones y validaciones --->
        <script src="EstiloAdministrador/funcionesyvalidaciones/Client.js" type="text/javascript"></script>
        
        
    </body>
</html>

<script>
    //scroll de la tabla y datatable cliente se coloco aqui por que genera un error al momento de editar
$(document).ready(function () {
    $('#dtHorizontalVerticalExample').DataTable({
        "scrollX": true,
        "scrollY": 480,
    });
    $('.dataTables_length').addClass('bs-select');
});

</script>