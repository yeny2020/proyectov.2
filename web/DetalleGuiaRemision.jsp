<%@page import="com.pe.gatasygatos.DAO.OrdendeCompraDAO"%>
<%@page import="com.pe.gatasygatos.DAO.ProductoDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.DetalleCompra"%>
<%@page import="com.pe.gatasygatos.DAO.ProvedorDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Compra"%>
<%@page import="com.pe.gatasygatos.DAO.CompraDAO"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <style>
        html {
        }
        .provider-brand {
            bottom: 0;
            position: absolute;
            right: 0;
            text-align: center;
        }

        .provider-brand-logo {
            display: block;
            height: auto;
            margin: 0;
            padding: 0;
            width: 20px;
            margin-left:20px;
        }

        div.DivLogos {
            left: 1px !important;
        }

        .imglogos {
            max-height: 100px !important;
        }

        .FormatLetraRazonEmisor {
            font-family: Verdana, Arial, Helvetica, sans-serif !important;
            font-size: 18px !important;
            text-align: center !important;
            font-weight: bold !important;
            height: 14px !important;
        }

        .FormatLetraDirEmisor {
            font-family: Verdana, Arial, Helvetica, sans-serif !important;
            font-size: 12px !important;
            text-align: center !important;
            font-weight: bold !important;
        }

        /* ESTILO CUADRO DONDE SE ENCUENTRA RUC , TIPO DE DOCUMENTO Y NUMERO DE LA FACTURA*/

        div.DivInvoceCuadroCabecera {
            height: 10px !important;
            margin-top: 0px !important;
            margin-bottom: 0px !important;
            margin-right: 10px !important;
        }

        div.DivContentInvoceCuadroCabecera {
            border: solid #383434 2px !important;
        }

        h4.FormatInvoiceCuadroCabecera {
            text-align: center !important;
            font-weight: bold !important;
            font-size: 18px !important;
        }
        /* ESTILO DEL DETALLE DE LOS PROVEEDORES : SEÑOR , DIRECCION ,
             RUC, FECHA EMISION, MONEDA , VENCIMIENTO*/

        div.PositionProveedorInvoice2 {
            right: 2px !important;
        }

        div.PositionProveedorInvoice6 {
            right: 1px !important;
        }

        .FormatAlingProveedorInvoice {
            font-weight: bold !important;
            font-family: Verdana, Arial, Helvetica, sans-serif !important;
            font-size: 11px !important;
        }

        .FormatAlingProveedorInvoiceDetall {
            font-family: Verdana, Arial, Helvetica, sans-serif !important;
            font-size: 11px !important;
        }
        /* ESTILO PARA LA TABLA DETALLE DE LA FACTURA */

        div.DivDimenTablaDetalleProveedorInvoice {
            bottom: -10px !important;
            width: 66% !important;
        }

        .table-responsive,
        div.DivDimTableResponsiveProveedorInvoice {
            /*min-height: 50% !important;*/
        }

        th.BoderDimTableResponsiveProveedorInvoice {
            border-right: 1px solid #333 !important;
            border-bottom: 1px solid #333 !important;
            border-top: 1px solid #333 !important;
        }

        th.BoderDimTableResponsiveProveedorInvoiceCancel {
            border-bottom: 1px solid #333 !important;
            border-top: 1px solid #333 !important;
        }

        .panel-default,
        .PanelDefaultResponsive {
            border-top: 1px solid #272525 !important;
            border-right: 1px solid #272525 !important;
            border-left: 1px solid #272525 !important;
            border-bottom: 1px solid #272525 !important;
        }

        .FormatTableResponsiveDetallCabecera {
            font-family: Verdana, Arial, Helvetica, sans-serif !important;
            font-weight: bold !important;
            text-align: center !important;
            font-size: 12px !important;
        }

        .FormatTableResponsiveDetallHijo1 {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 12px !important;
            border-left: 1px solid #333 !important;
            width: 40% !important;
            word-break: break-all;
        }

        .FormatTableResponsiveDetallHijo2 {
            font-family: Verdana, Arial, Helvetica, sans-serif !important;
            font-size: 12px !important;
            text-align: center !important;
            border-left: 1px solid #333 !important;
        }

        .FormatTableResponsiveDetallHijo3 {
            font-family: Verdana, Arial, Helvetica, sans-serif !important;
            font-size: 12px !important;
            text-align: center !important;
        }

        .FormatTableResponsiveDetallHijo4 {
            font-family: Verdana, Arial, Helvetica, sans-serif !important;
            font-size: 12px !important;
            text-align: center !important;
            border-right: 1px solid #272525 !important;
        }

        .table-condensed>tbody>tr>td,
        .table-condensed>tbody>tr>th,
        .table-condensed>tfoot>tr>td,
        .table-condensed>tfoot>tr>th,
        .table-condensed>thead>tr>td,
        .table-condensed>thead>tr>th,
        .table_Invoice_Detall {
            padding: 0px !important;
            line-height: none !important;
            border-top: none !important;
        }

        .panel-default,
        .PanelDefaultResponsive {
            border-top: 1px solid #333 !important;
            border-right: 1px solid #333 !important;
            border-left: 1px solid #333 !important;
            border-bottom: 1px solid #333 !important;
        }

        .TrTableDeltalleProveedor:last-child {
            height: 63% !important;
        }
        /* ESTILO DETALLE MONTOS */
        div.DivDimDetalleMontoInvoiceLetra {
            right: 0.5% !important;
        }

        .FormatDetalleMontoInvoiceLetra {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 12px !important;
            text-indent: 10px !important;
            text-align: left !important;
        }

        .FormatTablePieMontoInvoiceLetra1 {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 12px !important;
            text-indent: 10px !important;
            text-align: center !important;
            font-weight: bold !important;
        }

        .EstiloPanelDetalleDenominacionMontoInvoice {
            border-bottom: 1px solid #272525 !important;
            border-top: 0px solid #272525 !important;
            border-left: 1px solid #272525 !important;
            border-right: 1px solid #272525 !important;
        }

        div.DivDimTablaDetalleMontoInvoice {
            width: 30% !important;
            left: 10px !important;
        }

        .EstiloTablaDetalleMontoInvoice th,
        .EstiloTablaDetalleMontoInvoice td,
        table.EstiloTablaDetalleMontoInvoice {
            border-bottom: 1px solid #333 !important;
            border-top: 1px solid #333 !important;
            border-left: 1px solid #333 !important;
            border-right: 1px solid #333 !important;
        }

        .FormatTablaDetalleMontoInvoice1 {
            font-family: Verdana, Arial, Helvetica, sans-serif !important;
            font-weight: bold !important;
            font-size: 12px !important;
            text-align: left !important;
            padding-left: 15px !important
        }

        .FormatTablaDetalleMontoInvoice2 {
            font-family: Verdana, Arial, Helvetica, sans-serif !important;
            font-size: 12px !important;
            text-indent: 10px !important;
            text-align: center !important;
        }

        .FormatTableDetallCodBarraLetra2 {
            font-family: Verdana, Arial, Helvetica, sans-serif !important;
            font-size: 12px !important;
            text-align: left !important;
        }

        .table>tbody>tr>td,
        .table>tbody>tr>th,
        .table>tfoot>tr>td,
        .table>tfoot>tr>th,
        .table>thead>tr>td,
        .table>thead>tr>th,
        .TableDetallCodBarraLetra {
            padding: 0px !important;
            border-top: none !important;
        }

        div.DimPanelCodigoBarraDetalle {
            margin-top: 20px !important;
        }

        .FormatPanelCodigoBarraLetra {
            font-family: Verdana, Arial, Helvetica, sans-serif !important;
            font-size: 12px !important;
            text-indent: 8px !important;
            text-align: center !important;
        }

        .FormatTablePieMontoInvoiceLetra3 {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 12px !important;
            text-indent: 10px !important;
            text-align: center !important;
        }

        .FormatTablePieMontoInvoiceLetra4 {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 12px !important;
            text-indent: 10px !important;
            text-align: left !important;
        }

        .EstiloTablaDetalleDenominacionMontoInvoice th,
        table.EstiloTablaDetalleDenominacionMontoInvoice {
            border-bottom: 1px solid #272525 !important;
            border-top: 1px solid #272525 !important;
            border-left: 1px solid #272525 !important;
            border-right: 1px solid #272525 !important;
        }

        .EstiloTablaDetalleDenominacionMontoInvoice th,
        .EstiloTablaDetalleDenominacionMontoInvoice td,
        table.EstiloTablaDetalleDenominacionMontoInvoice {
            border-bottom: 1px solid #272525 !important;
            border-top: 1px solid #272525 !important;
            border-left: 1px solid #272525 !important;
            border-right: 1px solid #272525 !important;
        }

        .PanelDefaultCodigoBarra {
            border-top: 1px solid #333 !important;
            border-bottom: 1px solid #333 !important;
        }

        .imgDimBarcode {
            display: block !important;
            margin: 0 auto !important;
        }

        .FormatTableFooterDesc {
            padding-right: 10px;
        }

        hr {
            border-color: #444444 !important;
        }
        .cuentas {
            width: 100%;
        }

        .cuentas td,
        .cuentas th {
            border: 1px solid black;
            text-align: center;
            font-size: 10px;
            font-family: sans-serif;
        }
    </style>    
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Reporte Cliente</title>
        <%@include file="css-js.jsp" %>
        <link href="EstiloAdministrador/css/ESTILO_TAB.css" rel="stylesheet" type="text/css"/>
        <script src = " https://raw.githack.com/eKoopmans/html2pdf/master/dist/html2pdf.bundle.js "></script>
        <script src="EstiloAdministrador/funcionesyvalidaciones/PDF.js" type="text/javascript"></script>
        <link href="EstiloAdministrador/css/TamañoA4.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <%
            CompraDAO dao = new CompraDAO();
            int id = Integer.parseInt((String) request.getAttribute("idco"));
            Compra p = (Compra) dao.Reporte(id);
        %>
        <!--cabecera de Menu -->
        <%@include file="FrmAdmin.jsp" %>
        <!-- pageContent -->
        <section class="full-width pageContent">
            <div class="mdl-grid" style="background-color: #fff;width: 80%;">
                <div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop">
                    <dv>
                        <button ondblclick="generarpdf()" class="btn btn-primary" style="background: #0a8cb3;color:#fff;"><i class="zmdi zmdi-cloud-download"></i>Descargar</button>

                        <a href="Cotizacion.jsp"  class="btn btn-danger">Regresar</a>
                    </dv>

                    <!------------inicio pdf-->
                    <div id="invoce" class="divpdf">
                        <div id="PDF">
                            <br>
                            <div class="container">
                                <div class="row">
                                    <div class="col-xs-4 DivLogos">
                                        <h1 style = "font-size: 16px;"><strong>Detalles del proveedor:</strong></h1>
                                        <strong>Proveedor:</strong> <%= ProvedorDAO.getProveedor(p.getIdproveedor())%><br>
                                        <strong>E-mail : </strong> <%= ProvedorDAO.getEmail(p.getIdproveedor())%><br>
                                        <strong>Teléfono :</strong> <%= ProvedorDAO.getCelular(p.getIdproveedor())%> <br>
                                    </div>
                                    <div class="col-xs-3 DivInvoceCuadroCabecera">
                                        <div class="panel panel-default DivContentInvoceCuadroCabecera">
                                            <h4 class="FormatInvoiceCuadroCabecera">RUC:&nbsp;<%= ProvedorDAO.getRUC(p.getIdproveedor())%></h4>
                                            <h4 class="FormatInvoiceCuadroCabecera"><%=p.getTipocomprobante()%></h4>
                                            <h4 class="FormatInvoiceCuadroCabecera">REMITENTE</h4>
                                            <h4 class="FormatInvoiceCuadroCabecera">Nro.&nbsp;<%=p.getSerie()%> - <%=p.getCorrelativo()%></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div>
                                <div id="print-area">
                                    <div class="row pad-top font-big">
                                        <hr style="border-color: #dfe0e0;">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <strong>GATAS Y GATOS EMPRESA INDIVIDUAL DE RESPONSABILIDAD </strong>
                                            <br>
                                            <strong>Dirección :</strong>Jirón 2 de Mayo 479,Huánuco 10001
                                            <br>
                                            Huanuco, Peru. 
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <strong>E-mail   : </strong> info@obedalvarado.com<br>
                                            <strong>Teléfono :</strong> 936-809-722 <br>
                                            <strong>Fecha:</strong>  <%=p.getFechaYhora()%> 
                                            <strong>Condicion:</strong>  <%= ProvedorDAO.getCondicion(p.getIdproveedor())%> 
                                        </div>


                                    </div>
                                    <!----------tabla------------------------------------------------------------>
                                    <div class="row">
                                        <hr>
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <div class="table-responsive DivDimTableResponsiveProveedorInvoice">
                                                <table  border="1"  class="table table-striped  table-hover" style="height: 40%" >

                                                    <thead style="background-color:#3f51b5;color:white;">
                                                        <tr >
                                                            <th class="BoderDimTableResponsiveProveedorInvoice">
                                                                <h4 class="FormatTableResponsiveDetallCabecera">Código</h4>
                                                            </th>

                                                            <th class="BoderDimTableResponsiveProveedorInvoice">
                                                                <h4 class="FormatTableResponsiveDetallCabecera">Descripción</h4>
                                                            </th>
                                                            <th class="BoderDimTableResponsiveProveedorInvoice">
                                                                <h4 class="FormatTableResponsiveDetallCabecera">Unidad</h4>
                                                            </th>
                                                            <th class="BoderDimTableResponsiveProveedorInvoice">
                                                                <h4 class="FormatTableResponsiveDetallCabecera">Cantidad</h4>
                                                            </th>


                                                        </tr>
                                                    </thead>


                                                    <tbody>
                                                        <%  CompraDAO pdao = new CompraDAO();
                                                            List<DetalleCompra> listS = pdao.ticketDetalle(id);
                                                            Iterator<DetalleCompra> iterr = listS.iterator();
                                                            DetalleCompra pro = null;
                                                            double total = 0, igv = 0, subtotal = 0;
                                                            DecimalFormat formatea = new DecimalFormat("###,###.##");
                                                            String Msubtotal = "";
                                                            String Migv = "";
                                                            while (iterr.hasNext()) {
                                                                pro = iterr.next();%>

                                                        <tr>
                                                            <td class="FormatTableResponsiveDetallHijo4"><%=pro.getIdproducto()%></td>
                                                            <td class="FormatTableResponsiveDetallHijo4"><%= ProductoDAO.getProducto(pro.getIdproducto())%></td>
                                                            <td class="FormatTableResponsiveDetallHijo4"><%=OrdendeCompraDAO.getNombreunidad(pro.getIdproducto())%></td>
                                                            <td class="FormatTableResponsiveDetallHijo4"><%= pro.getCantidad()%></td>

                                                        </tr>
                                                        <%}%>
                                                    </tbody>


                                                </table>

                                            </div>
                                        </div>

                                    </div>
                                    <!------>   
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <div class="col-xs-4 DivDimDetalleMontoInvoiceLetra">
                                        <div>
                                            <div class="panel panel-default" style="padding:10px;">
                                                <span class="FormatTableFooterDesc" style="font-weight:bold;">Observaciones: <br>
                                                    <br><br>
                                                </span> 
                                            </div>
                                        </div>
                                    </div>
                                    <!------------->
                                </div>  

                                <!-----------todo pdf-->
                            </div>
                        </div>
                    </div>
                    <!--   <center><a href="#" onclick="ReportePDF()" class="btn btn-primary"> <i class="zmdi zmdi-cloud-download"></i>Reporte PDF</a>-->
                </div>
            </div>
        </section>
    </body>
    <%@include file="pdf-excel-js.jsp" %>

</html>