<%@page import="com.pe.gatasygatos.DAO.ProductoDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.DetalleVentas"%>
<%@page import="com.pe.gatasygatos.DAO.ClienteDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Venta"%>
<%@page import="com.pe.gatasygatos.DAO.VentaDAO"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>


<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Reporte Cliente</title>
        <%@include file="css-js.jsp" %>
        <link href="EstiloAdministrador/css/estiloDetalleVenta.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <%
            VentaDAO dao = new VentaDAO();
            int id = Integer.parseInt((String) request.getAttribute("idve"));
            Venta p = (Venta) dao.Reporte(id);
        %>
        <!--cabecera de Menu -->
     <%@include file="FrmAdmin.jsp" %>
        <!-- pageContent -->
        <section class="full-width pageContent">




            <div class="mdl-grid">
                <div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop">


                    <div id="PDF">   
                        <center>
                            <table class="table table-condensed table_Invoice_Detall" style="height: 40%">
                                <h1>Empresa</h1> <h3>Gatas y Gatos</h3>

                                <tr>
                                    <td> <h3>Tipo Comprobante : <%=p.getTipocomprobante()%></h3></td>
                                    <td><h3>  : N. <%=p.getCorrelativo()%></h3></td>
                                <center> <td><h3> Fecha :<%=p.getFechayhora()%></h3></td></center>
                                </tr>
                            </table>
                            <table>
                                <tr>
                                    <td>  <h3>Cliente : <%= ClienteDAO.getcliente(p.getIdcliente())%> <%= ClienteDAO.getapellido(p.getIdcliente())%></h3></td>
                                </tr>
                            </table>

                            <h3>Detalle Venta</h3>





                            <table  border="1">
                                <thead>
                                    <tr>


                                        <th >producto</th>
                                        <th >cantidad</th>
                                        <th >precio</th>
                                        <th >subtotal</th>


                                    </tr>
                                </thead>


                                <tbody>
                                    <%  VentaDAO pdao = new VentaDAO();
                                        List<DetalleVentas> listS = pdao.ticketDetalle(id);
                                        Iterator<DetalleVentas> iterr = listS.iterator();
                                        DetalleVentas pro = null;
                                        while (iterr.hasNext()) {
                                            pro = iterr.next();%>

                                    <tr>


                                        <td><%= ProductoDAO.getProducto(pro.getIdproducto())%></td>
                                        <td><%= pro.getCantidad()%></td>
                                        <td><%= pro.getProducto().getPrecioVenta()%></td>
                                        <td><%=pro.getCantidad()%>*<%=pro.getProducto().getPrecioVenta()%> </td>


                                    </tr>
                                    <%}%>

                                </tbody>
                            </table>
                            <table>


                                <td>
                                    <%=p.getSubtotal()%>
                                </td>


                                <td>
                                    <%=p.getIgv()%>
                                </td>



                                <td>
                                    <%=p.getTotal()%>  
                                </td>

                            </table>
                        </center>
                    </div>         
                </div>

            </div>


            <center><a href="#" onclick="ReportePDF()" class="btn btn-primary"  >Reporte PDF</a>
                <a href="Venta.jsp"  class="btn btn-danger"  >Salir</a></center>

        </section>
    </body>
    <%@include file="pdf-excel-js.jsp" %>

</html>