<%@page import="com.pe.gatasygatos.model.entity.Marca"%>
<%@page import="com.pe.gatasygatos.DAO.MarcaDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Producto"%>
<%@page import="com.pe.gatasygatos.DAO.ProductoDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Categoria"%>
<%@page import="com.pe.gatasygatos.DAO.CategoriaDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Categoria</title>
    </head>
    <body>
        <!-- add Modal HTML -->
        <div >
            <%CategoriaDAO dao = new CategoriaDAO();
                int id = Integer.parseInt((String) request.getAttribute("idcat"));
                Categoria m = (Categoria) dao.CategoriaID(id);
            %>
            <div class="modal-content">
                <div class="modal-header">      
                    <h4 class="modal-title"   id="myModalLabel">Editar Categoria</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div> 
                <form id="editcategoria"  method="post" action="ControllerCategoria" name="frm_edit"> 
                    <div class="modal-body">  
                        <div class="form-group">
                            <input  type="hidden" type="text"  name="txtid" value="<%=m.getIdcategoria()%>"readonly="">
                        </div> 
                        <div class="form-group">
                            <label>Codigo</label>
                            <input type="text" class="form-control"  name="TxtCod" value="<%=m.getCodigo()%>"readonly="">
                        </div>  
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" class="form-control"  name="Txtnombre" value="<%=m.getNombre()%>">
                        </div>  
                    </div>
                    <div class="modal-footer">
                        <a href="Categoria.jsp" class="btn btn-default" >Cancelar</a> 
                        <input onclick="return valeditarcategoria()" class="btn btn-success" type="submit" name="accion" value="Actualizar">
                    </div>
                </form>
            </div>
        </div>
    </body>
    <!--Funciones y validaciones --->
    <script src="EstiloAdministrador/funcionesyvalidaciones/Categoria.js" type="text/javascript"></script>
    <!--Funciones y validaciones --->
</html>