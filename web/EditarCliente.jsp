
<%@page import="com.pe.gatasygatos.model.entity.Cliente"%>
<%@page import="com.pe.gatasygatos.DAO.ClienteDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.pe.gatasygatos.model.entity.TipoDocumento"%>
<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.pe.gatasygatos.DAO.TipoDocumentoDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Categoria</title>
    </head>
    <body>
        <!-- add Modal HTML -->
        <%ClienteDAO dao = new ClienteDAO();
            int id = Integer.parseInt((String) request.getAttribute("idcli"));
            Cliente cl = (Cliente) dao.BuscarPorId(id);
        %>
        <div>
            <!-- add Modal HTML -->
            <div class="modal-content">
                <div class="modal-header">      
                    <h4 class="modal-title"   id="myModalLabel">Add Cliente</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div> 
                <form id="editcliente" action="ControllerCliente" method="Post" name="frm_edit">


                    <div class="modal-body"> 
                        <div class="form-group">
                            <input type="hidden" name="txtid" class="form-control" value="<%=cl.getIdcliente()%>">
                            <label for="Txtcodigo" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Codigo</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"><input type="text" name="Txtcodigo" id="Txtcodigo" class="border-focus-darkblue form-control" value="<%=cl.getCodigo()%>"  class="form-control" readonly=""><span class="help-block"></span></div></div>
                    </div> 
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="Txtapellidos" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Cliente</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"><input type="text" name="Txtapellidos" onkeypress="return soloLetras(event)" value="<%=cl.getCliente()%>" id="Txtapellidos" class="border-focus-darkblue form-control" required=""><span class="help-block"></span></div></div>
                    </div>
                        <div class="modal-body">
                        <div class="form-group">
                            <label for="validationDefault01" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Tipo de Doc. de Identidad</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" class="border-focus-darkblue form-control">
                                <select name="Txtidtipodocumento" id="Txtidtipodocumento" class="form-control" required="">
                                    <% TipoDocumentoDAO tdoc = new TipoDocumentoDAO();
                                        List<TipoDocumento> lista = tdoc.listarTipodocumento();
                                        for (TipoDocumento tipodo : lista) {
                                            if (tipodo.getIdtipodocumento() == cl.getIdtipodocumento()) {
                                                out.println("<option   value='" + tipodo.getIdtipodocumento()
                                                        + "'selected>" + tipodo.getTipoDocumento() + "</option>");
                                            } else {
                                                out.println("<option   value='" + tipodo.getIdtipodocumento()
                                                        + "'>" + tipodo.getTipoDocumento() + "</option>");
                                            }

                                        }

                                    %>  
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="Txtnumerodocumento" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Doc. ID</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"><input type="text" name="Txtnumerodocumento" onkeypress="return soloNumeros(event)" value="<%= cl.getNumerodocumento()%>" maxlength="11" id="Txtnumerodocumento" class="border-focus-darkblue form-control" required=""><span class="help-block"></span></div></div>
                    </div>                    
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="Txtcorreo" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Correo contacto</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"><input type="text" name="Txtcorreo" value="<%= cl.getCorreo()%>" id="Txtcorreo" class="border-focus-darkblue form-control" required=""><span class="help-block"></span>
                            </div> 
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="Txtcelular" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Teléfono</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"><input type="text" name="Txtcelular" value="<%= cl.getCelular()%>" onkeypress="return soloNumeros(event)"  id="Txtcelular" class="border-focus-darkblue form-control"><span class="help-block"></span></div></div>                                  
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="Txtdireccion" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Direccion</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"><input type="text" name="Txtdireccion" value="<%= cl.getDireccion()%>" id="Txtdireccion" class="border-focus-darkblue form-control" required=""><span class="help-block"></span></div></div>
                    </div>

                    <div class="modal-body">
                        <div class="form-group">
                            <label for="Txtfechaderegistro" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Fecha de Registro:</label>
                            <div class="col-lg-7 col-md-8 col-sm-12 col-xs-12"><input  name="Txtfechaderegistro" value="<%=cl.getFechaderegistro()%>" id="" class="border-focus-darkblue form-control" readonly="">
                                <span class="help-block"></span></div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="Txtsexo" class="col-lg-4 col-md-7 col-sm-10 col-xs-10 control-label">Genero</label>

                            <% if (cl.getSexo().equalsIgnoreCase("Masculino")) { %>
                            &nbsp;&nbsp;&nbsp;<input style="margin-left: 30px" type="radio" name="Txtsexo" value="Masculino" checked="checked" required="">&nbsp;Masculino
                            <% } else { %>
                            &nbsp;&nbsp;&nbsp;<input  type="radio" name="Txtsexo" value="Masculino" required="">&nbsp;Masculino 
                            <% } %>
                            <% if (cl.getSexo().equalsIgnoreCase("Femenino")) { %>
                            &nbsp;&nbsp;&nbsp;<input type="radio" name="Txtsexo" value="Femenino" checked="checked" required="">&nbsp;Femenino
                            <% } else { %>
                            &nbsp;&nbsp;&nbsp;<input style="margin-left: 10px" type="radio" name="Txtsexo" value="Femenino" required="">&nbsp;Femenino
                            <% }%>
                        </div>
                        <span class="help-block"></span>
                    </div>


                    <div class="modal-footer">
                        <a href="Cliente.jsp" class="btn btn-default" style="background-color: #B7BBB8" ><i class="far fa-window-close"></i>&nbsp; Cancelar</a> 
                        <input onclick="return validareditclientes()" class="btn btn-success" type="submit" name="accion" value="Actualizar">
                    </div>
                </form>
            </div>
        </div>
    </body>
    <!--Funciones y validaciones --->
    <script src="EstiloAdministrador/funcionesyvalidaciones/Client.js" type="text/javascript"></script>
    <!--Funciones y validaciones --->
</html>




