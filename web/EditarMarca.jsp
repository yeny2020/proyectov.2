<%@page import="com.pe.gatasygatos.model.entity.Marca"%>
<%@page import="com.pe.gatasygatos.DAO.MarcaDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Categoria</title>
    </head>
    <body>
        <!-- add Modal HTML -->
        <div >
            <%MarcaDAO dao = new MarcaDAO();
                int id = Integer.parseInt((String) request.getAttribute("idmar"));
                Marca m = (Marca) dao.BuscarporID(id);
            %>
            <div class="modal-content">
                <div class="modal-header" style="background-color:#aed581;">      
                    <h4 class="modal-title"   id="myModalLabel">Editar Marca</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div> 
                <form id="editmarca"  method="post" action="ControllerMarca" name="frm_edit"> 
                    <div class="modal-body">  
                        <div class="form-group">
                            <input  type="hidden" type="text"  name="txtid" value="<%=m.getIdmarca()%>"readonly="">
                        </div> 
                        <div class="form-group">
                            <label>Codigo</label>
                            <input type="text" class="form-control"  name="TxtCod" value="<%=m.getCodigo()%>"readonly="">
                        </div> 
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" class="form-control" name="TxtNom" value="<%=m.getNombre()%>">
                        </div>  
                    </div>
                    <div class="modal-footer">
                        <a href="Marca.jsp" class="btn btn-default" >Canselar</a> 
                        <input onclick="return valedit()" class="btn btn-success" type="submit" name="accion" value="Actualizar">
                    </div>
                </form>
            </div>
        </div>
    </body>
    <!--Funciones y validaciones --->
    <script src="EstiloAdministrador/funcionesyvalidaciones/Marca.js" type="text/javascript"></script>
    <!--Funciones y validaciones --->
</html>
