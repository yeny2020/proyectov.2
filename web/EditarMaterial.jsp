
<%@page import="com.pe.gatasygatos.model.entity.Almacen"%>
<%@page import="com.pe.gatasygatos.DAO.AlmacenDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Almacen</title>
    </head>
    <body>
        <!-- add Modal HTML -->
        <div >
            <%AlmacenDAO dao = new AlmacenDAO();
                                int id = Integer.parseInt((String) request.getAttribute("idmat"));
                                Almacen ma = (Almacen) dao.list(id);
                            %>
            <div class="modal-content">
                <div class="modal-header">      
                    <h4 class="modal-title"   id="myModalLabel">EDITAR ALMACEN</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div> 
                <form id="editmaterial"  method="post" action="ControllerMaterial" name="frm_edit"> 
                    <div class="modal-body">  
                        <div class="form-group">
                            <input  type="hidden" type="text"  name="txtid" value="<%=ma.getIdalmacen()%>"readonly="">
                        </div> 
                        <div class="form-group">
                            <label>Codigo</label>
                            <input type="text" class="form-control"  name="TxtCod" value="<%=ma.getCodigo()%>"readonly="">
                        </div> 
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" class="form-control"  name="TxtNom" value="<%=ma.getNombre()%>">
                        </div>  
                    </div>
                    <div class="modal-footer">
                        <a href="Almacen.jsp" class="btn btn-default" >Canselar</a> 
                        <input onclick="return validareditmaterial()" class="btn btn-success" type="submit" name="accion" value="Actualizar">
                    </div>
                </form>
            </div>
        </div>
    </body>
    <!--Funciones y validaciones --->
    <script src="EstiloAdministrador/funcionesyvalidaciones/Material.js" type="text/javascript"></script>
    <!--Funciones y validaciones --->
</html>