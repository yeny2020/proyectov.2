
<%@page import="com.pe.gatasygatos.model.entity.Motivo"%>
<%@page import="com.pe.gatasygatos.DAO.MotivoDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Categoria</title>
    </head>
    <body>
        <!-- add Modal HTML -->
        <div >
            <%MotivoDAO dao = new MotivoDAO();
                int id = Integer.parseInt((String) request.getAttribute("idmot"));
                Motivo m = (Motivo) dao.list(id);
            %>
            <div class="modal-content">
                <div class="modal-header" style="background-color:#aed581;">      
                    <h4 class="modal-title"   id="myModalLabel">Editar Marca</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div> 
                <form id="editmotivo"  method="post" action="ControllerMotivo" name="frm_edit"> 
                    <div class="modal-body">  
                        <div class="form-group">
                            <input  type="hidden" type="text"  name="txtid" value="<%=m.getIdmotivo()%>"readonly="">
                        </div> 
                        <div class="form-group">
                            <label>Codigo</label>
                            <input type="text" class="form-control"  name="TxtCod" value="<%=m.getCodigo()%>"readonly="">
                        </div> 
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text"  name="TxtNom" value="<%=m.getNombre()%>">
                        </div>  
                    </div>

                    <div class="modal-footer">
                        <a href="Motivo.jsp" class="btn btn-default" >Canselar</a> 
                        <input onclick="return validareditmotivo()" class="btn btn-success" type="submit" name="accion" value="Actualizar">
                    </div>
                </form>
            </div>
        </div>
    </body>
    <!--Funciones y validaciones --->
    <script src="EstiloAdministrador/funcionesyvalidaciones/Motivo.js" type="text/javascript"></script>
    <!--Funciones y validaciones --->
</html>
