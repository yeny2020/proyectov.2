<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.pe.gatasygatos.model.entity.Publico"%>
<%@page import="com.pe.gatasygatos.DAO.PublicoDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Producto"%>
<%@page import="com.pe.gatasygatos.DAO.ProductoDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.UnidadVenta"%>
<%@page import="com.pe.gatasygatos.DAO.UnidadVentaDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.UnidadCompra"%>
<%@page import="com.pe.gatasygatos.DAO.UnidadCompraDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Provedor"%>
<%@page import="com.pe.gatasygatos.DAO.ProvedorDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Marca"%>
<%@page import="com.pe.gatasygatos.DAO.MarcaDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Categoria"%>
<%@page import="com.pe.gatasygatos.DAO.CategoriaDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <%@include file="css-js.jsp" %>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Bootstrap CRUD Data Table for Database with Modal Form</title>
        <!--BOOSTRAP PARA DIV-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <!--BOOSTRAP PARA DIV-->
        <!--Estilo tabla-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link rel="stylesheet" href="EstiloAdministrador/css/Stilodetabla.css"> 
        <!--Estilo tabla-->
        <%--Buscador.---%>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
        <%-- finBuscador.---%>
    </head>
    <body>
        <!--cabecera de Menu -->
        <%@include file="FrmAdmin.jsp" %>
        <!-- pageContent -->
        <section class="full-width pageContent">

            <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect"> 


                <div class="mdl-tabs__panel is-active" id="tabNewProduct">
                    <div class="mdl-grid">
                        <div class="mdl-cell mdl-cell--2-col-phone mdl-cell--10-col-tablet mdl-cell--12-col-desktop mdl-cell--0-offset-desktop">
                            <div class="full-width panel mdl-shadow--2dp">
                                <div style="background-color:#4e96b3;" class="full-width panel-tittle  text-center tittles">
                                    Nuevo Producto
                                </div> 
                                <%ProductoDAO dao = new ProductoDAO();
                                    int id = Integer.parseInt((String) request.getAttribute("idpro"));
                                    Producto prod = (Producto) dao.list(id);
                                %>
                                <div class="full-width panel-content">

                                    <form id="editproducto"  action="ControllerProducto" method="Post" name="frm_nuevo" >
                                        <div class="d-flex">
                                            <div class="col-sm-12">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <label for="Txtcodigo" class="col-lg-2" style="text-align:right">Codigo: </label>
                                                        <div class="col-lg-4">
                                                            <input type="hidden" value="<%=prod.getIdproducto()%>" name="txtid" class="form-control">

                                                            <input type="text" value="<%=prod.getCodigo()%>" name="Txtcodigo" class="form-control" readonly="">
                                                            <span class="help-block"></span>
                                                        </div>
                                                        <div class="form-group">                                     
                                                            <label for="" class="col-lg-2" style="text-align:right"></label>
                                                            <div class="col-lg-4"><p  class="border-focus-darkblue form-control-static" readonly="" style="color: white">Productos</p><span class="help-block"></span></div>
                                                        </div>  

                                                        <div class="form-group">                                     
                                                            <label for="Txtdescripcion" class="col-lg-2" style="text-align:right">Descripcion: </label>
                                                            <div class="col-lg-10"> 
                                                                <textarea cols="30" rows="2"   name="Txtdescripcion" class="form-control" required=""><%=prod.getDescripcion()%></textarea>
                                                                <span class="help-block"></span></div>
                                                        </div>
                                                        <div class="form-group"> 
                                                            <label for="Txtcategoria" class="col-lg-2" style="text-align:right">Categoria:</label>
                                                            <div class="col-lg-4">
                                                                <select class="js-example-basic-single form-control" name="Txtcategoria">
                                                                    <% CategoriaDAO tdoc = new CategoriaDAO();
                                                                        List<Categoria> lista = tdoc.ListadoEstadoActivos();
                                                                        for (Categoria categoria : lista) {
                                                                            if (categoria.getIdcategoria() == prod.getIdcategoria()) {
                                                                                out.println("<option   value='" + categoria.getIdcategoria()
                                                                                        + "'selected>" + categoria.getNombre() + "</option>");
                                                                            } else {
                                                                                out.println("<option   value='" + categoria.getIdcategoria()
                                                                                        + "'>" + categoria.getNombre() + "</option>");
                                                                            }

                                                                        }

                                                                    %>
                                                                </select>

                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group"> 
                                                            <label  for="Txtmarca" class="col-lg-2" style="text-align:right">Marca : </label>                                                      
                                                            <div class="col-lg-4">
                                                                <select class="js-example-basic-single form-control" name="Txtmarca">
                                                                    <% MarcaDAO mar = new MarcaDAO();
                                                                        List<Marca> list = mar.ListadoEstadoActivos();
                                                                        for (Marca marca : list) {
                                                                            if (marca.getIdmarca() == prod.getIdmarca()) {
                                                                                out.println("<option   value='" + marca.getIdmarca()
                                                                                        + "'selected>" + marca.getNombre() + "</option>");
                                                                            } else {
                                                                                out.println("<option   value='" + marca.getIdmarca()
                                                                                        + "'>" + marca.getNombre() + "</option>");
                                                                            }

                                                                        }

                                                                    %>
                                                                </select>

                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group"> 
                                                            <label for="TxtIdpublico" class="col-lg-2" style="text-align:right">Publico: </label>                                                      
                                                            <div class="col-lg-4">
                                                                <select class="js-example-basic-single form-control" name="TxtIdpublico">
                                                                    <% PublicoDAO publ = new PublicoDAO();
                                                                        List<Publico> listoa = publ.listarPublico();
                                                                        for (Publico publico : listoa) {
                                                                            if (publico.getIdpublico()== prod.getIdpublico()) {
                                                                                out.println("<option   value='" + publico.getIdpublico()
                                                                                        + "'selected>" + publico.getNombre() + "</option>");
                                                                            } else {
                                                                                out.println("<option   value='" + publico.getIdpublico()
                                                                                        + "'>" + publico.getNombre() + "</option>");
                                                                            }

                                                                        }

                                                                    %>
                                                                </select>

                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group"> 
                                                            <label for="Txtproveedor" class="col-lg-2" style="text-align:right">Proveedor:</label>
                                                            <div class="col-lg-4">
                                                                <select class="js-example-basic-single form-control" name="Txtproveedor">
                                                                    <% ProvedorDAO prov = new ProvedorDAO();
                                                                        List<Provedor> li = prov.ListadoEstadoActivos();
                                                                        for (Provedor provedor : li) {
                                                                            if (provedor.getIdproveedor() == prod.getIdProveedor()) {
                                                                                out.println("<option   value='" + provedor.getIdproveedor()
                                                                                        + "'selected>" + provedor.getRazonsocial() + "</option>");
                                                                            } else {
                                                                                out.println("<option   value='" + provedor.getIdproveedor()
                                                                                        + "'>" + provedor.getRazonsocial() + "</option>");
                                                                            }

                                                                        }

                                                                    %>
                                                                </select>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group"> 
                                                            <label for="Txtunidadc" class="col-lg-2" style="text-align:right">Unidad Compra:</label>
                                                            <div class="col-lg-4">
                                                                <select class="js-example-basic-single form-control" name="Txtunidadc">
                                                                    <% UnidadCompraDAO uc = new UnidadCompraDAO();
                                                                        List<UnidadCompra> l = uc.ListaUnidadCompra();
                                                                        for (UnidadCompra unidadcompra : l) {
                                                                            if (unidadcompra.getIducompra() == prod.getIducompra()) {
                                                                                out.println("<option   value='" + unidadcompra.getIducompra()
                                                                                        + "'selected>" + unidadcompra.getNombre() + "</option>");
                                                                            } else {
                                                                                out.println("<option   value='" + unidadcompra.getIducompra()
                                                                                        + "'>" + unidadcompra.getNombre() + "</option>");
                                                                            }

                                                                        }

                                                                    %>
                                                                </select>


                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group"> 
                                                            <label for="Txtunidadv" class="col-lg-2" style="text-align:right">Unidad Venta:</label>
                                                            <div class="col-lg-4">
                                                                <select class="js-example-basic-single form-control" name="Txtunidadv">
                                                                    <% UnidadVentaDAO uv = new UnidadVentaDAO();
                                                                        List<UnidadVenta> lv = uv.ListaUnidadVenta();
                                                                        for (UnidadVenta unidadventa : lv) {
                                                                            if (unidadventa.getIduventa() == prod.getIduventa()) {
                                                                                out.println("<option   value='" + unidadventa.getIduventa()
                                                                                        + "'selected>" + unidadventa.getNombre() + "</option>");
                                                                            } else {
                                                                                out.println("<option   value='" + unidadventa.getIduventa()
                                                                                        + "'>" + unidadventa.getNombre() + "</option>");
                                                                            }

                                                                        }

                                                                    %>
                                                                </select>


                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>      
                                                        <div class="form-group">                                     
                                                            <label for="Txtpcompra" class="col-lg-2" style="text-align:right">Precio Compra S/: </label>
                                                            <div class="col-lg-4">
                                                                <input type="text" name="Txtpcompra" value="<%=prod.getPreciocompra()%>" id="Txtprecioc" class="form-control">
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">                                     
                                                            <label for="Txtpventa" class="col-lg-2" style="text-align:right">Precio Venta S/: </label>
                                                            <div class="col-lg-4">
                                                                <input type="text" name="Txtpventa" value="<%=prod.getPrecioVenta()%>" id="Txtunidad" class="form-control">
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>


                                                        <div class="form-group">

                                                        </div>

                                                        <div class="form-group">
                                                            <label for="Txtfechaderegistro" class="col-lg-2" style="text-align:right">Fecha Registro:</label>
                                                            <div class="col-lg-4"><input name="Txtfechaderegistro" value="<%=prod.getFechaRegistro()%>" id="" class="border-focus-darkblue form-control" readonly="">
                                                                <span class="help-block"></span></div>
                                                        </div>
                                                    </div>  

                                                    <div class="modal-footer">
                                                        <a href="Producto.jsp" style="background-color: #09bb04;color: #ffffff;"class="btn "><i class="fas fa-undo-alt"></i> <span>&nbsp; Regresar</span></a>    
                                                        <input onclick="return validareditproductos()" type="submit" style="background-color: #FC0817;" class="btn btn-success" name="accion" value="Actualizar">


                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>

    <script>
// In your Javascript (external .js resource or <script> tag)
        $(document).ready(function () {
            $('.js-example-basic-single').select2();
        });

    </script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>                                

    <script src="EstiloAdministrador/funcionesyvalidaciones/Producto.js" type="text/javascript"></script>

</html>