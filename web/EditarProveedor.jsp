<%@page import="com.pe.gatasygatos.model.entity.TipoDocumento"%>
<%@page import="com.pe.gatasygatos.DAO.TipoDocumentoDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Provedor"%>
<%@page import="com.pe.gatasygatos.DAO.ProvedorDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit proveedor</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <!--ICONOS-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <!--ICONOS-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
    </head>
    <body>
        <!-- add Modal HTML -->
        <div>
            <%ProvedorDAO dao = new ProvedorDAO();
                int id = Integer.parseInt((String) request.getAttribute("idprove"));
                Provedor pr = (Provedor) dao.list(id);
            %>
            <div class="modal-content">
                <div class="modal-header">      
                    <h4 class="modal-title"   id="myModalLabel">Editar Proveedor</h4>

                    <a href="Proveedor.jsp" class="close" ><i class="material-icons"  title="Close" style="color: #007bb6;">&times;</i></a>
                </div> 
                <form id="editProvedor"  method="post" action="ControllerProveedor" name="frm_edit"> 

                    <div class="modal-body"> 
                        <div class="form-group">
                            <input type="hidden" name="Txtid" value="<%=pr.getIdproveedor()%>" class="form-control" required>
                            <label for="TextCodigo" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Codigo</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"><input type="text" name="TextCodigo" readonly=»readonly»  value="<%=pr.getCodigo()%>" class="form-control" required pattern="[a-zA-ZáéíóúÁÉÍÓÚñÑ-z0-9]+"></span></div></div>
                    </div> 
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="TxtRazonsocial" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Razonsocial</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"><input type="text" name="TxtRazonsocial"  value="<%=pr.getRazonsocial()%>" class="form-control" required pattern="[a-zA-ZáéíóúÁÉÍÓÚñÑ-z0-9]+"></span></div></div>
                    </div>

                    <div class="modal-body">
                        <div class="form-group">
                            <label for="TxtApellido" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Direccion</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"><input type="text" name="TxtDireccion" value="<%=pr.getDireccion()%>" class="form-control" required></span></div></div>
                    </div>

                    <div class="modal-body">
                        <div class="form-group">
                            <label for="TxtCelular" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Celular</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"><input type="text" name="TxtCelular" value="<%=pr.getCelular()%>" onkeypress="return soloNumeros(event)" class="form-control" required pattern="[0-9]{9,9}"  onKeyPress="return SoloNumeros(event);"></span></div></div>
                    </div>

                    <div class="modal-body">
                        <div class="form-group">
                            <label for="TxtEmail" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">E_mail</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"> <input type="text" name="TxtEmail" value="<%=pr.getEmail()%>" class="form-control" required pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}"></span></div></div>
                    </div>



                    <div class="modal-body">    
                        <div class="form-group">
                            <label for="validationDefault01" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Tipo de Doc. de Identidad</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" class="border-focus-darkblue form-control">
                                <select name="TxtIdtipodocumento" id="txtid" class="form-control">
                                    <% TipoDocumentoDAO tdoc = new TipoDocumentoDAO();
                                        List<TipoDocumento> lista = tdoc.ObtenerRUC();
                                        for (TipoDocumento tipoDocumento : lista) {
                                            if (tipoDocumento.getIdtipodocumento() == pr.getIdtipodocumento()) {
                                                out.println("<option   value='" + tipoDocumento.getIdtipodocumento()
                                                        + "'selected>" + tipoDocumento.getTipoDocumento() + "</option>");
                                            } else {
                                                out.println("<option   value='" + tipoDocumento.getIdtipodocumento()
                                                        + "' >" + tipoDocumento.getTipoDocumento() + "</option>");
                                            }

                                        }


                                    %>
                                </select> 
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>

                    <div class="modal-body">
                        <div class="form-group">
                            <label for="TxtNumdocumento"  class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">N° Doc</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"><input required type="text" name="TxtNumdocumento" onkeypress="return soloNumeros(event)" value="<%=pr.getNumdocumento()%>" maxlength="11" class="form-control" required pattern="[0-9]{11,11}"  onKeyPress="return SoloNumeros(event);"><span class="help-block"></span></div></div>
                    </div>

                    <div class="modal-body">
                        <div class="form-group">
                            <label for="Txtfechaderegistro" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Fecha de Registro:</label>
                            <div class="col-lg-7 col-md-8 col-sm-12 col-xs-12"><input type="date" name="TxtFechaRegistro" readonly=»readonly» value="<%=pr.getFechaderegistro()%>" class="form-control"/>
                                <span class="help-block"></span></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-success" name="accion" value="Actualizar">
                    </div>
                </form>
            </div>

            <!--Funciones y validaciones --->
            <!--Funciones y validaciones --->
            <script src="EstiloAdministrador/funcionesyvalidaciones/Proveedor.js" type="text/javascript"></script>

    </body>
</html>
