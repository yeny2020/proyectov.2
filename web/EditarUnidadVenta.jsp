
<%@page import="com.pe.gatasygatos.model.entity.UnidadVenta"%>
<%@page import="com.pe.gatasygatos.DAO.UnidadVentaDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Categoria</title>
    </head>
    <body>
        <!-- add Modal HTML -->
        <div >
            <%UnidadVentaDAO dao = new UnidadVentaDAO();
                int id = Integer.parseInt((String) request.getAttribute("iduv"));
                UnidadVenta m = (UnidadVenta) dao.list(id);
            %>
            <div class="modal-content">
                <div class="modal-header">      
                    <h4 class="modal-title"   id="myModalLabel">Editar UVenta</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div> 
                <form id="edituv"  method="post" action="ControllerUnidadMedida" name="frm_edit"> 

                    
                    <div class="modal-body">  
                        <div class="form-group">
                            <input  type="hidden" type="text"  name="txtid" value="<%=m.getIduventa()%>"readonly="">
                        </div> 
                        <div class="form-group">
                            <label>Codigo</label>
                            <input type="text" class="form-control"  name="txtCod" value="<%=m.getCodigo()%>"readonly="">
                        </div>  
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" class="form-control"  name="txtNom" value="<%=m.getNombre()%>">
                        </div>  
                         <div class="form-group">
                            <label>Factor</label>
                            <input type="number" class="form-control"  name="txtCont" value="<%=m.getContenido()%>">
                        </div>  
                    </div>

                    <div class="modal-footer">
                        <a href="UnidadVenta.jsp" class="btn btn-default" >Cancelar</a> 
                        <input onclick="return valeditarunidadV()" class="btn btn-success" type="submit" name="accion" value="Editar">
                    </div>
                </form>
            </div>
        </div>
    </body>
    <!--Funciones y validaciones --->
    <script src="EstiloAdministrador/funcionesyvalidaciones/Unidadventa.js" type="text/javascript"></script>
    <!--Funciones y validaciones --->
</html>