<%-- 
    Document   : usuarios
    Created on : 14/10/2018, 01:33:19 PM
    Author     : HP
--%>
<%@page import="com.pe.gatasygatos.DAO.TipoDocumentoDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.TipoDocumento"%>
<%@page import="com.pe.gatasygatos.model.entity.Usuario"%>
<%@page import="com.pe.gatasygatos.DAO.UsuarioDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Empleado"%>
<%@page import="com.pe.gatasygatos.DAO.EmpleadoDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>usuario</title>
        <%@include file="css-js.jsp" %>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    <body>
        <!--cabecera de Menu -->
        <%@include file="FrmAdmin.jsp" %>

        <!-- pageContent -->
        <section class="full-width pageContent">

            <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
                <!--modal listar-->
                <div class="mdl-tabs__panel is-active" id="tabListCategory">
                    <div class="mdl-grid">
                        <div class="mdl-cell mdl-cell--2-col-phone mdl-cell--8-col-tablet mdl-cell--10-col-desktop mdl-cell--1-offset-desktop">
                            <div class="full-width panel mdl-shadow--2dp">
                                <div class="full-width panel-tittle bg-info text-center tittles">
                                    LISTA EMPLEADO
                                </div>
                                <div class="full-width panel-content">                        
                                    <div class="col-sm-6 mdl-textfield">   
                                        <a href="#addEmpleado" class="btn " style="background-color:#4e96b3;color: #F7F7F5;" data-toggle="modal">Nevo Trabajador</a>
                                    </div>
                                    <table id="Datatable" class="table table-bordered table-hover projects" >

                                        <thead style="background-color: skyblue;color: black;font-weight: bold">
                                            <tr>
                                                <th class="mdl-data-table__cell--non-numeric">nombre</th>
                                                <th>DNI</th>
                                                <th>Sueldo</th>
                                                <th>Direccion</th>
                                                <th>Telefono</th>
                                                <th>Email</th>
                                                <th>opcion</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%                                               EmpleadoDAO empldao = new EmpleadoDAO();
                                                List<Empleado> list = empldao.ListadoEmpleado();
                                                Iterator<Empleado> iter = list.iterator();
                                                Empleado per = null;
                                                while (iter.hasNext()) {
                                                    per = iter.next();
                                            %>

                                            <tr>
                                                <td><%= per.getNombre()%></td>
                                                <td><%= per.getDni()%></td>
                                                <td>$ <%= per.getSueldo()%></td>
                                                <td><%= per.getDireccion()%></td>
                                                <td><%= per.getTelefono()%></td>
                                                <td><%= per.getEmail()%></td>
                                                <td>
                                                    <a href="Admi_Empleadocontroller?accion=editar&id=<%= per.getId()%>"  class="edit" ><i class="material-icons"  title="Edit">&#xE254;</i></a>
                                                    <a href="Admi_Empleadocontroller?accion=eliminar&id=<%= per.getId()%>" class="delete" ><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
                                                </td>  

                                            </tr>
                                            <%}%>

                                        </tbody>

                                    </table>
                                    <h4 align="center">
                                    </h4>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>                                                             
            </div>
        </section>
                        
        <!-- add Modal HTML -->
        <div id="addEmpleado" class="modal fade">
            <div class="modal-dialog" role="document" style="z-index: 10000; width: 750px">
                <div class="modal-content">
                    <form>
                        <div class="modal-header">      
                            <h4 class="modal-title">Agregar Empleado</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">     
                            <div class="form-group">
                                <label >Nombre</label>
                                <input type="text" name="txtnombre" class="form-control" placeholder="Ingrese nombre completo" required>
                            </div>
                            <div class="form-group">
                                 <label for="Txtidtipodocumento" class="col-lg-2">Tip Doc:</label>
                                <div class="col-lg-4">
                                      <select name="Txtidtipodocumento" id="Txtidtipodocumento" class="form-control">
                                            <option   value=""disabled="" selected="">Tipo Documento</option>
                                            <% TipoDocumentoDAO tdoc = new TipoDocumentoDAO();
                                                List<TipoDocumento> lista = tdoc.listarTipodocumento();
                                                Iterator<TipoDocumento> ite = lista.iterator();
                                                TipoDocumento doc = null;
                                                while (ite.hasNext()) {
                                                    doc = ite.next();

                                            %>

                                            <option  value="<%=doc.getIdtipodocumento()%>" required><%=doc.getTipoDocumento()%></option>
                                            <%
                                                }
                                            %>
                                        </select>
                                </div>
                            </div>
                            <div class="form-group">                                     
                                <label for="txtdni" class="col-lg-2" >N° Doc: </label>
                                <div class="col-lg-4"><input type="text" name="txtdni" placeholder="Ingrese N° Documento" id="txtdni" class="border-focus-darkblue form-control"><span class="help-block"></span></div>
                            </div>
                            <div class="form-group">                                     
                                <label for="txtsueldo" class="col-lg-2" >Sueldo: </label>
                                <div class="col-lg-4"><input type="text" name="txtsueldo" placeholder="Ingrese Sueldo" id="txtsueldo" class="border-focus-darkblue form-control"><span class="help-block"></span></div>
                            </div>
                            <div class="form-group">                                     
                                <label for="txttelefono" class="col-lg-2" >Celular: </label>
                                <div class="col-lg-4"><input type="text" name="txttelefono" placeholder="Ingrese N° Celular" id="txtsueldo" class="border-focus-darkblue form-control"><span class="help-block"></span></div>
                            </div>
                            <div class="form-group">
                                <label >Direccion</label>
                                <input type="text" name="txtdireccion" class="form-control" placeholder="Ingrese Direccion" required>
                            </div>
                            <div class="form-group">
                                <label >Email</label>

                                <input type="email" name="txtemail" class="form-control" placeholder="Ingrese Email" required>
                            </div>     
                        </div>
                        <div class="modal-footer">
                            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                            <input type="submit" name="accion" class="btn btn-success" value="agregar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="EstiloAdministrador/js/ValidadForm.js"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>                                
    </body>
    <script type="text/javascript">

        $(document).ready(function () {
            $('#Datatable').DataTable();
        });
    </script>
</html>