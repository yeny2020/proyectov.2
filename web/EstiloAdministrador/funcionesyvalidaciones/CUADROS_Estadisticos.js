/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var ctx = document.getElementById('myChart').getContext('2d');
        var chart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                datasets: [{
                        label: 'Ingresos',
                        backgroundColor: '#42a5f5',
                        borderColor: 'gray',
                        data: [7, 8, 5, 2, 8, 10, 7, -7, 4, 9, -8, 5]
                    }, {
                        label: 'Gastos',
                        backgroundColor: '#ffab91',
                        borderColor: 'yellow',
                        data: [5, -8, 10, 3, -7, 6, 8, -2, -6, 9, -7, 2]
                    }
                ]},
            options: {responsive: true}
        });
        var ctx1 = document.getElementById('myChart1').getContext('2d');
        var chart = new Chart(ctx1, {
            type: 'line',
            data: {
                labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                datasets: [{
                        label: 'Ganancias',
                        backgroundColor: '#42a5f5',
                        borderColor: 'gray',
                        data: [7, 8, 5, 2, 8, 10, 7, -7, 4, 9, -8, 5]
                    }
                ]},
            options: {}
        });
        var ctx2 = document.getElementById('myChart2').getContext('2d');
        var chart = new Chart(ctx2, {
            type: 'doughnut',
            data:
                    {
                        datasets: [{
                                data: [60, 18, 10, 8, 4],
                                backgroundColor: ['#42a5f5', 'red', 'green', 'blue', 'violet'],
                                label: 'Comparacion de navegadores'
                            }],
                        labels: [
                            'Google Chrome',
                            'Safari',
                            'Edge',
                            'Firefox',
                            'Opera'
                        ]},
            options: {}
        });
