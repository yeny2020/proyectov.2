$(document).ready(function () {

    var now = new Date();

    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);

    var today = now.getFullYear() + "-" + (month) + "-" + (day);
    $("#fecha").val(today);
});

function comprobarRadio(radio)
{
    for (i = 0; i < radio.length; i++)
    {
        if (radio[i].checked)
        {
            return true;
        }
    }
    return false;
}

function soloLetras(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
    especiales = [8, 37, 39, 46];

    tecla_especial = false
    for (var i in especiales) {
        if (key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial)
        return false;
}

function soloNumeros(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = "0123456789";
    especiales = [8, 37, 39, 46];

    tecla_especial = false
    for (var i in especiales) {
        if (key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial)
        return false;
}


//Valiaciones de campos nuevo cliente
function validarnewcliente() {
    debugger
    var nombre = document.frm_nuevo.Txtapellidos.value;
    var tipodoc = document.frm_nuevo.Txtidtipodocumento.value;
    var documento = document.frm_nuevo.Txtnumerodocumento.value;
    var correo = document.frm_nuevo.Txtcorreo.value;
    var celular = document.frm_nuevo.Txtcelular.value;
    var dir = document.frm_nuevo.Txtdireccion.value;
    if (nombre === '') {
        swal("Ingrese Nombre!", {
            icon: "warning"
        });
        return false;
    }

    if (tipodoc === "4") {
        if (documento.length !== 11) {
            swal("falta caracteres  o demasiados caracteres RUC", {
                icon: "warning"
            });
            return false;
        }
    }
    if (tipodoc === "2") {
        if (documento.length !== 8) {
            swal("falta caracteres  o demasiados caracteres DNI", {
                icon: "warning"
            });
            return false;
        }
    }
    if (nombre === '') {
        swal("Ingrese Nombre!", {
            icon: "warning"
        });
    } else if (nombre.length > 50)
    {
        swal("Demasiados caracteres", {
            icon: "warning"
        });
    } else if (tipodoc === "" || tipodoc === null)
    {
        debugger;
        swal("seleciona el tipo de documento", {
            icon: "warning"
        });
    } else if (documento === '') {
        swal("Ingrese nro de doc", {
            icon: "warning"
        });
    } else if (correo === '') {
        swal("Ingrese correo", {
            icon: "warning"
        });
    } else if (!/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(correo)) {
        swal("Ingrese correo válido", {
            icon: "warning"
        });
    } else if (celular === '') {
        swal("Ingrese nro", {
            icon: "warning"
        });
    } else if (!dir === '') {
        swal("Ingrese direccion", {
            icon: "warning"
        });
    } else if (!comprobarRadio(document.frm_nuevo.Txtsexo)) {
        swal("Seleccione el genero  ", {
            icon: "warning"
        });
        return false;
    }
}


//Valiaciones de campos editar cliente
function validareditclientes() {
    var nombre = document.frm_edit.Txtapellidos.value;
    var tipodoc = document.frm_edit.Txtidtipodocumento.value;
    var documento = document.frm_edit.Txtnumerodocumento.value;
    var correo = document.frm_edit.Txtcorreo.value;
    var celular = document.frm_edit.Txtcelular.value;
    var dir = document.frm_edit.Txtdireccion.value;
    if (nombre === '') {
        swal("Ingrese Nombre!", {
            icon: "warning"
        });
        return false;
    }

    if (tipodoc === "4") {
        if (documento.length !== 11) {
            swal("falta caracteres  o demasiados caracteres RUC", {
                icon: "warning"
            });
            return false;
        }
    }
    if (tipodoc === "2") {
        if (documento.length !== 8) {
            swal("falta caracteres  o demasiados caracteres DNI", {
                icon: "warning"
            });
            return false;
        }
    }
    if (nombre === '') {
        swal("Ingrese Nombre!", {
            icon: "warning"
        });
    } else if (nombre.length > 50)
    {
        swal("Demasiados caracteres", {
            icon: "warning"
        });
    } else if (tipodoc === "" || tipodoc === null)
    {
        debugger;
        swal("seleciona el tipo de documento", {
            icon: "warning"
        });
    } else if (documento === '') {
        swal("Ingrese nro de doc", {
            icon: "warning"
        });
    } else if (correo === '') {
        swal("Ingrese correo", {
            icon: "warning"
        });
    } else if (!/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(correo)) {
        swal("Ingrese correo válido", {
            icon: "warning"
        });
    } else if (celular === '') {
        swal("Ingrese nro", {
            icon: "warning"
        });
    } else if (!dir === '') {
        swal("Ingrese direccion", {
            icon: "warning"
        });
    } else if (!comprobarRadio(document.frm_nuevo.Txtsexo)) {
        swal("Seleccione el genero  ", {
            icon: "warning"
        });
        return false;
    }
}
//sweetalert para guardar nuevo
$(function () {
    $("#newcliente").on("submit", function (e) {
        e.preventDefault();
        var data = $('#newcliente').serialize();
        $.post("ControllerCliente?accion=add", data, function (res, est, jqXHR) {
            if (res === "ok") {
                swal("¡Registro Exitoso! ", "¡ Haz clic en OK para Continuar! ", " éxito ", {
                    icon: "success"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "Cliente.jsp";
                            }

                        });
                ;
            } else {

                swal("¡ no! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "warning"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "Cliente.jsp";
                            }

                        });
                ;
            }
        });
    });
});
//sweetalert para Editar
$(function () {
    $("#editcliente").on("submit", function (e) {
        e.preventDefault();
        var data = $('#editcliente').serialize();
        $.post("ControllerCliente?accion=Actualizar", data, function (res, est, jqXHR) {
            if (res === "ok") {
                swal("¡Se actualizo! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "success"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "Cliente.jsp";
                            }

                        });
                ;
            } else {

                swal("¡ no se actualizo! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "warning"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "EditarCliente.jsp";
                            }

                        });
                ;
            }
        });
    });
});

//Eliminar
$(function () {
    $('tr #btn-eliminar').click(function (e) {
        e.preventDefault();

        swal({
            title: "Estas seguro de eliminar?",
            text: "Una vez eliminado no podrás recuperar el registro!",
            icon: "warning",
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
                .then((willDelete) => {
                    if (willDelete) {

                        //if (opcion) {
                        var fila = $(this).parent().parent();
                        console.log(fila);
                        var idcli = fila.find('#idcliet').text();
                        console.log(idcli);
                        var data = {"accion": "eliminar", idCli: idcli};
                        $.post("ControllerCliente", data, function (res, est, jqXHR) {
                            //console.log('res ',res);


                            //console.log(est);  
                            if (jqXHR.status === 200) {
                                //success
                                //info
                                ///error
                                swal("Registro eliminado correctamente!", {
                                    icon: "success"
                                }).then((willDelete) => {
                                    if (willDelete) {
                                        parent.location.href = "Cliente.jsp";
                                    }

                                });
                                ;
                            }
                            console.log(jqXHR);
                            fila.remove();
                        });
                        // }
                    } else {
                        swal("Your imaginary file is safe!");
                    }
                });
    });

});
//Confirmacion de cambio de estado
$(function () {
    $('tr #btn-estado').click(function (e) {
        e.preventDefault();

        swal({
            title: "Estas seguro de cambiar el estado?",
            text: "Ya nose vizualizara en registrar producto!",
            icon: "warning",
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
                .then((willDelete) => {
                    if (willDelete) {

                        //if (opcion) {
                        var fila = $(this).parent().parent();
                        console.log(fila);
                        var idcat = fila.find('#idcliet').text();
                        console.log(idcat);
                        var data = {"accion": "Estado", id: idcat};
                        $.post("ControllerCliente", data, function (res, est, jqXHR) {
                            //console.log('res ',res);


                            //console.log(est);  
                            if (jqXHR.status === 200) {
                                //success
                                //info
                                ///error
                                swal("Cambio de estado correcto!", {
                                    icon: "success"
                                }).then((willDelete) => {
                                    if (willDelete) {
                                        parent.location.href = "Cliente.jsp";
                                    }
                                });
                                ;
                            }
                        });
                        // }
                    } else {
                        swal("Your imaginary file is safe!");
                    }
                });
    });

});

