//scroll de la tabla y datatable
$(document).ready(function () {
    $('#dtHorizontalVerticalExample').DataTable({
        "scrollX": true,
        "scrollY": 150,
    });
    $('.dataTables_length').addClass('bs-select');
});

//eliminar carrito
$(function () {
    $('tr #deleteitem').click(function (e) {
        e.preventDefault();
        var elemento = $(this);
        var idproducto = elemento.parent().find('#idarticulo').text();
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this imaginary file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                        eliminar(idproducto);
                        swal("Poof! Your imaginary file has been deleted!", {
                            icon: "success",
                        }).then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "ControllerCompra?accion=procesarCarrito";
                            }

                        });
                    } else {
                        swal("Your imaginary file is safe!");
                    }
                });

        function eliminar(idproducto) {
            $.ajax({
                url: 'Deleteitemcompra',
                type: 'post',
                data: {idproducto: idproducto},
                success: function (data, textStatus, jqXHR) {
                }
            });
        }
    });


});


//actualizar cantidad

function Actualizarcantidad(idproducto) {

    var cantidad = $("#txt_cantidad" + idproducto).val();
    window.location.href = "ControllerCompra?accion=Actualizarcantidad&cant=" + cantidad + "&idproducto=" + idproducto;
}

//actualizar cantidad

function actualizarContenido(idproducto) {

    var contenido = $("#txtPro_contenido" + idproducto).val();
    window.location.href = "ControllerCompra?accion=actualizarContenido&cant=" + contenido + "&idproducto=" + idproducto;
}
//actualizar ´Costo

function actualizarpreciocompra(idproducto) {

    var pcompra = $("#pcompra" + idproducto).val();
    window.location.href = "ControllerCompra?accion=actualizarpreciocompra&cant=" + pcompra + "&idproducto=" + idproducto;
}

//Buscador en combobox
// In your Javascript (external .js resource or <script> tag)
$(document).ready(function () {
    $('.js-example-basic-single').select2();
});
//funcion guardar
$(function () {
    $("#newventa").on("submit", function (e) {
        e.preventDefault();
        var data = $('#newventa').serialize();
        $.post("ControllerCompra?accion=RegistrarCompra", data, function (res, est, jqXHR) {
            if (res === "oki") {
                swal("¡Se registro  con exito! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "success"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "Cotizacion.jsp";
                            }

                        });
                ;
            } else {

                swal("¡No se inserto ! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "warning"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "InsertarCompra.jsp";
                            }

                        });
                ;
            }
        });
    });
});




