
//funcion guardar
$(function () {
    $("#newFacturaCompra").on("submit", function (e) {
        e.preventDefault();
        var data = $('#newFacturaCompra').serialize();
        $.post("ControllerFacturaCompra?accion=RegistrarFacturaCompra", data, function (res, est, jqXHR) {
            if (res === "oki") {
                swal("¡Se registro de Factura Compra con exito! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "success"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "FacturaCompra.jsp";
                            }

                        });
                ;
            } else {

                swal("¡No se inserto la venta! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "warning"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "ReferenciarFacturaCompra.jsp";
                            }

                        });
                ;
            }
        });
    });
});
