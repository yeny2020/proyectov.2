/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//funcion guardar
$(function () {
    $("#newGuiaRemison").on("submit", function (e) {
        e.preventDefault();
        var data = $('#newGuiaRemison').serialize();
        $.post("ControllerGuiaRemison?accion=RegistrarGuiaRemison", data, function (res, est, jqXHR) {
            if (res === "oki") {
                swal("¡Se registro de Guia Remision con exito! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "success"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "GuiaRemision.jsp";
                            }

                        });
                ;
            } else {

                swal("¡No se inserto la venta! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "warning"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "ReferenciarGuiaRemision.jsp";
                            }

                        });
                ;
            }
        });
    });
});
