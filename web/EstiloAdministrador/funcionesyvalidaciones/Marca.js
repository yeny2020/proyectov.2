//Tablas pequeñas
$(document).ready(function () {
    $('#tablap').DataTable();
});
//Valiaciones de campos Guardar
function validar() {
    var nombre = document.frm_nuevo.txtNom.value;
    if (nombre === '') {
        swal("Ingrese Nombre!", {
            icon: "warning"
        });
        return false;
    } else if (nombre.length > 14) {
        swal("Error demaciados caracteres!", {
            icon: "warning"
        });
        return false;
    }
}
////Valiaciones de campos Editar
function valedit() {
    var nombre = document.frm_edit.Txtnombre.value;
    if (nombre === '') {
        swal("Ingrese Nombre!", {
            icon: "warning"
        });
        return false;
    } else if (nombre.length > 14) {
        swal("Error demaciados caracteres!", {
            icon: "warning"
        });
        return false;
    }
}
//sweetalert para Editar
$(function () {
    $("#editmarca").on("submit", function (e) {
        e.preventDefault();
        var data = $('#editmarca').serialize();
        $.post("ControllerMarca?accion=Actualizar", data, function (res, est, jqXHR) {
            if (res === "ok") {
                swal("¡Se actualizo! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "success"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "Marca.jsp";
                            }

                        });
                ;
            } else {

                swal("¡ no se actualizo! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "warning"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "Marca.jsp";
                            }

                        });
                ;
            }
        });
    });
});
//sweetalert para guardar
$(function () {
    $("#newmarca").on("submit", function (e) {
        e.preventDefault();
        var data = $('#newmarca').serialize();
        $.post("ControllerMarca?accion=add", data, function (res, est, jqXHR) {
            if (res === "ok") {
                swal("¡ Buen trabajo! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "success"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "Marca.jsp";
                            }

                        });
                ;
            } else {

                swal("¡ Ya existe la marca, ingrese uno nuevo! ", "¡ Haz clic en OK! ", " éxito ", {
                    icon: "warning"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "Marca.jsp";
                            }

                        });
                ;
            }
        });
    });
});
//Eliminar
$(function () {
    $('tr #btn-eliminar').click(function (e) {
        e.preventDefault();

        swal({
            title: "Estas seguro de eliminar?",
            text: "Una vez eliminado no podrás recuperar el registro!",
            icon: "warning",
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
                .then((willDelete) => {
                    if (willDelete) {

                        //if (opcion) {
                        var fila = $(this).parent().parent();
                        console.log(fila);
                        var idmar = fila.find('#idmar').text();
                        console.log(idmar);
                        var data = {"accion": "eliminar", idMar: idmar};
                        $.post("ControllerMarca", data, function (res, est, jqXHR) {
                            //console.log('res ',res);


                            //console.log(est);  
                            if (jqXHR.status === 200) {
                                //success
                                //info
                                ///error
                                swal("Registro eliminado correctamente!", {
                                    icon: "success"
                                }).then((willDelete) => {
                                    if (willDelete) {
                                        parent.location.href = "Marca.jsp";
                                    }

                                });;
                            }
                            console.log(jqXHR);
                            fila.remove();
                        });
                        // }
                    } else {
                        swal("Your imaginary file is safe!");
                    }
                });
    });

});
//Confirmacion de cambio de estado
$(function () {
    $('tr #btn-estado').click(function (e) {
        e.preventDefault();

        swal({
            title: "Estas seguro de cambiar el estado?",
            text: "Ya nose vizualizara en registrar producto!",
            icon: "warning",
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
                .then((willDelete) => {
                    if (willDelete) {

                        //if (opcion) {
                        var fila = $(this).parent().parent();
                        console.log(fila);
                        var idmar = fila.find('#idmar').text();
                        console.log(idmar);
                        var data = {"accion": "Estado", id: idmar};
                        $.post("ControllerMarca", data, function (res, est, jqXHR) {
                            //console.log('res ',res);


                            //console.log(est);  
                            if (jqXHR.status === 200) {
                                //success
                                //info
                                ///error
                                swal("Cambio de estado correcto!", {
                                    icon: "success"
                                }).then((willDelete) => {
                                    if (willDelete) {
                                        parent.location.href = "Marca.jsp";
                                    }
                                });;
                            }
                        });
                        // }
                    } else {
                        swal("Your imaginary file is safe!");
                    }
                });
    });

});
