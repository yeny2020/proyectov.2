//Data table
$(document).ready(function () {
    $('#Datatable').DataTable();
});

//Valiaciones de campos nueva categoria
function validarmotivo() {
    var nombre = document.frm_nuevo.txtNom.value;
    if (nombre === '') {
        swal("Ingrese Nombre!", {
            icon: "warning"
        });
        return false;
    } else if (nombre.length > 14) {
        swal("Error demaciados caracteres!", {
            icon: "warning"
        });
        return false;
    }
}
////Valiaciones de campos Editar
function validareditmotivo() {
    var nombre = document.frm_edit.txtNom.value;
    if (nombre === '') {
        swal("Ingrese Nombre!", {
            icon: "warning"
        });
        return false;
    } else if (nombre.length > 14) {
        swal("Error demaciados caracteres!", {
            icon: "warning"
        });
        return false;
    }
}
//sweetalert para Editar
$(function () {
    $("#editmotivo").on("submit", function (e) {
        e.preventDefault();
        var data = $('#editmotivo').serialize();
        $.post("ControllerMotivo?accion=Actualizar", data, function (res, est, jqXHR) {
            if (res === "ok") {
                swal("¡Se actualizo correctamente! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "success"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "Motivo.jsp";
                            }

                        });
                ;
            } else {

                swal("¡ no se actualizo! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "warning"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "Motivo.jsp";
                            }

                        });
                ;
            }
        });
    });
});
//sweetalert para guardar nuevo
$(function () {
    $("#newmotivo").on("submit", function (e) {
        e.preventDefault();
        var data = $('#newmotivo').serialize();
        $.post("ControllerMotivo?accion=add", data, function (res, est, jqXHR) {
            if (res === "ok") {
                swal("¡ Buen trabajo! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "success"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "Motivo.jsp";
                            }

                        });
                ;
            } else {
                swal("¡Ya existe el almacen! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "warning"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "Motivo.jsp";
                            }
                        });
                ;
            }
        });
    });
});
//Eliminar
$(function () {
    $('tr #btn-eliminar').click(function (e) {
        e.preventDefault();

        swal({
            title: "Estas seguro de eliminar?",
            text: "Una vez eliminado no podrás recuperar el registro!",
            icon: "warning",
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
                .then((willDelete) => {
                    if (willDelete) {

                        //if (opcion) {
                        var fila = $(this).parent().parent();
                        console.log(fila);
                        var idmot = fila.find('#idmot').text();
                        console.log(idmot);
                        var data = {"accion": "eliminar", idMot: idmot};
                        $.post("ControllerMotivo", data, function (res, est, jqXHR) {
                            //console.log('res ',res);


                            //console.log(est);  
                            if (jqXHR.status === 200) {
                                //success
                                //info
                                ///error
                                swal("Registro eliminado correctamente!", {
                                    icon: "success"
                                }).then((willDelete) => {
                                    if (willDelete) {
                                        parent.location.href = "Motivo.jsp";
                                    }

                                });;
                            }
                            console.log(jqXHR);
                            fila.remove();
                        });
                        // }
                    } else {
                        swal("Your imaginary file is safe!");
                    }
                });
    });

});

//Confirmacion de cambio de estado
$(function () {
    $('tr #btn-estado').click(function (e) {
        e.preventDefault();

        swal({
            title: "Estas seguro de cambiar el estado?",
            text: "Ya nose vizualizara en registrar producto!",
            icon: "warning",
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
                .then((willDelete) => {
                    if (willDelete) {

                        //if (opcion) {
                        var fila = $(this).parent().parent();
                        console.log(fila);
                        var idcat = fila.find('#idmot').text();
                        console.log(idcat);
                        var data = {"accion": "Estado", id: idcat};
                        $.post("ControllerMotivo", data, function (res, est, jqXHR) {
                            //console.log('res ',res);


                            //console.log(est);  
                            if (jqXHR.status === 200) {
                                //success
                                //info
                                ///error
                                swal("Cambio de estado correcto!", {
                                    icon: "success"
                                }).then((willDelete) => {
                                    if (willDelete) {
                                        parent.location.href = "Motivo.jsp";
                                    }
                                });;
                            }
                        });
                        // }
                    } else {
                        swal("Your imaginary file is safe!");
                    }
                });
    });

});



