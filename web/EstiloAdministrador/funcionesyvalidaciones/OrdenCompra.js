//scroll de la tabla y datatable
$(document).ready(function () {
    $('#dtHorizontalVerticalExample').DataTable({
        "scrollX": true,
        "scrollY": 185,
    });
    $('.dataTables_length').addClass('bs-select');
});
//funcion guardar
$(function () {
    $("#neworden").on("submit", function (e) {
        e.preventDefault();
        var data = $('#neworden').serialize();
        $.post("ControllerOrdenCompras?accion=RegistrarOrdenCompra", data, function (res, est, jqXHR) {
            if (res === "oki") {
                swal("¡Se registro Orden de compra con exito! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "success"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "OrdenCompra.jsp";
                            }

                        });
                ;
            } else {

                swal("¡No se inserto la orden! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "warning"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "ReferenciarOrden.jsp";
                            }

                        });
                ;
            }
        });
    });
});

    //actualizar cantidad
function Actualizarcant(idproducto) {

    var cantidad = $("#txt_cantidad" + idproducto).val();
    window.location.href = "ControllerOrdenCompras?accion=Actualizarcan&cant=" + cantidad + "&idproducto=" + idproducto;
}
//actualizar cantidad
function actualizarContenido(idproducto) {

    var contenido = $("#txtPro_contenido" + idproducto).val();
    window.location.href = "ControllerOrdenCompras?accion=actualizarContenido&cant=" + contenido + "&idproducto=" + idproducto;
}
//actualizar ´Costo

function actualizarpreciocompra(idproducto) {

    var pcompra = $("#pcompra" + idproducto).val();
    window.location.href = "ControllerOrdenCompras?accion=actualizarpreciocompra&cant=" + pcompra + "&idproducto=" + idproducto;
}
