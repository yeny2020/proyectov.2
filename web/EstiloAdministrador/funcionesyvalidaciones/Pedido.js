//actualizar cantidad
function actualizarcan(idproducto) {
    var cantidad = $("#cantidad" + idproducto).val();
    window.location.href = "VControllergestionarpedido?accion=Actualizarcantidad&cant=" + cantidad + "&idproducto=" + idproducto;
}
//eliminar carrito
$(function () {
    $('tr #deleteitem').click(function (e) {
        e.preventDefault();
        var elemento = $(this);
        var idproducto = elemento.parent().find('#idarticulo').text();
        swal({
            title: "Seguro que quieres quitar de la lista?",
            text: "...",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                        eliminar(idproducto);
                        swal("Se elimino el producto de la lista!", {
                            icon: "success",
                        }).then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "VControllergestionarpedido?accion=procesarCarrito";
                            }

                        });
                    } else {
                        swal("Your imaginary file is safe!");
                    }
                });

        function eliminar(idproducto) {
            $.ajax({
                url: 'VDeleteitempedido',
                type: 'post',
                data: {idproducto: idproducto},
                success: function (data, textStatus, jqXHR) {
                }
            });
        }
    });


});

//funcion guardar
$(function () {
    $("#newmovimiento").on("submit", function (e) {
        e.preventDefault();
        var data = $('#newmovimiento').serialize();
        $.post("VControllergestionarpedido?accion=RegistrarMovimiento", data, function (res, est, jqXHR) {
            if (res === "oki") {
                swal("¡Se registro con exito! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "success"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "VInsertarpedido.jsp";
                            }

                        });
                ;
            } else {

                swal("¡No se inserto Verificar! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "warning"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "VInsertarpedido.jsp";
                            }

                        });
                ;
            }
        });
    });
});



