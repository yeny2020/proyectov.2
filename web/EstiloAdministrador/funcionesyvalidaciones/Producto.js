/* global swal */

function comprobarRadio(radio)
{
  for(i = 0;i < radio.length;i++)
  {
    if(radio[i].checked)
    {
      return true;
    }
  }
  return false;
}
function soloLetras(e) {
      key = e.keyCode || e.which;
      tecla = String.fromCharCode(key).toLowerCase();
      letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
      especiales = [8, 37, 39, 46];
  
      tecla_especial === false;
      for(var i in especiales) {
          if(key === especiales[i]) {
              tecla_especial = true;
              break;
          }
      }
  
      if(letras.indexOf(tecla) === -1 && !tecla_especial)
          return false;
  }
  
 function soloNumeros(e) {
      key = e.keyCode || e.which;
      tecla = String.fromCharCode(key).toLowerCase();
      letras = "0123456789";
      especiales = [8, 37, 39, 46];
  
      tecla_especial === false;
      for(var i in especiales) {
          if(key === especiales[i]) {
              tecla_especial = true;
              break;
          }
      }
  
      if(letras.indexOf(tecla) === -1 && !tecla_especial)
          return false;
  }
  
  
//Valiaciones de campos nuevo cliente
function validarnewproducto() {
    var descrp = document.frm_nuevo.Txtdescripcion.value;
    var cate = document.frm_nuevo.Txtcategoria.value;
    var marca = document.frm_nuevo.Txtmarca.value;
    var publ = document.frm_nuevo.TxtIdpublico.value;
    var provd = document.frm_nuevo.Txtproveedor.value;
    var unid = document.frm_nuevo.Txtunidadc.value;
    var unidv = document.frm_nuevo.Txtunidadv.value;
    if (descrp === '' ) {
        swal("Ingrese la Descripcion!", {
            icon: "warning"
        });
        } 
    else if(descrp.length > 100)
         {
        swal("Demasiados caracteres", {
            icon: "warning"
        });
        } 
    else if(cate === "" || cate === null)
         {
        swal("selecione una categoria!", {
            icon: "warning"
        });
        } 
    else if(marca === "" || marca === null)
         {
        swal("selecione una marca!", {
            icon: "warning"
        });
        } 
    else if(publ === "" || publ === null)
         {
        swal("selecione un publico!", {
            icon: "warning"
        });
        } 
    else if(provd === "" || provd === null)
         {
        swal("selecione un proveedor!", {
            icon: "warning"
        });
        } 
    else if(unid === "" || unid === null)
         {
        swal("selecione unidad de compra!", {
            icon: "warning"
        });
        } 
    else if(unidv === "" || unidv === null)
         {
        swal("selecione unidad de venta!", {
            icon: "warning"
        });
       return false;
       } 
  }


//Valiaciones de campos editar cliente
function validareditproductos() {    
    var descrp = document.frm_nuevo.Txtdescripcion.value;
    var cate = document.frm_nuevo.Txtcategoria.value;
    var marca = document.frm_nuevo.Txtmarca.value;
    var publ = document.frm_nuevo.TxtIdpublico.value;
    var provd = document.frm_nuevo.Txtproveedor.value;
    var unid = document.frm_nuevo.Txtunidadc.value;
    var unidv = document.frm_nuevo.Txtunidadv.value;
    if (descrp === '' ) {
        swal("Ingrese la Descripcion!", {
            icon: "warning"
        });
        } 
    else if(descrp.length > 100)
         {
        swal("Demasiados caracteres", {
            icon: "warning"
        });
        } 
    else if(cate === "" || cate === null)
         {
        swal("selecione una categoria!", {
            icon: "warning"
        });
        } 
    else if(marca === "" || marca === null)
         {
        swal("selecione una marca!", {
            icon: "warning"
        });
        } 
    else if(publ === "" || publ === null)
         {
        swal("selecione un publico!", {
            icon: "warning"
        });
        } 
    else if(provd === "" || provd === null)
         {
        swal("selecione un proveedor!", {
            icon: "warning"
        });
        } 
    else if(unid === "" || unid === null)
         {
        swal("selecione unidad de compra!", {
            icon: "warning"
        });
        } 
    else if(unidv === "" || unidv === null)
         {
        swal("selecione unidad de venta!", {
            icon: "warning"
        });
       return false;
       }
   }

//sweetalert para Editar
$(function () {
    $("#editproducto").on("submit", function (e) {
        e.preventDefault();
        var data = $('#editproducto').serialize();
        $.post("ControllerProducto?accion=actualizar", data, function (res, est, jqXHR) {
            if (res === "ok") {
                swal("¡Se actualizo! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "success"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "Producto.jsp";
                            }

                        });
                ;
            } else {

                swal("¡ no se actualizo! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "warning"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "EditarProducto.jsp";
                            }

                        });
                ;
            }
        });
    });
});
//sweetalert para guardar nuevo
$(function () {
    $("#newproducto").on("submit", function (e) {
        e.preventDefault();
        var data = $('#newproducto').serialize();
        $.post("ControllerProducto?accion=add", data, function (res, est, jqXHR) {
            if (res === "ok") {
                swal("¡ Registro exitoso! ", "¡ Hiciste clic en OK para continuar! ", " éxito ", {
                    icon: "success"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "Producto.jsp";
                            }

                        });
                ;
            } else {

                swal("¡ Upps algo salio mal! ", "¡ Revise la informacion y vuelva a intentarlo! ", " éxito ", {
                    icon: "warning"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "InsertarProducto.jsp";
                            }

                        });
                ;
            }
        });
    });
});
//Eliminar
$(function () {
    $('tr #btn-eliminar').click(function (e) {
        e.preventDefault();

        swal({
            title: "Estas seguro de eliminar?",
            text: "Una vez eliminado no podrás recuperar el registro!",
            icon: "warning",
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
                .then((willDelete) => {
                    if (willDelete) {

                        //if (opcion) {
                        var fila = $(this).parent().parent();
                        console.log(fila);
                        var idpro = fila.find('#idpro').text();
                        console.log(idpro);
                        var data = {"accion": "eliminar", idPro: idpro};
                        $.post("ControllerProducto", data, function (res, est, jqXHR) {
                            //console.log('res ',res);


                            //console.log(est);  
                            if (jqXHR.status === 200) {
                                //success
                                //info
                                ///error
                                swal("Registro eliminado correctamente!", {
                                    icon: "success"
                                }).then((willDelete) => {
                                    if (willDelete) {
                                        parent.location.href = "Producto.jsp";
                                    }

                                });
                                ;
                            }
                            console.log(jqXHR);
                            fila.remove();
                        });
                        // }
                    } else {
                        swal("Your imaginary file is safe!");
                    }
                });
    });

});
//Confirmacion de cambio de estado
$(function () {
    $('tr #btn-estado').click(function (e) {
        e.preventDefault();

        swal({
            title: "Estas seguro de cambiar el estado?",
            text: "Ya nose vizualizara en registrar producto!",
            icon: "warning",
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
                .then((willDelete) => {
                    if (willDelete) {

                        //if (opcion) {
                        var fila = $(this).parent().parent();
                        console.log(fila);
                        var idpro = fila.find('#idpro').text();
                        console.log(idpro);
                        var data = {"accion": "estado", id: idpro};
                        $.post("ControllerProducto", data, function (res, est, jqXHR) {
                            //console.log('res ',res);


                            //console.log(est);  
                            if (jqXHR.status === 200) {
                                //success
                                //info
                                ///error
                                swal("Cambio de estado correcto!", {
                                    icon: "success"
                                }).then((willDelete) => {
                                    if (willDelete) {
                                        parent.location.href = "Producto.jsp";
                                    }
                                });
                                ;
                            }
                        });
                        // }
                    } else {
                        swal("Your imaginary file is safe!");
                    }
                });
    });

});






