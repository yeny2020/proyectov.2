/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//Hora y fecha actual
$(document).ready(function () {

    var now = new Date();

    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);

    var today = now.getFullYear() + "-" + (month) + "-" + (day);
    $("#fecha").val(today);
});

function comprobarRadio(radio)
{
    for (i = 0; i < radio.length; i++)
    {
        if (radio[i].checked)
        {
            return true;
        }
    }
    return false;
}
function soloLetras(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
    especiales = [8, 37, 39, 46];

    tecla_especial = false
    for (var i in especiales) {
        if (key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial)
        return false;
}

function soloNumeros(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = "01234567899";
    especiales = [8, 37, 39, 46];

    tecla_especial = false
    for (var i in especiales) {
        if (key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial)
        return false;
}


//Valiaciones de campos Proveedor
function validarnewProveedor() {
    var razoncial = document.frm_nuevo.TxtRazonsocial.value;
    var direccion = document.frm_nuevo.TxtDireccion.value;
    var celular = document.frm_nuevo.TxtCelular.value;
    var correo = document.frm_nuevo.TxtEmail.value;
    var documento = document.frm_nuevo.TxtIdtipodocumento.value;
    var ruc = document.frm_nuevo.TxtNumdocumento.value;
    
    if (razoncial === '') {
        swal("Ingrese Razoncial!", {
            icon: "warning"
        });
    } else if (razoncial.length > 50)
    {
        swal("Demasiados caracteres", {
            icon: "warning"
        });
    }
    else if (!direccion === '') {
        swal("Ingrese direccion", {
            icon: "warning"
        });
    }
    else if (celular === '') {
        swal("Ingrese celular", {
            icon: "warning"
        });
    }
    
    else if (correo === '') {
        swal("Ingrese correo", {
            icon: "warning"
        });
    } else if (!/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(correo)) {
        swal("Ingrese correo válido", {
            icon: "warning"
        });
    }
    
    else if (documento === "" || documento === null)
    {
        swal("seleciona el tipo de documento", {
            icon: "warning"
        });
    } else if (ruc === '') {
        swal("Ingrese nro de doc", {
            icon: "warning"
        });
    }    
        
    
}

//sweetalert para guardar nuevo
$(function () {
    $("#newproveedor").on("submit", function (e) {
        e.preventDefault();
        var data = $('#newproveedor').serialize();
        $.post("ControllerProveedor?accion=add", data, function (res, est, jqXHR) {
            if (res === "ok") {
                swal("¡Registro exitoso de proveedor! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "success"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "Proveedor.jsp";
                            }

                        });
                ;
            } else {

                swal("¡Ya exite Registrado el Proveedor! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "warning"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "Proveedor.jsp";
                            }

                        });
                ;
            }
        });
    });
});
//sweetalert para Editar
$(function () {
    $("#editProvedor").on("submit", function (e) {
        e.preventDefault();
        var data = $('#editProvedor').serialize();
        $.post("ControllerProveedor?accion=Actualizar", data, function (res, est, jqXHR) {
            if (res === "ok") {
                swal("¡Se actualizo! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "success"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "Proveedor.jsp";
                            }

                        });
                ;
            } else {

                swal("¡ no se actualizo! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "warning"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "C_ActualizarProveedor.jsp";
                            }

                        });
                ;
            }
        });
    });
});
//Eliminar
$(function () {
    $('tr #btn-eliminar').click(function (e) {
        e.preventDefault();

        swal({
            title: "Estas seguro de eliminar?",
            text: "Una vez eliminado no podrás recuperar el registro!",
            icon: "warning",
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
                .then((willDelete) => {
                    if (willDelete) {

                        //if (opcion) {
                        var fila = $(this).parent().parent();
                        console.log(fila);
                        var idprove = fila.find('#idprove').text();
                        console.log(idprove);
                        var data = {"accion": "eliminar", idprove: idprove};
                        $.post("ControllerProveedor", data, function (res, est, jqXHR) {
                            //console.log('res ',res);


                            //console.log(est);  
                            if (jqXHR.status === 200) {
                                //success
                                //info
                                ///error
                                swal("Registro eliminado correctamente!", {
                                    icon: "success"
                                }).then((willDelete) => {
                                    if (willDelete) {
                                        parent.location.href = "Proveedor.jsp";
                                    }

                                });
                                ;
                            }
                            console.log(jqXHR);
                            fila.remove();
                        });
                        // }
                    } else {
                        swal("Your imaginary file is safe!");
                    }
                });
    });

});

//Confirmacion de cambio de estado
$(function () {
    $('tr #btn-estadoProvedor').click(function (e) {
        e.preventDefault();

        swal({
            title: "Estas seguro de cambiar el estado?",
            text: "Ya nose vizualizara en registrar producto!",
            icon: "warning",
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
                .then((willDelete) => {
                    if (willDelete) {

                        //if (opcion) {
                        var fila = $(this).parent().parent();
                        console.log(fila);
                        var idprove = fila.find('#idprove').text();
                        console.log(idprove);
                        var data = {"accion": "Estado", id: idprove};
                        $.post("ControllerProveedor", data, function (res, est, jqXHR) {
                            //console.log('res ',res);


                            //console.log(est);  
                            if (jqXHR.status === 200) {
                                //success
                                //info
                                ///error
                                swal("Cambio de estado correcto!", {
                                    icon: "success"
                                }).then((willDelete) => {
                                    if (willDelete) {
                                        parent.location.href = "Proveedor.jsp";
                                    }
                                });
                                ;
                            }
                        });
                        // }
                    } else {
                        swal("Your imaginary file is safe!");
                    }
                });
    });

});