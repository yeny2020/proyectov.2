//Data table
$(document).ready(function () {
    $('#Datatable').DataTable();
});

//Valiaciones de campos nueva categoria
function validarunidadCnew() {
    var nombre = document.frm_nuevo.txtNom.value;
    if (nombre === '') {
        swal("Ingrese Nombre!", {
            icon: "warning"
        });
        return false;
    } else if (nombre.length > 14) {
        swal("Error demaciados caracteres!", {
            icon: "warning"
        });
        return false;
    }
}
////Valiaciones de campos Editar
function valeditarunidadC() {
    var nombre = document.frm_edit.Txtnombre.value;
    if (nombre === '') {
        swal("Ingrese Nombre!", {
            icon: "warning"
        });
        return false;
    } else if (nombre.length > 14) {
        swal("Error demaciados caracteres!", {
            icon: "warning"
        });
        return false;
    }
}
//sweetalert para Editar
$(function () {
    $("#edituc").on("submit", function (e) {
        e.preventDefault();
        var data = $('#edituc').serialize();
        $.post("ControllerUnidadMedida?accion=Actualizar", data, function (res, est, jqXHR) {
            if (res === "ok") {
                swal("¡Se actualizo! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "success"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "UnidadCompra.jsp";
                            }

                        });
                ;
            } else {

                swal("¡ no se actualizo! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "warning"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "UnidadCompra.jsp";
                            }

                        });
                ;
            }
        });
    });
});
//sweetalert para guardar nuevo
$(function () {
    $("#newuc").on("submit", function (e) {
        e.preventDefault();
        var data = $('#newuc').serialize();
        $.post("ControllerUnidadMedida?accion=adducompra", data, function (res, est, jqXHR) {
            if (res === "ok") {
                swal("¡ Buen trabajo! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "success"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "UnidadCompra.jsp";
                            }

                        });
                ;
            } else {

                swal("¡ Buen trabajo! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "warning"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "UnidadCompra.jsp";
                            }

                        });
                ;
            }
        });
    });
});
//Eliminar
$(function () {
    $('tr #btn-eliminar').click(function (e) {
        e.preventDefault();

        swal({
            title: "Estas seguro de eliminar?",
            text: "Una vez eliminado no podrás recuperar el registro!",
            icon: "warning",
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
                .then((willDelete) => {
                    if (willDelete) {

                        //if (opcion) {
                        var fila = $(this).parent().parent();
                        console.log(fila);
                        var iduc = fila.find('#iduc').text();
                        console.log(iduc);
                        var data = {"accion": "eliminarucompra", idUc: iduc};
                        $.post("ControllerUnidadMedida", data, function (res, est, jqXHR) {
                            //console.log('res ',res);


                            //console.log(est);  
                            if (jqXHR.status === 200) {
                                //success
                                //info
                                ///error
                                swal("Registro eliminado correctamente!", {
                                    icon: "success"
                                }).then((willDelete) => {
                                    if (willDelete) {
                                        parent.location.href = "UnidadCompra.jsp";
                                    }

                                });;
                            }
                            console.log(jqXHR);
                            fila.remove();
                        });
                        // }
                    } else {
                        swal("Your imaginary file is safe!");
                    }
                });
    });

});
//Confirmacion de cambio de estado
$(function () {
    $('tr #btn-estado').click(function (e) {
        e.preventDefault();

        swal({
            title: "Estas seguro de cambiar el estado?",
            text: "Ya nose vizualizara en registrar producto!",
            icon: "warning",
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
                .then((willDelete) => {
                    if (willDelete) {

                        //if (opcion) {
                        var fila = $(this).parent().parent();
                        console.log(fila);
                        var iduc = fila.find('#iduc').text();
                        console.log(iduc);
                        var data = {"accion": "Estadoucompra", id: iduc};
                        $.post("ControllerUnidadMedida", data, function (res, est, jqXHR) {
                            //console.log('res ',res);


                            //console.log(est);  
                            if (jqXHR.status === 200) {
                                //success
                                //info
                                ///error
                                swal("Cambio de estado correcto!", {
                                    icon: "success"
                                }).then((willDelete) => {
                                    if (willDelete) {
                                        parent.location.href = "UnidadCompra.jsp";
                                    }
                                });;
                            }
                        });
                        // }
                    } else {
                        swal("Your imaginary file is safe!");
                    }
                });
    });

});



