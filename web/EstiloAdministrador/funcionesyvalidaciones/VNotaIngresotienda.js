
//actualizar cantidad
function actualizarcantidad(idproducto) {

    var cantidad = $("#txtPro_cantidad" + idproducto).val();
    window.location.href = "VControllerGestionarnotaingresotienda?accion=NotaIActualizar&cant=" + cantidad + "&fila=" + idproducto;
}
//eliminar carrito
$(function () {
    $('tr #deleteitem').click(function (e) {
        e.preventDefault();
        var elemento = $(this);
        var idproducto = elemento.parent().find('#idarticulo').text();
        swal({
            title: "Seguro que quieres quitar de la lista?",
            text: "...",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                        eliminar(idproducto);
                        swal("Se elimino el producto de la lista!", {
                            icon: "success",
                        }).then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "VControllerGestionarnotaingresotienda?accion=procesarCarrito";
                            }

                        });
                    } else {
                        swal("Your imaginary file is safe!");
                    }
                });

        function eliminar(idproducto) {
            $.ajax({
                url: 'VDeleteitemnotaingreso',
                type: 'post',
                data: {idproducto: idproducto},
                success: function (data, textStatus, jqXHR) {
                }
            });
        }
    });


});

//funcion guardar
$(function () {
    $("#newNI").on("submit", function (e) {
        e.preventDefault();
        var data = $('#newNI').serialize();
        $.post("VControllerGestionarnotaingresotienda?accion=RegistrarNotaIngreso", data, function (res, est, jqXHR) {
            if (res === "oki") {
                swal("¡Se registro con exito! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "success"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "VListarpedido.jsp";
                            }

                        });
                ;
            } else {

                swal("¡No se inserto Verificar! ", "¡ Hiciste clic en el botón! ", " éxito ", {
                    icon: "warning"
                })
                        .then((willDelete) => {
                            if (willDelete) {
                                parent.location.href = "VInsertarNotaingresotienda.jsp";
                            }

                        });
                ;
            }
        });
    });
});






