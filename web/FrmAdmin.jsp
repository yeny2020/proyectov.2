
<%@page import="java.util.Set"%>
<%@page import="java.util.Iterator"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>
<!DOCTYPE html>
<%
    HttpSession sesion = request.getSession();
    if (sesion.getAttribute("tipo") == null) {

        response.sendRedirect("login.jsp");
    } else {
        String tipo = sesion.getAttribute("tipo").toString();
        if (!tipo.equals("Administrador")) {
            response.sendRedirect("login.jsp");
        }
    }
%>

<!-- navBar -->
<div class="full-width navBar">
    <div class="full-width navBar-options">
        <i class="zmdi zmdi-more-vert btn-menu" id="btn-menu"></i>	
        <div class="mdl-tooltip" for="btn-menu">Menu</div>
        <nav class="navBar-options-list">
            <ul class="list-unstyle">
                <li class="btn-exit" id="btn-exi">

                    <i class="zmdi zmdi-power"></i>
                    <div class="mdl-tooltip" for="btn-exi">Salir</div>

                </li>

                <li class="text-condensedLight noLink" ><small><%=sesion.getAttribute("usuario")%></small></li>
                <li class="noLink">
                    <figure>
                        <img src="EstiloAdministrador/assets/img/avatar-male.png" alt="Avatar" class="img-responsive">
                    </figure>
                </li>
            </ul>
        </nav>
    </div>
</div>
<!-- navLateral -->
<section class="full-width navLateral">
    <div class="full-width navLateral-bg btn-menu"></div>
    <div class="full-width navLateral-body">
        <!-- logo de la empresa -->
        <div class="full-width navLateral-body-logo text-center tittles">
            <i class="zmdi zmdi-close btn-menu"></i> 
            <a href="BienvenidoAdmin.jsp" class="simple-text logo-mini pl-0">
                <img src="Img/gatos.png" alt="" style="color: #fff;
                     width: 55px;
                     height: 55px;
                     border-radius: 800px;
                     overflow: hidden;
                     border: 2px solid #fff;
                     margin: 0;
                     padding: 0"/>
            </a>
            <a style="color: #fff;"href="BienvenidoAdmin.jsp" class="simple-text logo-normal">GATAS Y GATOS</a>
        </div>
        <br>
        <figure class="full-width" style="height: 77px;">
            <div class="navLateral-body-cl">
                <img src="EstiloAdministrador/assets/img/avatar-male.png" alt="Avatar" class="img-responsive">
            </div>
            <figcaption class="navLateral-body-cr hide-on-tablet">
                <span style="color: #fff;">
                    Bienvenido<br>
                    <small>Admin Usu</small>
                </span>
            </figcaption>
        </figure>
        <nav class="full-width">
            <ul class="full-width list-unstyle menu-principal">
                <li class="full-width divider-menu-h"></li>

                <!--inicio almacen---->
                <li class="full-width">
                    <a href="#" class="full-width btn-subMenu">
                        <div class="navLateral-body-cl">
                            <i class="zmdi zmdi-case"></i>
                        </div>
                        <div class="navLateral-body-cr hide-on-tablet">
                            ADMINISTRACION
                        </div>
                        <span class="zmdi zmdi-chevron-left"></span>
                    </a>
                    <ul class="full-width menu-principal sub-menu-options">   
                        <li class="full-width">
                            <a href="#" class="full-width btn-subMenu">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-case"></i>
                                </div>
                                <div class="navLateral-body-cr hide-on-tablet">
                                    PRODUCTOS-ALMACENES
                                </div>
                                <span class="zmdi zmdi-chevron-left"></span>
                            </a>

                            <ul class="full-width menu-principal sub-menu-options">   
                                <li class="full-width">
                                    <a href="Categoria.jsp" class="full-width">
                                        <div class="navLateral-body-cl">
                                            <i class="zmdi zmdi-washing-machine"></i>
                                        </div>
                                        <div class="navLateral-body-cr hide-on-tablet">
                                            Soporte Categorias    
                                        </div>
                                    </a>
                                </li>
                                <li class="full-width">
                                    <a href="Marca.jsp" class="full-width">
                                        <div class="navLateral-body-cl">
                                            <i class="zmdi zmdi-label"></i>
                                        </div>
                                        <div class="navLateral-body-cr hide-on-tablet">
                                            Soporte de Marca
                                        </div>                                
                                    </a>
                                </li>

                                <li class="full-width">
                                    <a href="UnidadCompra.jsp" class="full-width">
                                        <div class="navLateral-body-cl">
                                            <i class="zmdi zmdi-label"></i>
                                        </div>
                                        <div class="navLateral-body-cr hide-on-tablet">
                                            Unidad Compra
                                        </div>
                                    </a>
                                </li>
                                <li class="full-width">
                                    <a href="UnidadVenta.jsp" class="full-width">
                                        <div class="navLateral-body-cl">
                                            <i class="zmdi zmdi-label"></i>
                                        </div>
                                        <div class="navLateral-body-cr hide-on-tablet">
                                            Unidad Venta
                                        </div>
                                    </a>
                                </li>
                                <li class="full-width">
                                    <a href="Producto.jsp" class="full-width">                            
                                        <div class="navLateral-body-cl">
                                            <i class="zmdi zmdi-label"></i>
                                        </div>
                                        <div class="navLateral-body-cr hide-on-tablet">
                                            Soporte Productos
                                        </div>
                                    </a>
                                </li>
                                <li class="full-width">
                                    <a href="Almacen.jsp" class="full-width">
                                        <div class="navLateral-body-cl">
                                            <i class="zmdi zmdi-label"></i>
                                        </div>
                                        <div class="navLateral-body-cr hide-on-tablet">
                                            Soporte almacenes
                                        </div>
                                    </a>
                                </li>
                                <li class="full-width">
                                    <a href="Motivo.jsp" class="full-width">
                                        <div class="navLateral-body-cl">
                                            <i class="zmdi zmdi-label"></i>
                                        </div>
                                        <div class="navLateral-body-cr hide-on-tablet">
                                            Soporte motivo
                                        </div>
                                    </a>
                                </li>
                                <li class="full-width">
                                    <a href="Calculo.jsp" class="full-width">
                                        <div class="navLateral-body-cl">
                                            <i class="zmdi zmdi-shopping-cart"></i>							
                                        </div>
                                        <div  class="navLateral-body-cr hide-on-tablet">
                                            Calculo de Costo
                                        </div>
                                    </a>
                                </li>
                                <li class="full-width">
                                    <a href="Permisoalmacen.jsp" class="full-width">
                                        <div class="navLateral-body-cl">
                                            <i class="zmdi zmdi-shopping-cart"></i>							
                                        </div>
                                        <div  class="navLateral-body-cr hide-on-tablet">
                                            Permiso por Almacen
                                        </div>
                                    </a>
                                </li>

                            </ul>

                        </li> 
                        <li class="full-width">
                            <a href="#" class="full-width btn-subMenu">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-case"></i>
                                </div>
                                <div class="navLateral-body-cr hide-on-tablet">
                                    PERSONAL-AUXILIARES
                                </div>
                                <span class="zmdi zmdi-chevron-left"></span>
                            </a>

                            <ul class="full-width menu-principal sub-menu-options">   
                                <li class="full-width">
                                    <a href="Cliente.jsp" class="full-width">
                                        <div class="navLateral-body-cl">
                                            <i class="zmdi zmdi-account "></i>
                                        </div>
                                        <div class="navLateral-body-cr hide-on-tablet">
                                            Clientes
                                        </div>
                                    </a>
                                </li>
                                <li class="full-width">
                                    <a href="Proveedor.jsp" class="full-width">
                                        <div class="navLateral-body-cl">
                                            <i class="zmdi zmdi-account "></i>
                                        </div>
                                        <div class="navLateral-body-cr hide-on-tablet">
                                            Proveedor
                                        </div>
                                    </a>
                                </li>
                                <li class="full-width">
                                    <a href="Empleado.jsp" class="full-width">
                                        <div class="navLateral-body-cl">
                                            <i class="zmdi zmdi-account"></i>
                                        </div>
                                        <div class="navLateral-body-cr hide-on-tablet">
                                            Personal
                                        </div>
                                    </a>
                                </li>

                            </ul>

                        </li> 
                    </ul>
                </li>
                <!--fin almacen---->

                <!--inicio Compra---->                                        
                <li class="full-width divider-menu-h"></li>
                <li class="full-width">
                    <a href="#" class="full-width btn-subMenu">
                        <div class="navLateral-body-cl">
                            <i  class="zmdi zmdi-shopping-cart"></i>
                        </div>
                        <div class="navLateral-body-cr hide-on-tablet">
                            ALMACEN-COMPRA
                        </div>
                        <span class="zmdi zmdi-chevron-left"></span>
                    </a>
                    <ul class="full-width menu-principal sub-menu-options">
                        <li class="full-width divider-menu-h"></li>
                        <li class="full-width">
                            <a href="AlertaStockCompra.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-shopping-cart"></i>							
                                </div>
                                <div  class="navLateral-body-cr hide-on-tablet">
                                    Consultar Stock Producto
                                </div>
                            </a>
                        </li>
                        <li class="full-width">
                            <a href="InsertarCompra.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-shopping-cart"></i>							
                                </div>
                                <div  class="navLateral-body-cr hide-on-tablet">
                                    Documento de compras
                                </div>
                            </a>
                        </li>

                        <li class="full-width">
                            <a href="Cotizacion.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-shopping-cart"></i>							
                                </div>
                                <div  class="navLateral-body-cr hide-on-tablet">
                                    Lista Cotizacion
                                </div>
                            </a>
                        </li>
                        <li class="full-width">
                            <a href="OrdenCompra.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-shopping-cart"></i>							
                                </div>
                                <div  class="navLateral-body-cr hide-on-tablet">
                                    Lista de OrdenCompra
                                </div>
                            </a>
                        </li>
                        <li class="full-width">
                            <a href="NotaIngreso.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-shopping-cart"></i>							
                                </div>
                                <div  class="navLateral-body-cr hide-on-tablet">
                                    Nota de Ingreso
                                </div>
                            </a>
                        </li>
                        <li class="full-width">
                            <a href="GuiaRemision.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-shopping-cart"></i>							
                                </div>
                                <div  class="navLateral-body-cr hide-on-tablet">
                                    Guia Remision
                                </div>
                            </a>
                        </li>
                        <li class="full-width">
                            <a href="FacturaCompra.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-shopping-cart"></i>							
                                </div>
                                <div  class="navLateral-body-cr hide-on-tablet">
                                    Factura Compra
                                </div>
                            </a>
                        </li>
                    </ul>
                </li>
                <!--fin Compra---->

                <!--inicio Acceso---->
                <li class="full-width divider-menu-h"></li>
                <li class="full-width">
                    <a href="#" class="full-width btn-subMenu">
                        <div class="navLateral-body-cl">
                            <i class="zmdi zmdi-face"></i>
                        </div>
                        <div class="navLateral-body-cr hide-on-tablet">
                            ACCESO
                        </div>

                        <span class="zmdi zmdi-chevron-left"></span>
                    </a>
                    <ul class="full-width menu-principal sub-menu-options">

                        <li class="full-width">
                            <a href="Usuario.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-key"></i>
                                </div>
                                <div class="navLateral-body-cr hide-on-tablet">
                                    USUARIOS
                                </div>
                            </a>
                        </li>
                        <li class="full-width">
                            <a href="Empleado.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-face"></i>
                                </div>
                                <div class="navLateral-body-cr hide-on-tablet">
                                    TRABAJADOR
                                </div>
                            </a>
                        </li>
                    </ul>
                </li>
                <!--fin acceso---->



                <!--inicio reportes---->
                <li class="full-width divider-menu-h"></li>
                <li class="full-width">
                    <a href="#" class="full-width btn-subMenu">

                        <div class="navLateral-body-cl">
                            <i class="zmdi zmdi-cloud-download"></i>
                        </div>
                        <div class="navLateral-body-cr hide-on-tablet">
                            REPORTES
                        </div>

                        <span class="zmdi zmdi-chevron-left"></span>
                    </a>
                    <ul class="full-width menu-principal sub-menu-options">
                        <li class="full-width">
                            <a href="Tablaconscroll.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-download"></i>
                                </div>
                                <div class="navLateral-body-cr hide-on-tablet">
                                    VENTA
                                </div>
                            </a>
                        </li>

                        <li class="full-width">
                            <a href="tablascrollvertical.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-download"></i>
                                </div>
                                <div class="navLateral-body-cr hide-on-tablet">
                                    USUARIOS
                                </div>
                            </a>
                        </li>
                        <li class="full-width">
                            <a href="Scrollfila.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-download"></i>
                                </div>
                                <div class="navLateral-body-cr hide-on-tablet">
                                    CATEGORIA
                                </div>
                            </a>
                        </li>
                        <li class="full-width">
                            <a href="ReporteProducto.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-download"></i>
                                </div>
                                <div class="navLateral-body-cr hide-on-tablet">
                                    PRODUCTOS
                                </div>
                            </a>
                        </li>
                        <li class="full-width">
                            <a href="Movimiento.jsp" class="full-width">

                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-download"></i>
                                </div>
                                <div class="navLateral-body-cr hide-on-tablet">
                                    CLIENTES
                                </div>
                            </a>
                        </li>

                        <li class="full-width">
                            <a href="PDF.jsp" class="full-width">

                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-download"></i>
                                </div>
                                <div class="navLateral-body-cr hide-on-tablet">
                                    PDF
                                </div>
                            </a>
                        </li>




                    </ul>
                </li>
                <!--fin reportes---->

            </ul>
        </nav>



    </div>
</section>
<%--<%
    } else {
        response.sendRedirect("identificar.jsp");
    }
%>--%>