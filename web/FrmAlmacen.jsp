<%-- 
    Document   : FrmAlmacen
    Created on : 27/10/2018, 03:20:22 PM
    Author     : Angel Albinagorta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>
<%
    HttpSession sesion=request.getSession();
    if(sesion.getAttribute("tipo")==null){
        response.sendRedirect("login.jsp");
    }else{
        String tipo=sesion.getAttribute("tipo").toString();
        if(!tipo.equals("Almacen")){
            response.sendRedirect("login.jsp");
        }
    }
%>
<!-- navBar -->
<div class="full-width navBar">
    <div class="full-width navBar-options">
        <i class="zmdi zmdi-more-vert btn-menu" id="btn-menu"></i>	
        <div class="mdl-tooltip" for="btn-menu">Menu</div>
        <nav class="navBar-options-list">
            <ul class="list-unstyle">
                <li class="btn-exit" id="btn-exi" >

                    <i class="zmdi zmdi-power"></i>
                    <div class="mdl-tooltip" for="btn-exi" >Salir</div>

                </li>

                <li class="text-condensedLight noLink" ><small><%= sesion.getAttribute("usuario")%></small></li>
                <li class="noLink">
                    <figure>
                        <img src="EstiloAdministrador/assets/img/avatar-male.png" alt="Avatar" class="img-responsive">
                    </figure>
                </li>
            </ul>
        </nav>
    </div>
</div>
<!-- navLateral -->
<section class="full-width navLateral">
    <div class="full-width navLateral-bg btn-menu"></div>
    <div class="full-width navLateral-body">
        <!-- logo de la empresa -->
        <div class="full-width navLateral-body-logo text-center tittles">
            <i class="zmdi zmdi-close btn-menu"></i> 
            <a href="BienvenidoAlmacen.jsp" class="simple-text logo-mini pl-0">
                <img src="Img/gatos.png" alt="" style="color: #fff;
                     width: 55px;
                     height: 55px;
                     border-radius: 800px;
                     overflow: hidden;
                     border: 2px solid #fff;
                     margin: 0;
                     padding: 0"/>
            </a>
            <a style="color: #fff;"href="BienvenidoAlmacen.jsp" class="simple-text logo-normal">GATAS Y GATOS</a>
        </div>
         <br>
        <figure class="full-width" style="height: 77px;">
            <div class="navLateral-body-cl">
                <img src="EstiloAdministrador/assets/img/avatar-male.png" alt="Avatar" class="img-responsive">
            </div>
            <figcaption class="navLateral-body-cr hide-on-tablet">
                <span style="color: #fff;">
                    Bienvenido<br>
                    <small> Almacen Usu</small>
                </span>
            </figcaption>
        </figure>
        <nav class="full-width">
            <ul class="full-width list-unstyle menu-principal">
                <li class="full-width divider-menu-h"></li>

                <!--inicio almacen---->
                <li class="full-width">
                    <a href="#" class="full-width btn-subMenu">
                        <div class="navLateral-body-cl">
                            <i class="zmdi zmdi-case"></i>
                        </div>
                        <div class="navLateral-body-cr hide-on-tablet">
                            ADMINISTRACION
                        </div>
                        <span class="zmdi zmdi-chevron-left"></span>
                    </a>
                    <ul class="full-width menu-principal sub-menu-options">   
                        <li class="full-width">
                            <a href="#" class="full-width btn-subMenu">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-case"></i>
                                </div>
                                <div class="navLateral-body-cr hide-on-tablet">
                                    PRODUCTOS-ALMACENES
                                </div>
                                <span class="zmdi zmdi-chevron-left"></span>
                            </a>

                            <ul class="full-width menu-principal sub-menu-options">   
                                <li class="full-width">
                                    <a href="Producto_1.jsp" class="full-width">
                                        <div class="navLateral-body-cl">
                                            <i class="zmdi zmdi-washing-machine"></i>
                                        </div>
                                        <div class="navLateral-body-cr hide-on-tablet">
                                            Soporte Productos
                                        </div>
                                    </a>
                                </li>
                                <li class="full-width">
                                    <a href="Categoria.jsp" class="full-width">                            
                                        <div class="navLateral-body-cl">
                                            <i class="zmdi zmdi-label"></i>
                                        </div>
                                        <div class="navLateral-body-cr hide-on-tablet">
                                            Soporte Categorias
                                        </div>
                                    </a>
                                </li>
                                <li class="full-width">
                                    <a href="Marca.jsp" class="full-width">
                                        <div class="navLateral-body-cl">
                                            <i class="zmdi zmdi-label"></i>
                                        </div>
                                        <div class="navLateral-body-cr hide-on-tablet">
                                            Soporte de Marca
                                        </div>                                
                                    </a>
                                </li>
                                <li class="full-width">
                                    <a href="Material.jsp" class="full-width">
                                        <div class="navLateral-body-cl">
                                            <i class="zmdi zmdi-label"></i>
                                        </div>
                                        <div class="navLateral-body-cr hide-on-tablet">
                                            Soporte almacenes
                                        </div>
                                    </a>
                                </li>

                            </ul>

                        </li> 
                        <li class="full-width">
                            <a href="#" class="full-width btn-subMenu">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-case"></i>
                                </div>
                                <div class="navLateral-body-cr hide-on-tablet">
                                    PERSONAL-AUXILIARES
                                </div>
                                <span class="zmdi zmdi-chevron-left"></span>
                            </a>

                            <ul class="full-width menu-principal sub-menu-options">   
                                <li class="full-width">
                                    <a href="Cliente.jsp" class="full-width">
                                        <div class="navLateral-body-cl">
                                            <i class="zmdi zmdi-account "></i>
                                        </div>
                                        <div class="navLateral-body-cr hide-on-tablet">
                                            Clientes
                                        </div>
                                    </a>
                                </li>
                                <li class="full-width">
                                    <a href="Proveedor.jsp" class="full-width">
                                        <div class="navLateral-body-cl">
                                            <i class="zmdi zmdi-account "></i>
                                        </div>
                                        <div class="navLateral-body-cr hide-on-tablet">
                                            Proveedor
                                        </div>
                                    </a>
                                </li>
                                <li class="full-width">
                                    <a href="Marca.jsp" class="full-width">
                                        <div class="navLateral-body-cl">
                                            <i class="zmdi zmdi-label"></i>
                                        </div>
                                        <div class="navLateral-body-cr hide-on-tablet">
                                            Condicion de Documentos
                                        </div>                                
                                    </a>
                                </li>

                                <li class="full-width">
                                    <a href="Empleado.jsp" class="full-width">
                                        <div class="navLateral-body-cl">
                                            <i class="zmdi zmdi-account"></i>
                                        </div>
                                        <div class="navLateral-body-cr hide-on-tablet">
                                            Soporte de empleados
                                        </div>
                                    </a>
                                </li>

                            </ul>

                        </li> 
                    </ul>
                </li>
                <!--fin almacen---->
                
                 <!--inicio Movimiento ALMACEN---->
                <li class="full-width divider-menu-h"></li>
                <li class="full-width">
                    <a href="#" class="full-width btn-subMenu">
                        <div class="navLateral-body-cl">
                            <i class="zmdi zmdi-case"></i>
                        </div>
                        <div class="navLateral-body-cr hide-on-tablet">
                            MOVIMIENTO EN ALMACEN 
                        </div>

                        <span class="zmdi zmdi-chevron-left"></span>
                    </a>
                    <ul class="full-width menu-principal sub-menu-options">

                        <li class="full-width">
                            <a href="AlmacenPedidopendiente.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-shopping-cart"></i>							
                                </div>
                                <div  class="navLateral-body-cr hide-on-tablet">
                                    Lista de Pedido pendientes
                                </div>
                            </a>
                        </li>
                        <li class="full-width">
                            <a href="AlmacenNotasalida.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-shopping-cart"></i>							
                                </div>
                                <div  class="navLateral-body-cr hide-on-tablet">
                                    Lista Nota de Salida
                                </div>
                            </a>
                        </li>
                    </ul>
                </li>
                <!--fin Movimiento ALMACEN---->
                <!--inicio reportes---->
                <li class="full-width divider-menu-h"></li>
                <li class="full-width">
                    <a href="#" class="full-width btn-subMenu">

                        <div class="navLateral-body-cl">
                            <i class="zmdi zmdi-cloud-download"></i>
                        </div>
                        <div class="navLateral-body-cr hide-on-tablet">
                            REPORTES
                        </div>

                        <span class="zmdi zmdi-chevron-left"></span>
                    </a>
                    <ul class="full-width menu-principal sub-menu-options">
                        <li class="full-width">
                            <a href="ventas.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-download"></i>
                                </div>
                                <div class="navLateral-body-cr hide-on-tablet">
                                    VENTA
                                </div>
                            </a>
                        </li>

                        <li class="full-width">
                            <a href="Reporteusuario.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-download"></i>
                                </div>
                                <div class="navLateral-body-cr hide-on-tablet">
                                    USUARIOS
                                </div>
                            </a>
                        </li>
                        <li class="full-width">
                            <a href="ReporteCategoria.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-download"></i>
                                </div>
                                <div class="navLateral-body-cr hide-on-tablet">
                                    CATEGORIA
                                </div>
                            </a>
                        </li>
                        <li class="full-width">
                            <a href="ReporteProducto.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-download"></i>
                                </div>
                                <div class="navLateral-body-cr hide-on-tablet">
                                    PRODUCTOS
                                </div>
                            </a>
                        </li>
                        <li class="full-width">
                            <a href="ReporteCliente.jsp" class="full-width">

                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-download"></i>
                                </div>
                                <div class="navLateral-body-cr hide-on-tablet">
                                    CLIENTES
                                </div>
                            </a>
                        </li>

                        <li class="full-width">
                            <a href="ReporteEmpleado.jsp" class="full-width">

                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-download"></i>
                                </div>
                                <div class="navLateral-body-cr hide-on-tablet">
                                    EMPLEADOS
                                </div>
                            </a>
                        </li>




                    </ul>
                </li>
                <!--fin reportes---->

            </ul>
        </nav>



    </div>
</section>
<%--<%
    } else {
        response.sendRedirect("identificar.jsp");
    }
%>--%>

