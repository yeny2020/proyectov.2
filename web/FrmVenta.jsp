<%-- 
    Document   : FrmVenta
    Created on : 27/10/2018, 04:00:54 PM
    Author     : Angel Albinagorta
--%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Iterator"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>


<!DOCTYPE html>
<%
    HttpSession sesion = request.getSession();
    if (sesion.getAttribute("tipo") == null) {
        response.sendRedirect("login.jsp");
    } else {
        String tipo = sesion.getAttribute("tipo").toString();
        if (!tipo.equals("Ventas")) {
            response.sendRedirect("login.jsp");
        }
    }
%>
<!-- navBar -->
<div class="full-width navBar">
    <div class="full-width navBar-options">
        <i class="zmdi zmdi-more-vert btn-menu" id="btn-menu"></i>	
        <div class="mdl-tooltip" for="btn-menu">Menu</div>
        <nav class="navBar-options-list">
            <ul class="list-unstyle">
                <li class="btn-exit" id="btn-exi" >

                    <i class="zmdi zmdi-power"></i>
                    <div class="mdl-tooltip" for="btn-exi" >Salir</div>

                </li>

                <li class="text-condensedLight noLink" ><small><%= sesion.getAttribute("usuario")%></small></li>
                <li class="noLink">
                    <figure>
                        <img src="EstiloAdministrador/assets/img/avatar-male.png" alt="Avatar" class="img-responsive">
                    </figure>
                </li>
            </ul>
        </nav>
    </div>
</div>
<!-- navLateral -->
<section class="full-width navLateral">
    <div class="full-width navLateral-bg btn-menu"></div>
    <div class="full-width navLateral-body">
        <!-- logo de la empresa -->
        <div class="full-width navLateral-body-logo text-center tittles">
            <i class="zmdi zmdi-close btn-menu"></i> 
            <a href="BienvenidoVentas.jsp" class="simple-text logo-mini pl-0">
                <img src="Img/gatos.png" alt="" style="color: #fff;
                     width: 55px;
                     height: 55px;
                     border-radius: 800px;
                     overflow: hidden;
                     border: 2px solid #fff;
                     margin: 0;
                     padding: 0"/>
            </a>
            <a style="color: #fff;"href="BienvenidoVentas.jsp" class="simple-text logo-normal">GATAS Y GATOS</a>
        </div>
         <br>
        <figure class="full-width" style="height: 77px;">
            <div class="navLateral-body-cl">
                <img src="EstiloAdministrador/assets/img/avatar-male.png" alt="Avatar" class="img-responsive">
            </div>
            <figcaption class="navLateral-body-cr hide-on-tablet">
                <span style="color: #fff;">
                    Bienvenido<br>
                    <small>Ventas Usu</small>
                </span>
            </figcaption>
        </figure>
        <nav class="full-width">
            <ul class="full-width list-unstyle menu-principal">
                <li class="full-width divider-menu-h"></li>

                <!--inicio ventas---->
                <li class="full-width divider-menu-h"></li>
                <li class="full-width">
                    <a href="#" class="full-width btn-subMenu">
                        <div class="navLateral-body-cl">
                            <i  class="zmdi zmdi-shopping-cart"></i>
                        </div>
                        <div class="navLateral-body-cr hide-on-tablet">
                            VENTAS
                        </div>
                        <span class="zmdi zmdi-chevron-left"></span>
                    </a>
                    <ul class="full-width menu-principal sub-menu-options">
                        <li class="full-width divider-menu-h"></li>	

                        <li class="full-width">
                            <a href="VInsertarventa.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-shopping-cart"></i>							
                                </div>
                                <div  class="navLateral-body-cr hide-on-tablet">
                                    Punto de Venta
                                </div>
                            </a>
                        </li>
                        <li class="full-width">
                            <a href="VListarVenta.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-shopping-cart"></i>							
                                </div>
                                <div  class="navLateral-body-cr hide-on-tablet">
                                    Lista documentos de venta
                                </div>
                            </a>
                        </li>

                        <li class="full-width">
                            <a href="VStockminimo.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-shopping-cart"></i>							
                                </div>
                                <div  class="navLateral-body-cr hide-on-tablet">
                                    Lista Stock Minimo Tienda
                                </div>
                            </a>
                        </li>
                    </ul>
                </li>
                <!--fin ventas---->
               
                <!--inicio Movimiento---->
                <li class="full-width divider-menu-h"></li>
                <li class="full-width">
                    <a href="#" class="full-width btn-subMenu">
                        <div class="navLateral-body-cl">
                            <i class="zmdi zmdi-case"></i>
                        </div>
                        <div class="navLateral-body-cr hide-on-tablet">
                            MOVIMIENTO
                        </div>

                        <span class="zmdi zmdi-chevron-left"></span>
                    </a>
                    <ul class="full-width menu-principal sub-menu-options">

                        <li class="full-width">
                            <a href="VInsertarpedido.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-shopping-cart"></i>							
                                </div>
                                <div  class="navLateral-body-cr hide-on-tablet">
                                    Pedido de Productos
                                </div>
                            </a>
                        </li>
                        <li class="full-width">
                            <a href="VListarpedido.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-shopping-cart"></i>							
                                </div>
                                <div  class="navLateral-body-cr hide-on-tablet">
                                    Lista de Pedido 
                                </div>
                            </a>
                        </li>
                        <li class="full-width">
                            <a href="Notasalida.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-washing-machine" style="color: #FFC107;"></i>							
                                </div>
                                <div  class="navLateral-body-cr hide-on-tablet">
                                    Nota Ingreso
                                </div>
                            </a>
                        </li>
                        <li class="full-width">
                            <a href="NotaIgreso.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-washing-machine" style="color: #FFC107;"></i>							
                                </div>
                                <div  class="navLateral-body-cr hide-on-tablet">
                                    Lista Nota Ingreso
                                </div>
                            </a>
                        </li>
                    </ul>
                </li>
                <!--fin Movimiento---->

                <!--inicio reportes---->
                <li class="full-width divider-menu-h"></li>
                <li class="full-width">
                    <a href="#" class="full-width btn-subMenu">

                        <div class="navLateral-body-cl">
                            <i class="zmdi zmdi-cloud-download"></i>
                        </div>
                        <div class="navLateral-body-cr hide-on-tablet">
                            REPORTES
                        </div>

                        <span class="zmdi zmdi-chevron-left"></span>
                    </a>
                    <ul class="full-width menu-principal sub-menu-options">
                        <li class="full-width">
                            <a href="ventas.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-download"></i>
                                </div>
                                <div class="navLateral-body-cr hide-on-tablet">
                                    VENTA
                                </div>
                            </a>
                        </li>

                        <li class="full-width">
                            <a href="Reporteusuario.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-download"></i>
                                </div>
                                <div class="navLateral-body-cr hide-on-tablet">
                                    USUARIOS
                                </div>
                            </a>
                        </li>
                        <li class="full-width">
                            <a href="ReporteCategoria.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-download"></i>
                                </div>
                                <div class="navLateral-body-cr hide-on-tablet">
                                    CATEGORIA
                                </div>
                            </a>
                        </li>
                        <li class="full-width">
                            <a href="ReporteProducto.jsp" class="full-width">
                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-download"></i>
                                </div>
                                <div class="navLateral-body-cr hide-on-tablet">
                                    PRODUCTOS
                                </div>
                            </a>
                        </li>
                        <li class="full-width">
                            <a href="ReporteCliente.jsp" class="full-width">

                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-download"></i>
                                </div>
                                <div class="navLateral-body-cr hide-on-tablet">
                                    CLIENTES
                                </div>
                            </a>
                        </li>

                        <li class="full-width">
                            <a href="ReporteEmpleado.jsp" class="full-width">

                                <div class="navLateral-body-cl">
                                    <i class="zmdi zmdi-download"></i>
                                </div>
                                <div class="navLateral-body-cr hide-on-tablet">
                                    EMPLEADOS
                                </div>
                            </a>
                        </li>




                    </ul>
                </li>
                <!--fin reportes---->

            </ul>
        </nav>



    </div>
</section>
<%--<%
    } else {
        response.sendRedirect("identificar.jsp");
    }
%>--%>