
<%@page import="com.pe.gatasygatos.DAO.ClienteDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.pe.gatasygatos.model.entity.TipoDocumento"%>
<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.pe.gatasygatos.DAO.TipoDocumentoDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Insertar cliente</title>
    </head>
    <body>
        <!-- add Modal HTML -->
        <div >
            <!-- add Modal HTML -->
            <div class="modal-content">
                <div class="modal-header">      
                    <h4 class="modal-title"   id="myModalLabel">Add Cliente</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div> 
                <form id="newcliente" action="ControllerCliente" method="Post" name="frm_nuevo">
                    <div class="modal-body"> 
                        <%
                            ClienteDAO clie = new ClienteDAO();
                            String numserie = clie.Numserie();
                        %>
                        <div class="form-group">
                            <label for="txtCod" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Codigo</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"><input type="text" name="txtCod" id="txtCod" class="border-focus-darkblue form-control" value="<%=numserie%>"  class="form-control" readonly=""><span class="help-block"></span></div></div>
                    </div> 
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="Txtapellidos" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Cliente</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"><input type="text" name="Txtapellidos" onkeypress="return soloLetras(event)" placeholder="Ingrese datos del cliente" id="Txtapellidos" class="border-focus-darkblue form-control"><span class="help-block"></span></div></div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="validationDefault01" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Tipo de Doc. de Identidad</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" class="border-focus-darkblue form-control">
                                <select name="Txtidtipodocumento" id="Txtidtipodocumento" class="form-control">
                                    <option   value=""disabled="" selected="" >Documentos de Identidad</option>
                                    <% TipoDocumentoDAO tdoc = new TipoDocumentoDAO();
                                        List<TipoDocumento> lista = tdoc.listarTipodocumento();
                                        Iterator<TipoDocumento> iterr = lista.iterator();
                                        TipoDocumento doc = null;
                                        while (iterr.hasNext()) {
                                            doc = iterr.next();
                                    %>
                                    <option  value="<%=doc.getIdtipodocumento()%>"><%=doc.getTipoDocumento()%></option>
                                    <%
                                        }
                                    %>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="Txtnumerodocumento" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Doc. ID</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"><input type="text" name="Txtnumerodocumento" onkeypress="return soloNumeros(event)" placeholder="Ingrese un número de documento" maxlength="11" id="Txtnumerodocumento" class="border-focus-darkblue form-control"><span class="help-block"></span></div></div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="Txtcorreo" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Correo contacto</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"><input type="text" name="Txtcorreo" placeholder="Ingrese el correo de contacto" id="Txtcorreo" class="border-focus-darkblue form-control"><span class="help-block"></span></div> </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="Txtcelular" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Teléfono</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"><input type="text" name="Txtcelular" placeholder="Ingrese Teléfono" onkeypress="return soloNumeros(event)"  id="Txtcelular" class="border-focus-darkblue form-control"><span class="help-block"></span></div></div>                                  
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="Txtdireccion" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Direccion</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"><input type="text" name="Txtdireccion" placeholder="Ingrese direccion del cliente" id="Txtdireccion" class="border-focus-darkblue form-control"><span class="help-block"></span></div></div>
                    </div>
                    <% Date dNow = new Date();
                        SimpleDateFormat ft
                                = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        String currentDate = ft.format(dNow);
                    %>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="Txtfechaderegistro" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Fecha Registro</label>
                            <div class="col-lg-7 col-md-8 col-sm-12 col-xs-12"><input type="text" name="Txtfechaderegistro" value="<%=currentDate%>" id="" class="border-focus-darkblue form-control" readonly="">
                                <span class="help-block"></span></div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="Txtsexo" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Género</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" class="border-focus-darkblue form-control">
                                <input type="radio" name="Txtsexo" value="Masculino" >&nbsp; Masculino
                                <input type="radio" name="Txtsexo" value="Femenino" >&nbsp; Femenino

                            </div>
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="Cliente.jsp" class="btn btn-default" style="background-color: #B7BBB8" ><i class="far fa-window-close"></i>&nbsp; Cancelar</a> 
                        <input onclick="return validarnewcliente()"  class="btn btn-success" type="submit" name="accion" value="Grabar">
                    </div>
                </form>
            </div>
        </div>
    </body>
    <!--Funciones y validaciones --->
    <script src="EstiloAdministrador/funcionesyvalidaciones/Client.js" type="text/javascript"></script>
    <!--Funciones y validaciones --->
</html>




