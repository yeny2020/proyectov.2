
<%@page import="com.pe.gatasygatos.DAO.ProvedorDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Provedor"%>
<%@page import="com.pe.gatasygatos.model.entity.DetalleCompra"%>

<%@page import="com.pe.gatasygatos.DAO.OrdendeCompraDAO"%>
<%@page import="com.pe.gatasygatos.DAO.AlmacenDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Almacen"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Cliente</title>
        <%@include file="css-js.jsp" %>
        <!--ICONOS-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <!--ICONOS-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
        <!--Estilo tabla-->
        <!--BOOSTRAP PARA DIV-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <!--BOOSTRAP PARA DIV-->
        <!--Estilo tabla-->
        <link rel="stylesheet" href="EstiloAdministrador/css/Stilodetabla.css"> 
        <!--Estilo tabla-->
        <%--Buscador select.---%>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
        <%-- finBuscador select.---%>

    </head>
    <style> 
        .dtHorizontalVerticalExampleWrapper {
            max-width: 600px;
            margin: 0 auto;
        }
        #dtHorizontalVerticalExample th, td {
            white-space: nowrap;
        }
        table.dataTable thead .sorting:after,
        table.dataTable thead .sorting:before,
        table.dataTable thead .sorting_asc:after,
        table.dataTable thead .sorting_asc:before,
        table.dataTable thead .sorting_asc_disabled:after,
        table.dataTable thead .sorting_asc_disabled:before,
        table.dataTable thead .sorting_desc:after,
        table.dataTable thead .sorting_desc:before,
        table.dataTable thead .sorting_desc_disabled:after,
        table.dataTable thead .sorting_desc_disabled:before {
            bottom: .10em;
        }
    </style>
    <body>
        <!--cabecera de Menu -->
        <%@include file="FrmAdmin.jsp" %>
        <%            Provedor objProveedor = null;
            if (request.getSession().getAttribute("proveedor") != null) {
                objProveedor = (Provedor) request.getSession().getAttribute("proveedor");
            } else {
                objProveedor = new Provedor();
                objProveedor.setIdproveedor(0);
                objProveedor.setEmail("");
                objProveedor.setNumdocumento("0");
                objProveedor.setRazonsocial("");
                objProveedor.setDireccion("");
            }
        %>
        <!-- pageContent -->
        <section class="full-width pageContent">

            <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
                <!--modal listar-->
                <div class="mdl-tabs__panel is-active" id="tabListCategory">
                    <div class="mdl-grid">
                        <div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop mdl-cell--0-offset-desktop">

                            <div class="full-width panel mdl-shadow--2dp">
                                <div class="full-width panel-tittle  text-center tittles" style="background-color: #0a8cb3;">
                                    Generador de Documentos Compras
                                </div>
                                <form id="newventa" method="post" name="accion" action="ControllerCompra">
                                    <input  type="hidden" name="accion" value="RegistrarCompra" />
                                    <div class="card">
                                        <div class="card-footer">                                                
                                            <input class="btn btn-primary" style="background: #0a8cb3;color:#fff;float: right;margin-left: 10px" type="submit" value="Registrar Compra" name="btnVenta" style="float:right"> 

                                            <a class="btn btn-primary" style="background: #0a8cb3;color:#fff;float: right;margin-left: 10px" href="InsertarCompra.jsp" role="button" style="float:right">Salir</a>
                                            <a href="AgregarProductoCotizacion.jsp" class="btn btn-primary" style="background: #0a8cb3;color:#fff; float: right;">AgregarProductos</a>                                          
                                        </div>
                                    </div>
                                    <div class="d-flex">
                                        <div class="col-md-12 col-md-offset-0" style="color: #000000;">
                                            <div class="card">
                                                <div class="card-body  ">
                                                    <div class="d-flex">
                                                        <label class="col-sm-2" style="text-align:right">Proveedor:</label>
                                                        <input type="hidden" name="txtIdproveedor" value="<%=objProveedor.getIdproveedor() == 0 ? "" : objProveedor.getIdproveedor()%>"style="width:340px;height:20px;" required="">
                                                        <input type="text" name="txtNombre" value=" <%=objProveedor.getRazonsocial()%>"  style="width:340px;height:20px;">
                                                        <a href="#Buscarproveedor" class="btn btn-primary" style="background-color:#4e96b3;color: #F7F7F5;" data-toggle="modal" required="">Buscar</a>
                                                        <label class="col-sm-2" style="text-align:right">Documento:</label>
                                                        <select name="txtTipocompra" id="tipo_comprobante" required="" style="width:350px;height:20px;">
                                                            <option   value=""disabled="" selected="">Seleciona Comprobante</option>
                                                            <option value="Cotizacion">Cotizacion Compra</option>
                                                            <option value="Orden">Orden Compra</option>
                                                        </select>
                                                    </div> 
                                                    <div class="d-flex">
                                                        <label class="col-sm-2" style="text-align:right">RUC :</label>
                                                        <input type="text" name="txtI" value="<%=objProveedor.getNumdocumento()%>"  style="width:400px;height:20px;">

                                                        <label class="col-sm-2" style="text-align:right">Tienda:</label>
                                                        <select name="txtTienda" id="tipo_comprobante" required="" style="width:340px;height:20px;">
                                                            <option value="Gatas y Gatos">Gatas y Gatos</option>
                                                        </select>
                                                    </div>



                                                    <div class="d-flex">
                                                        <label class="col-sm-2" style="text-align:right">E-Mail:</label>
                                                        <input type="text" name="txtTota" value="<%=objProveedor.getEmail()%>"   style="width:400px;height:20px;">
                                                        <label class="col-sm-2" style="text-align:right">Almacen:</label>
                                                        <select name="txtAlmacen" id="tipo_comprobante" required="" style="width:340px;height:20px;">
                                                            <% AlmacenDAO mat = new AlmacenDAO();
                                                                List<Almacen> lis = mat.Almacengenral();
                                                                Iterator<Almacen> it = lis.iterator();
                                                                Almacen ma = null;
                                                                while (it.hasNext()) {
                                                                    ma = it.next();
                                                            %>

                                                            <option  value="<%=ma.getNombre()%>" required><%=ma.getNombre()%></option>
                                                            <%
                                                                }
                                                            %>
                                                        </select>                                                
                                                    </div>
                                                    <% Date dNow = new Date();
                                                        SimpleDateFormat ft
                                                                = new SimpleDateFormat("dd/MM/yyyy - hh:mm:ss");
                                                        String currentDate = ft.format(dNow);
                                                    %>
                                                    <div class="d-flex">

                                                        <label class="col-sm-2" style="text-align:right">Emision:</label>
                                                        <input type="text" name="txtfecha" value="<%=currentDate%>"  style="width:360px;height:20px;">
                                                        <label class="col-sm-2" style="text-align:right">Condicion:</label>
                                                        <input type="text" name="txtCondicion" value=""  style="width:100px;height:20px;" required="">
                                                        <label class="col-sm-1" style="text-align:right">Motivo:</label>
                                                        <input type="text" name="txtIdmotivo" value=""  style="width:100px;height:20px;" required="">
                                                    </div>
                                                    <div class="d-flex">
                                                        <label class="col-sm-2" style="text-align:right">Responsable:</label>
                                                        <input type="text" name="txtIdusuario" pattern="^[a-zA-ZáéíóúÁÉÍÓÚñÑ-\s]+$"value=""  style="width:400px;height:20px;" required="">
                                                        <label class="col-sm-2" style="text-align:right">Direccion:</label>
                                                        <input type="text" name="txtSubto" value="<%=objProveedor.getDireccion()%>"  style="width:340px;height:20px;">
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="d-flex">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="form-group">                                                   
                                                        <div class="table-responsive cart_info"  id="cart-container"> 
                                                            <table id="dtHorizontalVerticalExample" class="table table-striped table-bordered table-sm " cellspacing="0"
                                                                   width="100%">
                                                                <thead class="table-dark" style="background-color:#0a8cb3;" >
                                                                    <tr class="cart_menu" style="width: 25%;">
                                                                        <td class="image">Codigo</td>
                                                                        <td class="image">Nombre</td>
                                                                        <td class="description">P.Compra(U)</td>
                                                                        <td class="price">Unidad</td>
                                                                        <td class="description">Contenido</td>
                                                                        <td class="description">Cantidad</td> 
                                                                        <td class="description">Precio Unt</td> 
                                                                        <td class="quantity">Sub Total</td>
                                                                        <td class="total">Quitar</td>
                                                                    </tr>

                                                                </thead>

                                                                <%
                                                                    DecimalFormat df = new DecimalFormat("0.00");
                                                                    DecimalFormatSymbols dfs = df.getDecimalFormatSymbols();
                                                                    dfs.setDecimalSeparator('.');
                                                                    df.setDecimalFormatSymbols(dfs);
                                                                    double total = 0;
                                                                    double igv = 0.0;
                                                                    double costoTotal = 0.0;

                                                                    ArrayList<DetalleCompra> listar = (ArrayList<DetalleCompra>) session.getAttribute("cotizacion");
                                                                    int fila = 0;

                                                                    if (listar != null) {
                                                                        for (DetalleCompra d : listar) {
                                                                %>
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="cart_description">
                                                                            <p><%= d.getProducto().getCodigo()%></p>
                                                                        </td>
                                                                        <td class="cart_description">
                                                                            <p><%= d.getProducto().getDescripcion()%></p>
                                                                        </td>


                                                                        <td class="col-md-1 col-md-offset-2">
                                                                            <input class="form-control text-center" id="pcompra<%=fila%>"   value="<%= d.getCosto()%>" name="txtCosto"  onchange="actualizarpreciocompra(<%= fila%>)" required="">
                                                                        </td>

                                                                        <td class="col-md-2 col-md-offset-3">

                                                                            <p class="col-lg-6"><%=OrdendeCompraDAO.getNombreunidad(d.getProducto().getIducompra())%></p>

                                                                            <input class="form-control text-center col-lg-5 "  type="number" value="<%= d.getCantidad()%>" min="1" id="txt_cantidad<%=fila%>" name="txtPro_cantidad" size="100" onchange="Actualizarcantidad(<%= fila%>)">
                                                                        </td>


                                                                        <td class="col-md-2 col-md-offset-3">
                                                                            <input class="form-control text-center col-lg-5 "  type="number" value="<%= d.getContenido()%>" min="1" id="txtPro_contenido<%=fila%>" name="txtContenido" size="100" onchange="actualizarContenido(<%= fila%>)">
                                                                        </td>
                                                                        <td class="cart_description">
                                                                            <p><%=d.getCantidad() * d.getContenido()%></p>
                                                                        </td>
                                                                        <td class="cart_quantity"><%=Math.round(d.getCosto() / d.getContenido()) %> </td>
                                                                        <td class="cart_quantity"><%=Math.round(d.getCantidad() * d.getCosto())%> </td>
                                                                        <td >
                                                                            <span id="idarticulo" style="display:none;"><%= d.getProducto().getIdproducto()%></span>

                                                                            <button style="background-color: transparent; color: red; border: none " id="deleteitem" class="delete">
                                                                                <i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i>
                                                                            </button>
                                                                        </td>


                                                                    </tr>
                                                                    <%
                                                                                fila++;
                                                                                costoTotal = costoTotal + d.getCantidad() * d.getCosto();
                                                                                igv = costoTotal * 0.18;
                                                                                total = costoTotal - igv;
                                                                            }
                                                                        }
                                                                    %>
                                                                </tbody>
                                                            </table>
                                                        </div> 
                                                        <br>
                                                        <div class="col-md-7 col-md-offset-5" style="color: #0083C9;">
                                                            <div class="card">

                                                                <div class="card-body  ">
                                                                    <div class="d-flex">
                                                                        <label class="col-sm-5">SUB TOTAL :</label>
                                                                        <input type="text" name="txtSubtotal" value="<%= Math.round(total * 100.0) / 100.0%>" readonly="readonly">
                                                                    </div>



                                                                    <div class="d-flex">
                                                                        <label class="col-sm-5">IGV :</label>
                                                                        <input type="text" name="txtIgv" value="<%= Math.round(igv * 100.0) / 100.0%>" readonly="readonly">
                                                                    </div>



                                                                    <div class="d-flex">
                                                                        <label class="col-sm-5">TOTAL :</label>
                                                                        <input type="text" name="txtTotal" value="<%= Math.round(costoTotal * 100.0) / 100.0%>" readonly="readonly">
                                                                    </div>

                                                                </div>

                                                            </div> 

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                  
                                        </div>                                                                                                                                             
                                    </div>
                                    <br>
                                </form>                                                                                                      
                            </div>
                        </div>                                                             
                    </div>
                </div>
            </div>
            <!-- add Modal HTML -->
            <div id="Buscarproveedor" class="modal fade" >
                <div class="modal-dialog" role="document" style="z-index: 10999; width:1000px">
                    <div class="modal-content">

                        <form method="post" action="ControllerAgregarProducto" accion="AnadirCarrito">
                            <div class="modal-header" > 
                               
                                <h4 class="modal-title">Lista  de Proveedores</h4>
                                <button type="button" class="close " data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                                <table id="tablaproveedor" class="table table-striped table-bordered table-sm " cellspacing="0"
                                       width="100%">
                                    <thead>
                                        <tr>
                                            <th style="display:none;">ID</th>
                                            <th>Agregar</th>
                                            <th>Razonsocial</th>
                                            <th>Ruc</th>
                                            <th>Correo</th> 
                                            <th>Direccion</th> 

                                        </tr>
                                    </thead>
                                    <tbody >
                                        <% ProvedorDAO prbv = new ProvedorDAO();
                                            List<Provedor> lest = prbv.ListadoProveedor();
                                            Iterator<Provedor> itre = lest.iterator();
                                            Provedor pp = null;
                                            while (itre.hasNext()) {
                                                pp = itre.next();

                                        %>
                                        <tr>
                                            <td  style="display:none;"id="idpro"><%=pp.getIdproveedor()%></td>
                                            <td>
                                                <a  href="ControllerProveedor?accion=buscarPorId&idProveedor=<%=pp.getIdproveedor()%>" ><i class="material-icons" style="color: #09bb04" data-toggle="tooltip" title="Ver">&#xe147;</i></a>
                                            </td>
                                            <td><%=pp.getRazonsocial()%></td>
                                            <td><%=pp.getNumdocumento()%></td> 
                                            <td><%=pp.getEmail()%></td>
                                            <td><%=pp.getDireccion()%></td>>
                                        </tr>
                                        <%
                                            }
                                        %>
                                    </tbody>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <a href="InsertarCompra.jsp" class="btn btn-default" >Canselar</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </section>  

        <!--Data table --->
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>                                
        <!--Data table --->
        <script src="EstiloAdministrador/funcionesyvalidaciones/Compras.js" type="text/javascript"></script>

    </body>

</html>

