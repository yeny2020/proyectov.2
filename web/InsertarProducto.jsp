<%@page import="com.pe.gatasygatos.model.entity.Publico"%>
<%@page import="com.pe.gatasygatos.DAO.PublicoDAO"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.pe.gatasygatos.model.entity.Almacen"%>
<%@page import="com.pe.gatasygatos.DAO.AlmacenDAO"%>
<%@page import="com.pe.gatasygatos.DAO.ProductoDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.UnidadVenta"%>
<%@page import="com.pe.gatasygatos.DAO.UnidadVentaDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.UnidadCompra"%>
<%@page import="com.pe.gatasygatos.DAO.UnidadCompraDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Provedor"%>
<%@page import="com.pe.gatasygatos.DAO.ProvedorDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Marca"%>
<%@page import="com.pe.gatasygatos.DAO.MarcaDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Categoria"%>
<%@page import="com.pe.gatasygatos.DAO.CategoriaDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <%@include file="css-js.jsp" %>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Bootstrap CRUD Data Table for Database with Modal Form</title>
        <!--BOOSTRAP PARA DIV-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <!--BOOSTRAP PARA DIV-->
        <!--Estilo tabla-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link rel="stylesheet" href="EstiloAdministrador/css/Stilodetabla.css"> 
        <!--Estilo tabla-->
        <%--Buscador.---%>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
        <%-- finBuscador.---%>
    </head>
    <body>
        <!--cabecera de Menu -->
        <%@include file="FrmAdmin.jsp" %>
        <!-- pageContent -->
        <section class="full-width pageContent">

            <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect"> 


                <div class="mdl-tabs__panel is-active" id="tabNewProduct">
                    <div class="mdl-grid">
                        <div class="mdl-cell mdl-cell--2-col-phone mdl-cell--10-col-tablet mdl-cell--12-col-desktop mdl-cell--0-offset-desktop">
                            <div class="full-width panel mdl-shadow--2dp">
                                <div style="background-color:#4e96b3;" class="full-width panel-tittle  text-center tittles">
                                    Nuevo Producto
                                </div>                             
                                <div class="full-width panel-content">

                                    <form id="newproducto"  action="ControllerProducto" method="Post" name="frm_nuevo" >
                                        <div class="d-flex">
                                            <div class="col-sm-12">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <%                                                            ProductoDAO com = new ProductoDAO();
                                                            String numserie = com.Numserie();

                                                        %>
                                                        <label for="Txtcodigo" class="col-lg-2" style="text-align:right">Codigo: </label>
                                                        <div class="col-lg-4">
                                                            <input type="text" name="Txtcodigo" value="<%=numserie%>" id="Txtnombre" class="form-control" readonly="">
                                                            <span class="help-block"></span>
                                                        </div>

                                                        <div class="form-group">                                     
                                                            <label for="" class="col-lg-2" style="text-align:right"></label>
                                                            <div class="col-lg-4"><p  class="border-focus-darkblue form-control-static" readonly="" style="color: white">Productos</p><span class="help-block"></span></div>
                                                        </div>  
                                                        <div class="form-group">                                     
                                                            <label for="Txtdescripcion" class="col-lg-2" style="text-align:right">Descripcion: </label>
                                                            <div class="col-lg-10"> <textarea cols="30" rows="2" name="Txtdescripcion" id="txtDescripcion" class="border-focus-darkblue form-control"></textarea>
                                                                <span class="help-block"></span></div>
                                                        </div>
                                                        <div class="form-group"> 
                                                            <label for="Txtcategoria" class="col-lg-2" style="text-align:right">Categoria:</label>
                                                            <div class="col-lg-4">
                                                                <select class="js-example-basic-single form-control" name="Txtcategoria" id="txtIdcategoria">
                                                                    <%--select class="js-example-basic-single" name="Txtcategoria" id="txtIdcategoria"--%>
                                                                    <option   value="" disabled="" selected="">Seleciona Categoria</option>

                                                                    <% CategoriaDAO cat = new CategoriaDAO();
                                                                        List<Categoria> lista = cat.ListadoEstadoActivos();
                                                                        Iterator<Categoria> iter = lista.iterator();
                                                                        Categoria c = null;
                                                                        while (iter.hasNext()) {
                                                                            c = iter.next();

                                                                    %>

                                                                    <option  value="<%=c.getIdcategoria()%>" required><%=c.getNombre()%></option>
                                                                    <%
                                                                        }
                                                                    %>
                                                                </select>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group"> 
                                                            <label  for="Txtmarca" class="col-lg-2" style="text-align:right">Marca : </label>                                                      
                                                            <div class="col-lg-4">
                                                                <select class="js-example-basic-single form-control" name="Txtmarca" id="txtIdmarca">
                                                                    <%--select  class="js-example-basic-single" name="Txtmarca" id="txtIdcategoria" --%>
                                                                    <option   value=""disabled="" selected="">Seleciona Marca</option>
                                                                    <% MarcaDAO mar = new MarcaDAO();
                                                                        List<Marca> list = mar.ListadoEstadoActivos();
                                                                        Iterator<Marca> ite = list.iterator();
                                                                        Marca m = null;
                                                                        while (ite.hasNext()) {
                                                                            m = ite.next();

                                                                    %>

                                                                    <option  value="<%=m.getIdmarca()%>" required><%=m.getNombre()%></option>
                                                                    <%
                                                                        }
                                                                    %>
                                                                </select>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group"> 
                                                            <label for="TxtIdpublico" class="col-lg-2" style="text-align:right">Publico: </label>                                                      
                                                            <div class="col-lg-4">
                                                                <select class="js-example-basic-single form-control" name="TxtIdpublico" id="TxtIdpublico">
                                                                    <option   value="" disabled="" selected="">Seleciona Publico</option>

                                                                    <% PublicoDAO pd = new PublicoDAO();
                                                                        List<Publico> listaa = pd.listarPublico();
                                                                        Iterator<Publico> iterr = listaa.iterator();
                                                                        Publico pbl = null;
                                                                        while (iterr.hasNext()) {
                                                                            pbl = iterr.next();

                                                                    %>

                                                                    <option  value="<%=pbl.getIdpublico()%>" required><%=pbl.getNombre()%></option>
                                                                    <%
                                                                        }
                                                                    %>
                                                                </select>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group"> 
                                                            <label for="Txtproveedor" class="col-lg-2" style="text-align:right">Proveedor:</label>
                                                            <div class="col-lg-4">
                                                                <select class="js-example-basic-single form-control" name="Txtproveedor" id="txtIdproveedor">                                          
                                                                    <option   value=""disabled="" selected="">Seleciona Proveedor</option>
                                                                    <% ProvedorDAO pro = new ProvedorDAO();
                                                                        List<Provedor> li = pro.ListadoEstadoActivos();
                                                                        Iterator<Provedor> i = li.iterator();
                                                                        Provedor pr = null;
                                                                        while (i.hasNext()) {
                                                                            pr = i.next();

                                                                    %>
                                                                    <option  value="<%=pr.getIdproveedor()%>" required> <%=pr.getRazonsocial()%></option>
                                                                    <%
                                                                        }
                                                                    %>
                                                                </select>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group"> 
                                                            <label for="Txtunidadc" class="col-lg-2" style="text-align:right">Unidad Compra:</label>
                                                            <div class="col-lg-4">
                                                                <select class="js-example-basic-single form-control" name="Txtunidadc" id="Txtunidadc">
                                                                    <option   value=""disabled="" selected="">Unidad Compra</option>
                                                                    <% UnidadCompraDAO uni = new UnidadCompraDAO();
                                                                        List<UnidadCompra> l = uni.ListaUnidadCompra();
                                                                        Iterator<UnidadCompra> itr = l.iterator();
                                                                        UnidadCompra u = null;
                                                                        while (itr.hasNext()) {
                                                                            u = itr.next();

                                                                    %>
                                                                    <option  value="<%=u.getIducompra()%>" required> <%=u.getNombre()%></option>
                                                                    <%
                                                                        }
                                                                    %>
                                                                </select>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group"> 
                                                            <label for="Txtunidadv" class="col-lg-2" style="text-align:right">Unidad Venta:</label>
                                                            <div class="col-lg-4">
                                                                <select class="js-example-basic-single form-control" name="Txtunidadv" id="Txtunidadv">
                                                                    <option   value=""disabled="" selected="">Unidad Venta</option>
                                                                    <% UnidadVentaDAO univ = new UnidadVentaDAO();
                                                                        List<UnidadVenta> lt = univ.ListaUnidadVenta();
                                                                        Iterator<UnidadVenta> itra = lt.iterator();
                                                                        UnidadVenta uv = null;
                                                                        while (itra.hasNext()) {
                                                                            uv = itra.next();

                                                                    %>
                                                                    <option  value="<%=uv.getIduventa()%>" required> <%=uv.getNombre()%></option>
                                                                    <%
                                                                        }
                                                                    %>
                                                                </select>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>      
                                                        <div class="form-group">                                     
                                                            <label for="Txtpcompra" class="col-lg-2" style="text-align:right">Precio Compra S/: </label>
                                                            <div class="col-lg-4">
                                                                <input type="text" name="Txtpcompra" placeholder="Precio de compra" id="Txtprecioc" value="00.00" class="form-control">
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">                                     
                                                            <label for="Txtpventa" class="col-lg-2" style="text-align:right">Precio Venta S/: </label>
                                                            <div class="col-lg-4">
                                                                <input type="text" name="Txtpventa" placeholder="Precio venta" id="Txtunidad" value="00.00" class="form-control">
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>


                                                        <% Date dNow = new Date();
                                                            SimpleDateFormat ft
                                                                    = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                                            String currentDate = ft.format(dNow);
                                                        %>

                                                        <div class="form-group">
                                                            <label for="Txtfechaderegistro" class="col-lg-2" style="text-align:right">Fecha Registro:</label>
                                                            <div class="col-lg-4"><input type="" name="Txtfechaderegistro"value="<%=currentDate%>" id="fecha" class="border-focus-darkblue form-control" readonly="">
                                                                <span class="help-block"></span></div>
                                                        </div>
                                                    </div>  

                                                    <div class="modal-footer"> 
                                                        <a href="Producto.jsp" style="background-color: #09bb04;color: #ffffff;"class="btn " ><i class="fas fa-undo-alt"></i><span>&nbsp; Regresar</span></a>    
                                                        <input onclick="return validarnewproducto()" style="background-color:#4e96b3;color: #eeeeee;" type="submit" class="btn btn-outline-primary" name="accion" value="Grabar">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>

    <script>
// In your Javascript (external .js resource or <script> tag)
        $(document).ready(function () {
            $('.js-example-basic-single').select2();
        });

    </script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>                                

    <script src="EstiloAdministrador/funcionesyvalidaciones/Producto.js" type="text/javascript"></script>
</html>