<%@page import="com.pe.gatasygatos.model.entity.Provedor"%>
<%@page import="com.pe.gatasygatos.DAO.ProvedorDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.pe.gatasygatos.model.entity.TipoDocumento"%>
<%@page import="com.pe.gatasygatos.DAO.TipoDocumentoDAO"%>
<%@page import="java.util.Date"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Categoria</title>
        <link href="EstiloAdministrador/css/ESTILOSPROVEEDOR.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <!-- add Modal HTML -->
        <div>
            <%   ProvedorDAO com = new ProvedorDAO();
                String numserie = com.Numserie();

            %>

            <div class="modal-content">
                <form id="newproveedor" action="ControllerProveedor" method="Post" name="frm_nuevo">

                    <div style="background-image: linear-gradient(to top, #2196f37a 0%, #ffffff 100%);"class="modal-header">      
                        <h4 class="modal-title" style="color:#000;">Agregar Proveedor</h4>
                        <a href="Proveedor.jsp" class="close" ><i class="material-icons"  title="Close" style="color: #000;">&times;</i></a>
                    </div>
                    <div class="modal-body"> 
                        <div class="form-group">
                            <label for="txtCod" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Codigo</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"><input type="text" name="TextCodigo" value="<%=numserie%>"class="form-control" readonly=""><span class="help-block"></span></div>
                        </div>
                    </div> 
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="TxtRazonsocial" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Razon social</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                <input type="text" name="TxtRazonsocial" id="Txtnombre" onkeypress="return soloLetras(event)" class="border-focus-darkblue form-control"   pattern="^[a-zA-ZáéíóúÁÉÍÓÚñÑ-\s]+$">
                                <span class="help-block"></span></div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="TxtDireccion" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Direccion</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"><input type="text" name="TxtDireccion"  id="Txtcelular" class="border-focus-darkblue form-control"><span class="help-block"></span></div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="TxtCelular"  class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Celular</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"><input type="text" name="TxtCelular"  id="Txtdireccion" class="border-focus-darkblue form-control" maxlength="9" pattern="[0-9]{9,9}" onkeypress="return soloNumeros(event)"><span class="help-block"></span></div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="TxtEmail"  class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Correo contacto</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"><input type="text" name="TxtEmail"  class="border-focus-darkblue form-control" pattern="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$" required><span class="help-block"></span></div>
                        </div>
                    </div>
                   <div class="modal-body">
                        <div class="form-group">
                            <label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Tipo de Doc. de Identidad</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" class="border-focus-darkblue form-control">
                                <select name="TxtIdtipodocumento" id="Txtidtipodocumento" class="form-control">
                                    <% TipoDocumentoDAO tdoc = new TipoDocumentoDAO();
                                        List<TipoDocumento> lista = tdoc.ObtenerRUC();
                                        Iterator<TipoDocumento> iterr = lista.iterator();
                                        TipoDocumento doc = null;
                                        while (iterr.hasNext()) {
                                            doc = iterr.next();

                                    %>

                                    <option  value="<%=doc.getIdtipodocumento()%>"><%=doc.getTipoDocumento()%></option>
                                    <%
                                        }
                                    %>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="TxtNumdocumento" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">(Doc. principal)</label>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"><input required type="text" name="TxtNumdocumento" id="input_ruc"  onkeypress="return soloNumeros(event)"oninput="validarInput(this)"
                                                                                      placeholder="Ingrese su RUC" maxlength="11" pattern="[0-9]{11,11}" id="Txtnumerodocumento" class="border-focus-darkblue form-control">
                               <!--  <pre id="resultado"></pre>
                                <pre id="existente"></pre>-->
                                <span class="help-block"></span>
                               
                            </div>
                        </div>                                
                    </div>
                    <% Date dNow = new Date();
                        SimpleDateFormat ft
                                = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        String currentDate = ft.format(dNow);
                    %>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-lg-4 col-md-4 col-sm-12 col-xs-12 control-label">Fecha Registro</label>
                            <div class="col-lg-7 col-md-8 col-sm-12 col-xs-12">
                                <input type="text" name="TxtFechaRegistro" value="<%=currentDate%>" id="" class="border-focus-darkblue form-control" readonly="">
                                <span class="help-block"></span></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input onclick="return validarnewProveedor()"  class="btn btn-success" type="submit"  style="background-color:#0A8CB3" name="accion" value="add">
                    </div>
                </form>
            </div>

        </section>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>                                

        <!--Funciones y validaciones --->
        <!--Funciones y validaciones --->
        <script src="EstiloAdministrador/funcionesyvalidaciones/Proveedor.js" type="text/javascript"></script>


</body>
</html>
