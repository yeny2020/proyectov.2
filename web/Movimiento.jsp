
<%@page import="com.pe.gatasygatos.DAO.ClienteDAO"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.pe.gatasygatos.model.entity.Venta"%>
<%@page import="com.pe.gatasygatos.DAO.VentaDAO"%>
<%@page import="com.pe.gatasygatos.DAO.MaterialDAO"%>
<%@page import="com.pe.gatasygatos.DAO.MarcaDAO"%>
<%@page import="com.pe.gatasygatos.DAO.CategoriaDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Producto"%>
<%@page import="com.pe.gatasygatos.DAO.ProductoDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<link href="EstiloAdministrador/css/ESTILO_TABLAS.css" rel="stylesheet" type="text/css"/>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Categoria</title>

        <%@include file="css-js.jsp" %>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Bootstrap CRUD Data Table for Database with Modal Form</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" href="EstiloAdministrador/css/Stilodetabla.css"> 
        <script type="text/javascript" src="EstiloAdministrador/js/ValidadForm.js"></script>

    </head>
    <style>
        /* estado descactivado */
        markdesactivado{

            background-color: #ff1744;
            color: black;
            border-radius: 71px 69px 72px 71px;
            -moz-border-radius: 71px 69px 72px 71px;
            -webkit-border-radius: 71px 69px 72px 71px;
        }
        /* estado activo */
        markactivo{

            background-color: #00FF00;
            color: black;
            border-radius: 71px 69px 72px 71px;
            -moz-border-radius: 71px 69px 72px 71px;
            -webkit-border-radius: 71px 69px 72px 71px;
        }
    </style>

    <body>

        <!--cabecera de Menu -->
      <%@include file="FrmAdmin.jsp" %>

        <!-- pageContent -->
        <section class="full-width pageContent">

            <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
                <!--modal listar-->
                <div class="mdl-tabs__panel is-active" id="tabListCategory">
                    <div class="mdl-grid">
                        <div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop mdl-cell--0-offset-desktop">
                            <div class="full-width panel mdl-shadow--2dp">
                                <div  style="background-color:#4e96b3;" class="full-width panel-tittle text-center tittles">
                                    Gestionar Venta
                                </div>

                                <%-- <%ProductoDAO pdaoStcok = new ProductoDAO();
                                    List<Producto> listStock = pdaoStcok.ListadoStockMinimo();
                                    Iterator<Producto> iterstock = listStock.iterator();
                                    Producto pstock = null;
                                    while (iterstock.hasNext()) {
                                        pstock = iterstock.next();%>

                                <div class="full-width panel-tittle bg-danger text-center tittles">
                                    Alerta Stock:<%=pstock.getNombre()%>=<%= pstock.getStocktienda()%>

                                </div>
                                <%}%>--%>
                                <div class="full-width panel-content">                        
                                    <div class="col-sm-6 mdl-textfield">
                                        <a href="Listadeproductosparapedido.jsp" style="background-color:#4e96b3;color: #ffffff;"class="btn " ><span>Nueva venta</span></a>    
                                    </div>


                                    <table id="Datatable" class="table table-bordered table-hover projects"style="border-collapse: collapse;" >
                                        <thead style="background-color: #009688;">
                                            <tr>

                                                <th>N°</th>
                                                <th class="mdl-data-table__cell--non-numeric">Cliente</th>
                                                <th>Documento</th>
                                                <th>Serie</th>
                                                <th>Correlativo</th>
                                                <th>Fecha</th>
                                                <th>total</th>
                                                <th>Estado</th>
                                                <th>opcion</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%  VentaDAO pdao = new VentaDAO();
                                                List<Venta> listS = pdao.ListadoVenta();
                                                Iterator<Venta> iterr = listS.iterator();
                                                DecimalFormat formatea = new DecimalFormat("###,###.##");
                                                String totales = "";
                                                Venta pro = null;
                                                double total = 0;
                                                while (iterr.hasNext()) {
                                                    pro = iterr.next();%>

                                            <tr>
                                                <td><%= pro.getIdventa()%></td>
                                                <td class="mdl-data-table__cell--non-numeric"><%= ClienteDAO.getcliente(pro.getIdcliente())%></td>
                                                <td><%= pro.getTipocomprobante()%></td>
                                                <td><%= pro.getSerie()%></td>
                                                <td><%= pro.getCorrelativo()%></td>
                                                <td><%= pro.getFechayhora()%></td>
                                                <td>S/ <%= pro.getTotal()%></td>
                                                <% total = total + pro.getTotal();
                                                    totales = formatea.format(total);
                                                %>
                                                <% String Estado = pro.getEstado();
                                                    if (Estado.equalsIgnoreCase("Vendido")) {%>

                                                <td><markactivo><%= Estado%></markactivo></td>   
                                                <%   } else {%>

                                        <td><markdesactivado><%= Estado%></markdesactivado></td>    
                                            <%     }
                                            %>
                                        <td>

                                            <%
                                                if (Estado.equalsIgnoreCase("Vendido")) {%>

                                            <a href="Detalleventacontroller?accion=estado&id=<%= pro.getIdventa()%>" class="btn btn-danger" >Anular</a>

                                            <%   } else { %>

                                            <%     }
                                            %>
                                            <a href="Detalleventacontroller?accion=reporte&id=<%= pro.getIdventa()%>" class="btn btn-success" >Detalle</a>

                                        </td>
                                        </tr>
                                        <%}%>

                                        </tbody>
                                    </table>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>                                                             
            </div>
        </section>
        <!-- Delete Modal HTML -->
        <div id="deleteEmployeeModal" class="modal fade" >
            <div class="modal-dialog" role="document" style="z-index: 9999; width: 450px">
                <div class="modal-content">
                    <form>
                        <div class="modal-header">      
                            <h4 class="modal-title">Eliminar Categoria</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">     
                            <p>Seguro que quiere Eliminar?</p>
                            <!--    <p class="text-warning"><small>This action cannot be undone.</small></p>-->
                        </div>
                        <div class="modal-footer">
                            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                            <input type="submit" class="btn btn-danger" value="Delete">
                            <a href="ControllerCategoria?accion=eliminar&id=  " class="btn btn-danger"><i class="entypo-cancel"></i> Borrar  </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>    
        <!--Formulario de Modificar-->
        <div class="modal fade" id="myModalEditProducto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document" style="z-index: 9999; width: 450px">
                <div class="modal-content">

                    <div class="modal-body">


                    </div>
                </div>                    
            </div>
        </div>
        <h2 align="center">
            <%
                if (request.getAttribute("mensaje") != null) {
                    out.print("<Script>");
                    out.print("alert('" + request.getAttribute("mensaje") + "')");
                    out.print("</Script>");
                }
            %>            
        </h2>


        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>                                

    </body>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#Datatable').DataTable();
        });

        function sendData(c) {
            var parametros = {"action": "editar", 'id': c};
            $.ajax({
                url: '../Controller',
                method: 'POST',
                data: parametros,

                success: function (response) {
                    var resp = $.parseJSON(response);


                }
            })


        }
    </script>

</html>
