
<%@page import="com.pe.gatasygatos.DAO.ProvedorDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Compra"%>
<%@page import="com.pe.gatasygatos.DAO.CompraDAO"%>
<%@page import="com.pe.gatasygatos.DAO.ClienteDAO"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.pe.gatasygatos.model.entity.Venta"%>

<%@page import="com.pe.gatasygatos.model.entity.Producto"%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<link href="EstiloAdministrador/css/ESTILO_TABLAS.css" rel="stylesheet" type="text/css"/>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Categoria</title>
        <%@include file="css-js.jsp" %> 
        <!--ICONOS-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <!--ICONOS-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
        <!--Estilo tabla-->
        <link rel="stylesheet" href="EstiloAdministrador/css/Stilodetabla.css"> 
        <link href="EstiloAdministrador/css/STYLE_TABLE.css" rel="stylesheet" type="text/css"/>
        <!--Estilo tabla-->
    </head>
    <style>
        /* estado descactivado */
        markdesactivado{
            background-color: #ff1744;
            color: black;
            border-radius: 71px 69px 72px 71px;
            -moz-border-radius: 71px 69px 72px 71px;
            -webkit-border-radius: 71px 69px 72px 71px;
        }
        /* estado activo */
        markactivo{
            background-color: #00FF00;
            color: black;
            border-radius: 71px 69px 72px 71px;
            -moz-border-radius: 71px 69px 72px 71px;
            -webkit-border-radius: 71px 69px 72px 71px;
        }
    </style>
    <body>
        <!--cabecera de Menu -->
        <%@include file="FrmAdmin.jsp" %>
        <!-- pageContent -->
        <section class="full-width pageContent">

            <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
                <!--modal listar-->
                <div class="mdl-tabs__panel is-active" id="tabListCategory">
                    <div class="mdl-grid">
                        <div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop mdl-cell--0-offset-desktop">
                            <div class="full-width panel mdl-shadow--2dp">
                                <div  style="background-color:#0a8cb3;" class="full-width panel-tittle text-center tittles">
                                    Listado Nota de Ingreso
                                </div>
                                <%-- <%ProductoDAO pdaoStcok = new ProductoDAO();
                                    List<Producto> listStock = pdaoStcok.ListadoStockMinimo();
                                    Iterator<Producto> iterstock = listStock.iterator();
                                    Producto pstock = null;
                                    while (iterstock.hasNext()) {
                                        pstock = iterstock.next();%>

                                <div class="full-width panel-tittle bg-danger text-center tittles">
                                    Alerta Stock:<%=pstock.getNombre()%>=<%= pstock.getStocktienda()%>
                                </div>
                                <%}%>--%>
                                <div class="full-width panel-content">                        
                                    <div class="col-sm-6 mdl-textfield">
                                        <a href="OrdenCompra.jsp" style="background-color:#4e96b3;color: #ffffff;"class="btn "><i class="material-icons" style="color: #fffff;" data-toggle="tooltip" title="Agregar Cotizacion"></i></a>    

                                    </div>
                                    <table id="dtHorizontalVerticalExample" class="table table-striped table-bordered table-sm " cellspacing="0"
                                           width="100%">
                                        <thead style="background-color: #0a8cb3;">
                                            <tr>
                                                <th>N°</th>
                                                <th>Serie</th>
                                                <th>Correlativo</th>
                                                 <th>Documento</th>
                                                <th>Total</th>
                                                <th>Fecha</th>
                                                <th>opcion</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%  CompraDAO pdao = new CompraDAO();
                                                List<Compra> listS = pdao.ListadoNotaIngreso();
                                                Iterator<Compra> iterr = listS.iterator();
                                                DecimalFormat formatea = new DecimalFormat("###,###.##");
                                                String totales = "";
                                                Compra com = null;
                                                double total = 0;
                                                while (iterr.hasNext()) {
                                                    com = iterr.next();%>
                                            <tr>
                                                <td><%=com.getIdcompra()%></td>
                                                <td><%=com.getSerie()%></td>
                                                <td><%=com.getCorrelativo()%></td>
                                                <td><%= com.getTipocomprobante()%></td>
                                                <td>S/<%= com.getTotal()%></td>
                                                <% total = total + com.getTotal();
                                                    totales = formatea.format(total);
                                                %>
                                                <td><%= com.getFechaYhora()%></td>
                                                
                                          
                                        <td>
                                         
                                            <a href="ControllerDetallleCompra?accion=reporte&id=<%= com.getIdcompra()%>" class="btn btn-primary" style="background: #5e35b1;color:#fff;">Detalle</a>
                                            <a href="ControllerFacturaCompra?accion=ReferenciarRegistrarFactura&id=<%= com.getIdcompra()%>" class="btn btn-primary" style="background: #4c64e8;color:#fff;">Registrar Factura</a>

                                        </td>
                                        </tr>
                                        <%}%>
                                        </tbody>
                                    </table>



                                </div>
                            </div>
                        </div>
                    </div>
                </div>                                                             
            </div>
        </section>
        <!-- Delete Modal HTML -->
        <div id="deleteEmployeeModal" class="modal fade" >
            <div class="modal-dialog" role="document" style="z-index: 9999; width: 450px">
                <div class="modal-content">
                    <form>
                        <div class="modal-header">      
                            <h4 class="modal-title">Eliminar Categoria</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">     
                            <p>Seguro que quiere Eliminar?</p>
                            <!--    <p class="text-warning"><small>This action cannot be undone.</small></p>-->
                        </div>
                        <div class="modal-footer">
                            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                            <input type="submit" class="btn btn-danger" value="Delete">
                            <a href="ControllerCategoria?accion=eliminar&id=  " class="btn btn-danger"><i class="entypo-cancel"></i> Borrar  </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>    
        <!--Formulario de Modificar-->
        <div class="modal fade" id="myModalEditProducto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document" style="z-index: 9999; width: 450px">
                <div class="modal-content">

                    <div class="modal-body">


                    </div>
                </div>                    
            </div>
        </div>
        <h2 align="center">
            <%
                if (request.getAttribute("mensaje") != null) {
                    out.print("<Script>");
                    out.print("alert('" + request.getAttribute("mensaje") + "')");
                    out.print("</Script>");
                }
            %>            
        </h2>
        <!--Data table --->
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>                                
        <!--Data table --->
        <!--Funciones y validaciones --->
        <!--Funciones y validaciones ---> 
    </body>
</html>

<script>

    //scroll de la tabla y datatable
    $(document).ready(function () {
        $('#dtHorizontalVerticalExample').DataTable({
            "scrollX": true,
            "scrollY": 280,
        });
        $('.dataTables_length').addClass('bs-select');
    });
</script>