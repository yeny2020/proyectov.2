
<%@page import="com.pe.gatasygatos.DAO.PublicoDAO"%>
<%@page import="com.pe.gatasygatos.DAO.UnidadVentaDAO"%>
<%@page import="com.pe.gatasygatos.DAO.UnidadCompraDAO"%>
<%@page import="com.pe.gatasygatos.DAO.MarcaDAO"%>
<%@page import="com.pe.gatasygatos.DAO.CategoriaDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Producto"%>
<%@page import="com.pe.gatasygatos.DAO.ProductoDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Producto</title>
        <%@include file="css-js.jsp" %> 
        <!--ICONOS-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <!--ICONOS-->
        <link rel="stylesheet" type="text/css" href="EstiloAdministrador/table/datatables/datatables.min.css"/>
        <link rel="stylesheet"  type="text/css" href="EstiloAdministrador/table/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">
        <!--Estilo tabla-->
        <link rel="stylesheet" href="EstiloAdministrador/css/Stilodetabla.css"> 
        <link href="EstiloAdministrador/css/STYLE_TABLE.css" rel="stylesheet" type="text/css"/>
    </head>
    <style>
        /* estado descactivado */
        markdesactivado{

            background-color: #ff1744;
            color: black;
            border-radius: 71px 69px 72px 71px;
            -moz-border-radius: 71px 69px 72px 71px;
            -webkit-border-radius: 71px 69px 72px 71px;
        }
        /* estado activo */
        markactivo{

            background-color: #00FF00;
            color: black;
            border-radius: 71px 69px 72px 71px;
            -moz-border-radius: 71px 69px 72px 71px;
            -webkit-border-radius: 71px 69px 72px 71px;
        }
    </style>

    <body>

        <!--cabecera de Menu -->
        <%@include file="FrmAdmin.jsp" %>

        <!-- pageContent -->
        <section class="full-width pageContent">

            <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
                <!--modal listar-->
                <div class="mdl-tabs__panel is-active" id="tabListCategory">
                    <div class="mdl-grid">
                        <div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop mdl-cell--0-offset-desktop">
                            <div class="full-width panel mdl-shadow--2dp">
                                <div  style="background-color:#4e96b3;" class="full-width panel-tittle text-center tittles">
                                    Gestionar Productos
                                </div>
                                <div class="col-sm-6 mdl-textfield">
                                    <a href="InsertarProducto.jsp" style="background-color:#4e96b3;color: #ffffff;"class="btn " ><i class="fas fa-folder-plus"></i><span>&nbsp; Nuevo</span></a>    
                                </div>
                                <div class="full-width panel-content"> 


                                    <table id="dtHorizontalVerticalExample" class="table table-striped table-bordered table-sm " cellspacing="0"
                                           width="100%">
                                        <thead>
                                            <tr>
                                                <th style="display:none;">Id</th>
                                                <th>Codigo</th>
                                                <th>Descripcion</th>
                                                <th>Categoria</th>
                                                <th>Marca</th>
                                                <th>Costo</th>
                                                <th>Precio</th>
                                                <th>Und.Venta</th>
                                                <th>Und.Compra</th>
                                                <th>Estado</th>
                                                <th>Publico</th>
                                                <th>Provedor</th>                                               
                                                <th>Fecha Registro</th>                                               
                                                <th>Opciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%ProductoDAO pdao = new ProductoDAO();
                                                List<Producto> listS = pdao.ListadoProducto();
                                                Iterator<Producto> iterr = listS.iterator();
                                                Producto pro = null;
                                                while (iterr.hasNext()) {
                                                    pro = iterr.next();%>

                                            <tr>
                                                <td style="display:none;;" id="idpro"><%= pro.getIdproducto()%></td>

                                                <td><%= pro.getCodigo()%></td>
                                                <td><%= pro.getDescripcion()%></td>
                                                <td><%= CategoriaDAO.getNombreCategoria(pro.getIdcategoria())%></td>
                                                <td><%= MarcaDAO.getNombreMarca(pro.getIdmarca())%></td>

                                                <td><%= pro.getPreciocompra()%></td>
                                                <td><%= pro.getPrecioVenta()%></td>
                                                <td><%= UnidadVentaDAO.getNombreUnidadventa(pro.getIduventa())%></td>
                                                <td><%= UnidadCompraDAO.getNombreUnidadcompra(pro.getIducompra())%></td>


                                                <% String Estado = ProductoDAO.estado(pro.getIdproducto());
                                                    if (Estado.equalsIgnoreCase("Activo")) {%>

                                                <td><markactivo><%= Estado%></markactivo></td>   
                                                <%   } else {%>

                                        <td><markdesactivado><%= Estado%></markdesactivado></td>    
                                            <%     }
                                            %>
                                        <td><%= PublicoDAO.getNombrePublico(pro.getIdpublico())%></td>
                                        <td><%=ProductoDAO.getNombreprovedor(pro.getIdProveedor())%></td>
                                        <td><%= pro.getFechaRegistro()%></td>
                                        <td>

                                            <a href="ControllerProducto?accion=editar&id=<%=pro.getIdproducto()%>"  class="editar" ><i class="material-icons"  title="Editar" style="color: #FFA006;">&#xE254;</i></a>
                                            <!--<a  href="Productodetalle.jsp?Id=<%=pro.getIdproducto()%>"><i class="material-icons" style="color:  #0000FF;" data-toggle="tooltip" title="Detalle">&#xe8f4;</i></a>-->
                                            <button style="background-color: transparent; color: red; border: none " id='btn-estado' class="delete">
                                                <i class="material-icons" data-toggle="tooltip" title="Estado" style="color:  #9575cd;">&#xE627;</i>
                                            </button>
                                            <!--<a<button style="background-color: transparent; color: red; border: none " id='btn-eliminar' class="delete">
                                                <i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i>
                                            </button>-->

                                        </td>

                                        </tr>
                                        <%}%>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>                                                             
    </section>
    <!--Funciones y validaciones --->
    <script src="EstiloAdministrador/funcionesyvalidaciones/Producto.js" type="text/javascript"></script>
    <!-- jQuery, Popper.js, Bootstrap JS -->
    <script src="EstiloAdministrador/table/jquery/jquery-3.3.1.min.js" type="text/javascript"></script>
    <script src="EstiloAdministrador/table/popper/popper.min.js" type="text/javascript"></script>
    <script src="EstiloAdministrador/table/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- datatables JS -->    
    <script src="EstiloAdministrador/table/datatables/datatables.min.js" type="text/javascript"></script>
    <!-- para usar botones en datatables JS -->  
    <script src="EstiloAdministrador/table/datatables/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>  
    <script src="EstiloAdministrador/table/datatables/JSZip-2.5.0/jszip.min.js"></script>    
    <script src="EstiloAdministrador/table/datatables/Buttons-1.5.6/js/buttons.html5.min.js"></script>
 
</body>

<script>
    //Scroll tabla
    /*  $(document).ready(function () {
     $('#dtHorizontalVerticalExample').DataTable({
     "scrollX": true,
     "scrollY": 480
     });
     $('.dataTables_length').addClass('bs-select');
     });*/

    $(document).ready(function () {
        $('#dtHorizontalVerticalExample').DataTable({
            "scrollX": true,
            "scrollY": 340,
            language: {
                "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "No se encontraron resultados",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sSearch": "Buscar:",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "sProcessing": "Procesando...",
            },
            //para usar los botones   
            responsive: "true",
            dom: 'Bfrtilp',
            buttons: [
                {
                    extend: 'excelHtml5',
                    text: '<i class="fas fa-file-excel">&nbsp; &nbsp; Exportar</i> ',
                    titleAttr: 'Exportar a Excel',
                    className: 'btn btn-success'
                },
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i> ',
                    titleAttr: 'Imprimir',
                    className: 'btn btn-info'
                }
            ]
        });
    });
</script>

</html>