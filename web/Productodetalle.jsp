<%-- 
    Document   : anadircarrito
    Created on : 03/10/2020, 12:17:00 AM
    Author     : yenny
--%>

<%@page import="com.pe.gatasygatos.DAO.MaterialDAO"%>
<%@page import="com.pe.gatasygatos.DAO.MarcaDAO"%>
<%@page import="com.pe.gatasygatos.DAO.CategoriaDAO"%>
<%@page import="com.pe.gatasygatos.DAO.ProductoDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Producto"%>
<%@page import="java.util.*" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Producto p = ProductoDAO.obtenerProducto(Integer.parseInt(request.getParameter("Id")));
%>
<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <%@include file="css-js.jsp" %>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Bootstrap CRUD Data Table for Database with Modal Form</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="EstiloAdministrador/css/Stilodetabla.css"> 
        <script type="text/javascript" src="EstiloAdministrador/js/ValidadForm.js"></script>


        <link href="EstiloAdministrador/css/mainC.css" rel="stylesheet">


    </head>
    <body>

        <!--cabecera de Menu -->
        <!--cabecera de Menu -->
        <%@include file="FrmAdmin.jsp" %>

        <!-- pageContent -->
        <section class="full-width pageContent">

            <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
                <!--modal listar-->
                <div class="mdl-tabs__panel is-active" id="tabListCategory">
                    <div class="mdl-grid">
                        <div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop mdl-cell--0-offset-desktop">
                            <div class="full-width panel mdl-shadow--2dp">
                                <div class="full-width panel-tittle  text-center tittles" style="background-color: #0000FF;">
                                    DETALLE DEL PRODUCTO
                                </div>

                                <br>
                                <form method="post" action="ControllerAgregarProducto">
                                    <div class="d-flex">                                 

                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="form-group">
                                                        <div class="form-group">                                     
                                                            <label style="color: blue;" for="Txtpcodigo" class="col-lg-2 col-md-4 col-sm-12 col-xs-12 control-label">PRECIO DE COMPRA S/: </label>
                                                            <div class="col-lg-4 col-md-2 col-sm-8 col-xs-12"><h5><%=p.getCodigo()%></h5>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>                                      
                                                        <div class="form-group">                                     
                                                            <label style="color: blue;" for="Txtnombre" class="col-lg-2 col-md-4 col-sm-12 col-xs-12 control-label">NOMBRE: </label>
                                                            <div class="col-lg-4 col-md-2 col-sm-8 col-xs-12"> <h5><%=p.getNombre()%></h5>
                                                                <span class="help-block"></span></div>
                                                        </div>

                                                        <div class="form-group">                                     
                                                            <label style="color: blue;" for="Txtdescripcion" class="col-lg-2 col-md-4 col-sm-12 col-xs-12 control-label">DESCRIPCION: </label>
                                                            <div class="col-lg-4 col-md-2 col-sm-8 col-xs-12"> <h5>hhhhh h hkjjkdfjff jdfkjkfjdk djhjfhfd djhfvhv cvbhcvbhc fhhhhdh dhhdhd dhhdhd dhdhhdhd dhdhhd ddjdjjd ddhhd dhhdhdh</h5>
                                                                <span class="help-block"></span></div>
                                                        </div>
                                                        <div class="form-group">                                     
                                                            <label style="color: blue;" for="Txtcategoria" class="col-lg-2 col-md-4 col-sm-12 col-xs-12 control-label">CATEGORIA: </label>
                                                            <div class="col-lg-4 col-md-2 col-sm-8 col-xs-12"><h5><%= CategoriaDAO.getCategoria(p.getIdcategoria())%></h5>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div> 

                                                        <div class="form-group">                                     
                                                            <label style="color: blue;" for="Txtmarca" class="col-lg-2 col-md-4 col-sm-12 col-xs-12 control-label">MARCA: </label>
                                                            <div class="col-lg-4 col-md-2 col-sm-8 col-xs-12"><h5><%= MarcaDAO.getmarca(p.getIdmarca())%></h5>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div> 
                                                        <div class="form-group">                                     
                                                            <label style="color: blue;" for="Txtmaterial" class="col-lg-2 col-md-4 col-sm-12 col-xs-12 control-label">MATERIAL: </label>
                                                            <div class="col-lg-4 col-md-2 col-sm-8 col-xs-12"><h5><%= MaterialDAO.getmaterial(p.getIdmaterial())%></h5>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">                                     
                                                            <label style="color: blue;" for="Txtproveedor" class="col-lg-2 col-md-4 col-sm-12 col-xs-12 control-label">PROVEEDOR: </label>
                                                            <div class="col-lg-4 col-md-2 col-sm-8 col-xs-12"><h5>proveedor gggggggg ccccc</h5>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">                                     
                                                            <label style="color: blue;" for="Txtunidadc" class="col-lg-2 col-md-4 col-sm-12 col-xs-12 control-label">UNIDAD COMPRA: </label>
                                                            <div class="col-lg-4 col-md-2 col-sm-8 col-xs-12"><h5>caja</h5>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div> 
                                                        <div class="form-group">                                     
                                                            <label style="color: blue;" for="Txtunidadv" class="col-lg-2 col-md-4 col-sm-12 col-xs-12 control-label">UNIDAD VENTA: </label>
                                                            <div class="col-lg-4 col-md-2 col-sm-8 col-xs-12"><h5>unidad</h5>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div> 
                                                        <div class="form-group">                                     
                                                            <label style="color: blue;" for="Txtnombre" class="col-lg-2 col-md-4 col-sm-12 col-xs-12 control-label">GENERO: </label>
                                                            <div class="col-lg-4 col-md-2 col-sm-8 col-xs-12"><h5><%=p.getGenero()%></h5>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">                                     
                                                            <label style="color: blue;" for="Txtcolor" class="col-lg-2 col-md-4 col-sm-12 col-xs-12 control-label">COLOR: </label>
                                                            <div class="col-lg-4 col-md-2 col-sm-8 col-xs-12"><h5><%=p.getColor()%></h5>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div> 

                                                        <div class="form-group">                                     
                                                            <label style="color: blue;" for="Txtpcompra" class="col-lg-2 col-md-4 col-sm-12 col-xs-12 control-label">PRECIO DE COMPRA S/: </label>
                                                            <div class="col-lg-4 col-md-2 col-sm-8 col-xs-12"><h5><%=p.getPreciocompra()%></h5>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div> 
                                                        <div class="form-group">                                     
                                                            <label style="color: blue;" for="Txtpventa" class="col-lg-2 col-md-4 col-sm-12 col-xs-12 control-label">PRECIO DE VENTA S/: </label>
                                                            <div class="col-lg-4 col-md-2 col-sm-8 col-xs-12"><h5><%=p.getPrecioVenta()%></h5>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">                                     
                                                            <label style="color: blue;" for="Txtstocktienda" class="col-lg-2 col-md-4 col-sm-12 col-xs-12 control-label">STOCK TIENDA: </label>
                                                            <div class="col-lg-4 col-md-2 col-sm-8 col-xs-12"><h5><%=p.getStocktienda()%></h5>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div> 
                                                        <div class="form-group">                                     
                                                            <label style="color: blue;" for="Txtstocktienda" class="col-lg-2 col-md-4 col-sm-12 col-xs-12 control-label">STOCK ALMACEN: </label>
                                                            <div class="col-lg-4 col-md-2 col-sm-8 col-xs-12"><h5><%=p.getStockalmacen()%></h5>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div> 
                                                        <div class="form-group">                                     
                                                            <label style="color: blue;" for="Txtstockminimo" class="col-lg-2 col-md-4 col-sm-12 col-xs-12 control-label">STOCK MINIMO/: </label>
                                                            <div class="col-lg-4 col-md-2 col-sm-8 col-xs-12"><h5><%=p.getStockminimo()%></h5>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">                                     
                                                            <label style="color: blue;" for="Txtfecha" class="col-lg-2 col-md-4 col-sm-12 col-xs-12 control-label">FECHA DE REGISTRO: </label>
                                                            <div class="col-lg-4 col-md-2 col-sm-8 col-xs-12"><h5><%=p.getFechaRegistro()%></h5>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">                                     
                                                            <label style="color: blue;" for="Txtestado" class="col-lg-2 col-md-4 col-sm-12 col-xs-12 control-label">ESTADO: </label>
                                                            <div class="col-lg-4 col-md-2 col-sm-8 col-xs-12"><h5><%=p.getEstado()%></h5>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>
                                                                <p class="modal-footer">
                                                        <a href="Admi_Producto.jsp" style="background-color: #09bb04;color: #ffffff;"class="btn " > <span>Regresar</span></a>    
                                                    </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>

                                </form>                                                                                                      
                            </div>
                        </div>                                                             
                    </div>
                </div>
        </section>  
    </body>
    <!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>-->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <!--  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>-->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="EstiloAdministrador/js/ValidadForm.js"></script>
    <!--<script type="text/javascript" src="JS/jquery.1.9.1.min.js"></script>-->
    <!--<script src="Java S/jquery.min.js"></script>-->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>                                
</html>
