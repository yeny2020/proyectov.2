
<%@page import="com.pe.gatasygatos.model.entity.Provedor"%>
<%@page import="com.pe.gatasygatos.DAO.ProvedorDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.pe.gatasygatos.model.entity.TipoDocumento"%>
<%@page import="com.pe.gatasygatos.DAO.TipoDocumentoDAO"%>
<%@page import="java.util.Date"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Categoria</title>
        <%@include file="css-js.jsp" %> 
        <!--ICONOS-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <!--ICONOS-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
        <!--Estilo tabla-->
        <link rel="stylesheet" href="EstiloAdministrador/css/Stilodetabla.css"> 
        <!--Estilo tabla-->
        <link href="EstiloAdministrador/css/ESTILO_TABLAS.css" rel="stylesheet" type="text/css"/>
    </head>
    <style>
        /* estado descactivado */
        markdesactivado{

            background-color: #ff1744;
            color: black;
            border-radius: 71px 69px 72px 71px;
            -moz-border-radius: 71px 69px 72px 71px;
            -webkit-border-radius: 71px 69px 72px 71px;
        }
        /* estado activo */
        markactivo{

            background-color: #00FF00;
            color: black;
            border-radius: 71px 69px 72px 71px;
            -moz-border-radius: 71px 69px 72px 71px;
            -webkit-border-radius: 71px 69px 72px 71px;
        }
    </style>
    <body>
        <!--cabecera de Menu -->
        <%@include file="FrmAdmin.jsp" %>

        <!-- pageContent -->
        <section class="full-width pageContent">

            <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
                <!--modal listar-->
                <div class="mdl-tabs__panel is-active" id="tabListCategory">
                    <div class="mdl-grid">
                        <div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop mdl-cell--0-offset-desktop">
                            <div class="full-width panel mdl-shadow--2dp">
                                <div style="background-color:#0A8CB3;"  class="full-width panel-tittle  text-center tittles">
                                    Lista de Proveedores
                                </div>
                                <br>
                                <div class="col-sm-6 mdl-textfield">
                                    <a  class="btn" href="ControllerdatosModal?accion=InsertarProveedor" style="background-color:#0A8CB3;color: #ffffff; margin-right:25px;"   data-toggle="modal" data-target="#myModaadd" class="edit">Nuevo Proveedor</a>
                                </div>
                                <div class="full-width panel-content">                        
                                    <table id="dtHorizontalVerticalExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%" >

                                        <thead>
                                            <tr>
                                                <th style="display:none;">N°</th>
                                                <th>Codigo</th>
                                                <th>Razon Social</th>
                                                 <th>RUC</th>
                                                <th>Direccion</th>
                                                <th>Celular</th> 
                                                <th>E-mail</th>
                                                <th>Fecha de Registro</th>
                                                <th>Estado</th>
                                                <th>Opciones</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <% ProvedorDAO prov = new ProvedorDAO();
                                                List<Provedor> lista = prov.ListadoProveedor();
                                                Iterator<Provedor> iter = lista.iterator();
                                                Provedor pv = null;
                                                while (iter.hasNext()) {
                                                    pv = iter.next();
                                            %>
                                            <tr>
                                                <td  style="display:none;;"id="idprove"><%=pv.getIdproveedor()%></td>
                                                <td><%= pv.getCodigo()%></td>
                                                <td><%= pv.getRazonsocial()%></td>
                                                <td><%= pv.getNumdocumento()%></td>
                                                <td><%=pv.getDireccion()%></td>
                                                <td><%=pv.getCelular()%></td>
                                                <td><%=pv.getEmail()%></td>
                                                <td><%=pv.getFechaderegistro()%></td>
                                                <% String Estado = pv.getEstado();
                                                    if (Estado.equalsIgnoreCase("Activo")) {%>
                                                <td><markactivo><%= Estado%></markactivo></td>   
                                                <%   } else {%>
                                        <td><markdesactivado><%= Estado%></markdesactivado></td>    
                                            <%     }
                                            %> 

                                        <td>

                                            <a href="ControllerdatosModal?accion=editarProveedor&id=<%=pv.getIdproveedor()%>"  data-toggle="modal" data-target="#myModalEdit" class="edit"><i class="material-icons" data-toggle="tooltip" title="Editar">&#xE254;</i></a>
                                            <button style="background-color: transparent; color: red; border: none " id='btn-eliminar' class="delete">
                                                <i class="material-icons" data-toggle="tooltip" title="Eliminar">&#xE872;</i>
                                            </button>
                                            <button style="background-color: transparent; color: red; border: none " id='btn-estadoProvedor' class="delete">
                                                <i class="material-icons" data-toggle="tooltip" title="Cambiar estado">&#xE878;</i>
                                            </button>
                                        </td>
                                        </tr>
                                        <%}%>

                                        </tbody>

                                    </table>

                                </div>


                            </div>

                        </div>
                    </div>
                </div>                                                             
            </div>
        </section>


        <!-- add Modal HTML -->
        <div class="modal fade" id="myModaadd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document" style="z-index: 9999; width: 450px">
                <div class="modal-content">
                    <%   ProvedorDAO com = new ProvedorDAO();
                        String numserie = com.Numserie();

                    %>
                </div>                    
            </div>
        </div>
        <!--Formulario de Modificar-->
        <div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document" style="z-index: 9999; width: 450px">
                <div class="modal-content">

                    <div class="modal-body">
                    </div>
                </div>                    
            </div>
        </div>

        <!--Data table --->
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>                                
        <!--Data table --->
        <script src="EstiloAdministrador/funcionesyvalidaciones/Proveedor.js" type="text/javascript"></script>
        <!--Funciones y validaciones --->


    </body>
</html>
<script>
    //------------------------------------------------------------------------------
//Scroll tabla
$(document).ready(function () {
    $('#dtHorizontalVerticalExample').DataTable({
        "scrollX": true,
        "scrollY": 285,
    });
    $('.dataTables_length').addClass('bs-select');
});
</script>

