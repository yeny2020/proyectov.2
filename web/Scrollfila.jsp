
<%@page import="com.pe.gatasygatos.model.entity.Cliente"%>
<%@page import="com.pe.gatasygatos.DAO.ClienteDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Categoria</title>
        <%@include file="css-js.jsp" %> 
        <!--ICONOS-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <!--ICONOS-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
        <!--Estilo tabla-->
        <link rel="stylesheet" href="EstiloAdministrador/css/Stilodetabla.css"> 
        <link href="EstiloAdministrador/css/STYLE_TABLE.css" rel="stylesheet" type="text/css"/>
        <!--Estilo tabla-->
    </head>
    <style>
        .ea_table {
            border: 1px solid #ddd;
            display: block;
            background: #fff;
            overflow-y: hidden;
            box-sizing: border-box;
            float: left;
            height:auto;
            width: 100%;
        }

        .ea_table tbody, thead {
            flex-direction: column;
            display: flex;
        }

        .ea_table tbody {
            height: 300px;
            overflow: auto;
        }

        .ea_table thead {
            border-bottom: 1px solid #ddd;
        }

        .ea_table tr {
            display: flex;
        }


        .ea_table tbody tr:nth-child(2n+1) {
            background: #f8f8f8;
        }

        .ea_table td, .ea_table th {
            text-align: left;
            font-size: 0.75rem;
            padding: 1.5rem;
            flex: 1;
        }
    </style>

    <body>

        <!--cabecera de Menu -->
        <%@include file="FrmAdmin.jsp" %>

        <!-- pageContent -->
        <section class="full-width pageContent">

            <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
                <!--modal listar-->
                <div class="mdl-tabs__panel is-active" id="tabListCategory">
                    <div class="mdl-grid">
                        <div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop mdl-cell--0-offset-desktop">
                            <div class="full-width panel mdl-shadow--2dp">
                                <div class="full-width panel-tittle bg-info text-center tittles">
                                    Lista Cliente
                                </div>
                                <div class="full-width panel-content">                        
                                    <div class="col-sm-6 mdl-textfield">
                                        <a href="Insertarcliente.jsp" class="btn btn-info" ><i class="material-icons">&#xE147;</i> <span>Nuevo</span></a>    
                                    </div>
                                    <table class="ea_table">
                                        <thead>
                                            <tr>
                                                <th>Something Long</th>
                                                <th>Something </th>
                                                <th>Something Very Long</th>
                                                <th>Something Long</th>
                                                <th>Some</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td> Lorem Ipsum Dolar Sit Amet</td>
                                                <td> Lorem </td>
                                                <td> Lorem Ipsum </td>
                                                <td> Lorem </td>
                                                <td> Lorem Ipsum Dolar </td>
                                            </tr>
                                            <tr>
                                                <td> Lorem </td>
                                                <td> Lorem Ipsum Dolar Sit Amet</td>
                                                <td> Lorem </td>
                                                <td> Lorem Ipsum </td>
                                                <td> Lorem Ipsum Dolar </td>
                                            </tr>
                                            <tr>
                                                <td> Lorem Ipsum Dolar Sit Amet</td>
                                                <td> Lorem </td>
                                                <td> Lorem Ipsum </td>
                                                <td> Lorem </td>
                                                <td> Lorem Ipsum Dolar </td>
                                            </tr>
                                            <tr>
                                                <td> Lorem Ipsum Dolar Sit Amet</td>
                                                <td> Lorem </td>
                                                <td> Lorem Ipsum </td>
                                                <td> Lorem </td>
                                                <td> Lorem Ipsum Dolar </td>
                                            </tr>
                                            <tr>
                                                <td> Lorem Ipsum Dolar Sit Amet</td>
                                                <td> Lorem </td>
                                                <td> Lorem Ipsum </td>
                                                <td> Lorem </td>
                                                <td> Lorem Ipsum Dolar </td>
                                            </tr>
                                            <tr>
                                                <td> Lorem Ipsum Dolar Sit Amet</td>
                                                <td> Lorem </td>
                                                <td> Lorem Ipsum </td>
                                                <td> Lorem </td>
                                                <td> Lorem Ipsum Dolar </td>
                                            </tr>
                                            <tr>
                                                <td> Lorem Ipsum Dolar Sit Amet</td>
                                                <td> Lorem </td>
                                                <td> Lorem Ipsum </td>
                                                <td> Lorem </td>
                                                <td> Lorem Ipsum Dolar </td>
                                            </tr>
                                            <tr>
                                                <td> Lorem Ipsum Dolar Sit Amet</td>
                                                <td> Lorem </td>
                                                <td> Lorem Ipsum </td>
                                                <td> Lorem </td>
                                                <td> Lorem Ipsum Dolar </td>
                                            </tr>

                                        </tbody>

                                    </table>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>                                                             
        </div>
    </section>
    <!--Data table --->
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>                                
    <!--Data table --->
    <!--Funciones y validaciones --->
    <script src="EstiloAdministrador/funcionesyvalidaciones/Client.js" type="text/javascript"></script>

    <!--Funciones y validaciones --->  </body>
</html>
<script>
    $(document).ready(function () {
        $('#dtHorizontalExample').DataTable({
            "scrollX": true
        });
        $('.dataTables_length').addClass('bs-select');
    });


</script>