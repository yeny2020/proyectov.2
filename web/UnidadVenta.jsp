
<%@page import="com.pe.gatasygatos.model.entity.UnidadVenta"%>
<%@page import="com.pe.gatasygatos.DAO.UnidadVentaDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Categoria</title>
        <%@include file="css-js.jsp" %> 
        <!--ICONOS-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <!--ICONOS-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
        <!--Estilo tabla-->
        <link rel="stylesheet" href="EstiloAdministrador/css/Stilodetabla.css"> 
        <!--Estilo tabla-->
    </head>
    <style>
        /* estado descactivado */
        markdesactivado{

            background-color: #ff1744;
            color: black;
            border-radius: 71px 69px 72px 71px;
            -moz-border-radius: 71px 69px 72px 71px;
            -webkit-border-radius: 71px 69px 72px 71px;
        }
        /* estado activo */
        markactivo{

            background-color: #00FF00;
            color: black;
            border-radius: 71px 69px 72px 71px;
            -moz-border-radius: 71px 69px 72px 71px;
            -webkit-border-radius: 71px 69px 72px 71px;
        }
    </style>
    <body>
        <!--cabecera de Menu -->
        <%@include file="FrmAdmin.jsp" %>

        <!-- pageContent -->
        <section class="full-width pageContent">

            <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
                <!--modal listar-->
                <div class="mdl-tabs__panel is-active" id="tabListCategory">
                    <div class="mdl-grid">
                        <div class="mdl-cell mdl-cell--2-col-phone mdl-cell--8-col-tablet mdl-cell--6-col-desktop mdl-cell--2-offset-desktop">
                            <div class="full-width panel mdl-shadow--2dp">
                                <div class="full-width panel-tittle bg-info text-center tittles">
                                    LISTA UNIDAD MEDIDA VENTA
                                </div>
                                <div class="full-width panel-content">                        
                                    <div class="col-sm-6 mdl-textfield">   
                                        <a href="#adducompra" class="btn " style="background-color:#4e96b3;color: #F7F7F5;" data-toggle="modal">Nuevo Categoria</a>
                                    </div>
                                    <div class="table-responsive">
                                    <table id="Datatable" class="table table-bordered table-hover projects" >

                                        <thead style="background-color: skyblue;color: black;font-weight: bold">
                                            <tr>
                                                <th style="display:none;">codigo</th>
                                                <th>Codigo</th>
                                                <th>Nombre</th>
                                                <th>Factor</th>
                                                <th>Estado</th>
                                                <th>Editar-Estado-Eliminar</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <%   UnidadVentaDAO dao = new UnidadVentaDAO();
                                                List<UnidadVenta> list = dao.ListaUnidadVenta();
                                                Iterator<UnidadVenta> iter = list.iterator();
                                                UnidadVenta per = null;
                                                while (iter.hasNext()) {
                                                    per = iter.next();

                                            %>
                                            <tr>
                                                <td  style="display:none;;"id="iduv"><%=per.getIduventa()%></td>
                                                <td><%= per.getCodigo()%></td>
                                                <td><%= per.getNombre()%></td>
                                                <td><%= per.getContenido()%></td> 
                                                <% String Estado = UnidadVentaDAO.getUventaEstado(per.getIduventa());
                                                    if (Estado.equalsIgnoreCase("Activo")) {%>

                                                <td><markactivo><%= Estado%></markactivo></td>   
                                                <%   } else {%>

                                        <td><markdesactivado><%= Estado%></markdesactivado></td>    
                                            <%     }
                                            %> 

                                        <td>
                                            <a href="ControllerdatosModal?accion=editaruventa&id=<%=per.getIduventa()%>"  data-toggle="modal" data-target="#myModalEdit" class="edit"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                                            <a id='btn-eliminar' class="delete" >
                                                <i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i>
                                            </a>
                                            <a id='btn-estado' class="delete" >
                                                <i class="material-icons" data-toggle="tooltip" title="Delete"style="color:  #9575cd;">&#xE627;</i>
                                            </a>
                                        </td>
                                        </tr>
                                        <%}%>

                                        </tbody>

                                    </table>
                                </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>                                                             
            </div>
        </section>


        <!-- add Modal HTML -->
        <div id="adducompra" class="modal fade" >
            <div class="modal-dialog" role="document" style="z-index: 9999; width: 450px">
                <div class="modal-content">

                    <form id="newuv" action="ControllerUnidadMedida" method="Post" name="frm_nuevo">

                        <div class="modal-header">      
                            <h4 class="modal-title">Agregar U.compra</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>

                        <div class="modal-body">  
                            <%
                                UnidadVentaDAO com = new UnidadVentaDAO();
                                String numserie = com.Numserie();

                            %>

                            <div class="form-group">
                                <label>Codigo</label>
                                <input type="text" name="txtCod" value="<%=numserie%>"  class="form-control" readonly="" >
                            </div>
                            <div class="form-group">
                                <label>Nombre</label>
                                <input type="text" name="txtNom" class="form-control" placeholder="Nombre">

                            </div> 
                            <div class="form-group">
                                <label>Factor</label>
                                <input type="number" name="txtCont" value="1" class="form-control">
                            </div> 
                        </div>

                        <div class="modal-footer">
                            <input type="button"  class="btn btn-default" data-dismiss="modal" value="Cancel">
                            <input onclick="return validarunidadVnew()" class="btn btn-success" type="submit" name="accion" value="adduventa">
                        </div>
                    </form>
                </div>
            </div>
        </div> 
        <!--Formulario de Modificar-->
        <div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document" style="z-index: 9999; width: 450px">
                <div class="modal-content">

                    <div class="modal-body">
                    </div>
                </div>                    
            </div>
        </div>
        <!--Data table --->
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>                                
        <!--Data table --->
        <!--Funciones y validaciones --->
        <script src="EstiloAdministrador/funcionesyvalidaciones/Unidadventa.js" type="text/javascript"></script>
        <!--Funciones y validaciones --->
    </body>
</html>