<%-- 
    Document   : usuarios
    Created on : 14/10/2018, 01:33:19 PM
    Author     : HP
--%>
<%@page import="com.pe.gatasygatos.model.entity.Usuario"%>
<%@page import="com.pe.gatasygatos.DAO.UsuarioDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Empleado"%>
<%@page import="com.pe.gatasygatos.DAO.EmpleadoDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>usuario</title>
        <%@include file="css-js.jsp" %>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="EstiloAdministrador/css/Stilodetabla.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <!--cabecera de Menu -->
        <%@include file="FrmAdmin.jsp" %>

        <!-- pageContent -->
        <section class="full-width pageContent">

            <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
                <!--modal listar-->
                <div class="mdl-tabs__panel is-active" id="tabListCategory">
                    <div class="mdl-grid">
                        <div class="mdl-cell mdl-cell--2-col-phone mdl-cell--8-col-tablet mdl-cell--10-col-desktop mdl-cell--1-offset-desktop">
                            <div class="full-width panel mdl-shadow--2dp">
                                <div class="full-width panel-tittle bg-info text-center tittles">
                                    LISTA USUARIO
                                </div>
                                <div class="full-width panel-content">                        
                                    <div class="col-sm-6 mdl-textfield">   
                                        <a href="#addUsuario" class="btn " style="background-color:#4e96b3;color: #F7F7F5;" data-toggle="modal">Nevo Usuario</a>
                                    </div>
                                    <table id="Datatable" class="table table-bordered table-hover projects" >

                                        <thead style="background-color: skyblue;color: black;font-weight: bold">
                                            <tr>
                                                <th>Empleado</th>
                                                <th>Usuario</th>
                                                <th>Rol</th>
                                                <th>Direccion</th>
                                                <th>Sueldo</th>
                                                <th>opcion</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%                                        UsuarioDAO dao = new UsuarioDAO();
                                                List<Usuario> list = dao.ListadoUsuario();
                                                Iterator<Usuario> iter = list.iterator();
                                                Usuario per = null;
                                                while (iter.hasNext()) {
                                                    per = iter.next();

                                            %>
                                            <tr>
                                                <td ><%= EmpleadoDAO.getEmpleadoNombre(per.getIdempleado())%> </td>
                                                <td ><%= per.getUsu()%> </td>
                                                <td><%= per.getRol()%> </td>
                                                <td ><%=EmpleadoDAO.getEmpleadoDireccion(per.getIdempleado())%> </td>
                                                <td><%= EmpleadoDAO.getEmpleadoSueldo(per.getIdempleado())%> </td>
                                                <td>
                                                    <a href="Admi_Usuariocontroller?accion=editar&id=<%= per.getId()%>"  class="edit" ><i class="material-icons"  title="Edit">&#xE254;</i></a>
                                                    <a href="Admi_Usuariocontroller?accion=eliminar&id=<%= per.getId()%>" class="delete" ><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
                                                </td>  

                                            </tr>
                                            <%}%>

                                        </tbody>

                                    </table>
                                    <h4 align="center">
                                        <%
                                            if (request.getAttribute("mesg") != null) {
                                                out.println(request.getAttribute("mesg"));
                                            }
                                        %>
                                    </h4>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>                                                             
            </div>
        </section>
        <!-- add Modal HTML -->
        <div id="addUsuario" class="modal fade" >
            <div class="modal-dialog" role="document" style="z-index: 9999; width: 450px">
                <div class="modal-content">
                    <form action="Usuariocontroller" id="frm_nuevo">
                        <div class="modal-header">      
                            <h4 class="modal-title">Agregar Usuario</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body"> 
                            <label for="validationDefault01">Seleciona Empleado</label>
                            <select name="txtidempleado" class="form-control" required="">
                                <option  value=""disabled="" selected="">Seleciona Empleado</option>
                                <%   EmpleadoDAO edao = new EmpleadoDAO();
                                    List<Empleado> lista = edao.ListadoEmpleado();
                                    Iterator<Empleado> itera = lista.iterator();
                                    Empleado empl = null;
                                    while (itera.hasNext()) {
                                        empl = itera.next();%>

                                <option  value="<%= empl.getId()%>" ><%= empl.getNombre()%></option>
                                <%}%>

                            </select>
                        </div>
                        <div class="modal-body">
                            <label>User name</label>
                                <input name="txtusu" type="text" class="form-control" required>
                        </div>
                        <div class="modal-body">
                            <label >Password</label>
                            <input name="txtpassword" type="password" id="passwordAdmin" class="form-control" required>
                        </div>

                        <div class="modal-body">
                            <label for="validationDefault01">Seleciona Rol</label>
                            <select name="txtrol" class="form-control" required="">
                                <option  value=""disabled="" selected="">Seleciona Rol</option>
                                <option >Administrador</option>
                                <option >Ventas</option>
                                <option >Almacen</option>
                                <option >Produccion</option>
                            </select>
                        </div>
                        <div class="modal-footer">
                            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                            <input id="btnguardar" type="submit" class="btn btn-success" name="accion" value="agregar">
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!--Formulario de Modificar-->
        <div class="modal fade" id="myModalEditusuario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document" style="z-index: 9999; width: 450px">
                <div class="modal-content">

                    <div class="modal-body">


                    </div>
                </div>                    
            </div>
        </div>                                       
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="EstiloAdministrador/js/ValidadForm.js"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>                                
    </body>
    <script type="text/javascript">

        $(document).ready(function () {
            $('#Datatable').DataTable();
        });
    </script>
</html>