
<%@page import="com.pe.gatasygatos.DAO.ProductoxalmacenDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Productoxalmacen"%>
<%@page import="com.pe.gatasygatos.DAO.MarcaDAO"%>
<%@page import="com.pe.gatasygatos.DAO.CategoriaDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Producto"%>
<%@page import="com.pe.gatasygatos.DAO.ProductoDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Marca</title>
        <%@include file="css-js.jsp" %> 
        <!--ICONOS-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <!--ICONOS-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
        <!--Estilo tabla-->
        <link rel="stylesheet" href="EstiloAdministrador/css/Stilodetabla.css"> 
        <link href="EstiloAdministrador/css/ColordeEstado.css" rel="stylesheet" type="text/css"/>
        <!--Estilo tabla-->
    </head>

    <body>
        <!--cabecera de Menu -->
        <%@include file="FrmVenta.jsp" %>

        <!-- pageContent -->
        <section class="full-width pageContent">

            <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
                <!--modal listar-->
                <div class="mdl-tabs__panel is-active" id="tabListCategory">
                    <div class="mdl-grid">
                        <div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--10-col-desktop mdl-cell--1-offset-desktop">
                            <div class="full-width panel mdl-shadow--2dp">
                                <div  style="background-color:#aed581;color: #ffffff;" class="full-width panel-tittle text-center tittles">
                                    Agregar Item
                                </div>
                                <div class="full-width panel-content">                        
                                    <table id="Datatable" class="table table-bordered table-hover projects" >
                                        <thead>
                                            <tr>
                                                <th>Agregar Item</th>
                                                <th>Codigo</th>
                                                <th>Nombre</th>
                                                <th>Stock Minimo</th>
                                                <th>Stock en tienda </th>
                                                <th>Almacen</th>                                           
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%ProductoDAO pdaoStcok = new ProductoDAO();
                                                List<Productoxalmacen> listStock = pdaoStcok.ListadoStockminimopxa();
                                                Iterator<Productoxalmacen> iterstock = listStock.iterator();
                                                Productoxalmacen pstock = null;
                                                while (iterstock.hasNext()) {
                                                    pstock = iterstock.next();%>
                                            <tr>
                                                <td>
                                                    <a href="ControllerdatosModal?accion=pedido&id=<%=pstock.getIdalmacenxproducto()%>"  data-toggle="modal" data-target="#myModalEdit" ><i class="material-icons" style="color: #09bb04" data-toggle="tooltip" title="Ver">&#xe147;</i></a>
                                                </td>
                                                <td><%=ProductoxalmacenDAO.getProductocodigo(pstock.getIdproducto())%></td>
                                                <td><%=ProductoxalmacenDAO.getProductodescripcion(pstock.getIdproducto())%></td>
                                                <td style="  background: red;"><%=pstock.getStockminimo()%></td>
                                                <td><%=pstock.getStock()%></td>  
                                                <td ><%= pstock.getIdalmacen()%></td>
                                            </tr>
                                            <%}%>
                                        </tbody>
                                    </table>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>                                                             
            </div>
        </section>
        <!--Formulario de Modificar-->
        <div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document" style="z-index: 9999; width: 450px">
                <div class="modal-content">

                    <div class="modal-body">


                    </div>
                </div>                    
            </div>
        </div>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>                                
        <script src="EstiloAdministrador/funcionesyvalidaciones/Datatable.js" type="text/javascript"></script>
    </body>
</html>
