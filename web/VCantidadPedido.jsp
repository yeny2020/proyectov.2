
<%@page import="com.pe.gatasygatos.DAO.UnidadCompraDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Productoxalmacen"%>
<%@page import="com.pe.gatasygatos.DAO.ProductoxalmacenDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Producto"%>
<%@page import="com.pe.gatasygatos.DAO.ProductoDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Categoria"%>
<%@page import="com.pe.gatasygatos.DAO.CategoriaDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Categoria</title>
    </head>
    <body>
        <!-- add Modal HTML -->
        <div >
            <%
                ProductoxalmacenDAO dao = new ProductoxalmacenDAO();
                int id = Integer.parseInt((String) request.getAttribute("idped"));
                Productoxalmacen p = (Productoxalmacen) dao.listproductoxalmacen(id);
            %>
            <div class="modal-content">
                <div class="modal-header" style="background-color:#aed581;">      
                    <h4 class="modal-title"   id="myModalLabel">Ingrese Cantidad Producto</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <form method="post" action="VControllergestionarpedido" accion="AnadirCarrito"> 
                    <div class="modal-body">  
                        <div class="form-group">
                            <input  type="hidden" type="text"  name="txtPro_id" value="<%=p.getIdproducto()%>"readonly="">
                        </div>  
                        <div class="form-group">
                            <label>Descripcion</label>
                            <input type="text" class="form-control" name="txtPro_descripcion" value="<%=ProductoxalmacenDAO.getProductodescripcion(p.getIdproducto())%>"readonly="">
                        </div> 
                        <div class="form-group">
                            <label>Und</label>
                            <input type="text" class="form-control" name="txtPro_descripcion" value="<%=ProductoxalmacenDAO.getUndVenta(p.getIdproducto())%>" readonly="">
                        </div> 
                        <div class="form-group"> 
                            <label>Cantidad</label>   
                            <input type="number" class="form-control" value="1" min="1" id="txtPro_cantidad" name="txtPro_cantidad" value="0" required="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="AgregarproductosVenta.jsp" class="btn btn-default" >Canselar</a> 
                        <input type="submit" value="Añadir" name="btnAnadir">
                        <input type="hidden" name="accion" value="AnadirCarrito"/>
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>
