<%@page import="com.pe.gatasygatos.DAO.ProductoxalmacenDAO"%>
<%@page import="com.pe.gatasygatos.DAO.UnidadVentaDAO"%>
<%@page import="com.pe.gatasygatos.DAO.OrdendeCompraDAO"%>
<%@page import="com.pe.gatasygatos.DAO.AlmacenDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Almacen"%>
<%@page import="com.pe.gatasygatos.model.entity.Categoria"%>
<%@page import="com.pe.gatasygatos.DAO.CategoriaDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Provedor"%>
<%@page import="com.pe.gatasygatos.DAO.ProvedorDAO"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.pe.gatasygatos.model.entity.Cliente"%>
<%@page import="com.pe.gatasygatos.DAO.ClienteDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Producto"%>
<%@page import="com.pe.gatasygatos.DAO.ProductoDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.DetalleVentas"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Bootstrap CRUD Data Table for Database with Modal Form</title>
        <%@include file="css-js.jsp" %>
        <!--ICONOS-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <!--ICONOS-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
        <!--Estilo tabla-->
        <!--BOOSTRAP PARA DIV-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <!--BOOSTRAP PARA DIV-->
        <!--Estilo tabla-->
        <link rel="stylesheet" href="EstiloAdministrador/css/Stilodetabla.css"> 
        <!--Estilo tabla-->
        <%--Buscador select.---%>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
        <%-- finBuscador select.---%>

    </head>
    <style> 
        .dtHorizontalVerticalExampleWrapper {
            max-width: 600px;
            margin: 0 auto;
        }
        #dtHorizontalVerticalExample th, td {
            white-space: nowrap;
        }
        table.dataTable thead .sorting:after,
        table.dataTable thead .sorting:before,
        table.dataTable thead .sorting_asc:after,
        table.dataTable thead .sorting_asc:before,
        table.dataTable thead .sorting_asc_disabled:after,
        table.dataTable thead .sorting_asc_disabled:before,
        table.dataTable thead .sorting_desc:after,
        table.dataTable thead .sorting_desc:before,
        table.dataTable thead .sorting_desc_disabled:after,
        table.dataTable thead .sorting_desc_disabled:before {
            bottom: .10em;
        }
    </style>
    <body>
        <!--cabecera de Menu -->
        <%@include file="FrmVenta.jsp" %>
        <%           Cliente objCliente = null;
            if (request.getSession().getAttribute("cliente") != null) {
                objCliente = (Cliente) request.getSession().getAttribute("cliente");
            } else {
                objCliente = new Cliente();
                objCliente.setIdcliente(0);
                objCliente.setCorreo("");
                objCliente.setNumerodocumento("0");
                objCliente.setCliente("");
                objCliente.setDireccion("");
            }
        %>
        <!-- pageContent -->
        <section class="full-width pageContent">

            <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
                <!--modal listar-->
                <div class="mdl-tabs__panel is-active" id="tabListCategory">
                    <div class="mdl-grid">
                        <div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop mdl-cell--0-offset-desktop">

                            <div class="full-width panel mdl-shadow--2dp">
                                <div class="full-width panel-tittle  text-center tittles" style="background-color: #aed581;">
                                    Generador de Documentos Ventas/Egresos
                                </div>
                                <form id="newventa" method="post" name="accion" action="VControllergestionarventa">
                                    <input type="hidden" name="accion" value="RegistrarVenta" />
                                    <div class="card">
                                        <div class="card-footer">                                                
                                            <input class="btn btn-info" type="submit" value="Registrar Venta" name="btnVenta" style="float:right"> 

                                            <a class="btn btn-info" href="Venta.jsp" role="button" style="float:right">Salir</a>
                                            <a href="VVentaproductosactivos.jsp" class="btn btn-info"  style="float:right">Agregar Productos</a>                                          
                                        </div>
                                    </div>
                                    <div class="d-flex">
                                        <div class="col-md-12 col-md-offset-0" style="color: #001a35">
                                            <div class="card">
                                                <div class="card-body  ">
                                                    <div class="d-flex">
                                                        <label class="col-sm-2" style="text-align:right">Cliente :</label>
                                                        <input type="hidden" name="txtIdcli" value="<%=objCliente.getIdcliente() == 0 ? "" : objCliente.getIdcliente()%>"style="width:340px;height:20px;">
                                                        <input type="text" name="txtNombre" value=" <%=objCliente.getCliente()%>"  style="width:340px;height:20px;">
                                                        <a href="#Buscarcliente" class="btn" style="background-color:#4e96b3;color: #F7F7F5;width:60px;height:20px;" data-toggle="modal">Buscar</a>
                                                        <label class="col-sm-2" style="text-align:right">Documento:</label>
                                                        <select name="txtTipoventa" id="tipo_comprobante" required="" style="width:340px;height:20px;">
                                                            <option   value=""disabled="" selected="">Seleciona Comprobante</option>
                                                            <option value="Boleta">Boleta</option>
                                                            <option value="Factura">Factura</option>
                                                        </select>
                                                    </div> 
                                                    <div class="d-flex">
                                                        <label class="col-sm-2" style="text-align:right">RUC :</label>
                                                        <input type="text" name="txtI" value="<%=objCliente.getNumerodocumento()%>"  style="width:400px;height:20px;">

                                                        <label class="col-sm-2" style="text-align:right">Tienda:</label>
                                                        <select name="txtTienda" id="tipo_comprobante" required="" style="width:340px;height:20px;">
                                                            <option value="Gatas y Gatos">Gatas y Gatos</option>
                                                        </select>
                                                    </div>



                                                    <div class="d-flex">
                                                        <label class="col-sm-2" style="text-align:right">E-Mail:</label>
                                                        <input type="text" name="txtTota" value="<%=objCliente.getCorreo()%>"   style="width:400px;height:20px;">
                                                        <label class="col-sm-2" style="text-align:right">Almacen:</label>
                                                        <select name="txtAlmacen" id="tipo_comprobante" required="" style="width:340px;height:20px;">
                                                            <% AlmacenDAO mat = new AlmacenDAO();
                                                                List<Almacen> lis = mat.ListadoEstadoActivos();
                                                                Iterator<Almacen> it = lis.iterator();
                                                                Almacen ma = null;
                                                                while (it.hasNext()) {
                                                                    ma = it.next();
                                                            %>

                                                            <option  value="<%=ma.getNombre()%>" required><%=ma.getNombre()%></option>
                                                            <%
                                                                }
                                                            %>
                                                        </select>                                                
                                                    </div>
                                                    <% Date dNow = new Date();
                                                        SimpleDateFormat ft
                                                                = new SimpleDateFormat("dd/MM/yyyy - hh:mm:ss");
                                                        String currentDate = ft.format(dNow);
                                                    %>
                                                    <div class="d-flex">

                                                        <label class="col-sm-2" style="text-align:right">Emision:</label>
                                                        <input type="text" name="txtfecha" value="<%=currentDate%>"  style="width:400px;height:20px;">
                                                        <label class="col-sm-2" style="text-align:right">Condicion:</label>
                                                        <input type="text" name="txtCondicion" value="Contado"  style="width:340px;height:20px;">
                                                    </div>
                                                    <div class="d-flex">
                                                        <label class="col-sm-2" style="text-align:right">Responsable:</label>
                                                        <input type="text" name="txtIdusuario" value="<%=sesion.getAttribute("usuario")%>"  style="width:400px;height:20px;">
                                                        <label class="col-sm-2" style="text-align:right">Direccion:</label>
                                                        <input type="text" name="txtSubto" value="<%=objCliente.getDireccion()%>"  style="width:340px;height:20px;">
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="d-flex">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="form-group">                                                   
                                                        <div class="table-responsive cart_info"  id="cart-container"> 
                                                            <table id="dt" class="table table-striped table-bordered table-sm " cellspacing="0"
                                                                   width="100%">
                                                                <thead class="table-dark" style="background-color: #aed581;" >
                                                                    <tr class="cart_menu" style="width: 25%;">
                                                                        <td class="image">Codigo</td>
                                                                        <td class="image">Nombre</td>
                                                                        <td class="price">Precio</td>
                                                                        <td class="description">Cantidad</td>    
                                                                        <td class="description">Und</td>  
                                                                        <td class="quantity">Sub Total</td>
                                                                        <td class="total">Quitar</td>
                                                                    </tr>

                                                                </thead>

                                                                <%                                                           
                                                                    double total =0.00;
                                                                    double igv = 0.00;
                                                                    double ventatotal = 0.00;

                                                                    ArrayList<DetalleVentas> listar = (ArrayList<DetalleVentas>) session.getAttribute("car");
                                                                    int fila = 0;

                                                                    if (listar != null) {
                                                                        for (DetalleVentas d : listar) {
                                                                %>
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="cart_description">
                                                                            <p><%= d.getProducto().getCodigo()%></p>
                                                                        </td>
                                                                        <td class="cart_description">
                                                                            <p><%= d.getProducto().getDescripcion()%></p>
                                                                        </td>
                                                                        <td class="cart_description">
                                                                            <p><%= d.getProducto().getPrecioVenta()%></p>
                                                                        </td>

                                                                        <td >
                                                                            <input  size="40" type="number" value="<%= d.getCantidad()%>"  min="1" max="<%=ProductoxalmacenDAO.getValidarstock(d.getIdproducto())%>" id="txtPro_cantidad<%=fila%>" name="txtPro_cantidad"  onchange="Ventaactualizarcantidad(<%= fila%>)" required="">
                                                                        </td>

                                                                        <td class="cart_description">
                                                                            <p><%=UnidadVentaDAO.getNombreUnidadventa(d.getProducto().getIduventa())%></p>
                                                                        </td>

                                                                        <td class="cart_quantity"><%=d.getCantidad() * d.getProducto().getPrecioVenta()%></td>
                                                                        <td >
                                                                            <span id="idarticulo" style="display:none;"><%= d.getProducto().getIdproducto()%></span>

                                                                            <button style="background-color: transparent; color: red; border: none " id="deleteitem" class="delete">
                                                                                <i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i>
                                                                            </button>
                                                                        </td>


                                                                    </tr>
                                                                    <%
                                                                                fila++;
                                                                                ventatotal = ventatotal + d.getCantidad() * d.getProducto().getPrecioVenta();
                                                                                igv = ventatotal * 0.18;
                                                                                total = ventatotal - igv;
                                                                            }
                                                                        }
                                                                    %>
                                                                </tbody>
                                                            </table>
                                                        </div> 
                                                        <br>
                                                        <div class="col-md-3 col-md-offset-9" style="color: #0083C9;">
                                                            <div class="card">

                                                                <div class="card-body  ">
                                                                    <div class="d-flex">
                                                                        <label class="col-sm-5">SUB TOTAL :</label>
                                                                        <input type="text" name="txtSubtotal" value="<%= total%>" readonly="readonly">
                                                                    </div>



                                                                    <div class="d-flex">
                                                                        <label class="col-sm-5">IGV :</label>
                                                                        <input type="text" name="txtIgv" value="<%= Math.round(igv * 100.00) / 100.00%>" readonly="readonly">
                                                                    </div>



                                                                    <div class="d-flex">
                                                                        <label class="col-sm-5">TOTAL :</label>
                                                                        <input type="text" name="txtTotal" value="<%= Math.round(ventatotal * 100.00) / 100.00%>" readonly="readonly">
                                                                    </div>

                                                                </div>

                                                            </div> 

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                  
                                        </div>                                                                                                                                             
                                    </div>
                                    <br>
                                </form>                                                                                                      
                            </div>
                        </div>                                                             
                    </div>
                </div>

                                                                    
                <div id="Buscarcliente" class="modal fade">
                    <div class="modal-dialog " role="document" style="z-index:1800; width: 750px">
                        <div class="modal-content">
                            <form>
                                <div class="modal-header">      
                                    <h4 class="modal-title">Lista cliente</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                <div class="modal-body">

                                    <table id="tablacliente" class="table table-striped table-bordered table-sm " cellspacing="0"
                                           width="100%">
                                        <thead class="table-dark" style="background-color: #aed581;" >
                                            <tr class="cart_menu" >
                                                <th style="display:none;"></th>
                                                <th>Agregar</th>
                                                <th>Nombre</th>
                                                <th>N.Doc</th>
                                                <th>Correo</th>
                                                <th>Direccion</th>
                                            </tr>
                                        </thead>
                                        <tbody >
                                            <% ClienteDAO prov = new ClienteDAO();
                                                List<Cliente> list = prov.ListadoCliente();
                                                Iterator<Cliente> ite = list.iterator();
                                                Cliente p = null;
                                                while (ite.hasNext()) {
                                                    p = ite.next();

                                            %>
                                            <tr>
                                                <td  style="display:none;"id="idcat"><%=p.getIdcliente()%></td>
                                                <td>
                                                    <%--<a  href="javascript:void(0)" onclick="CargarDatos(<%=p.getIdproveedor()%>)"><i class="material-icons" style="color: #09bb04" data-toggle="tooltip" title="Ver">&#xe147;</i></a>--%>

                                                    <a  href="ControllerCliente?accion=buscarPorId&idCliente=<%=p.getIdcliente()%>" ><i class="material-icons" style="color: #09bb04" data-toggle="tooltip" title="Ver">&#xe147;</i></a>
                                                </td>
                                                <td><%=p.getCliente()%></td>
                                                <td><%=p.getNumerodocumento()%></td> 
                                                <td><%=p.getCorreo()%></td>
                                                <td><%=p.getDireccion()%></td>
                                            </tr>
                                            <%
                                                }
                                            %>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <a href="InsertarVenta.jsp" class="btn btn-default" >Canselar</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

        </section>  

        <!--Data table --->
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>                                
        <!--Data table --->
        <script src="EstiloAdministrador/funcionesyvalidaciones/Ventas.js" type="text/javascript"></script>
        <script src="EstiloAdministrador/funcionesyvalidaciones/Datatable.js" type="text/javascript"></script>
    </body>

</html>

