
<%@page import="com.pe.gatasygatos.DAO.ClienteDAO"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.pe.gatasygatos.model.entity.Venta"%>
<%@page import="com.pe.gatasygatos.DAO.VentaDAO"%>
<%@page import="com.pe.gatasygatos.DAO.MarcaDAO"%>
<%@page import="com.pe.gatasygatos.DAO.CategoriaDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Producto"%>
<%@page import="com.pe.gatasygatos.DAO.ProductoDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Categoria</title>
        <%@include file="css-js.jsp" %> 
        <!--ICONOS-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <!--ICONOS-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
        <!--Estilo tabla-->
        <link rel="stylesheet" href="EstiloAdministrador/css/Stilodetabla.css"> 
        <link href="EstiloAdministrador/css/ColordeEstado.css" rel="stylesheet" type="text/css"/>
        <!--Estilo tabla-->
    </head>
    <body>
        <%@include file="FrmVenta.jsp" %>

        <!-- pageContent -->
        <section class="full-width pageContent">

            <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
                <div class="mdl-tabs__tab-bar">
                    <a href="#BOLETA" class="mdl-tabs__tab is-active">BOLETA</a>
                    <a href="#FACTURA" class="mdl-tabs__tab ">FACTURA</a>
                </div>

                <!--modal listar-->
                <div class="mdl-tabs__panel is-active" id="BOLETA">
                    <div class="mdl-grid">
                        <div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop mdl-cell--0-offset-desktop">
                            <div class="full-width panel mdl-shadow--2dp">
                                <div class="full-width panel-tittle bg-success text-center tittles">
                                    BOLETA
                                </div>
                                <div class="full-width panel-content">                        
                                    <table id="dtHorizontalVerticalExample" class="table table-striped table-bordered table-sm " cellspacing="0"
                                           width="100%">
                                        <thead >
                                            <tr>
                                                <th style="display:none;">N°</th>
                                                <th>Serie</th>
                                                <th>Correlativo</th>
                                                <th>Cliente</th>
                                                <th>Documento</th>         
                                                <th>Fecha</th>
                                                <th>total</th>
                                                <th>Estado</th>
                                                <th>opcion</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%  VentaDAO pdao = new VentaDAO();
                                                List<Venta> listS = pdao.ListadoventaBoleta();
                                                Iterator<Venta> iterr = listS.iterator();
                                                DecimalFormat formatea = new DecimalFormat("###,###.##");
                                                String totales = "";
                                                Venta pro = null;
                                                double total = 0;
                                                while (iterr.hasNext()) {
                                                    pro = iterr.next();%>
                                            <tr>
                                                <td style="display:none;;" id="idv"><%= pro.getIdventa()%></td>
                                                <td><%= pro.getSerie()%></td>
                                                <td><%= pro.getCorrelativo()%></td>
                                                <td ><%= ClienteDAO.getcliente(pro.getIdcliente())%></td>
                                                <td><%= pro.getTipocomprobante()%></td>     
                                                <td><%= pro.getFechayhora()%></td>
                                                <td>S/ <%= pro.getTotal()%></td>
                                                <% total = total + pro.getTotal();
                                                    totales = formatea.format(total);
                                                %>
                                                <% String Estado = pro.getEstado();
                                                    if (Estado.equalsIgnoreCase("Vendido")) {%>
                                                <td><markactivo><%= Estado%></markactivo></td>   
                                                <%   } else {%>
                                        <td><markdesactivado><%= Estado%></markdesactivado></td>    
                                            <%     }
                                            %>
                                        <td>
                                            <%
                                                if (Estado.equalsIgnoreCase("Vendido")) {%>
                                            <button  type="button" id='btn-anular' class="btn btn-danger">Anular</button>
                                            <%   } else { %>
                                            <%     }
                                            %>
                                            <a href="VControllerdetallerPDF?accion=reporte&id=<%= pro.getIdventa()%>" class="btn btn-success" >Detalle</a>
                                        </td>
                                        </tr>
                                        <%}%>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>     
                <!--modal listar-->
                <div class="mdl-tabs__panel" id="FACTURA">
                    <div class="mdl-grid">
                        <div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop mdl-cell--0-offset-desktop">
                            <div class="full-width panel mdl-shadow--2dp">
                                <div class="full-width panel-tittle bg-success text-center tittles">
                                    FACTURA
                                </div>
                                <div class="full-width panel-content">                        
                                    <table id="dtHorizontalVertical" class="table table-striped table-bordered table-sm " cellspacing="0"
                                           width="100%">
                                        <thead >
                                            <tr>
                                                <th style="display:none;">N°</th>
                                                <th>Serie</th>
                                                <th>Correlativo</th>
                                                <th>Cliente</th>
                                                <th>Documento</th>
                                                <th>Fecha</th>
                                                <th>total</th>
                                                <th>Estado</th>
                                                <th>opcion</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%  VentaDAO pd = new VentaDAO();
                                                List<Venta> li = pd.ListadoventaFactura();
                                                Iterator<Venta> i = li.iterator();
                                                Venta pr = null;
                                                while (i.hasNext()) {
                                                    pr = i.next();%>
                                            <tr>
                                                <td style="display:none;;" id="idv"><%= pr.getIdventa()%></td>
                                                <td><%= pr.getSerie()%></td>
                                                <td><%= pr.getCorrelativo()%></td>
                                                <td ><%= ClienteDAO.getcliente(pr.getIdcliente())%></td>
                                                <td><%= pr.getTipocomprobante()%></td>
                                                <td><%= pr.getFechayhora()%></td>
                                                <td>S/ <%= pr.getTotal()%></td>
                                                <% total = total + pr.getTotal();
                                                    totales = formatea.format(total);
                                                %>
                                                <% String Estado = pr.getEstado();
                                                    if (Estado.equalsIgnoreCase("Vendido")) {%>
                                                <td><markactivo><%= Estado%></markactivo></td>   
                                                <%   } else {%>
                                        <td><markdesactivado><%= Estado%></markdesactivado></td>    
                                            <%     }
                                            %>
                                        <td>
                                            <%
                                                if (Estado.equalsIgnoreCase("Vendido")) {%>
                                                 <button  type="button" id='btn-anular' class="btn btn-danger">Anular</button>
                                            <%   } else { %>
                                            <%     }
                                            %>
                                            <a href="VControllerdetallerPDF?accion=reporte&id=<%= pr.getIdventa()%>" class="btn btn-success" >Detalle</a>
                                        </td>
                                        </tr>
                                        <%}%>
                                        </tbody>
                                    </table>   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>   
            </div>
        </section>
        <!--Data table --->
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>                                
        <!--Data table --->
        <!--Funciones y validaciones --->
        <script src="EstiloAdministrador/funcionesyvalidaciones/Ventas.js" type="text/javascript"></script>
        <script src="EstiloAdministrador/funcionesyvalidaciones/Datatable.js" type="text/javascript"></script>
        <!--Funciones y validaciones ---> 
    </body>
</html>
