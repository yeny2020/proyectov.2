
<%@page import="com.pe.gatasygatos.DAO.ProductoxalmacenDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Productoxalmacen"%>
<%@page import="com.pe.gatasygatos.DAO.MarcaDAO"%>
<%@page import="com.pe.gatasygatos.DAO.CategoriaDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Producto"%>
<%@page import="com.pe.gatasygatos.DAO.ProductoDAO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Marca</title>
        <%@include file="css-js.jsp" %> 
        <!--ICONOS-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <!--ICONOS-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
        <!--Estilo tabla-->
        <link rel="stylesheet" href="EstiloAdministrador/css/Stilodetabla.css"> 
        <link href="EstiloAdministrador/css/ColordeEstado.css" rel="stylesheet" type="text/css"/>
        <!--Estilo tabla-->
    </head>
    <body>

        <!--cabecera de Menu -->
        <%@include file="FrmVenta.jsp" %>

        <!-- pageContent -->
        <section class="full-width pageContent">

            <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
                <!--modal listar-->
                <div class="mdl-tabs__panel is-active" id="tabListCategory">
                    <div class="mdl-grid">
                        <div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--10-col-desktop mdl-cell--1-offset-desktop">
                            <div class="full-width panel mdl-shadow--2dp">
                                <div  style="background-color:#aed581;color: #ffffff;" class="full-width panel-tittle text-center tittles">
                                    Agregar Item
                                </div>
                                <div class="full-width panel-content">                        
                                    <table id="dtHorizontalVertical" class="table table-striped table-bordered table-sm " cellspacing="0"
                                           width="100%">
                                        <thead>
                                            <tr>
                                                <th style="display:none;">Id</th>
                                                <th>Agregar Item</th>
                                                <th>Codigo</th>
                                                <th>Descripcion</th>
                                                <th>U.medida</th>
                                                <th>Stock</th>
                                                <th>Precio</th>
                                                <th>Estado</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%ProductoxalmacenDAO pdao = new ProductoxalmacenDAO();
                                                List<Productoxalmacen> listS = pdao.Listadodeproactivosentienda();
                                                Iterator<Productoxalmacen> iterr = listS.iterator();
                                                Productoxalmacen pro = null;
                                                while (iterr.hasNext()) {
                                                    pro = iterr.next();%>
                                            <tr>
                                                <td style="display:none;;"><%= pro.getIdproducto()%></td>
                                                <td>
                                                    <a  href="ControllerdatosModal?accion=agregar&id=<%=pro.getIdalmacenxproducto()%>"  data-toggle="modal" data-target="#myModalEdit" ><i class="material-icons" style="color: #09bb04" data-toggle="tooltip" title="Ver">&#xe147;</i></a>
                                                </td>
                                                <td><%=ProductoxalmacenDAO.getProductocodigo(pro.getIdproducto())%></td>
                                                <td><%=ProductoxalmacenDAO.getProductodescripcion(pro.getIdproducto())%></td>
                                                <td><%=ProductoxalmacenDAO.getUndVenta(pro.getIdproducto())%></td>
                                                <td><%= pro.getStock()%></td>
                                                <td><%=ProductoDAO.getProductoprecio(pro.getIdproducto())%></td>

                                                <% String Estado = ProductoxalmacenDAO.estado(pro.getIdalmacenxproducto());
                                                    if (Estado.equalsIgnoreCase("Activo")) {%>
                                                <td><markactivo><%= Estado%></markactivo></td>   
                                                <%   } else {%>
                                        <td><markdesactivado><%= Estado%></markdesactivado></td>    
                                            <%     }
                                            %>

                                        </tr>
                                        <%}%>

                                        </tbody>
                                    </table>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>                                                             
            </div>
        </section>
      <!--Formulario de Modificar-->
        <div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document" style="z-index: 9999; width: 450px">
                <div class="modal-content">

                    <div class="modal-body">


                    </div>
                </div>                    
            </div>
        </div>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>                                
        <script src="EstiloAdministrador/funcionesyvalidaciones/Datatable.js" type="text/javascript"></script>
    </body>
</html>
