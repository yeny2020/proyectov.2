<%@page import="com.pe.gatasygatos.DAO.ProductoxalmacenDAO"%>
<%@page import="com.pe.gatasygatos.DAO.UnidadVentaDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Venta"%>
<%@page import="com.pe.gatasygatos.DAO.ProductoDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.DetalleVentas"%>
<%@page import="com.pe.gatasygatos.DAO.VentaDAO"%>
<%@page import="com.pe.gatasygatos.DAO.TipoDocumentoDAO"%>
<%@page import="com.pe.gatasygatos.DAO.ClienteDAO"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Reporte Cliente</title>
        <%@include file="css-js.jsp" %>


        <style>
            html {
            }
            .provider-brand {
                bottom: 0;
                position: absolute;
                right: 0;
                text-align: center;
            }

            .provider-brand-logo {
                display: block;
                height: auto;
                margin: 0;
                padding: 0;
                width: 20px;
                margin-left:20px;
            }

            div.DivLogos {
                left: 1px !important;
            }

            .imglogos {
                max-height: 100px !important;
            }

            .FormatLetraRazonEmisor {
                font-family: Verdana, Arial, Helvetica, sans-serif !important;
                font-size: 18px !important;
                text-align: center !important;
                font-weight: bold !important;
                height: 14px !important;
            }

            .FormatLetraDirEmisor {
                font-family: Verdana, Arial, Helvetica, sans-serif !important;
                font-size: 12px !important;
                text-align: center !important;
                font-weight: bold !important;
            }

            /* ESTILO CUADRO DONDE SE ENCUENTRA RUC , TIPO DE DOCUMENTO Y NUMERO DE LA FACTURA*/

            div.DivInvoceCuadroCabecera {
                height: 10px !important;
                margin-top: 0px !important;
                margin-bottom: 0px !important;
                margin-right: 10px !important;
            }

            div.DivContentInvoceCuadroCabecera {
                border: solid #383434 2px !important;
            }

            h4.FormatInvoiceCuadroCabecera {
                text-align: center !important;
                font-weight: bold !important;
                font-size: 18px !important;
            }
            /* ESTILO DEL DETALLE DE LOS PROVEEDORES : SEÑOR , DIRECCION ,
                 RUC, FECHA EMISION, MONEDA , VENCIMIENTO*/

            div.PositionProveedorInvoice2 {
                right: 2px !important;
            }

            div.PositionProveedorInvoice6 {
                right: 1px !important;
            }

            .FormatAlingProveedorInvoice {
                font-weight: bold !important;
                font-family: Verdana, Arial, Helvetica, sans-serif !important;
                font-size: 11px !important;
            }

            .FormatAlingProveedorInvoiceDetall {
                font-family: Verdana, Arial, Helvetica, sans-serif !important;
                font-size: 11px !important;
            }
            /* ESTILO PARA LA TABLA DETALLE DE LA FACTURA */

            div.DivDimenTablaDetalleProveedorInvoice {
                bottom: -10px !important;
                width: 66% !important;
            }

            .table-responsive,
            div.DivDimTableResponsiveProveedorInvoice {
                /*min-height: 50% !important;*/
            }

            th.BoderDimTableResponsiveProveedorInvoice {
                border-right: 1px solid #333 !important;
                border-bottom: 1px solid #333 !important;
                border-top: 1px solid #333 !important;
            }

            th.BoderDimTableResponsiveProveedorInvoiceCancel {
                border-bottom: 1px solid #333 !important;
                border-top: 1px solid #333 !important;
            }

            .panel-default,
            .PanelDefaultResponsive {
                border-top: 1px solid #272525 !important;
                border-right: 1px solid #272525 !important;
                border-left: 1px solid #272525 !important;
                border-bottom: 1px solid #272525 !important;
            }

            .FormatTableResponsiveDetallCabecera {
                font-family: Verdana, Arial, Helvetica, sans-serif !important;
                font-weight: bold !important;
                text-align: center !important;
                font-size: 12px !important;
            }

            .FormatTableResponsiveDetallHijo1 {
                font-family: Verdana, Arial, Helvetica, sans-serif;
                font-size: 12px !important;
                border-left: 1px solid #333 !important;
                width: 40% !important;
                word-break: break-all;
            }

            .FormatTableResponsiveDetallHijo2 {
                font-family: Verdana, Arial, Helvetica, sans-serif !important;
                font-size: 12px !important;
                text-align: center !important;
                border-left: 1px solid #333 !important;
            }

            .FormatTableResponsiveDetallHijo3 {
                font-family: Verdana, Arial, Helvetica, sans-serif !important;
                font-size: 12px !important;
                text-align: center !important;
            }

            .FormatTableResponsiveDetallHijo4 {
                font-family: Verdana, Arial, Helvetica, sans-serif !important;
                font-size: 12px !important;
                text-align: center !important;
                border-right: 1px solid #272525 !important;
            }

            .table-condensed>tbody>tr>td,
            .table-condensed>tbody>tr>th,
            .table-condensed>tfoot>tr>td,
            .table-condensed>tfoot>tr>th,
            .table-condensed>thead>tr>td,
            .table-condensed>thead>tr>th,
            .table_Invoice_Detall {
                padding: 0px !important;
                line-height: none !important;
                border-top: none !important;
            }

            .panel-default,
            .PanelDefaultResponsive {
                border-top: 1px solid #333 !important;
                border-right: 1px solid #333 !important;
                border-left: 1px solid #333 !important;
                border-bottom: 1px solid #333 !important;
            }

            .TrTableDeltalleProveedor:last-child {
                height: 63% !important;
            }
            /* ESTILO DETALLE MONTOS */
            div.DivDimDetalleMontoInvoiceLetra {
                right: 0.5% !important;
            }

            .FormatDetalleMontoInvoiceLetra {
                font-family: Verdana, Arial, Helvetica, sans-serif;
                font-size: 12px !important;
                text-indent: 10px !important;
                text-align: left !important;
            }

            .FormatTablePieMontoInvoiceLetra1 {
                font-family: Verdana, Arial, Helvetica, sans-serif;
                font-size: 12px !important;
                text-indent: 10px !important;
                text-align: center !important;
                font-weight: bold !important;
            }

            .EstiloPanelDetalleDenominacionMontoInvoice {
                border-bottom: 1px solid #272525 !important;
                border-top: 0px solid #272525 !important;
                border-left: 1px solid #272525 !important;
                border-right: 1px solid #272525 !important;
            }

            div.DivDimTablaDetalleMontoInvoice {
                width: 30% !important;
                left: 10px !important;
            }

            .EstiloTablaDetalleMontoInvoice th,
            .EstiloTablaDetalleMontoInvoice td,
            table.EstiloTablaDetalleMontoInvoice {
                border-bottom: 1px solid #333 !important;
                border-top: 1px solid #333 !important;
                border-left: 1px solid #333 !important;
                border-right: 1px solid #333 !important;
            }

            .FormatTablaDetalleMontoInvoice1 {
                font-family: Verdana, Arial, Helvetica, sans-serif !important;
                font-weight: bold !important;
                font-size: 12px !important;
                text-align: left !important;
                padding-left: 15px !important
            }

            .FormatTablaDetalleMontoInvoice2 {
                font-family: Verdana, Arial, Helvetica, sans-serif !important;
                font-size: 12px !important;
                text-indent: 10px !important;
                text-align: center !important;
            }

            .FormatTableDetallCodBarraLetra2 {
                font-family: Verdana, Arial, Helvetica, sans-serif !important;
                font-size: 12px !important;
                text-align: left !important;
            }

            .table>tbody>tr>td,
            .table>tbody>tr>th,
            .table>tfoot>tr>td,
            .table>tfoot>tr>th,
            .table>thead>tr>td,
            .table>thead>tr>th,
            .TableDetallCodBarraLetra {
                padding: 0px !important;
                border-top: none !important;
            }

            div.DimPanelCodigoBarraDetalle {
                margin-top: 20px !important;
            }

            .FormatPanelCodigoBarraLetra {
                font-family: Verdana, Arial, Helvetica, sans-serif !important;
                font-size: 12px !important;
                text-indent: 8px !important;
                text-align: center !important;
            }

            .FormatTablePieMontoInvoiceLetra3 {
                font-family: Verdana, Arial, Helvetica, sans-serif;
                font-size: 12px !important;
                text-indent: 10px !important;
                text-align: center !important;
            }

            .FormatTablePieMontoInvoiceLetra4 {
                font-family: Verdana, Arial, Helvetica, sans-serif;
                font-size: 12px !important;
                text-indent: 10px !important;
                text-align: left !important;
            }

            .EstiloTablaDetalleDenominacionMontoInvoice th,
            table.EstiloTablaDetalleDenominacionMontoInvoice {
                border-bottom: 1px solid #272525 !important;
                border-top: 1px solid #272525 !important;
                border-left: 1px solid #272525 !important;
                border-right: 1px solid #272525 !important;
            }

            .EstiloTablaDetalleDenominacionMontoInvoice th,
            .EstiloTablaDetalleDenominacionMontoInvoice td,
            table.EstiloTablaDetalleDenominacionMontoInvoice {
                border-bottom: 1px solid #272525 !important;
                border-top: 1px solid #272525 !important;
                border-left: 1px solid #272525 !important;
                border-right: 1px solid #272525 !important;
            }

            .PanelDefaultCodigoBarra {
                border-top: 1px solid #333 !important;
                border-bottom: 1px solid #333 !important;
            }

            .imgDimBarcode {
                display: block !important;
                margin: 0 auto !important;
            }

            .FormatTableFooterDesc {
                padding-right: 10px;
            }

            hr {
                border-color: #444444 !important;
            }
            .cuentas {
                width: 100%;
            }

            .cuentas td,
            .cuentas th {
                border: 1px solid black;
                text-align: center;
                font-size: 10px;
                font-family: sans-serif;
            }
        </style>        

    </head>


    <body>
        <%
            VentaDAO dao = new VentaDAO();
            int id = Integer.parseInt((String) request.getAttribute("idve"));
            Venta p = (Venta) dao.Reporte(id);
        %>
        <!--cabecera de Menu -->
        <%@include file="FrmVenta.jsp" %>
        <!-- pageContent -->
        <section class="full-width pageContent">
            <div class="mdl-grid" style="background-color: #fff;width: 72%;">
                <div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--10-col-desktop" >
                    <div id="PDF" >   
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-1 DivLogos">
                                    <img src="Img/gatos.png" class="imglogos">
                                </div>
                                <div class="col-xs-4 DivLogos">
                                    <h6 class="FormatLetraRazonEmisor">GATAS & GATOS</h6>
                                    <h6 class="FormatLetraDirEmisor">AV. HUANUCO 232 - HUANUCO - HUANUCO </h6>
                                    <h6 class="FormatLetraDirEmisor">VENTA DE JUGUETES AL POR MAYOR Y MENOR</h6>
                                </div>
                                <div class="col-xs-3 DivInvoceCuadroCabecera">
                                    <div class="panel panel-default DivContentInvoceCuadroCabecera">
                                        <h4 class="FormatInvoiceCuadroCabecera">RUC:&nbsp;203003300303</h4>
                                        <h4 class="FormatInvoiceCuadroCabecera"><%=p.getTipocomprobante()%> ELECTRÓNICA</h4>
                                        <h4 class="FormatInvoiceCuadroCabecera">Nro.&nbsp;<%=p.getSerie()%> - <%=p.getCorrelativo()%></h4>
                                    </div>
                                </div>
                            </div>
                          
                            <div class="row" style="margin-top:2px !important">
                                <div class="col-xs-5">
                                    <div class="row">
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <h6 class="FormatAlingProveedorInvoice"style="height: 3px;">CLIENTE:</h6>
                                            <h6 class="FormatAlingProveedorInvoice" style="height: 3px; margin-top: 20px !important;">DOC. ID</h6>
                                            <h6 class="FormatAlingProveedorInvoice" style="height: 3px; margin-top: 20px !important;">DIRECCIÓN</h6>
                                            <h6 class="FormatAlingProveedorInvoice" style="height: 3px; margin-top: 20px !important;">RESPONSABLE</h6>
                                            <h6 class="FormatAlingProveedorInvoice" style="height: 3px; margin-top: 20px !important;">SUCURSAL</h6>
                                            <h6 class="FormatAlingProveedorInvoice" style="height: 3px; margin-top: 20px !important;">VENDEDOR</h6>
                                        </div>
                                        <div class="col-md-6 col-sm-3 col-xs-3">
                                            <h5 class="FormatAlingProveedorInvoiceDetall" style="height: 3px;">: <%= ClienteDAO.getcliente(p.getIdcliente())%></h5>
                                            <h5 class="FormatAlingProveedorInvoiceDetall" style="height: 3px; margin-top: 20px !important;">: <%= ClienteDAO.getnrodocumento(p.getIdcliente())%> </h5>
                                            <h5 class="FormatAlingProveedorInvoiceDetall" style="height: 3px; margin-top: 20px !important;">: <%= ClienteDAO.getDireccion(p.getIdcliente())%></h5>
                                            <h5 class="FormatAlingProveedorInvoiceDetall" style="height: 3px; margin-top: 20px !important;">: YENY CESPECES CABRERA</h5>
                                            <h5 class="FormatAlingProveedorInvoiceDetall" style="height: 3px; margin-top: 20px !important;">: HUANUCO</h5>
                                            <h5 class="FormatAlingProveedorInvoiceDetall" style="height: 3px; margin-top: 20px !important;">: - </h5>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-6">
                                    <div style="">
                                        <div class="row">
                                            <div class="col-md-2 col-sm-1 col-xs-2">
                                                <h6 class="FormatAlingProveedorInvoice" style="height: 3px;">F.EMISIÓN</h6>
                                                <h6 class="FormatAlingProveedorInvoice" style="height: 3px; margin-top: 20px;">MONEDA</h6>
                                                <h6 class="FormatAlingProveedorInvoice" style="height: 3px; margin-top: 20px;">F.VCTO</h6>
                                                <h6 class="FormatAlingProveedorInvoice" style="height: 3px; margin-top: 20px;">COND.PAGO</h6>
                                            </div>
                                            <div class="col-md-5 col-sm-3 col-xs-3">
                                                <h5 class="FormatAlingProveedorInvoiceDetall" style="height: 3px;">: <%=p.getFechayhora()%></h5>
                                                <h5 class="FormatAlingProveedorInvoiceDetall" style="height: 3px; margin-top: 20px;">: PEN</h5>
                                                <h5 class="FormatAlingProveedorInvoiceDetall" style="height: 3px; margin-top: 20px;">: - </h5>
                                                <h5 class="FormatAlingProveedorInvoiceDetall" style="height: 3px; margin-top: 20px !important;">: <%=p.getCondicion()%></h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 DivDimenTablaDetalleProveedorInvoice">
                                    <div class="panel panel-default PanelDefaultResponsive">
                                        <div class="table-responsive DivDimTableResponsiveProveedorInvoice">
                                            <table class="table table-condensed table_Invoice_Detall" style="height: 40%">
                                                <thead>
                                                    <tr>
                                                        <th class="BoderDimTableResponsiveProveedorInvoice">
                                                            <h4 class="FormatTableResponsiveDetallCabecera">Código</h4>
                                                        </th>
                                                        <th class="BoderDimTableResponsiveProveedorInvoice">
                                                            <h4 class="FormatTableResponsiveDetallCabecera">Cantidad</h4>
                                                        </th>
                                                        <th class="BoderDimTableResponsiveProveedorInvoice">
                                                            <h4 class="FormatTableResponsiveDetallCabecera">UM</h4>
                                                        </th>
                                                        <th class="BoderDimTableResponsiveProveedorInvoice">
                                                            <h4 class="FormatTableResponsiveDetallCabecera">Descripción</h4>
                                                        <th class="BoderDimTableResponsiveProveedorInvoice">
                                                            <h4 class="FormatTableResponsiveDetallCabecera">Dscto.</h4>
                                                        </th>
                                                        <th class="BoderDimTableResponsiveProveedorInvoice">
                                                            <h4 class="FormatTableResponsiveDetallCabecera">P. Unitario</h4>
                                                        </th>
                                                        <th class="BoderDimTableResponsiveProveedorInvoiceCancel">
                                                            <h4 class="FormatTableResponsiveDetallCabecera">Importe</h4>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <%  VentaDAO pdao = new VentaDAO();
                                                        List<DetalleVentas> listS = pdao.ticketDetalle(id);
                                                        Iterator<DetalleVentas> iterr = listS.iterator();
                                                        DetalleVentas pro = null;
                                                        double total = 0, igv = 0, subtotal = 0;
                                                        DecimalFormat formatea = new DecimalFormat("###,###.##");
                                                        String Msubtotal = "";
                                                        String Migv = "";
                                                        while (iterr.hasNext()) {
                                                            pro = iterr.next();%>

                                                    <tr class="TrTableDeltalleProveedor">
                                                        <td class="FormatTableResponsiveDetallHijo4"><%=ProductoDAO.getCodProd(pro.getIdproducto())%></td>
                                                        <td class="FormatTableResponsiveDetallHijo3"><%=pro.getCantidad()%></td>
                                                        <td class="FormatTableResponsiveDetallHijo2"><%=ProductoxalmacenDAO.getUndVenta(pro.getIdproducto())%></td>
                                                        <td class="FormatTableResponsiveDetallHijo1"><div style="padding-left: 8px; padding-right: 8px;"><%=ProductoDAO.getProducto(pro.getIdproducto())%></div></td>
                                                        <td class="FormatTableResponsiveDetallHijo2">0.00</td>
                                                        <td class="FormatTableResponsiveDetallHijo2"><%=ProductoDAO.getProductoprecio(pro.getIdproducto())%></td>
                                                        <td class="FormatTableResponsiveDetallHijo2"><%= ProductoDAO.getProductoprecio(pro.getIdproducto()) * pro.getCantidad()%></td>
                                                    </tr>
                                                    <%}%>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 DivDimDetalleMontoInvoiceLetra">
                                  <%--  <h5 class="FormatDetalleMontoInvoiceLetra" style="width: 170% !important; word-break: break-all;"><span style="font-weight: bold !important;">SON:</span>&nbsp{{letraTotal}}&nbsp{{#TipoDeMoneda document.tipoMoneda}}{{/TipoDeMoneda}}</h5>
                               --%> </div>
                            </div>
                            <div class="col-xs-4 DivDimDetalleMontoInvoiceLetra">
                                <div>
                                    <div class="panel panel-default" style="padding:10px;">
                                        <span class="FormatTableFooterDesc" style="font-weight:bold;">Observaciones: <br/>
                                            <br><br>
                                        </span> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-3 DivDimTablaDetalleMontoInvoice" style="margin-left:23px">
                                <div class="panel panel-default" style="border: 0px solid !important">
                                    <table class="table EstiloTablaDetalleMontoInvoice">
                                        <tr>
                                            <th>
                                                <h4 class="FormatTablaDetalleMontoInvoice1">Total Op. Gravada</h4></th>
                                            <td>
                                                <h5 class="FormatTablaDetalleMontoInvoice2">
                                                    <span style="float:left; margin-left:15px;"></span>
                                                    <span style="float:right; padding-right:20px"><%=p.getSubtotal()%></span></h5></td>
                                        </tr>
                                        <tr>
                                            <th>
                                                <h4 class="FormatTablaDetalleMontoInvoice1">Total Op. Inafecta</h4>
                                            </th>
                                            <td>
                                                <h5 class="FormatTablaDetalleMontoInvoice2">
                                                    <span style="float:left; margin-left:15px;"></span>
                                                    <span style="float:right; padding-right:20px">0.00</span>
                                                </h5>
                                            </td>
                                        </tr>

                                        <tr>
                                            <th>
                                                <h4 class="FormatTablaDetalleMontoInvoice1">Total Op. Exonerada</h4>
                                            </th>
                                            <td>
                                                <h5 class="FormatTablaDetalleMontoInvoice2">
                                                    <span style="float:left; margin-left:15px;"></span>
                                                    <span style="float:right; padding-right:20px">0.00</span>
                                                </h5>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                <h4 class="FormatTablaDetalleMontoInvoice1">Total IGV </h4>
                                            </th>
                                            <td>
                                                <h5 class="FormatTablaDetalleMontoInvoice2">
                                                    <span style="float:left; margin-left:15px;"></span>
                                                    <span style="float:right; padding-right:20px"><%=p.getIgv()%></span></h5>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <h4 class="FormatTablaDetalleMontoInvoice1">Importe Total</h4>
                                            </td>
                                            <td>
                                                <h5 class="FormatTablaDetalleMontoInvoice2">
                                                    <span style="float:left; margin-left:15px;"> </span>
                                                    <span style="float:right; padding-right:20px"><%=p.getTotal()%></span></h5>                     
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div> 
                    </div>         
                </div>
            </div>
            <center><a href="#" onclick="ReportePDF()" class="btn btn-primary"> <i class="zmdi zmdi-cloud-download"></i>Reporte PDF</a>
                <a href="VListaVenta.jsp"  class="btn btn-danger"  >Regresar</a></center>
        </section>                   
    </body>
    <%@include file="pdf-excel-js.jsp" %>
</html>
