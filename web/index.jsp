<%-- 
    Document   : index
    Created on : 27/02/2019, 10:22:03 AM
    Author     : Angel Albinagorta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Login</title>
	 <%@include file="css-js.jsp" %>
     </head>   
<body class="cover">
	<div class="container-login">
		<p class="text-center" style="font-size: 80px;">
			<i class="zmdi zmdi-account-circle"></i>
		</p>
		<p class="text-center text-condensedLight">INGRESE SUS DATOS</p>
		<form  action="logincontroller" method="POST">
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input  name="txtUsuario" class="mdl-textfield__input" type="text" id="userName" required="">
			    <label  class="mdl-textfield__label" for="userName">USUARIO</label>
			</div>
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			    <input name="txtPassword"  class="mdl-textfield__input" type="password" id="pass" required>
			    <label class="mdl-textfield__label" for="pass">PASSWORD</label>
			</div>
                      <input type="submit" name="btnEntrar" value="Iniciar" id="SingIn" class="mdl-button mdl-js-button mdl-js-ripple-effect" style="color: #3F51B5; float:right;">
                      <a href="categoria.jsp">Cate</a>     
                </form>
	</div>
     
</body>
</html>