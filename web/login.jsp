<%-- 
    Document   : index
    Created on : 14/10/2018, 04:14:04 PM
    Author     : HP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login</title>
        <%@include file="css-js.jsp" %>
        <link href="EstiloAdministrador/css/estiloLogin.css" rel="stylesheet" type="text/css"/>
        <script src="https://unpkg.com/boxicons@latest/dist/boxicons.js"></script>
    </head>   
    <body class="estiloLogin">
        <div class="container_form">
            <p class="text-center" style="font-size: 80px;">
             <figure class="text-center">
                <img src="Img/gatos.png" alt=""
                     width="100" height="70"  class="img-fluid mx-auto" alt="Logo gatas y gatos" border-radius="50%">
            </figure>

        </p>${mensaje}
        <p class="text-center text-condensedLight">INGRESE SUS DATOS </p>
        <form  action="logincontroller" method="POST">

            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <box-icon type="solid" name="user" style="width:200%;float:left;"></box-icon>
                <input  name="txtUsuario" class="mdl-textfield__input" type="text" id="userName" required="">

                <label  class="mdl-textfield__label" for="userName">USUARIO</label>

            </div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <box-icon type='solid'style="width:200%;float:left;" name='key'></box-icon>
                <input name="txtPassword"  class="mdl-textfield__input" type="password" id="pass" required>

                <label class="mdl-textfield__label" for="pass">PASSWORD</label>
            </div>
            
            <input type="submit" name="btnEntrar" style="color: #f1f2f7;width:100%;float: right;background-color: #009688;" value="Iniciar" id="SingIn" class="mdl-button mdl-js-button mdl-js-ripple-effect" style="color: #3F51B5; float:right;">
            <hr>
           
            <%--<a  style="color: #f1f2f7; width:20%; float:left;background-color: #009688;" class="mdl-button mdl-js-button mdl-js-ripple-effect"href="login.jsp">SALIR</a>--%>
            <hr>
            <br>
            
            <div class="box_pass">
                <a href="index.jsp" class="" id="#" style="color: #2215ea;" cursor="pointer" text-decoration="underline">¿Olvidaste tu contraseña?</a>
            </div>
        </form>
    </div>
    <%
        HttpSession sesion = request.getSession();
        String tipo = "";
        if (request.getAttribute("tipo") != null) {
            tipo = request.getAttribute("tipo").toString();
            if (tipo.equalsIgnoreCase("Administrador")) {
                sesion.setAttribute("usuario", request.getAttribute("usuario"));
                sesion.setAttribute("tipo", tipo);
                response.sendRedirect("BienvenidoAdmin.jsp");
            } else if (tipo.equalsIgnoreCase("Almacen")) {
                sesion.setAttribute("usuario", request.getAttribute("usuario"));
                sesion.setAttribute("tipo", tipo);
                response.sendRedirect("BienvenidoAlmacen.jsp");
            } else if (tipo.equalsIgnoreCase("Produccion")) {
                sesion.setAttribute("usuario", request.getAttribute("usuario"));
                sesion.setAttribute("tipo", tipo);
                response.sendRedirect("BienvenidoProduccion.jsp");
            } else if (tipo.equalsIgnoreCase("Ventas")) {
                sesion.setAttribute("usuario", request.getAttribute("usuario"));
                sesion.setAttribute("tipo", tipo);
                response.sendRedirect("BienvenidoVentas.jsp");
            }
        }

        if (request.getParameter("cerrar") != null) {
            session.invalidate();
        }
    %>

</body>
</html>