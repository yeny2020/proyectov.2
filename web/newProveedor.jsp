<%-- 
    Document   : panel
    Created on : Oct 22, 2019, 9:53:01 PM
    Author     : santi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.1.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    </head>

    <body>




        <div id="divCrearProveedor">
            <form id="newproveedor" action="Admi_Material" method="post" name="frm_nuevo">
                <table id="newProve" border="1" >

                    <thead>

                    <th>Nombre proveedor</th>

                    </thead>
                    <tr>

                        <td><input type="Text" name="Txtnombre" placeholder="Ingrese el Nombre"></td>
                        <label id="error1" style="color: red"></label>



                    </tr>



                </table>
                <input onclick="return validar()" type="submit" name="accion" value="add">
            </form>
        </div>
        <br>
        <br>
        <a href="panel.jsp">Regresar</a>
        
    </body>
    <script>
     $(function () {
    $('#newproveedor').validate({
        
        submitHandler: function (form) {
            var data = $('#newproveedor').serialize();
            $.post("Admi_Material?accion=add", data, function (res, est, jqXHR) {
                alert(res);
                if (res ==="ok") {
                    location.href = "Material.jsp";
                }
                else {
                    location.href = "newProveedor.jsp";
                }
            });
        }
    });

});
    
</script>

</html>
