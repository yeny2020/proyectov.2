<%-- 
    Document   : pdf-excel-js
    Created on : 27/02/2019, 10:32:43 AM
    Author     : Angel Albinagorta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
         <!-- libreria Excel -->
       <script src="EstiloAdministrador/js/ReporteExcel.js"></script>
	<!-- inicio libreria PDF -->
	<script src="EstiloAdministrador/js/jspdf.js"></script>
	<script src="EstiloAdministrador/js/jquery-2.1.3.js"></script>
	<script src="EstiloAdministrador/js/ReportePDF.js"></script>  
    </body>
</html>
