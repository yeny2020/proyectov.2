<%@page import="com.pe.gatasygatos.DAO.OrdendeCompraDAO"%>
<%@page import="com.pe.gatasygatos.DAO.ProductoDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.DetalleCompra"%>
<%@page import="com.pe.gatasygatos.DAO.ProvedorDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Compra"%>
<%@page import="com.pe.gatasygatos.DAO.CompraDAO"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Reporte Cliente</title>
        <%@include file="css-js.jsp" %>
        <link href="EstiloAdministrador/css/ESTILO_TAB.css" rel="stylesheet" type="text/css"/>
        <script src = " https://raw.githack.com/eKoopmans/html2pdf/master/dist/html2pdf.bundle.js "></script>
        <script src="EstiloAdministrador/funcionesyvalidaciones/PDF.js" type="text/javascript"></script>
        <link href="EstiloAdministrador/css/TamañoA4.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <%
            CompraDAO dao = new CompraDAO();
            int id = Integer.parseInt((String) request.getAttribute("idco"));
            Compra p = (Compra) dao.Reporte(id);
        %>
        <!--cabecera de Menu -->
        <%@include file="FrmAdmin.jsp" %>
        <!-- pageContent -->
        <section class="full-width pageContent">
            <div class="mdl-grid" style="background-color: #fff;width: 80%;">
                <div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop">
                    <dv>
                        <button ondblclick="generarpdf()" class="btn btn-primary" style="background: #0a8cb3;color:#fff;"><i class="zmdi zmdi-cloud-download"></i>Descargar</button>

                        <a href="Cotizacion.jsp"  class="btn btn-danger">Regresar</a>
                    </dv>

                    <!------------inicio pdf-->
                    <div id="invoce" class="divpdf">
                        <div id="PDF" >   
                            <br>
                            <br>
                            <div>
                                <div id="print-area">
                                    <div class="row pad-top font-big">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <img src="Img/gatos.png" alt=""
                                                 width="100" height="70"  class="img-fluid mx-auto" alt="Logo gatas y gatos" border-radius="50%"</a>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <strong>E-mail   : </strong> info@obedalvarado.com<br>
                                            <strong>Teléfono :</strong> 936-809-722 <br>
                                            <strong>Tienda:</strong>  Gatas y Gatos 
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <strong>Sistema Web Gatas y Gatos  </strong>
                                            <br>
                                            <strong>Dirección :</strong>Jirón 2 de Mayo 479,Huánuco 10001
                                            <br>
                                            Huanuco, Peru. 
                                        </div>
                                    </div>

                                    <div class="row pad-top font-big">
                                        <hr style="border-color: #dfe0e0;">

                                        <div class="col-lg-5 col-md-5 col-sm-5">
                                            <h2 style = "font-size: 16px;"><strong>Detalles del proveedor:</strong></h2>
                                            <strong>Proveedor:</strong> <%= ProvedorDAO.getProveedor(p.getIdproveedor())%><br>
                                            <strong>E-mail : </strong> <%= ProvedorDAO.getEmail(p.getIdproveedor())%><br>
                                            <strong>Teléfono :</strong> <%= ProvedorDAO.getCelular(p.getIdproveedor())%> <br>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <h3 style = "font-size: 16px;"><strong>Tipo Comprobante : <%=p.getTipocomprobante()%></strong></h3>
                                            <strong>Serie:</strong><%=p.getSerie()%><br>
                                            <strong>N°Doc: </strong><%=p.getCorrelativo()%>                   <br>
                                            <strong>Fecha:</strong><%=p.getFechaYhora()%><br>
                                        </div>

                                    </div>
                                    <!----------tabla------------------------------------------------------------>
                                    <div class="row">
                                        <hr>
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <div class="table-responsive">
                                                <table class="table table-striped  table-hover" >
                                                    <thead>
                                                        <tr style="background-color:#0a8cb3;color:white;">

                                                            <th>DESCRIPCION</th>
                                                            <th>UM</th>
                                                            <th>CANTIDAD</th>
                                                            <th>PRECIO UNITARIO</th>
                                                            <th>PRECIO TOTAL</th>


                                                        </tr>
                                                    </thead>


                                                    <tbody>
                                                        <%  CompraDAO pdao = new CompraDAO();
                                                            List<DetalleCompra> listS = pdao.ticketDetalle(id);
                                                            Iterator<DetalleCompra> iterr = listS.iterator();
                                                            DetalleCompra pro = null;
                                                            double total = 0, igv = 0, subtotal = 0;
                                                            DecimalFormat formatea = new DecimalFormat("###,###.##");
                                                            String Msubtotal = "";
                                                            String Migv = "";
                                                            while (iterr.hasNext()) {
                                                                pro = iterr.next();%>

                                                        <tr>


                                                            <td><%= ProductoDAO.getProducto(pro.getIdproducto())%></td>
                                                            <td><%=OrdendeCompraDAO.getNombreunidad(pro.getIdproducto())%></td>
                                                            <td><%= pro.getCantidad()*pro.getContenido()%></td>
                                                            <td><%= pro.getCosto() / pro.getContenido()%></td>
                                                            <td><%=pro.getCosto() * pro.getCantidad()%></td>
                                                        </tr>
                                                        <%}%>
                                                    </tbody>

                                                </table>
                                                <div style="float: right;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <h4>SubTotal s/ <%=p.getSubtotal()%></h4>
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <h4 >IGV s/ <%=p.getIgv()%></h4>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <h4>Total s/ <%=p.getTotal()%></h4>  
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <!------>      
                                </div>  
                                <!-----------todo pdf-->
                            </div>
                        </div>
                    </div>
                    <!--   <center><a href="#" onclick="ReportePDF()" class="btn btn-primary"> <i class="zmdi zmdi-cloud-download"></i>Reporte PDF</a>-->
                </div>
            </div>
        </section>
    </body>
    <%@include file="pdf-excel-js.jsp" %>

</html>