
<%@page import="com.pe.gatasygatos.model.entity.Provedor"%>
<%@page import="com.pe.gatasygatos.DAO.ProvedorDAO"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.pe.gatasygatos.model.entity.Cliente"%>
<%@page import="com.pe.gatasygatos.DAO.ClienteDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.Producto"%>
<%@page import="com.pe.gatasygatos.DAO.ProductoDAO"%>
<%@page import="com.pe.gatasygatos.model.entity.DetalleVentas"%>
<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <%@include file="css-js.jsp" %>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Bootstrap CRUD Data Table for Database with Modal Form</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="EstiloAdministrador/css/Stilodetabla.css"> 
        <script type="text/javascript" src="EstiloAdministrador/js/ValidadForm.js"></script>


        <link href="EstiloAdministrador/css/mainC.css" rel="stylesheet">


    </head>
    <body>

        <!--cabecera de Menu -->
        <!--cabecera de Menu -->
        <%@include file="FrmAdmin.jsp" %>

        <!-- pageContent -->
        <section class="full-width pageContent">

            <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
                <!--modal listar-->
                <div class="mdl-tabs__panel is-active" id="tabListCategory">
                    <div class="mdl-grid">
                        <div class="mdl-cell mdl-cell--4-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop mdl-cell--0-offset-desktop">

                            <div class="full-width panel mdl-shadow--2dp">
                                <div class="full-width panel-tittle  text-center tittles" style="background-color: #aed581;">
                                    Ventas
                                </div>

                                <br>
                                <form method="post" name="accion" action="ControllerVentas">
                                    <input type="hidden" name="accion" value="registra" />
                                    <div class="d-flex">
                                        <div class="col-sm-4">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="form-group">
                                                        <div class="form-group">
                                                            <h4>Datos del cliente</h4>
                                                        </div>
                                                        <div class="form-group d-flex">

                                                            <div class="col-sm-12">
                                                                <label for="">Cliente:</label>
                                                                <input type="text"  name="txtIdcli" value="" id="txtdireccion" class="form-control">
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <h4>Tipo de Comprobante</h4>
                                                        </div>


                                                        <div class="form-group d-flex">
                                                            <div class="col-sm-12">
                                                                <label>Comprobante:</label>
                                                                <select name="txtTipoventa" id="tipo_comprobante"class="form-control selectpicker" required="">
                                                                    <option   value=""disabled="" selected="">Seleciona Comprobante</option>
                                                                    <option value="Boleta">Boleta</option>
                                                                    <option value="Factura">Factura</option>
                                                                </select>
                                                            </div>   

                                                        </div>
                                                        <div class="form-group d-flex">
                                                            <% Date dNow = new Date();
                                                                SimpleDateFormat ft
                                                                        = new SimpleDateFormat("dd/MM/yyyy - hh:mm:ss");
                                                                String currentDate = ft.format(dNow);
                                                            %>
                                                            <div class="col-sm-12">
                                                                <label for="">Fecha:</label>
                                                                <input type="text"  name="txtfecha" value="<%=currentDate%>" id="txtdireccion" class="form-control">
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>

                                        </div>


                                        <div class="col-sm-8">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="form-group">                                                   
                                                        <div class="col-md-4 col-md-offset-9">
                                                            <a href="Ventaproductosactivos.jsp" class="btn btn-info" style="color: #FFFFFF">Agregar Productos</a>
                                                        </div>
                                                        <br>
                                                        <br>

                                                        <br>
                                                        <div class="table-responsive cart_info"  id="cart-container"> 
                                                            <table id="tablaRegistrarVenta"  class="table table-bordered table-hover projects"  >
                                                                <thead class="table-dark" style="background-color: #aed581;" >
                                                                    <tr class="cart_menu" style="width: 25%;">

                                                                        <td class="image">Nombre</td>
                                                                        <td class="price">Costo</td>
                                                                        <td class="description">Cantidad</td>                                 
                                                                        <td class="quantity">Sub Total</td>
                                                                        <td class="total">Quitar</td>
                                                                    </tr>

                                                                </thead>

                                                                <%
                                                                    DecimalFormat df = new DecimalFormat("0.00");
                                                                    DecimalFormatSymbols dfs = df.getDecimalFormatSymbols();
                                                                    dfs.setDecimalSeparator('.');
                                                                    df.setDecimalFormatSymbols(dfs);
                                                                    double total = 0;
                                                                    double igv = 0.0;
                                                                    double ventatotal = 0.0;

                                                                    ArrayList<DetalleVentas> listar = (ArrayList<DetalleVentas>) session.getAttribute("car");
                                                                    int fila = 0;

                                                                    if (listar != null) {
                                                                        for (DetalleVentas d : listar) {
                                                                %>
                                                                <tbody>
                                                                    <tr>

                                                                        <td class="cart_description">
                                                                            <p><%= d.getProducto().getIdproducto()%></p>
                                                                        </td>
                                                                        <td class="cart_description">
                                                                            <p><%= d.getProducto().getPrecioVenta()%></p>
                                                                        </td>

                                                                        <td class="cart_quantity">
                                                                            <div class="cart_quantity_button">
                                                                                <input type="number" value="<%= d.getCantidad()%>" value="1" min="1" max="<%=d.getProducto().getStocktienda()%>" id="txtPro_cantidad<%=fila%>" name="txtPro_cantidad"  onchange="actualizarcantidad(<%= fila%>)" value="0">
                                                                            </div>
                                                                        </td>

                                                                        <td class="cart_quantity"><%=d.getCantidad() * d.getProducto().getPrecioVenta()%></td>
                                                                        <td >
                                                                            <span id="idarticulo" style="display:none;"><%= d.getProducto().getIdproducto()%></span>

                                                                            <button style="background-color: transparent; color: red; border: none " id="deleteitem" class="delete">
                                                                                <i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i>
                                                                            </button>
                                                                        </td>


                                                                    </tr>
                                                                    <%
                                                                                fila++;
                                                                                total = total + d.getCantidad() * d.getProducto().getPrecioVenta();
                                                                                igv = total * 0.18;
                                                                                ventatotal = total + igv;
                                                                            }
                                                                        }
                                                                    %>
                                                                </tbody>
                                                            </table>
                                                            <%if (listar == null) {%>
                                                            <h4>No hay productos agregado</h4>
                                                            <%
                                                                }%>   


                                                        </div> 
                                                        <div class="col-md-7 col-md-offset-5" style="color: #0083C9;">
                                                            <div class="card">

                                                                <div class="card-body  ">
                                                                    <div class="d-flex">
                                                                        <label class="col-sm-5">SUB TOTAL :</label>
                                                                        <input type="text" name="txtSubtotal" value="<%= Math.round(total * 100.0) / 100.0%>" readonly="readonly">
                                                                    </div>



                                                                    <div class="d-flex">
                                                                        <label class="col-sm-5">IGV :</label>
                                                                        <input type="text" name="txtIgv" value="<%= Math.round(igv * 100.0) / 100.0%>" readonly="readonly">
                                                                    </div>



                                                                    <div class="d-flex">
                                                                        <label class="col-sm-5">TOTAL :</label>
                                                                        <input type="text" name="txtTotal" value="<%= Math.round(ventatotal * 100.0) / 100.0%>" readonly="readonly">
                                                                    </div>

                                                                </div>

                                                            </div> 

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="card-footer">
                                                    <div class="form-group ">                                            
                                                        <div class="col-sm-5 ">
                                                            <input

                                                            <input type="submit" value="Registrar Venta" name="btnVenta"> 
                                                        </div>
                                                        <div class="col-sm-4 ">
                                                            <a href="Venta.jsp" class="btn btn-info btn-block">Salir</a> 
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>                  
                                        </div>                                                                                                                                             
                                    </div>
                                    <br>
                                </form>                                                                                                      
                            </div>
                        </div>                                                             
                    </div>
                </div>
        </section>  



    </body>
    <!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>-->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <!--  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>-->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="EstiloAdministrador/js/ValidadForm.js"></script>
    <!--<script type="text/javascript" src="JS/jquery.1.9.1.min.js"></script>-->
    <!--<script src="Java S/jquery.min.js"></script>-->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>                                





    <script>

                                                                                    $(function () {
                                                                                        calcularimpuesto();

                                                                                    });
                                                                                    function calcularimpuesto() {
                                                                                        var tipo = $("#txtTipoventa").val();
                                                                                        var impuesto = 0;
                                                                                        if (tipo === "Boleta") {
                                                                                            impuesto = 0;
                                                                                        } else if (tipo === "Factura") {
                                                                                            impuesto = 0.18;
                                                                                        }
                                                                                        $("#txtimpuesto").val(impuesto);

                                                                                    }

                                                                                    function calcularsubtotal() {
                                                                                        var tipo = $("#txtTipoventa").val();
                                                                                        var tipo = $("#txtTipoventa").val();
                                                                                        var impuesto = 0;
                                                                                        if (tipo === "Boleta") {
                                                                                            impuesto = 0;
                                                                                        } else if (tipo === "Factura") {
                                                                                            impuesto = 0.18;
                                                                                        }
                                                                                        $("#txtimpuesto").val(impuesto);

                                                                                    }

                                                                                    function actualizarcantidad(idproducto) {

                                                                                        var cantidad = $("#txtPro_cantidad" + idproducto).val();
                                                                                        window.location.href = "ControllerVentas?accion=actualizarcantidad&cant=" + cantidad + "&idproducto=" + idproducto;
                                                                                    }

                                                                                    $(function () {
                                                                                        $('tr #deleteitem').click(function (e) {
                                                                                            e.preventDefault();
                                                                                            var elemento = $(this);
                                                                                            var idproducto = elemento.parent().find('#idarticulo').text();
                                                                                            $.ajax({
                                                                                                url: 'Deleteitemventa',
                                                                                                type: 'post',
                                                                                                data: {idproducto: idproducto},
                                                                                                success: function (r) {
                                                                                                    swal({
                                                                                                        title: "Estas seguro que quieres eleiminar?",
                                                                                                        icon: "warning",
                                                                                                        buttons: true,
                                                                                                        dangerMode: true
                                                                                                    })
                                                                                                            .then((willDelete) => {
                                                                                                                if (willDelete) {
                                                                                                                    elemento.parent().parent().remove();
                                                                                                                    var elementostabla = $('#tablaRegistrarVenta tr');
                                                                                                                    if (elementostabla.length <= 1) {
                                                                                                                        $('#cart-container').append("<h4>No hay Articulos en el carro</h4>");
                                                                                                                    }
                                                                                                                    swal("Se elimino el producto!", {
                                                                                                                        icon: "success"
                                                                                                                    }).then((willDelete) => {
                                                                                                                        if (willDelete) {
                                                                                                                            parent.location.href = "ControllerVentas?accion=procesarCarrito";
                                                                                                                        }

                                                                                                                    });
                                                                                                                } else {
        <%-- swal("Canselaste!");--%>
                                                                                                                }

                                                                                                            });


                                                                                                }
                                                                                            });

                                                                                        });


                                                                                    });

    </script>

    <script type="text/javascript">

        $(document).ready(function () {

            var now = new Date();

            var day = ("0" + now.getDate()).slice(-2);
            var month = ("0" + (now.getMonth() + 1)).slice(-2);

            var today = now.getFullYear() + "-" + (month) + "-" + (day);
            $("#fecha").val(today);
        });


    </script>

</html>
